class PeriodoPropedeutico < ActiveRecord::Base

  has_many :solicitudes_propedeuticos
  has_many :grupos_propedeuticos
  belongs_to :ciclo

  def self.is_period_of_application_for_tokens?
    activo = false
    periodos_fichas = PeriodoPropedeutico.where("actual = ?", true)
    dia_actual = Time.now
    periodos_fichas.each do |periodo_ficha|
      if dia_actual.strftime("%Y-%m-%d") >= periodo_ficha.fecha_inicio and dia_actual.strftime("%Y-%m-%d") <= periodo_ficha.fecha_fin
        activo = true
        break
      else
        activo = false
      end
    end
    return activo
  end

  def self.get_current_token_period
    periodo_activo = PeriodoPropedeutico.new
    periodos_fichas = PeriodoPropedeutico.where("actual = ?", true)
    dia_actual = Time.now
    periodos_fichas.each do |periodo_ficha|
      if dia_actual.strftime("%Y-%m-%d") >= periodo_ficha.fecha_inicio and dia_actual.strftime("%Y-%m-%d") <= periodo_ficha.fecha_fin
        periodo_activo = periodo_ficha
        break
      end
    end
    periodo_activo
  end

  def self.get_actual
    current_ciclo = Ciclo.get_actual
    ciclo = Ciclo.find_by_inicio_and_fin_and_tipo(current_ciclo.inicio, current_ciclo.fin, 'A')
    periodo_prope = PeriodoPropedeutico.find_by_actual_and_ciclo_id(true,ciclo.id)
  end

end
