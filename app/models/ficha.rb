class Ficha < ActiveRecord::Base
  belongs_to :aspirante
  belongs_to :solicitud_ficha
  belongs_to :carrera
  belongs_to :ciclo

  ENTREGADA = 'ENTREGADA'
  ENVIADO = 'ENVIADO'
  TERMINADO = 'TERMINADO'
  CANCELADO = 'CANCELADO'

  VENTANILLA = 'VENTANILLA'
  ONLINE = 'EN LINEA'

  def self.get_number_available(periodo)
    numero_nes = Token.where("ciclo_id = ?", periodo).order("CAST(numero as integer) desc").limit(1)
    if numero_nes.blank?
      numero_nes = 0
    else
      numero_nes = numero_nes.first.numero.to_i
    end

    numero_inscripciones = Ficha.where("ciclo_id = ?", periodo).order("CAST(numero as integer) desc").limit(1)
    if numero_inscripciones.blank?
      numero_inscripciones = 0
    else
      numero_inscripciones = numero_inscripciones.first.numero.to_i
    end

    maximo = 1
    if numero_nes > numero_inscripciones
      maximo = numero_nes.to_i + 1
    else
      maximo = numero_inscripciones.to_i + 1
    end

    '%04d' % (maximo)
  end
end
