class Inscripcion < ActiveRecord::Base
  establish_connection :development_sec
  ACEPTADO = 1
  NO_ACEPTADO = 2
  APROBADO = 3
  REPROBADO = 4
  REGULAR = 'REGULAR'
  IRREGULAR = 'IRREGULAR'
  REPETIDOR = 'REPETIDOR'

  belongs_to :alumno
  belongs_to :grupo
  belongs_to :ciclo
  belongs_to :carrera
  belongs_to :semestr,
             :class_name => "Semestr",
             :foreign_key => 'semestre_id'

  has_one :lectura,
          :dependent => :destroy

  has_many :inscripciones_cursos,
           :dependent => :destroy
  has_many :cursos,
           :through => :inscripciones_cursos

  delegate :nombre,
           :to => :grupo,
           :prefix => true,
           :allow_nil => true
  delegate :nombre_carrera,
           :to => :carrera,
           :prefix => true,
           :allow_nil => true
  delegate :clave_semestre,
           :to => :semestr,
           :prefix => true,
           :allow_nil => true
  delegate :ciclo,
           :to => :ciclo,
           :prefix => true,
           :allow_nil => true

  def self.get_all_by_alumno_id(alumno_id)
    select("inscripciones.*, ciclos.ciclo as nombre_ciclo, semestres.clave_semestre as nombre_semestre, grupos.nombre as nombre_grupo").
        joins(:ciclo, :semestr, :grupo).where(:inscripciones => {:alumno_id => alumno_id}).order("semestres.clave, ciclos.ciclo")
  end

  def self.get_all_by_alumno_id_and_carrera_id(alumno_id, carrera_id)
    select("inscripciones.*, ciclos.ciclo as nombre_ciclo, semestres.clave_semestre as nombre_semestre, grupos.nombre as nombre_grupo").
        joins(:ciclo, :semestr, :grupo).where(:inscripciones => {:alumno_id => alumno_id, :carrera_id => carrera_id}).order("semestres.clave, ciclos.ciclo")
  end

  def self.get_all_by_alumno_id_and_carrera_id_and_clave_semestre(alumno_id, carrera_id, clave_semestre)
    select("inscripciones.*, ciclos.ciclo as nombre_ciclo, semestres.clave_semestre as nombre_semestre, grupos.nombre as nombre_grupo").
        joins(:ciclo, :semestr, :grupo).where(:inscripciones => {:alumno_id => alumno_id, :carrera_id => carrera_id },:semestres => {:clave => clave_semestre}).order("ciclos.ciclo")
  end

=begin
  def self.get_sexo_and_total_alumnos_by_carrera_id_and_periodo_id_and_semestre_id(carrera,periodo,semestre)
    select("distinct datos_personales.sexo , count(datos_personales.alumno_id) as total").
        joins(:alumno => :dato_personal).
        where("inscripciones.carrera_id = ? and inscripciones.ciclo_id = ? and inscripciones.semestre_id = ?",carrera, periodo,semestre).group("datos_personales.sexo")
  end
=end

=begin
  def self.get_edad_and_total_alumnos_by_carrera_id_and_periodo_id_and_semestre_id(carrera,periodo,semestre)
    select("distinct date_part('year',age(datos_personales.fecha_nacimiento )) AS edad, count(datos_personales.alumno_id) as total").
        joins(:alumno => :dato_personal).
        where("inscripciones.carrera_id = ? and inscripciones.ciclo_id = ? and inscripciones.semestre_id = ?",carrera, periodo,semestre).group("datos_personales.sexo")
  end
=end

  def self.get_edad_and_sexo_and_total_alumnos_by_carrera_id_and_periodo_id_and_semestre_id(carrera,periodo,semestre)
    select("distinct date_part('year',age(datos_personales.fecha_nacimiento )) AS edad, datos_personales.sexo  , count(datos_personales.alumno_id) as total").
        joins(:alumno => :dato_personal).
        where("inscripciones.carrera_id = ? and inscripciones.ciclo_id = ? and inscripciones.semestre_id = ?",carrera, periodo,semestre).group("datos_personales.sexo")
  end

  def self.get_last_by_alumno(alumno)
    joins(:ciclo, :semestr).
        where(:inscripciones => {:alumno_id => alumno, :status =>['REGULAR','IRREGULAR','REPETIDOR']}, :ciclos => {:tipo => ['A', 'B']}).
        order("ciclos.ciclo desc, semestres.clave desc").first
  end

  def self.get_last_verano_by_alumno(alumno)
    joins(:ciclo).
        where(:inscripciones => {:alumno_id => alumno, :status =>['REGULAR', 'IRREGULAR', 'REPETIDOR']}, :ciclos => {:tipo => ['V']}).
        order("ciclos.ciclo desc").first
  end

  def self.get_first_by_carrera_and_alumno_and_semestre(carrera, alumno, semestre)
    joins(:semestr, :ciclo).
        where(:inscripciones => {:alumno_id => alumno, :carrera_id => carrera}, :semestres => {:clave => semestre}).
        order("ciclos.ciclo").
        first
  end

  def self.exist?(alumno, semestre)
    !joins(:semestr).
        where(:inscripciones => {:alumno_id => alumno}, :semestres => {:clave => semestre}).
        blank?
  end

  def get_periodo
    return !self.ciclo.nil? ? self.ciclo.ciclo : ""
  end

  def get_grupo
    return !self.grupo.nil? ? self.grupo.nombre : "Sin grupo"
  end

=begin
  def self.get_list_bachilleratos_estado_procedencia_and_alumnos(ciclo,carrera,semestre)
    select("distinct estados.nombre as estado,  count(inscripciones.id) as total").
        joins(:alumno => { :antecedente_academico => {:escuela_procedente => {:direccion => { :ciudad => :estado }}}}).
        where(:inscripciones => {:ciclo_id => ciclo, :carrera_id =>carrera ,:semestre_id =>semestre}, :direcciones => {:tabla_type => "EscuelaProcedente"}).
        group("estados.nombre").
        order("estados.nombre")
  end

  def self.get_list_bachillerato_procedencia_and_num_alumnos_by_ciclo_and_carrera_and_semestre(ciclo,carrera,semestre)
    select("distinct escuelas_procedentes.nombre, count(alumnos.id) as total").
        joins(:alumno => {:antecedente_academico => :escuela_procedente}).
        where(:inscripciones => {:ciclo_id => ciclo, :carrera_id =>carrera ,:semestre_id =>semestre}).
        group("escuelas_procedentes.nombre").
        order("escuelas_procedentes.nombre")
  end
=end

  def self.alumno_ya_inscrito?(alumno_id, periodo_id)
    !Inscripcion.where(:alumno_id => alumno_id, :ciclo_id => periodo_id, :status => [REGULAR, IRREGULAR, REPETIDOR]).blank?
  end
end