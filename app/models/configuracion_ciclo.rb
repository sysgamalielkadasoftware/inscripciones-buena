class ConfiguracionCiclo < ActiveRecord::Base
  establish_connection :development_sec

  belongs_to :ciclo

  def get_fechas_for_parcial_1
    "Del #{self.inicio_parcial1.to_s} al #{self.fin_parcial1.to_s}"
  end

  def get_fechas_for_parcial_2
    "Del #{self.inicio_parcial2.to_s} al #{self.fin_parcial2.to_s}"
  end

  def get_fechas_for_parcial_3
    "Del #{self.inicio_parcial3.to_s} al #{self.fin_parcial3.to_s}"
  end

  def get_fechas_for_ordinario
    "Del #{self.inicio_final.to_s} al #{self.fin_final.to_s}"
  end

  def get_fechas_for_extraordinario_1
    "Del #{self.inicio_extra1.to_s} al #{self.fin_extra1.to_s}"
  end

  def get_fechas_for_extraordinario_2
    "Del #{self.inicio_extra2.to_s} al #{self.fin_extra2.to_s}"
  end

  def get_fechas_for_especial
    "Del #{self.inicio_especial.to_s} al #{self.fin_especial.to_s}"
  end

  def are_especial_dates_eql?
    self.inicio_especial.eql?(self.fin_especial)
  end
end