class Documento < ActiveRecord::Base
  ACTIVO = 'ACTIVO'
  INACTIVO = 'INACTIVO'

  has_many :documentaciones_entregadas
  has_many :aspirantes, through: :documentaciones_entregadas
end
