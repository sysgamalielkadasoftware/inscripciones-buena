require 'consulta/tutor_lib'
class Formato < ActiveRecord::Base
  establish_connection :development_sec

  MEXICO_ID = Pais.find_by_nombre("MÉXICO").present? ? Pais.find_by_nombre("MÉXICO").id : nil
  OAXACA_ID = Estado.find_by_nombre("OAXACA").present? ? Estado.find_by_nombre("OAXACA").id : nil
  MEXICO_NAME =  Pais.find(MEXICO_ID).nombre
  NINGUNO = "Ninguno"
  SN = 'S/N'
  SC =  'S/C'
  SIN_NUMERO =  'Sin número'
  SIN_CALLE =  'Sin calle'
  SIN_COLONIA =  'Sin colonia'

  belongs_to :ciclo
end
