class Familiar < ActiveRecord::Base
  PADRE = 'PADRE'
  MADRE = 'MADRE'

  has_many :aspirante_familiares
  has_many :aspirantes, through: :aspirante_familiares

  belongs_to :direccion

  def full_name
    self.get_apellido_paterno + self.get_apellido_materno + self.nombre.to_s
  end

  def get_apellido_paterno
    self.apellido_paterno.blank? ? '' : self.apellido_paterno.to_s.strip + ' '
  end

  def get_apellido_materno
    self.apellido_materno.blank? ? '' : self.apellido_materno.to_s.strip + ' '
  end

  def self.get_tutor(aspirante_id)
    AspiranteFamiliar.joins(:familiar).where("aspirantes_familiares.aspirante_id = ? AND familiares.tutor = ?", aspirante_id, true).first
  end
end
