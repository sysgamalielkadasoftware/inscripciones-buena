class OriginSchool < ActiveRecord::Base
  establish_connection :development_sec
  self.table_name = 'escuelas_procedentes'

  has_many :academic_backgrounds

  has_one :address, :as => :tabla

  accepts_nested_attributes_for :address
end