class Fecha < ActiveRecord::Base
  establish_connection :development_sec

  INSCRIPCION = 'INSCRIPCIONES'

  PROCESOS = {:entrega_documentos => "ENTREGA DE DOCUMENTOS", :inscripciones => "INSCRIPCIONES",
              :regularizacion => "REGULARIZACIÓN", :entrega_credenciales => "ENTREGA DE CREDENCIALES",
              :prope_corto => "PROPEDÉUTICO CORTO", :prope_largo => "PROPEDÉUTICO LARGO",
              :inicio_clases => "CLASES"
  }

  def is_date_in_period?(date)
    date = String(date)
    fecha = case date
            when /\A\d{4}-\d{2}-\d{2}\Z/ then date
            when /\A\d{2}-\d{2}-\d{4}\Z/ then date.split("-").reverse.join("-")
            else
              nil
            end
    return false if fecha.blank?
    fecha = fecha.to_date
    inicio = self[:fecha_inicio]
    fin = self[:fecha_fin]
    inicio <= fecha and fin >= fecha
  end

  def self.is_inscripcion_period_active?
    ciclo = Ciclo.get_actual
    periodo_inscripcion = Fecha.find_by_ciclo_id_and_proceso(ciclo.id, INSCRIPCION)
    periodo_activo = false
    # if DateTime.now.between?(periodo_inscripcion.fecha_inicio, periodo_inscripcion.fecha_fin)
    if !periodo_inscripcion.blank? and (periodo_inscripcion.fecha_inicio .. periodo_inscripcion.fecha_fin).cover?(Date.today)
      periodo_activo = true
    else
      periodo_activo = false
    end
    return periodo_activo
  end
end