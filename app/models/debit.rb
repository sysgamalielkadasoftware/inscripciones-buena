class Debit < ActiveRecord::Base
  establish_connection :sirefi_db

  ACTIVO = "ALIVE"
  PAGADO = "PAID"
  ANULADO = "CANCELED"
  CONVENIO = "AGREEMENT"
  ESTADOS = {:ALIVE => "ACTIVO", :PAID => "PAGADO", :CANCELED => "CANCELADO", :AGREEMENT => "ASIGNADO A CONVENIO" }
  IMPUESTO = 1.12

  before_save :encoding, :redondear, :asignar_ciclo_sirefi
  belongs_to :alumno
  belongs_to :concept
  belongs_to :ciclo
  has_one :tramite

  validate :concept_generates_debits
  # Se agrega restricción para evitar que se creen registros duplicados tomando como llave al alumno_id, concepto_id, ciclo_id y description
  validates_uniqueness_of :alumno_id, scope: [:concept_id, :ciclo_id, :description]

  def concept_generates_debits
    errors.add(:base, 'El concepto no genera adeudo. Verifique la configuración') unless self.concept.genera_adeudo
  end

  def asignar_ciclo_sirefi
    self.ciclo = Ciclo.get_ciclo_actual
  end

  def encoding
    #      self.description = Miscelaneo.basic_encode_utf(self.description)
  end

  def redondear
    self.amount = self.amount.round
  end

  def Debit.debe_ficha(alumno)
    concepto = "Ficha"
    return Debit.debe_concepto(alumno, concepto)

  end

  def Debit.debe_cursoPropedeutico(alumno, tipo)
    concepto = "Curso Propedeutico" + " " + tipo
    return debe_concepto(alumno, concepto)
  end

  def Debit.debe_inscripcion(alumno)
    concepto = "Inscripción"
    return debe_concepto(alumno, concepto)
  end

  def Debit.debe_reinscripcion(alumno)
    concepto = "Reinscripción"
    return debe_concepto(alumno, concepto)
  end

  def Debit.debe_concepto(alumno, concepto)
    proceso_fechas = concepto
    concepto = Concept.find_by(description:  concepto)
    unless concepto.blank?
      concepto_id = concepto.id
      adeudosFinancieros = Debit.where(alumno_id: alumno , concept_id: concepto_id , status: ACTIVO)
      !adeudosFinancieros.blank?
    else
      return nil
    end
  end


  def Debit.generar_adeudo(concepto_id, alumno_id, extra_description, ciclo_id)
    return nil if (concepto_id.blank? || alumno_id.blank?)
    @concepto = Concept.active_concepts.find_by_id(concepto_id)
    @alumno = Alumno.find_by_id(alumno_id)
    id = nil
    # puts "============================================================================"
    # puts "alumno_id: #{alumno_id}"
    # puts "@alumno: #{@alumno}"
    # puts "alumno_find: #{Alumno.find_by_curp('VASA990624HOCSNL03')}"
    # puts "============================================================================"
    unless @concepto.blank?
      adeudo_financiero = Debit.new
      monto = @concepto.total_amount
      if @alumno.tipo == 'BAJA_TEMP' or @alumno.tipo == 'BAJA_DEF'
        monto = @concepto.total_amount
      else
        monto *= @alumno.get_last_beca if @concepto.grant
      end
      adeudo_financiero[:amount] = monto
      adeudo_financiero[:alumno_id] = alumno_id
      adeudo_financiero[:concept_id]= concepto_id
      adeudo_financiero[:status] = ACTIVO
      adeudo_financiero[:status] = PAGADO if monto == 0
      adeudo_financiero[:description] = @concepto[:description] + extra_description.to_s
      adeudo_financiero[:ciclo_id] = ciclo_id
      adeudo_financiero
      return (adeudo_financiero.save)? adeudo_financiero.id : nil
    end
    nil
  end

  def Debit.consultar_deudas_del_alumno(alumno)
    datos_debits = Array.new
    debits = nil
    debits = Debit.where(:alumno_id => alumno, :status => Debit::ACTIVO) unless alumno.blank?
    datos_debits =  debits.to_a
    datos_debits
  end

  def Debit.monto_total_de_debits(alumno)
    deudas = Debit.consultar_deudas_del_alumno(alumno)
    monto_total = 0.00
    deudas.each do |d|
      monto_total += d[:amount]
    end
    monto_total
  end

  def self.get_human_date_and_type(month)
    date=Hash.new
    case month
    when '1'
      date[:mes]='ENERO'
      date[:tipo]='A'
    when '2'
      date[:mes]='FEBRERO'
      date[:tipo]='A'
    when '3'
      date[:mes]='MARZO'
      date[:tipo] = 'B'
    when '4'
      date[:mes]='ABRIL'
      date[:tipo] = 'B'
    when '5'
      date[:mes]='MAYO'
      date[:tipo] = 'B'
    when '6'
      date[:mes]='JUNIO'
      date[:tipo] = 'B'
    when '7'
      date[:mes]='JULIO'
      date[:tipo] = 'B'
    when '8'
      date[:mes]='AGOSTO'
      date[:tipo] = 'V'
    when '9'
      date[:mes]='SEPTIEMBRE'
      date[:tipo] = 'V'
    when '10'
      date[:mes]='OCTUBRE'
      date[:tipo] ='A'
    when '11'
      date[:mes]='NOVIEMBRE'
      date[:tipo]='A'
    when '12'
      date[:mes]='DICIEMBRE'
      date[:tipo]='A'
    end
    date
  end
end