class DatoMedico < ActiveRecord::Base
  TIPO_SANGRE = [
      {id: 1, nombre: 'DESCONOCIDO'},
      {id: 2, nombre: 'A +'},
      {id: 3, nombre: 'A -'},
      {id: 4, nombre: 'AB +'},
      {id: 5, nombre: 'AB -'},
      {id: 6, nombre: 'B +'},
      {id: 7, nombre: 'B -'},
      {id: 8, nombre: 'O +'},
      {id: 9, nombre: 'O -'}
  ]
  DISCAPACIDAD = [
      {id: 1, nombre: 'NINGUNA'},
      {id: 2, nombre: 'DISCAPACIDAD AUDITIVA: HIPOACUSIA'},
      {id: 3, nombre: 'DISCAPACIDAD AUDITIVA: SORDERA'},
      {id: 4, nombre: 'DISCAPACIDAD FÍSICA/MOTRIZ'},
      {id: 5, nombre: 'DISCAPACIDAD INTELECTUAL'},
      {id: 6, nombre: 'DISCAPACIDAD MÚLTIPLE'},
      {id: 7, nombre: 'DISCAPACIDAD PSICOSOCIAL'},
      {id: 8, nombre: 'DISCAPACIDAD VISUAL: BAJA VISIÓN'},
      {id: 9, nombre: 'DISCAPACIDAD VISUAL: CEGUERA'}
  ]

  has_one :dato_personal
end
