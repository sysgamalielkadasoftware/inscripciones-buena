class Seriada < ActiveRecord::Base
  establish_connection :development_sec

  belongs_to :asignatura, :foreign_key => 'materias_planes_id'

  def get_clave
    clave_seriada = Asignatura.find_by_id(self.materia_seriada_id)

    return !clave_seriada.nil? ? clave_seriada.clave : ""
  end

  def get_id_seriada
    self.materia_seriada_id
  end
end
