class Lectura < ActiveRecord::Base
  establish_connection :development_sec

  belongs_to :inscripcion

  def valor_lectura_evaluacion(evaluacion, periodo)
    if periodo.is_tipo_v?
      true
    else
      case (evaluacion)
      when Calificacion::PARCIAL_1
        lectura_1
      when Calificacion::PARCIAL_2
        lectura_1 and lectura_2
      when Calificacion::PARCIAL_3,  Calificacion::ORDINARIO
        lectura_1 and lectura_2 and lectura_3
      else
        false
      end
    end
  end
end
