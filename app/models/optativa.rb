class Optativa < ActiveRecord::Base
  establish_connection :development_sec

  def get_id_optativa
    self.materia_optativa_id
  end

  def self.get_father(materia_id)
    optativa = Optativa.where(:materia_optativa_id => materia_id).first
    padre = Asignatura.find_by_id(optativa.materias_planes_id) unless optativa.blank?
    padre
  end

  def self.father_name(materia_id)
    nombre = " "
    padre = self.get_father(materia_id)
    nombre = padre.nombre.upcase.strip unless padre.blank?
    nombre
  end

  def self.father_orden(materia_id)
    orden = 0
    padre = self.get_father(materia_id)
    orden = padre.orden unless padre.blank?
    orden
  end
end
