class Estado < ActiveRecord::Base
  establish_connection :development_sec

  belongs_to :pais
  has_many :ciudades
  has_many :municipios
end