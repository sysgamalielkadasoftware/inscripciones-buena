class PersonalDetail < ActiveRecord::Base
  establish_connection :development_sec

  self.table_name = 'datos_personales'

  belongs_to :alumno
  belongs_to :ciudad

  has_and_belongs_to_many :indigenous_languages

end