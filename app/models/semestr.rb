class Semestr < ActiveRecord::Base
  establish_connection :development_sec

  PRIMERO = 1
  SEGUNDO = 2
  TERCERO = 3
  CUARTO = 4
  QUINTO = 5
  SEXTO = 6
  SEPTIMO = 7
  OCTAVO = 8
  NOVENO = 9
  DECIMO = 10

  has_many :planes_estudio,
           :foreign_key => "semestre_id"
  has_many :inscripciones, :foreign_key => "semestre_id"

  def self.get_semestre_by_curso_id(curso_id)
    joins(:planes_estudio => {:asignaturas => :cursos}).where(:cursos => {:id => curso_id}).first
  end

  def self.get_nombre_by_curso_id(curso_id)
    semestre = Semestr.get_semestre_by_curso_id(curso_id)

    return !semestre.nil? ? semestre.clave_semestre : "SIN SEMESTRE"
  end

  def self.get_clave_semestre_by_carrera_and_periodo_and_grupo(carrera,periodo,grupo)
    select("distinct semestres.clave_semestre").
        joins(:inscripciones).
        where(:inscripciones => {:carrera_id => carrera, :ciclo_id => periodo, :grupo_id => grupo}).first
  end

  def self.get_id_by_clave(clave)
    semestre = Semestr.where("clave = ?" , clave).first
    return !semestre.nil? ? semestre.id : nil
  end

  def self.get_clave_by_semestre_id(id)
    semestre = Semestr.where("id = ?" , id).first
    return !semestre.nil? ? semestre.clave : nil
  end

  def self.has_inscripciones_by_alumno_and_clave_semestre(alumno,clave)
    select("distinct semestr.clave ").
        joins(:inscripciones).
        where("inscripciones.alumno_id = ? and semestr.clave = ?", alumno, clave)
  end

  def is_first?
    self.clave.eql?(PRIMERO)
  end

  def self.get_nombre_by_clave(clave)
    semestre = Semestr.where("clave = ?" , clave).first
    return !semestre.nil? ? semestre.clave_semestre.capitalize : ''
  end
end