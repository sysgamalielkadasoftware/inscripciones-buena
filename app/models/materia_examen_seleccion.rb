class MateriaExamenSeleccion < ActiveRecord::Base
  has_many :examenes_materias
  has_many :examenes_seleccion, through: :examenes_materias

  belongs_to :carrera
end
