class Lugar < ActiveRecord::Base
  PAIS = 'PAIS'
  ESTADO = 'ESTADO'
  MUNICIPIO = 'MUNICIPIO'
  LOCALIDAD = 'LOCALIDAD'

  has_one :direccion
  has_one :aspirante, :through => :direccion
  has_one :escuela_procedente, :through => :direccion
  has_one :familiar, :through => :direccion

  def self.get_mexico
    Lugar.find_by_nombre_and_tipo("MÉXICO", Lugar::PAIS)
  end

  def self.get_estados
    Lugar.where("tipo = ?", Lugar::ESTADO)
  end
end
