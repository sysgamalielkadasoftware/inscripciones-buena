class PreregistroPago < ActiveRecord::Base
  belongs_to :aspirante
  belongs_to :concept
  belongs_to :ciclo

  POR_VERIFICAR = 'POR VERIFICAR'
  VERIFICADO = 'VERIFICADO'
end
