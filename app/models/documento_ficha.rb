class DocumentoFicha < ActiveRecord::Base

  def self.has_documentos_fichas(aspirante)
    documentos_fichas = DocumentoFicha.order(:obligatorio, :nombre_documento).all
    # Se considera documentos que ya hayan sido enviados, aquellos donde campo nombre_archivo contenga datos registrados diferente de nulo.
    documentos_aspirante = AspiranteDocumento.where("nombre_archivo is not null and aspirante_id = ?", aspirante)

    if documentos_fichas.blank?
      return true
    else
      if documentos_aspirante.blank?
        return false
      else
        return true
      end
    end
  end
end
