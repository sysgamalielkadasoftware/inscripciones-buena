class Grupo < ActiveRecord::Base
  establish_connection :development_sec

  LETRA = {
      '0' => 'A',
      '1' => 'B',
      '2' => 'C',
      '3' => 'D',
      '4' => 'E',
      '5' => 'F',
      '6' => 'G',
      '7' => 'H',
      '8' => 'I',
      '9' => 'J',
      '10' => 'K',
      '11' => 'L',
      '12' => 'M',
      '13' => 'N',
      '14' => 'O',
      '15' => 'P',
      '16' => 'Q',
      '17' => 'R',
      '18' => 'S',
      '19' => 'T',
      '20' => 'U',
      '21' => 'V',
      '22' => 'W',
      '23' => 'X',
      '24' => 'Y',
      '25' => 'Z'
  }

  belongs_to :ciclo

  has_many :inscripciones
  has_many :cursos, :through => :grupos_cursos
  has_many :grupos_cursos

  def self.get_all_by_carrera_and_periodo(carrera, periodo)
    grupo_ids = Inscripcion.where(:carrera_id => carrera, :ciclo_id => periodo).map(&:grupo_id).uniq
    Grupo.where(:id => grupo_ids).order("grupos.nombre")
  end

  def self.get_all_by_carrera_and_periodo_and_semestre(carrera, periodo,semestre)
    select("distinct grupos.id, grupos.nombre, grupos.contador, carreras.nombre_carrera").
        joins(:cursos, :inscripciones => [:alumno, :carrera] ).
        where(:grupos => {:ciclo_id => periodo},
              :cursos => {:ciclo_id => periodo},
              :inscripciones => {:carrera_id => carrera, :ciclo_id => periodo,:semestre_id => semestre}).
        order("grupos.nombre")
  end

  def self.get_all_by_nombre_and_periodo(nombre, periodo)
    where("ciclo_id = ? and nombre like ?", periodo.id, nombre + '%').
        order("nombre")
  end

  def get_description_full
    self.nombre_carrera << ' | ' + ('%8.7s' % self.nombre.to_s) + ' | ' + ('%02d' % self.contador.to_s) + ' Alumno(s) inscritos(s)'
  end

  def get_description
    nombre + " | " + contador.to_s + " Alumno(s) inscrito(s)"
  end

  def self.get_name_by_id(grupo_id)
    grupo = Grupo.find_by_id(grupo_id)
    return !grupo.blank? ? grupo.nombre : "SIN GRUPO"
  end
end