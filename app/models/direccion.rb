class Direccion < ActiveRecord::Base
  has_one :aspirante
  has_one :dato_personal
  has_one :escuela_procedente
  has_one :familiar

  belongs_to :lugar

  def self.get_calle(id)
    direccion = Direccion.find(id)
    direccion.calle
  end

  def self.get_numero(id)
    direccion = Direccion.find(id)
    direccion.numero
  end

  def self.get_colonia(id)
    direccion = Direccion.find(id)
    direccion.colonia
  end

  def self.get_codigo_postal(id)
    direccion = Direccion.find(id)
    direccion.codigo_postal
  end

  def get_lugar
    direccion = Direccion.find(self.id)
    lugar = Lugar.find(direccion.lugar_id)
    ubicacion = ''
    if lugar.tipo == Lugar::PAIS
      ubicacion = lugar.nombre
    else
      puts "tipo: " + lugar.lugar_id.to_s
      municipio = Lugar.find(lugar.lugar_id)
      estado = Lugar.find(municipio.lugar_id)
      pais = Lugar.find(estado.lugar_id)
      # municipio = Lugar.find_by_lugar_id(lugar.id)
      # puts "municipio: "+ municipio.nombre
      ubicacion = lugar.nombre + ', ' + municipio.nombre  + ', ' + estado.nombre + ', ' + pais.nombre
    end
  end

  def get_short
    calle.to_s + ', No. ' + numero.to_s + ', Col: ' + colonia.to_s
  end
end
