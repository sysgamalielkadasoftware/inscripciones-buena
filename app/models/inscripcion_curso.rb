class InscripcionCurso < ActiveRecord::Base
  establish_connection :development_sec

  APROBADA = 1
  REPROBADA = 2
  REGULAR = 'REGULAR'
  IRREGULAR = 'IRREGULAR'
  REPETIDOR = 'REPETIDOR'

  belongs_to :inscripcion
  belongs_to :curso

  has_one :calificacion, :dependent => :destroy

  def self.exist?(alumno, periodo, semestre, asignatura)
    !joins(:curso, :inscripcion => :semestr).
        where(:inscripciones => {:alumno_id => alumno, :ciclo_id => periodo},
              :semestres => {:clave => semestre},
              :cursos => {:ciclo_id => periodo, :materias_planes_id => asignatura}).
        blank?
  end

  def self.is_inscrito?(alumno, asignatura)
    !joins(:curso, :inscripcion).
        where(:inscripciones => {:alumno_id => alumno},
              :cursos => {:materias_planes_id => asignatura}).
        blank?
  end

  #
  # Regresa las inscripciones que el alumno se ha tenido de la asignatura proporcionada.
  # Se contabilizan las inscripciones que se encuentren registradas en la base de datos.
  #
  def self.cuantos_inscrito?(alumno, asignatura)
    joins(:curso, :inscripcion).
        where(:inscripciones => {:alumno_id => alumno},
              :cursos => {:materias_planes_id => asignatura})
  end
end