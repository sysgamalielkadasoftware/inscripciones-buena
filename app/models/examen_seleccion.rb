class ExamenSeleccion < ActiveRecord::Base
  has_many :examenes_materias
  has_many :materias_examen_seleccion, through: :examenes_materias
end
