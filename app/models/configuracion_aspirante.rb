class ConfiguracionAspirante < ActiveRecord::Base
  establish_connection :development_sec
  FICHA = 'FICHA'
  PROPE = 'PROPE'
  CORTO = 'CORTO'
  LARGO = 'LARGO'

  belongs_to :ciclo
  has_many :tokens

  delegate :ciclo,
           :to => :ciclo,
           :prefix => true,
           :allow_nil => true

  def self.get_ciclo_actual_configuracion_aspirante(proceso)
    return ConfiguracionAspirante.where(:actual => true, :proceso => proceso).first
  end

  protected
  def set_nil_to_fichas
    if proceso.eql?(PROPE)
      self.hora_examen = nil
      self.fecha_examen = nil
      self.lugar = nil

      unless ConfiguracionAspirante.where(:ciclo_id => ciclo_id, :proceso => PROPE, :tipo => tipo).blank?
        errors.add :proceso, "Ya se encuentra registrado en el sistema."
      end
    end

    self.actual = !ConfiguracionAspirante.where(:actual => true, :ciclo_id => ciclo_id).blank?
    nil
  end

  def self.get_configuracion_aspirante(configuracion_id)
    configuracion = nil
    configuracion = ConfiguracionAspirante.find_by_id(configuracion_id)
    configuracion
  end

  def self.get_all_fechas_examen_seleccion_hash
    today = Date.today.to_datetime
    fechas_solicitud= get_availables_configuracion_aspirante_ficha
    datos = Hash.new
    fechas_solicitud.each do |f|
      cadena = ApplicationController.helpers.format_date(String(f.fecha_examen)) + " Examen en " + String(f.lugar) + " --- Curso " + String(f.tipo)
      datos[cadena] = f.id
    end
    datos
  end

  def self.get_availables_configuracion_aspirante_ficha
    today = Date.today.to_datetime
    fechas_solicitud= ConfiguracionAspirante.where(:actual => true, :proceso => ConfiguracionAspirante::FICHA ).
        where("configuraciones_aspirantes.fecha_inicio <= :today and configuraciones_aspirantes.fecha_examen > :today",
              {today: today} ).order(:fecha_examen)
  end

  def self.get_availables_configuracion_aspirante_prope
    today = Date.today.to_datetime
    fechas_solicitud= ConfiguracionAspirante.where(:actual => true, :proceso => ConfiguracionAspirante::PROPE ).
        where("configuraciones_aspirantes.fecha_inicio <= :today and configuraciones_aspirantes.fecha_fin >= :today",
              {today: today} ).order(:fecha_inicio)
    fechas_solicitud
  end

  def self.get_all_availables_propedeuticos_hash(tipo=nil)
    propedeuticos_disponibles = get_availables_configuracion_aspirante_prope
    propedeuticos_disponibles = propedeuticos_disponibles.where(:tipo => tipo).order(:fecha_inicio) unless tipo.blank?
    datos = Hash.new
    propedeuticos_disponibles.each do |f|
      cadena = String(f.fecha_inicio) + " - Curso " + String(f.tipo)
      datos[cadena] = f.id
    end
    datos
  end

  def self.exist_period_inscription_reinscription?
    Fecha.where(:proceso => Fecha::PROCESOS[:inscripciones], :ciclo_id => Ciclo.get_ciclo_actual.id).last
  end
end