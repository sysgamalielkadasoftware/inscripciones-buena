class Curso < ActiveRecord::Base
  establish_connection :development_sec

  belongs_to :asignatura, :foreign_key => 'materias_planes_id'
  belongs_to :ciclo
  belongs_to :profesor

  has_many :examenes, :dependent => :destroy
  has_many :grupos_cursos
  has_many :grupos, :through => :grupos_cursos
  has_many :inscripciones_cursos
  has_many :inscripciones, :through => :inscripciones_cursos

  def self.search(search)
    if search
      where('nombre LIKE ?', "%#{search}%")
    else
      scoped
    end
  end

  def ciclo_is_null
    errors.add :ciclo_id, :ciclo_in_blank if (self.ciclo_id.to_i) < 1
  end

  def self.en_grupos(id_profe, id_ciclo, id_materias_planes)
    where('profesor_id=? and ciclo_id=? and materias_planes_id=?', id_profe, id_ciclo, id_materias_planes)
  end

  def self.get_all_by_carrera_and_periodo_and_grupo(carrera, periodo, grupo)
    select("distinct cursos.id, materias_planes.nombre, materias_planes.orden, materias_planes.optativa_hija").
        joins(:asignatura, :inscripciones => :alumno).
        where(:inscripciones => {:carrera_id => carrera, :ciclo_id => periodo, :grupo_id => grupo}).
        order("materias_planes.orden")
  end

  def self.get_examen_by_curso_and_tipo(curso, tipo)
    select("fecha").joins(:examenes).where(:examenes => {:curso_id => curso, :tipo => tipo}).first
  end

  def self.get_fecha_aplicacion_by_curso_and_tipo(curso, tipo)
    curso = self.get_examen_by_curso_and_tipo(curso, tipo)
    return !curso.nil? ? curso.fecha.to_s : nil
  end

  def self.get_by_grupo_and_periodo_and_asignatura(grupo, periodo, asignatura)
    joins(:grupos).where(:grupos => {:nombre => grupo, :ciclo_id => periodo},
                         :cursos => {:ciclo_id => periodo, :materias_planes_id => asignatura}).
        first
  end

  public
  def get_nombre
    self.asignatura.get_nombre
  end

  def get_clave
    self.asignatura.clave
  end

  def has_especial?
    Examen.exists?(:curso_id => self, :tipo => Examen::ESPECIAL)
  end
end