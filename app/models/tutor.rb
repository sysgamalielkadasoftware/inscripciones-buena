class Tutor < ActiveRecord::Base
  establish_connection :development_sec

  PARENTESCOS = ['MADRE', 'PADRE', 'ESPOSO', 'ESPOSA', 'MADRASTRA', 'PADRASTRO',
                 'HERMANO', 'HERMANA', 'MEDIO HERMANO', 'MEDIA HERMANA',
                 'HERMANASTRO', 'HERMANASTRA', 'ABUELO', 'ABUELA',
                 'BISABUELO', 'BISABUELA', 'TATARABUELO', 'TATARABUELA',
                 'NIETO', 'NIETA', 'BISNIETO', 'BISNIETA',
                 'TATARANIETO', 'TATARANIETA', 'TÍO', 'TÍA',
                 'SOBRINO','SOBRINA', 'PRIMO', 'PRIMA',
                 'SUEGRO', 'SUEGRA', 'CONSUEGRO', 'CONSUEGRA',
                 'NUERA', 'YERNO', 'CUÑADO', 'CUÑADA',
                 'CONCUÑO', 'CONCUÑA', 'PADRINO', 'MADRINA',
                 'AHIJADO', 'AHIJADA','COMADRE', 'COMPADRE',
                 'PARIENTE', 'NO ESPECIFICADO']

  has_and_belongs_to_many :alumnos #relacion muchos a muchos
  has_one :address, :as => :tabla, :dependent => :destroy
  accepts_nested_attributes_for :address, :allow_destroy => true
  def apellido_is_null
    errors.add :apellido_paterno, :apellido_is_null if (self.apellido_materno.blank? and self.apellido_paterno.blank?)
  end

  def full_name
    self.nombre.to_s + " " + self.apellido_paterno.to_s + " " + self.apellido_materno.to_s
  end
end