class SolicitudFicha < ActiveRecord::Base
  ACEPTADO = 'ACEPTADO'
  TERMINADO = 'TERMINADO'
  CANCELADO = 'CANCELADO'
  ENVIADO = 'ENVIADO'
  ASIGNADO = 'ASIGNADO'

  VENTANILLA = 'VENTANILLA'
  ONLINE = 'EN LINEA'
  RUTA_COMPROBANTES = './public/inscripciones/fichas'

  has_one :ficha

  belongs_to :sede_ficha
  belongs_to :aspirante
  belongs_to :carrera

end
