class Anuncio < ActiveRecord::Base
  MODULO = [
    {id: 1, nombre:'Home'},
    {id: 2, nombre:'Solicitud de ficha'},
    {id: 3, nombre:'Nuevo Aspirante'},
    {id: 4, nombre:'Consulta de ficha'},
    {id: 5, nombre:'Documentos'},
    {id: 6, nombre:'Fichas'},
    {id: 7, nombre:'Consulta de propedeútico'},
    {id: 8, nombre:'Solicitud de inscripción'},
    {id: 9, nombre:'Solicitud de reinscripción'}
  ]
  #Se crea un arreglo de opciones para mostrar los mensajes en el lugar seleccionado
  validates :mensaje, presence: true, length:{maximum: 200}
  validates :desde, presence: true
  validates :hasta, presence: true
  validates :modulo, presence: true

  #Método para mostrar el lugar que se haya seleccionado para el mensaje
  def self.get_lugar_anuncio(id)
    lugar = 'NINGUNO'
    MODULO.each do |mod|
      if mod[:id] == id
        lugar = mod[:nombre]
      end
    end
    lugar
  end

  #el partial _mensajes, muestra los mensajes que haya para el lugar al que ingresaron, y que tenga seleccionado el módulo
end
