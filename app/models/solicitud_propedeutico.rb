class SolicitudPropedeutico < ActiveRecord::Base
  ENVIADO = 'ENVIADO'
  TERMINADO = 'TERMINADO'
  CANCELADO = 'CANCELADO'
  VENTANILLA = 'VENTANILLA'
  ONLINE = 'EN LINEA'

  has_one :inscripcion_propedeutico
  belongs_to :aspirante
  belongs_to :carrera
  belongs_to :periodo_propedeutico
end
