class IndigenousLanguage < ActiveRecord::Base
  establish_connection :development_sec
  self.table_name = 'lenguas_indigenas'

  has_and_belongs_to_many :personal_detail
end