class Universidad < ActiveRecord::Base
  establish_connection :development_sec

  has_many :campus

  def self.get_nombre_by_alumno_id(alumno_id)
    select('universidades.nombre').joins(:campus => [:carreras => :inscripciones]).where(:inscripciones => {:alumno_id=> alumno_id}).last.nombre.to_s
  end
end
