class Concept < ActiveRecord::Base
  establish_connection :sirefi_db

  ACTIVE = 'ACTIVE'

  ficha = Concept.select("code").where("description like ? and status = ?", "%FICHA%", Concept::ACTIVE).first
  ficha_id = Concept.select("id").where("description like ? and status = ?", "%FICHA%", Concept::ACTIVE).first
  propec = Concept.select("code").where("description like ? and description like ? and status = ?", "%PROPE%", "%CORTO%", Concept::ACTIVE).first
  propel = Concept.select("code").where("description like ? and description like ? and status = ?", "%PROPE%", "%LARGO%", Concept::ACTIVE).first
  inscripcion = Concept.select("code").where("description like ? and status = ?", "INSCRIPCI%", Concept::ACTIVE).first
  reinscripcion = Concept.select("code").where("description like ? and status = ?", "%REINSCRIPCI%", Concept::ACTIVE).first
  colegiatura = Concept.select("code").where("description like ? and status = ?", "%COLEGIATURA%", Concept::ACTIVE).first

  FICHA_CODE = ficha.blank? ? "" : ficha.code #"KE01"
  FICHA_ID = ficha_id.blank? ? "" : ficha_id.id
  PROPEC_CODE = propec.blank? ? "" : propec.code
  PROPEL_CODE =  propel.blank? ? "" : propel.code
  INSCRIPCION_CODE = inscripcion.blank? ? "" : inscripcion.code # código del pago  de inscripción
  REINSCRIPCION_CODE = reinscripcion.blank? ? "" : reinscripcion.code # código del pago  de reinscripción
  COLEGIATURA_CODE = colegiatura.blank? ? "" : colegiatura.code
  CODES_ONLINE = {:FICHA => FICHA_CODE, :PROPEDEUTICO => PROPEC_CODE, :PROPEDEUTICO_LARGO => PROPEL_CODE, :INSCRIPCION => INSCRIPCION_CODE, :REINSCRIPCION => REINSCRIPCION_CODE}

  scope :active_concepts, -> {where(status: Concept::ACTIVE).order('description')}
  scope :generating_debit_active_concepts, -> {where(status: Concept::ACTIVE, genera_adeudo: true).order('description')}
  scope :no_debit_active_concepts, -> {where(status: Concept::ACTIVE, genera_adeudo: [false, nil]).order('description')}

  has_many :debits
  has_many :preregistros_pagos
  has_many :preregistro_pago_nes

  def total_amount(beca=nil)
    _beca = beca.blank? ? 1 : beca
    _beca = 1 if !self.grant
    _amount  = BigDecimal.new(self.amount.to_s)
    amount = Concept.round_for_amount(_amount, _beca)
    impuesto =    self.tax ?  Concept.round_for_tax(_amount, _beca) : 0.0
    amount + impuesto
  end

  def total_amount_nes
    amount  = BigDecimal.new(self.amount.to_s)
    total  = self.tax ? amount * Debit::IMPUESTO : amount
    total.round
  end

  def self.round_for_amount(amount, beca=nil)
    raise "No es un número" if amount.blank?
    _beca = beca.blank? ? 1 : beca
    _amount =  BigDecimal.new(amount, 9) * _beca
    _amount.frac <= 0.5 ? _amount.floor :  _amount.ceil
  end

  def self.round_for_tax(amount, beca=nil)
    raise "No es un número" if amount.blank?
    _beca = beca.blank? ? 1 : beca
    _tax =  BigDecimal.new(amount, 9) * (1.12 - 1.0) # El 1.12 es el impuesto que se encuentra en el modelo de Debit del portal sirefi
    _tax *= _beca
    _tax.frac < 0.5 ? _tax.floor :  _tax.ceil
  end

  def Concept.obtener_datos_del_concepto(concepto_id)
    concepto = Concept.find_by_id(concepto_id)
    unless concepto.blank?
      @datos_concepto = {:code => concepto.code, :description => concepto.description, :amount => Float(concepto.total_amount)}
      return @datos_concepto
    else
      return nil
    end
  end

  def Concept.obtener_concepto_del_tramite(tramite)
    codigo = CODES_ONLINE[tramite.upcase.to_sym]
    dato = Concept.where(code: codigo, status: ACTIVE).last
    Rails.logger.info "EL CONCEPTO #{FICHA_CODE}  PARA " + tramite.capitalize + ' NO ESTÁ REGISTRADO EN EL SISTEMA DE RECURSOS FINACIEROS' if dato.blank?
    dato
  end

  def Concept.obtener_debits_y_concepto_del_alumno(alumno, tramite)
    concepto = Hash.new
    conceptos_debits = Array.new
    debits = Debit.consultar_deudas_del_alumno(alumno)
    conceptos_debits += debits
  end
end