class Municipio < ActiveRecord::Base
  establish_connection :development_sec

  belongs_to :estado

  has_many :ciudades
end