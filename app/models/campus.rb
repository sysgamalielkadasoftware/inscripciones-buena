class Campus < ActiveRecord::Base
  establish_connection :development_sec

  belongs_to :universidad

  # has_many :direcciones, :as => :tabla
  has_many :carreras

  delegate :nombre, :to => :universidad, :prefix => true, :allow_nil => true
  delegate :siglas, :to => :universidad, :prefix => true, :allow_nil => true
  delegate :clave, :to => :universidad, :prefix => true, :allow_nil => true
  delegate :nombre_vicerrector_academico, :to => :universidad, :prefix => true, :allow_nil => true
  delegate :nombre_jefe_servicios_escolares, :to => :universidad, :prefix => true, :allow_nil => true
  delegate :lema, :to => :universidad, :prefix => true, :allow_nil => true
  delegate :lema_dialecto, :to => :universidad, :prefix => true, :allow_nil => true
  delegate :nombre_rector, :to => :universidad, :prefix => true, :allow_nil => true

  def full_name
    nombre + ((clave.nil? or clave.blank?) ? '' : '-' + clave)
  end

  def get_full_direccion
    self.direcciones.first.get_full if self.direcciones.exists?
  end
end
