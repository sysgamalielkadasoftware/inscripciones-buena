class InscripcionPropedeutico < ActiveRecord::Base
  PENDIENTE = 'PENDIENTE'
  ACEPTADO = 'ACEPTADO'
  NO_ACEPTADO = 'NO ACEPTADO'
  CONDICIONADO = 'CONDICIONADO'
  PASAR_ESCOLARES = 'PASAR A ESCOLARES'
  STATUS = [PENDIENTE, ACEPTADO, NO_ACEPTADO, CONDICIONADO, PASAR_ESCOLARES]

  has_many :calificaciones_propedeuticos, :dependent => :destroy

  belongs_to :aspirante
  belongs_to :autorizacion
  belongs_to :carrera
  belongs_to :grupo_propedeutico
  belongs_to :solicitud_propedeutico

  def is_aceptado?
    estado.eql?(ACEPTADO)
  end
end
