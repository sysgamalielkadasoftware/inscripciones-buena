class Alumno < ActiveRecord::Base
  establish_connection(:development_sec)

  ORDER_FULL_NAME = "alumnos.id as alumno_id,
    alumnos.tipo,
    alumnos.matricula,
    trim(
      case
        when alumnos.apellido_paterno is null then ''
        when length(alumnos.apellido_paterno) = 0 then ''
        else alumnos.apellido_paterno || ' '
      end ||
      case
        when alumnos.apellido_materno is null then ''
        when length(alumnos.apellido_materno) = 0 then ''
        else alumnos.apellido_materno || ' '
      end ||
      alumnos.nombre)
    as full_name_as_string"

  NUEVO = "NUEVO"
  FICHA = "FICHA"
  PROPE = "PROPE"
  ALUMNO = "ALUMNO"
  BAJA_TEMP = "BAJA_TEMP"
  BAJA_DEF = "BAJA_DEF"
  EGRESADO = "EGRESADO"
  ADEUDO_ACTIVO = "V"
  ADEUDO_PAGADO = "H"

  belongs_to :carrera
  belongs_to :profesor

  has_many :addresses, :as => :tabla, :dependent => :destroy
  has_many :ciclos, :through => :fichas
  has_many :debits
  has_many :grupos, :through => :inscripciones
  has_many :inscripciones
  has_many :preregistros_pagos_nes
  has_many :tokens

  has_and_belongs_to_many :tutores

  has_one :personal_detail
  has_one :tramite

  accepts_nested_attributes_for :addresses

  delegate :nombre_carrera, :to => :carrera, :prefix => true, :allow_nil => true

  def get_apellido_paterno
    self.apellido_paterno.blank? ? '' : self.apellido_paterno.to_s.strip + ' '
  end

  def get_apellido_materno
    self.apellido_materno.blank? ? '' : self.apellido_materno.to_s.strip + ' '
  end

  def get_apellido_materno_sort
    self.apellido_materno.blank? ? '' : self.apellido_materno.to_s
  end

  def get_nombre
    self.nombre.blank? ? '' : self.nombre.to_s.strip + ' '
  end

  def full_name
    self.get_apellido_paterno + self.get_apellido_materno + self.nombre.to_s
  end

  def full_name_sort
    self.nombre.to_s + " " + self.get_apellido_paterno + self.get_apellido_materno_sort
  end

  def apellido_is_null
    errors.add :apellido_paterno, :apellido_is_null if (self.apellido_materno.blank? and self.apellido_paterno.blank? )
  end

  def has_inscripcion?
    Inscripcion.where("alumno_id = ?", self.id)
  end

  def tiene_inscripcion?
    tiene =false
    inscripciones = Inscripcion.where("alumno_id = ? AND status != 'OBSOLETA' AND ciclo_id != ?", self.id, Ciclo.get_actual)
    unless inscripciones.blank?
      tiene = true
    else
      tiene = false
    end
    tiene
  end

  def is_egresado?
    tipo.eql?(EGRESADO)
  end

  def self.are_there_alumnos_with_calificaciones?(carrera, periodo, grupo, curso)
    !joins(:inscripciones => {:inscripciones_cursos => :calificacion}).
        where(:inscripciones => {:carrera_id => carrera, :ciclo_id => periodo, :grupo_id => grupo}, :inscripciones_cursos => {:curso_id => curso}).blank?
  end

  def self.get_all_by_grupo_and_periodo(grupo, periodo)
    select(Alumno::ORDER_FULL_NAME + ", user_id, correo_electronico, alumnos.carrera_id, inscripciones.*").
        joins(:inscripciones).
        where(:inscripciones => {:ciclo_id => periodo, :grupo_id => grupo}).order("full_name_as_string")
  end

  def self.get_list_by_carrera_and_periodo_and_grupo(carrera, periodo, grupo)
    select(ORDER_FULL_NAME + ", inscripciones.id as alumno_inscripcion_id").
        joins(:inscripciones).
        where(:inscripciones => {:carrera_id => carrera, :ciclo_id => periodo, :grupo_id => grupo}).order("full_name_as_string")
  end

  def self.get_all_by_grupo_and_curso_without_calificaciones(grupo, curso)
    select(ORDER_FULL_NAME + ", inscripciones_cursos.id as inscripcion_curso_id").
        joins(:inscripciones => :inscripciones_cursos).
        where(:inscripciones => {:grupo_id => grupo}, :inscripciones_cursos => {:curso_id => curso}).
        order("full_name_as_string")
  end

  def self.get_list_by_carrera_and_periodo_and_grupo_and_curso_without_calificaciones(carrera, periodo, grupo, curso)
    select(ORDER_FULL_NAME + ", inscripciones_cursos.id as inscripcion_curso_id, inscripciones.id as alumno_inscripcion_id, inscripciones_cursos.status").
        joins(:inscripciones => :inscripciones_cursos).
        where(:inscripciones => {:carrera_id => carrera, :ciclo_id => periodo, :grupo_id => grupo}, :inscripciones_cursos => {:curso_id => curso}).
        order("full_name_as_string")
  end

  def self.get_list_by_carrera_and_periodo_and_grupo_and_curso_with_calificaciones(carrera, periodo, grupo, curso)
    select(ORDER_FULL_NAME + ", calificaciones.*, calificaciones.promedio as calif , calificaciones.asistencia_final as asistencia").
        joins(:inscripciones => {:inscripciones_cursos => :calificacion}).
        where(:inscripciones => {:carrera_id => carrera, :ciclo_id => periodo, :grupo_id => grupo}, :inscripciones_cursos => {:curso_id => curso}).
        order("full_name_as_string")
  end

  def self.get_promedio_by_curso(alumno_id, curso_id)
    calificaciones_alumno = select("calificaciones.promedio").
        joins(:inscripciones => {:inscripciones_cursos => :calificacion}).
        where(:inscripciones => {:alumno_id => alumno_id}, :inscripciones_cursos => {:curso_id => curso_id}).first
    return calificaciones_alumno.blank? ? "-" : calificaciones_alumno.promedio.to_s
  end

  def self.get_calificaciones_by_curso(alumno_id, curso_id)
    select("calificaciones.*").
        joins(:inscripciones => {:inscripciones_cursos => :calificacion}).
        where(:inscripciones => {:alumno_id => alumno_id}, :inscripciones_cursos => {:curso_id => curso_id}).first
  end

  def get_beca(periodo_id)
    periodo_id = periodo_id.id if periodo_id.class.eql?(Ciclo)
    ins = self.inscripciones.find_by(:ciclo_id => periodo_id)
    unless ins.blank?
      conversion_descuento(ins.beca)
    else
      get_last_beca()
    end
  end

  def get_last_beca
    conversion_descuento(self.inscripciones.last.blank? ? 0 : self.inscripciones.last.beca)
  end

  def conversion_descuento(beca)
    beca.blank? ? 1 : 1-(beca/100.0)
  end
end