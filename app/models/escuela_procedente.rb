class EscuelaProcedente < ActiveRecord::Base
  AREA_CONOCIMIENTO = [
      {id: 1, nombre: 'Administración y negocios'},
      {id: 2, nombre: 'Agronomía y veterinaria'},
      {id: 3, nombre: 'Artes y humanidades'},
      {id: 4, nombre: 'Ciencias de la salud'},
      {id: 5, nombre: 'Ciencias naturales, matemáticas y estadística'},
      {id: 6, nombre: 'Ciencias sociales y derecho'},
      {id: 7, nombre: 'Componente Básico Inicial'},
      {id: 8, nombre: 'Educación'},
      {id: 9, nombre: 'Ingeniería, manufactura y construcción'},
      {id: 10, nombre: 'Servicios'},
      {id: 11, nombre: 'Tecnologías de la información y la comunicación'}
  ]

  has_many :antecedentes_academicos

  belongs_to :direccion
end
