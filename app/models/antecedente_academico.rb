class AntecedenteAcademico < ActiveRecord::Base
  belongs_to :aspirante
  belongs_to :escuela_procedente
end
