class Carrera < ActiveRecord::Base
  establish_connection :development_sec

  has_many :fichas
  has_many :grupos_propedeuticos
  has_many :inscripciones
  has_many :inscripciones_propedeuticos
  has_many :materias_examen_seleccion
  has_many :materias_propedeuticos
  has_many :planes_estudio
  has_many :profesores
  has_many :solicitudes_fichas
  has_many :solicitudes_propedeuticos
  has_many :tokens

  def self.get_carreras
    carreras = Carrera.order(:nombre_carrera).all
    carreras
  end

  protected
  def has_planes_estudio
    errors.add :carrera , "La carrera ya cuenta con algún plan de estudios registrado." unless PlanEstudio.where(:carrera_id => self.id).blank?
  end

  def has_inscripciones
    errors.add :carrera , "La carrera ya cuenta con inscripciones." unless Inscripcion.where(:carrera_id => self.id).blank?
  end

  public
  def self.get_carrera_by_curso_id(curso_id)
    joins(:planes_estudio => {:asignaturas => :cursos}).where(:cursos => {:id => curso_id}).first
  end

  def self.get_nombre_by_curso_id(curso_id)
    carrera = Carrera.get_carrera_by_curso_id(curso_id)

    return !carrera.nil? ? carrera.nombre_carrera : "SIN CARRERA"
  end

  def self.get_id_by_curso_id(curso_id)
    carrera = Carrera.get_carrera_by_curso_id(curso_id)

    return !carrera.nil? ? carrera.id : nil
  end

  def self.get_nombre_by_alumno_id(alumno_id)
    carrera = joins(:inscripciones => :semestr).where(:inscripciones => {:alumno_id => alumno_id}).order("semestres.clave").last

    return !carrera.nil? ? carrera.nombre_carrera : "SIN CARRERA"
  end

  def self.get_id_by_alumno_id(alumno_id)
    carrera = joins(:inscripciones => :semestr).where(:inscripciones => {:alumno_id => alumno_id}).order("semestres.clave").last

    return !carrera.nil? ? carrera.id : nil
  end

  def self.get_carrera_by_periodo_and_grupo(periodo,grupo)
    select("distinct carreras.id as id, carreras.nombre_carrera as nombre_carrera").
        joins(:inscripciones).
        where(:inscripciones => {:ciclo_id => periodo, :grupo_id => grupo}).first
  end

  def self.get_all_by_periodo_id(periodo)
    select("distinct carreras.id as id, carreras.nombre_carrera as nombre_carrera").
        joins(:inscripciones).
        where(:inscripciones => {:ciclo_id => periodo})
  end

  def self.has_planes_de_estudio?
    !joins(:planes_estudio, :semestr).
        where(:semestres => {:clave => Semestr::PRIMERO}).blank?
  end

  def get_clave_with_nombre
    self.codigo_carrera.to_s + " - " + self.nombre_carrera.to_s
  end


  def self.get_all_carreras_hash
    carreras = Carrera.all.order(:nombre_carrera)
    datos = Hash.new
    carreras.each do |car|
      datos[car.nombre_carrera] = car.id
    end
    datos
  end
end