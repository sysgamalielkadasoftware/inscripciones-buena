class Ciclo < ActiveRecord::Base
  establish_connection :development_sec

  IMPAR = 0
  PAR = 1
  VERANO = 2

  has_one :configuracion_ciclo, :dependent => :destroy

  has_many :cursos
  has_many :inscripciones
  has_many :fichas
  has_many :grupos
  has_many :configuraciones_aspirantes
  has_many :periodos_fichas
  has_many :periodos_propedeuticos
  has_many :plan_estudios
  has_many :preregistros_pagos
  has_many :propedeuticos
  has_many :tokens

  belongs_to :tramite

  accepts_nested_attributes_for :configuracion_ciclo, :allow_destroy => true

  def self.get_actual
    ciclo = Ciclo.find_by_actual(true)
  end

  public
  def self.get_periodo_by_curso_id(curso_id)
    joins(:cursos).where(:cursos => {:id => curso_id}).first
  end

  def self.get_nombre_by_curso_id(curso_id)
    periodo = Ciclo.get_periodo_by_curso_id(curso_id)

    return !periodo.blank? ? periodo.ciclo : "SIN PERIODO"
  end

  def self.is_verano?(ciclo_id)
    exists?(:id => ciclo_id, :tipo => 'V')
  end

  def self.actual_has_calendario_escolar?
    !joins(:configuracion_ciclo).where(:actual => true).blank?
  end

  def self.get_ciclo_by_periodo(per)
    periodo = Ciclo.find_all_by_ciclo(per).first

    return !periodo.blank? ? periodo.inicio.to_s + "-" + periodo.fin.to_s : "SIN CICLO"
  end

  def self.get_nombre_ciclo_by_alumno_id_and_semestre(alumno, semestre)
    periodo = Ciclo.joins(:inscripciones).where(:inscripciones => {:semestre_id => semestre, :alumno_id => alumno}).first
    return !periodo.blank? ? periodo.ciclo.to_s : "SIN PERIODO"
  end

  def is_tipo_a?
    self.tipo.eql?('A')
  end

  def is_tipo_b?
    self.tipo.eql?('B')
  end

  def is_tipo_v?
    self.tipo.eql?('V')
  end

  def has_relations?
    (!self.inscripciones.empty? or
        !self.cursos.empty? or
        !self.grupos.empty? or
        !self.configuraciones_aspirantes.empty? or
        !self.propedeuticos.empty? or
        !self.fichas.empty?)
  end

  def has_not_relations?
    !has_relations?
  end

  def get_tipo_formated
    case tipo
    when 'A' then 'A (Impar)'
    when 'B' then 'B (Par)'
    when 'V' then 'V (Verano)'
    end
  end

  def get_nombre
    "#{self.inicio}-#{self.fin}"
  end

  def has_calendario_escolar?
    !self.configuracion_ciclo.nil?
  end


  def Ciclo.get_ciclo_actual
    return Ciclo.find_by_actual(true)
  end

  def Ciclo.get_ciclo_anterior(n=1)
    ciclos = Ciclo.where(:tipo => ["A", "B"]).order('ciclo').to_a
    actual_index = ciclos.index(Ciclo.get_ciclo_actual)
    index = actual_index - n
    if index > ciclos.count
      index = ciclos.count
    elsif index < 0
      index = 0
    end
    ciclos[index]
  end

  def Ciclo.get_ciclo_posterior(n=1)
    ciclos = Ciclo.where(:tipo => ["A", "B"]).order('ciclo').to_a
    actual_index = ciclos.index(Ciclo.get_ciclo_actual)
    index = actual_index + n
    if index > ciclos.count
      index = ciclos.count
    elsif index < 0
      index = 0
    end
    ciclos[index]
  end

  def self.get_ciclo_at_fecha(fecha)
    year, month, day  = String(fecha).split("-")
    mes = Integer(Float(month))
    year = Integer(Float(year))
    @tipo = nil
    case mes
    when 3..7 then @tipo = 'B'
    when 8..9 then @tipo = 'V'
    else @tipo = 'A'
    end
    if mes < 10
      ciclo = Ciclo.find_by(:fin => year, :tipo => @tipo)
    else
      ciclo = Ciclo.find_by(:inicio => year, :tipo => @tipo)
    end
    ciclo
  end
end