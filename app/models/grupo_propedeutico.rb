class GrupoPropedeutico < ActiveRecord::Base

  has_many :inscripciones_propedeuticos

  belongs_to :periodo_propedeutico
  belongs_to :carrera
end
