class PreregistroPagoNes < ActiveRecord::Base
  establish_connection(:development_sec)
  self.table_name = 'preregistros_pagos'

  POR_VERIFICAR = 'POR VERIFICAR'
  VERIFICADO = 'VERIFICADO'

  belongs_to :alumno

  # Se agrega restricción para evitar que se creen registros duplicados tomando como llave al alumno_id, concepto_id y ciclo_id
  validates_uniqueness_of :alumno_id, scope: [:concept_id, :ciclo_id]
end