class GrupoCurso < ActiveRecord::Base
  establish_connection :development_sec

  belongs_to :curso
  belongs_to :grupo
end