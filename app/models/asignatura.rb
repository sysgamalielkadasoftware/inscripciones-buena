class Asignatura < ActiveRecord::Base
  establish_connection :development_sec
  self.table_name= "materias_planes"

  belongs_to :especialidad
  belongs_to :plan_estudio

  has_many :cursos, :foreign_key => 'materias_planes_id'
  has_many :optativas, :foreign_key => 'materias_planes_id', :dependent => :destroy
  has_many :seriadas, :foreign_key => 'materias_planes_id', :dependent => :destroy

  has_and_belongs_to_many :tramites

  accepts_nested_attributes_for :seriadas, :allow_destroy => true, :reject_if => proc { |seriada| seriada[:materia_seriada_id].blank? }

  validates_associated :seriadas

  after_update :save_seriadas

  def new_seriada_attributes=(seriada_attributes)
    seriada_attributes.each do |attributes|
      seriadas.build(attributes) unless attributes[:materia_seriada_id].blank?
    end
  end

  def existing_seriada_attributes=(seriada_attributes)
    seriadas.reject(&:new_record?).each do |seriada|
      attributes = seriada_attributes[seriada.id.to_s]
      if attributes
        seriada.attributes = attributes
      else
        seriadas.delete(seriada)
      end
    end
  end

  def save_seriadas
    seriadas.each do |seriada|
      seriada.save(false)
    end
  end

  def is_seriada?
    !self.seriadas.blank?
  end

  def get_claves_seriadas
    claves_seriadas = ""
    self.seriadas.each {|seriada| claves_seriadas += seriada.get_clave + " " }

    claves_seriadas
  end

  def get_ids_seriadas
    ids_array = Array.new
    self.seriadas.each {|seriada| ids_array << seriada.get_id_seriada}

    ids_array
  end

  def is_optativa?
    #!self.optativas.blank?
    self.optativa
  end

  def get_ids_optativas
    ids_array = Array.new
    self.optativas.each {|optativa| ids_array << optativa.get_id_optativa}

    ids_array
  end

  def get_nombre
    self.nombre.to_s + (self.optativa_hija.eql?(true) ? " - (OPT)" : "")
  end

  def get_clave_and_nombre
    self.clave + ' | ' + get_nombre
  end
end
