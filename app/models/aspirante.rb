class Aspirante < ActiveRecord::Base
  HOMBRE = 'H'
  MUJER = 'M'
  SOLICITUD = 'SOLICITUD'
  NUEVO_DOCUMENTO = 'NUEVO_DOCUMENTO'

  has_one :dato_personal, :dependent => :destroy
  has_one :antecedente_academico, :dependent => :destroy

  has_many :aspirante_familiares
  has_many :documentaciones_entregadas
  has_many :documentos, through: :documentaciones_entregadas
  has_many :familiares, through: :aspirante_familiares
  has_many :fichas
  has_many :inscripciones_propedeuticos
  has_many :preregistros_pagos
  has_many :solicitudes_fichas
  has_many :solicitudes_propedeuticos

  belongs_to :usuario

  accepts_nested_attributes_for :dato_personal
  accepts_nested_attributes_for :antecedente_academico

  def full_name
    self.get_apellido_paterno + self.get_apellido_materno + self.nombre.to_s
  end

  def get_apellido_paterno
    self.apellido_paterno.blank? ? '' : self.apellido_paterno.to_s.strip + ' '
  end

  def get_apellido_materno
    self.apellido_materno.blank? ? '' : self.apellido_materno.to_s.strip + ' '
  end

  def tiene_ficha?
    tiene = ""
    periodo_propedeutico = PeriodoFicha.where("actual = ?", true).first
    if !periodo_propedeutico.id.blank?
      ficha = SolicitudFicha.joins(:ficha, :sede_ficha => :periodo_ficha).
          where("fichas.ciclo_id = ? AND fichas.aspirante_id = ? AND periodos_fichas.tipo = ?", periodo_propedeutico.ciclo_id, self.id, periodo_propedeutico.tipo).first
      unless ficha.blank?
        tiene = ficha.ficha.estado unless ficha.ficha.blank?
      else
        tiene = "NINGUNO"
      end
    end
    tiene
  end

  def tiene_prope?
    tiene = ""
    periodo_propedeutico = PeriodoPropedeutico.where("actual = ?", true).first

    if !periodo_propedeutico.id.blank?
      prope = InscripcionPropedeutico.joins(:solicitud_propedeutico => :periodo_propedeutico).
          where("solicitudes_propedeuticos.aspirante_id = ? AND periodos_propedeuticos.ciclo_id = ? AND periodos_propedeuticos.tipo = ? ", self.id, periodo_propedeutico.ciclo_id, periodo_propedeutico.tipo).first
      unless prope.blank?
        tiene= prope.estado
      else
        tiene= "NINGUNO"
      end
    end
    tiene
  end

  #Este método me permite saber si un aspirante, ya ha realizado o no alguna solicitud en el periodo en el que se encuentra actualmente el periodo de fichas
  def is_aspirante_with_request?
    tiene = ""

    periodos_fichas = PeriodoFicha.where("actual = ?", true)
    ciclo_periodo = periodos_fichas[0]
    solicitud_ficha_ciclo_actual = SolicitudFicha.joins('INNER JOIN aspirantes ON solicitudes_fichas.aspirante_id = aspirantes.id').where(:aspirantes => {:curp => self.curp.upcase},:solicitudes_fichas => {:ciclo_id => ciclo_periodo.ciclo_id}) unless ciclo_periodo.blank?

    # Caso if: el aspirante cuenta con una solicitud en el ciclo actual
    # Caso else: el aspirante no cuenta con una solicitud en el ciclo actual
    if !solicitud_ficha_ciclo_actual.blank?
      documentos = DocumentoFicha.has_documentos_fichas(self.id)
      if !documentos.blank?
        tiene = SOLICITUD
      else
        tiene = NUEVO_DOCUMENTO
      end
    else
      tiene = NUEVO_DOCUMENTO
    end
    return tiene
  end

  def validacion_calf_aspi?
    periodos_fichas = PeriodoPropedeutico.where("actual = ?", true).first
    @propecalifi = CalificacionPropedeutico.joins(:inscripcion_propedeutico => [:aspirante, :solicitud_propedeutico=>:periodo_propedeutico]).
        where("inscripciones_propedeuticos.aspirante_id = ? AND solicitudes_propedeuticos.periodo_propedeutico_id =?",  self.id,periodos_fichas.id).first

  end

  #Este metodo busca el periodo activo en el portal de NES, busca los periodos de prope pertenecientes al ciclo marcado como actual
  # busca si el estado del aspirtante es ACEPTADO O ACEPTADO CONDICIONADO, para despues filtrar el estado del prope
  # En la vista _sidebar se manda a llamar el def tiene_prope_q y se hace la comparacion para que visualice la opcion de solicitud inscripcion.
  def tiene_prope_q?
    tiene = "NINGUNO"
    ciclo=Ciclo.get_ciclo_actual
    periodos_propedeuticos = PeriodoPropedeutico.where("ciclo_id = ?",ciclo.id )
    if !periodos_propedeuticos.blank?
    periodos_propedeuticos.each do |pp|

     prope = InscripcionPropedeutico.joins(:calificaciones_propedeuticos, :solicitud_propedeutico => :periodo_propedeutico).
          where("solicitudes_propedeuticos.aspirante_id = ? AND periodos_propedeuticos.id = ?", self.id, pp.id).first
     if !prope.blank?
       if prope.estado.eql?("ACEPTADO") or prope.estado.eql?("ACEPTADO CONDICIONADO")
       if !prope.calificaciones_propedeuticos.blank?
         tiene= prope.estado
         break
       end
       end
       end
      end
    end
    tiene
  end
end
