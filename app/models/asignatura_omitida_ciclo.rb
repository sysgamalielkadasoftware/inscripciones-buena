class AsignaturaOmitidaCiclo < ActiveRecord::Base
  establish_connection :development_sec
  self.table_name= "materias_planes_omitidas_ciclos"

  has_many :asignaturas
  has_many :ciclos

end
