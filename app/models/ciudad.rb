class Ciudad < ActiveRecord::Base
  establish_connection :development_sec

  belongs_to :distrito
  belongs_to :estado
  belongs_to :municipio
  belongs_to :region

  has_one :personal_detail

  has_many :direcciones, :as => :tabla

  def self.get_nombre(ciudad_id)
    ciudad = find_by_id(ciudad_id)

    return nil if ciudad.blank?

    ciudad_nombre = ciudad.id.to_s + " | " + ciudad.nombre
    ciudad_nombre += (ciudad.municipio.blank? ? ",sin municipio" : "," + ciudad.municipio.nombre)
    ciudad_nombre += (ciudad.distrito.blank? ? ",sin distrito" : "," + ciudad.distrito.nombre)
    ciudad_nombre += (ciudad.region.blank? ? ",sin region" : "," + ciudad.region.nombre)

    return ciudad_nombre
  end

  def self.get_municipio(ciudad_id)
    ciudad = find_by_id(ciudad_id)
    return nil if ciudad.blank?
    ciudad_nombre = (ciudad.municipio.blank? ? "sin municipio" :  ciudad.municipio.nombre)
    return ciudad_nombre
  end

  def self.get_estado(ciudad_id)
    ciudad = find_by_id(ciudad_id)
    return nil if ciudad.blank?
    ciudad_nombre = (ciudad.estado.blank? ? "sin estado" :  ciudad.estado.nombre)
    return ciudad_nombre
  end

  def self.get_colonia(ciudad_id)
    ciudad = find_by_id(ciudad_id)
    return nil if ciudad.blank?
    ciudad_nombre = (ciudad.direcciones.colonia.blank? ? "sin colonia" :  ciudad.direcciones.colonia)
    return ciudad_nombre
  end
end