class SedeFicha < ActiveRecord::Base
  belongs_to :periodo_ficha

  has_many :solicitudes_fichas

  def self.get_sedes_examen_by_date
    periodo_ficha = PeriodoFicha.get_current_token_period
    puts "periodo: " + periodo_ficha.id.to_s
    sedes_fichas = SedeFicha.where("periodo_ficha_id = ? and deshabilitado = ?", periodo_ficha.id, false)
    sedes_fichas
  end
end
