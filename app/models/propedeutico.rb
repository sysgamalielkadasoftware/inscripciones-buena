class Propedeutico < ActiveRecord::Base

  establish_connection(:development_sec)
  PENDIENTE = 'PENDIENTE'
  ACEPTADO = 'ACEPTADO'
  NO_ACEPTADO = 'NO ACEPTADO'
  STATUS = [PENDIENTE, ACEPTADO, NO_ACEPTADO]

  belongs_to :alumno
  belongs_to :carrera
  belongs_to :ciclo
  belongs_to :configuracion_aspirante

  # has_many :calificaciones_prope, :dependent => :destroy
  # has_many :materias_aspirantes, :through => :calificaciones_prope

  delegate :ciclo,
           :to => :ciclo,
           :prefix => true,
           :allow_nil => true

  delegate :nombre_carrera,
           :to => :carrera,
           :prefix => true,
           :allow_nil => true

  def get_inscripcion_alumno
    errors.add(:alumno_id, "Ya cuenta con al menos una inscripción a semestre.")  unless !self.alumno.inscripciones.exists?
  end

  def self.get_all_by_carrera_and_periodo(carrera, periodo)
    select(Alumno::ORDER_FULL_NAME + ", propedeuticos.*").joins(:alumno).
        where(:propedeuticos => {:carrera_id => carrera, :ciclo_id => periodo}).
        order("full_name_as_string")
  end

 def self.get_all_by_carrera_and_periodo_and_grupo_and_tipo(carrera, periodo, grupo, tipo)
    select(Alumno::ORDER_FULL_NAME + ", propedeuticos.*").joins(:alumno).
        where(:propedeuticos => {:carrera_id => carrera, :ciclo_id => periodo, :grupo => grupo, :tipo => tipo}).
        order("full_name_as_string")
  end

  def self.get_next_numero(periodo)
    '%04d' % (maximum(:numero, :conditions => {:ciclo_id => periodo}).to_i + 1)
  end

  def is_aceptado?
    status.eql?(ACEPTADO)
  end

  def get_numero_ficha
    alumno=Alumno.find(self.alumno_id)
    ciclo=Ciclo.find(self.ciclo_id)
    inscripciones = Inscripcion.joins(:ciclo).where(:alumno_id => self.alumno_id)
    ficha = Ficha.joins(:ciclo).where(:alumno_id => self.alumno_id, :ciclo_id => self.ciclo_id).last
    inscripciones.each do |i|
      return alumno.matricula if i.ciclo.ciclo < ciclo.ciclo
    end
    return ficha.blank? ? 'sin ficha' : ficha.numero
  end

  def Propedeutico.aprobo_curso_propedeutico(alumno_id)
    prope = Propedeutico.where(alumno_id: alumno_id, ciclo_id: Ciclo.get_ciclo_actual).order(:created_at).last
    return nil if prope.blank?
    prope.is_aceptado?
  end

  def self.aprobo_prope(alumno_id)
    status = "ACEPTADO"
    last_fichas = Propedeutico.where(alumno_id: alumno_id).joins(:ciclo).order('ciclos.ciclo').group_by{|f| f.ciclo_id}.to_a.last
    if last_fichas.blank?
      return nil
    elsif last_fichas.last.select{|f| f.status == status}.present?
      return true
    else
      return false
    end
  end

  def self.guardar_propedeutico(curp,alumno_id)
    alumno = Formato.find_by_curp(curp)
    if !alumno.blank?
      configuracion = ConfiguracionAspirante.find_by_id(alumno.configuracion_aspirante_id)
      datos = Hash.new
      datos[:numero] = sprintf("%04d",String((Propedeutico.all.order("numero DESC").first).numero.to_i + 1))
      datos[:status] = "PENDIENTE"
      datos[:tipo] = configuracion.tipo
      datos[:alumno_id] = alumno_id
      datos[:ciclo_id] = configuracion.ciclo_id unless configuracion.blank?
      datos[:carrera_id] = alumno.carrera_id
      datos[:configuracion_aspirante_id] = alumno.configuracion_aspirante_id
      prope = Propedeutico.where(:alumno_id => alumno_id, :configuracion_aspirante_id => alumno.configuracion_aspirante_id ).last
      #comentario
      if prope.blank?
        prope = Propedeutico.new(datos)
        return true if (prope.save)
      else
        return true if prope.update(datos)
      end
      return false
    else
      return nil
    end
  end
end
