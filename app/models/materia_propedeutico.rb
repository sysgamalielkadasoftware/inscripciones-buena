class MateriaPropedeutico < ActiveRecord::Base
  has_many :calificaciones_propedeuticos

  belongs_to :carrera
end
