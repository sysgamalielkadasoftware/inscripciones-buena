class DatoPersonal < ActiveRecord::Base
  ESTADO_CIVIL = [
      {id: 1, nombre: 'CASADO (A)'},
      {id: 2, nombre:'DIVORCIADO (A)'},
      {id: 3, nombre:'SEPARADO (A)'},
      {id: 4, nombre:'SOLTERO (A)'},
      {id: 5, nombre:'VIUDO (A)'},
      {id: 6, nombre:'UNION LIBRE'},
      {id: 7, nombre:'MADRE SOLTERA'}
  ]

  has_many :datos_personales_lenguas_indigenas, :dependent => :destroy
  has_many :lenguas_indigenas, through: :datos_personales_lenguas_indigenas, :dependent => :destroy

  belongs_to :aspirante
  belongs_to :direccion, :dependent => :destroy
  belongs_to :dato_medico
end
