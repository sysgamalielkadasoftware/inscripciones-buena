class Address < ActiveRecord::Base
  establish_connection :development_sec
  self.table_name = 'direcciones'

  belongs_to :ciudad
  belongs_to :tabla, :polymorphic => true

  belongs_to :tabla_alumno,
             -> { where :addresses => {:tabla_type => "Alumno"}},
             :foreign_key => :tabla_id,
             :class_name => "Alumno"

  belongs_to :tabla_tutor,
             -> { where :addresses => {:tabla_type => "Tutor", :tabla_id => :foreign_key}},
             :foreign_key => :tabla_id,
             :class_name => "Tutor"

  def get_full
    direccion_completa = ""

    direccion_completa = (self.calle + " ") unless self.calle.blank?
    direccion_completa += (self.numero + ", ") unless self.numero.blank?
    direccion_completa += (self.colonia + ", ") unless self.colonia.blank?
    direccion_completa += self.ciudad.nombre + ", " unless self.ciudad.nil?
    direccion_completa += self.ciudad.estado.nombre
    direccion_completa += (", " + self.telefono) unless self.telefono.blank?

    direccion_completa
  end

  def get_short
    calle.to_s + ', No. ' + numero.to_s + ', Col: ' + colonia.to_s
  end
end