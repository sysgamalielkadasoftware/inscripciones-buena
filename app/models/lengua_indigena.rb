class LenguaIndigena < ActiveRecord::Base
  has_many :datos_personales_lenguas_indigenas
  has_many :dato_personales, through: :datos_personales_lenguas_indigenas

end
