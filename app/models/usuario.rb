class Usuario < ActiveRecord::Base
  has_one :aspirante
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :authentication_keys => [:username]


  validates :username, :uniqueness=> true, :presence=> true
  validates_format_of :username, :with=> /\A[A-Z][AEIOUX][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][MH][A-Z][BCDEFGHJKLMNÑPQRSTVWXYZ]{4}[0-9A-Z][0-9]\z/i, :multiline => true, :allow_blank => true
  
  
 
def self.validacion_pass(password)
  if password.match( /(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&#.$($)$-$_])[A-Za-z\d$@$!%*?&#.$($)$-$_]{8,255}$/).blank?
    return false
  else
    return true
  end
end


  def self.get_birthdate(user_id)
    user = Usuario.find(user_id)
    puts "birthday: " + user.username[4, 2].to_s
    year = user.username[4, 2].to_i + 1900
    if year < 1909
      year = year + 100
    end

    month = user.username[6,2].to_i
    day = user.username[8,2].to_i

    puts "Mes: " + month.to_s
    puts "Año: " + year.to_s
    puts "Dia: " + day.to_s

    birthday = DateTime.new(year, month, day)
    birthday
  end

  def self.get_age(birthdate)
    age = ((Time.zone.now - birthdate.to_time) / 1.year.seconds).floor
    age
  end
end
