class DatoPersonalLenguaIndigena < ActiveRecord::Base
  belongs_to :dato_personal
  belongs_to :lengua_indigena
end
