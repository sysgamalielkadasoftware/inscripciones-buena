class Token < ActiveRecord::Base
  establish_connection :development_sec
  self.table_name = 'fichas'

  PENDIENTE = 'PENDIENTE'
  ACEPTADO = 'ACEPTADO'
  NO_ACEPTADO = 'NO ACEPTADO'
  STATUS = [PENDIENTE, ACEPTADO, NO_ACEPTADO]

  belongs_to :alumno
  belongs_to :carrera
  belongs_to :ciclo
  belongs_to :configuracion_aspirante

  # has_many :calificaciones_fichas, :dependent => :destroy
  # has_many :materia_aspirantes, :through => :calificaciones_fichas

  #before_validation :get_inscripcion_alumno
  #before_validation :get_inscripcion_prope

  delegate :ciclo,
           :to => :ciclo,
           :prefix => true,
           :allow_nil => true

  delegate :nombre_carrera,
           :to => :carrera,
           :prefix => true,
           :allow_nil => true

  def get_inscripcion_alumno
    errors.add(self.alumno.curp + " | " + self.alumno.full_name, ", ya cuenta con al menos una inscripción a semestre.")  unless !self.alumno.inscripciones.exists?
  end

  def self.aprobo_seleccion(alumno_id)
    status = "ACEPTADO"
    last_fichas = Token.where(alumno_id: alumno_id).joins(:ciclo).order('ciclos.ciclo').group_by{|f| f.ciclo_id}.to_a.last
    if last_fichas.blank?
      return nil
    elsif last_fichas.last.select{|f| f.status == status}.present?
      return true
    else
      return false
    end
  end

  def get_inscripcion_prope
    errors.add(self.alumno.curp + " | " + self.alumno.full_name, ", ya cuenta con una inscripción a propedéutico en este periodo.")  unless !self.alumno.propedeuticos.exists?(:ciclo_id => self.ciclo_id)
  end

  def self.get_all_by_periodo_and_tipo(periodo, tipo)
    select(Alumno::ORDER_FULL_NAME + ", fichas.*").joins(:alumno).
        where(:tokens => {:ciclo_id => periodo, :tipo => tipo}).
        order("full_name_as_string")
  end

  def self.get_all_by_carrera_and_periodo_and_tipo(carrera, periodo, tipo)
    select(Alumno::ORDER_FULL_NAME + ", fichas.*").joins(:alumno).
        where(:tokens => {:carrera_id => carrera, :ciclo_id => periodo, :tipo => tipo}).
        order("full_name_as_string")
  end

  def self.get_all_by_carrera_and_periodo_order_by_lugar(carrera, periodo,configuracion_id)
    if configuracion_id.blank?
      select(Alumno::ORDER_FULL_NAME + ", fichas.*").joins(:alumno,:configuracion_aspirante).
          where(:tokens => {:carrera_id => carrera, :ciclo_id => periodo}).
          order("configuraciones_aspirantes.fecha_examen,configuraciones_aspirantes.lugar,full_name_as_string")
    else
      select(Alumno::ORDER_FULL_NAME + ", fichas.*").joins(:alumno,:configuracion_aspirante).
          where(:tokens => {:carrera_id => carrera, :ciclo_id => periodo, :configuracion_aspirante_id => configuracion_id}).
          order("configuraciones_aspirantes.fecha_examen,configuraciones_aspirantes.lugar, full_name_as_string")
    end
  end

  #
  # Obtiene las fichas de todos los alumnos de acuerdo a la carrera y periodo proporcionados, ordenados alfabeticamente
  # full_name_as_string constante contiene el nombre completo del alumno desde la base de datos
  #
  def self.get_all_by_carrera_and_periodo(carrera, periodo)
    select(Alumno::ORDER_FULL_NAME + ", fichas.*").joins(:alumno).
        where(:tokens => {:carrera_id => carrera, :ciclo_id => periodo}).
        order("full_name_as_string")
  end

  def self.get_all_by_carrera_and_periodo_and_status_and_proceso(carrera, periodo, status, tipo_proceso)
    if tipo_proceso == 'TODOS'
      select(Alumno::ORDER_FULL_NAME + ", fichas.*").joins(:alumno).
          where(:tokens => {:carrera_id => carrera, :ciclo_id => periodo , :status => status}).
          order("full_name_as_string")
    else
      select(Alumno::ORDER_FULL_NAME + ", fichas.*").joins(:alumno).
          where(:tokens => {:carrera_id => carrera, :ciclo_id => periodo , :status => status, :tipo => tipo_proceso}).
          order("full_name_as_string")
    end

  end

  def self.get_sexo_and_total_alumnos_by_carrera_id_and_periodo_id(carrera,periodo)
    select("distinct datos_personales.sexo , count(datos_personales.alumno_id) as total").
        joins(:alumno => :dato_personal).
        where("fichas.carrera_id = ? and fichas.ciclo_id = ?",carrera, periodo).group("datos_personales.sexo")
  end

  def self.get_number_available(periodo)
    numero =Token.where("ciclo_id = ? and CAST(numero as integer) not in (select CAST(numero as integer)-1 from fichas where ciclo_id = ?)", periodo, periodo).order(:numero).first.numero.to_i + 1
    '%04d' % (numero.nil? ? 1 : numero)
  end

  def self.guardar_ficha(alumno_id, config_id, carrera_id)
    guardada = nil
    if !config_id.blank?
      configuracion = ConfiguracionAspirante.find_by_id(config_id)
      ficha = Token.where(:alumno_id => alumno_id, :configuracion_aspirante_id => config_id, :carrera => carrera_id ).last
      #comentario
      guardada = false
      if ficha.blank?
        unless configuracion.blank?
          datos = Hash.new
          datos[:numero] = Token.get_number_available(configuracion.ciclo)  #sprintf("%04d",String((Ficha.all.order("numero DESC").first).numero.to_i + 1))
          datos[:status] = "PENDIENTE"
          datos[:fecha_solicitud] = String(Time.now).split(" ").first
          datos[:tipo] = configuracion.tipo
          datos[:lugar] = configuracion.lugar
          datos[:alumno_id] = alumno_id
          datos[:ciclo_id] = configuracion.ciclo_id
          datos[:carrera_id] = carrera_id
          datos[:configuracion_aspirante_id] = config_id
          ficha = Token.new(datos)
          guardada = true if (ficha.save)
        end
      else
        guardada = true
      end
    end
    return guardada
  end

  def self.find_ficha_by_alumno_id_and_config_and_carrera(alumno_id, config_id, carrera_id)
    ficha = Token.where(:alumno_id => alumno_id, :configuracion_aspirante_id => config_id,  :carrera => carrera_id ).last
  end

  def get_data(tramite, alumno_id, campus_id)
    t = tramite
    ficha = Hash.new
    ficha[:tramite_id] = t.id
    ficha[:alumno_id] = alumno_id
    ficha[:campus_id] = campus_id
    ficha[:configuracion_aspirante] = ConfiguracionAspirante.find_by_id(t[:configuracion_aspirante_id])
    ficha[:tipo] = ficha[:configuracion_aspirante].tipo
    ficha[:carrera] = "N/D"
    ficha[:carrera] = Carrera.find_by_id(t[:carrera_id]).nombre_carrera.gsub(/[ÁÉÍÓÚ]/, 'Á' => 'á', 'É' => 'é', 'Í' => 'í' , 'Ó' => 'ó', 'Ú' => 'ú')
    ficha[:numero] = "N/D"
    ficha[:guardado] = nil
    ficha[:debit] = t.debit
    ficha[:debit_pagado] = t[:debit_pagado]
    if ficha[:debit_pagado]
      ficha[:guardado] = Token::guardar_ficha(alumno_id, t[:configuracion_aspirante_id], t[:carrera_id])
      if ficha[:guardado] == true
        f = Token.where(:alumno_id => alumno_id, :configuracion_aspirante_id => t[:configuracion_aspirante_id]).first  #verificar ciclo
        #f = Ficha.where(:alumno_id => alumno_id, :configuracion_aspirante_id => t[:configuracion_aspirante_id], :ciclo_id => @ciclo_id).first
        ficha[:id] = f[:id]
        ficha[:numero] = f[:numero]
        ficha[:notice] = "Solicitud de Ficha completada satisfactoriamente."
      else
        ficha[:notice] = "Error, la ficha no fue registrada. "
      end
    else
      ficha[:notice] = "El adeudo de esta ficha aún está activo. "
    end
  end
end