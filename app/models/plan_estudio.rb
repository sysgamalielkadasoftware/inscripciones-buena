class PlanEstudio < ActiveRecord::Base
  establish_connection :development_sec

  belongs_to :carrera
  belongs_to :semestr,
             :class_name => "Semestr",
             :foreign_key => 'semestre_id'
  belongs_to :ciclo
  has_many :asignaturas,
           :dependent => :destroy

  delegate :nombre_carrera, :to => :carrera, :prefix => true, :allow_nil => true
  delegate :ciclo, :to => :ciclo, :prefix => true, :allow_nil => true

  public
  def self.get_plan_estudio_by_curso_id(curso_id)
    joins(:materias_planes => :cursos).where(:cursos => {:id => curso_id}).first
  end

  def self.get_nombre_by_curso_id(curso_id)
    plan_estudio = PlanEstudio.get_plan_estudio_by_curso_id(curso_id)

    return !plan_estudio.nil? ? plan_estudio.clave_plan : "SIN PLAN DE ESTUDIOS"
  end

  def self.get_other_planes_by_carrera_and_clave_plan(carrera_id,clave_plan)
    select("distinct clave_plan").where("clave_plan !=? and carrera_id = ?",clave_plan,carrera_id)
  end

  def self.get_all_by_carrera_id(carrera_id)
    select("distinct clave_plan, numero_plan").where(" carrera_id = ?",carrera_id)
  end

  def self.get_creditos_by_plan(clave_plan)
    select("sum (materias.creditos) as creditos").
        joins(:materias_planes => :materia).where(:clave_plan => clave_plan).first
  end

  def get_inicio_vigencia
    self.ciclo.nil? ? "No tiene un periodo de inicio de vigencia." : self.ciclo_ciclo
  end
end