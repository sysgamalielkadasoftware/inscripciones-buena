class ExamenProfesor < ActiveRecord::Base
  establish_connection :development_sec

  TITULAR = 1
  CO_TITULAR = 2

  belongs_to :examen
  belongs_to :profesor

  def get_nombre_for_tipo
    case(self.tipo)
    when TITULAR
      'TITULAR'
    when CO_TITULAR
      'CO-TITULAR'
    else
      'SIN TIPO'
    end
  end

  def is_titular?
    self.tipo.eql?(TITULAR)
  end

  def is_co_titular?
    self.tipo.eql?(CO_TITULAR)
  end
end