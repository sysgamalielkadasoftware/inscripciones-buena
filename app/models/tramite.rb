class Tramite < ActiveRecord::Base
  establish_connection :development_sec

  belongs_to :alumno
  belongs_to :debit

  has_one :ciclo

  ENVIADO = 'ENVIADO'
  FINALIZADO = 'FINALIZADO'
  FICHA = "FICHA"
  PROPE = "PROPEDEUTICO"
  INSCRIPCION = "INSCRIPCION"
  REINSCRIPCION = "REINSCRIPCION"
  TRAMITES = {"FICHA" => 0, "PROPEDEUTICO" => 1, 'PROPEDEUTICO_LARGO' => 1, "INSCRIPCION" => 2, "REINSCRIPCION" => 3}
  EXAMEN_SELECCION = "EXAMEN DE SELECCION"

  #Se agrega restricción para evitar que se creen registros duplicados tomando como llave al alumno_id, el proceso y ciclo_id
  validates_uniqueness_of :alumno_id, scope: [:proceso, :ciclo_id]

  def folio(permit_blank=false)
    univ = Universidad.first.siglas
    campus = Campus.first.numero
    num_folio = self.read_attribute(:folio)
    if num_folio.blank?
      resultado = permit_blank ? nil : "Inválido"
    else
      resultado = univ.to_s + "-" + "%04d" %num_folio
    end
    resultado
  end

  def get_debit_description()
    datos_tramite = nil
    folio = self.folio(true)
    description = ""
    description = " T:#{folio} " if folio.present?
    case self.proceso
    when FICHA ||PROPE
      datos_tramite = ConfiguracionAspirante.find_by_id(self.configuracion_aspirante_id) unless  self.configuracion_aspirante_id.blank?
      if !datos_tramite.blank?
        description += datos_tramite.ciclo.ciclo + " "
        description += ApplicationController.helpers.format_date(datos_tramite[:fecha_examen].to_s)
      end
    when INSCRIPCION || REINSCRIPCION
      datos_tramite =  Ciclo.get_ciclo_actual
      description += datos_tramite[:ciclo] unless datos_tramite.blank?
    else
      nil
    end
    description +=  " " + self.proceso + " Portal en línea."
    description
  end

  def Tramite.arrange_asignaturas_for_update_reinscripcion(reinscripcion)
    materias = Array.new
    User::send_to_logger_info(["", "Arrange_for_reinscripcion: ", reinscripcion])
    if !reinscripcion.blank?
      reinscripcion.each do |index, data|
        unless data["asignaturas_ids"]["general"].blank?
          data["asignaturas_ids"]["general"].each do |k, asignatura|
            materia = Hash.new
            materia[:semestre_clave] = Integer(data["clave"])
            materia[:asignatura_id] = Integer(asignatura['id'])
            materia[:optativa] = false
            materia[:status] = asignatura["status"]
            materias.push(materia.clone)
          end
        end
        unless data["asignaturas_ids"]["optativas"].blank?
          data["asignaturas_ids"]["optativas"].each do |k, asignatura|
            materia = Hash.new
            materia[:semestre_clave] = Integer(data["clave"])
            materia[:asignatura_id] = Integer(asignatura['id'])
            materia[:status] = asignatura["status"]
            materia[:optativa] = true
            materias.push(materia.clone)
          end
        end
      end
    end
    materias
  end

  def Tramite.find_tramite_for_alumno(alumno_id, tramite_palabra, carrera_id, configuracion_aspirante_id = nil)
    tramite = Tramite.where(:alumno_id => alumno_id, :carrera_id => carrera_id)
    if (tramite_palabra == Tramite::FICHA || tramite_palabra == Tramite::PROPE)
      if !configuracion_aspirante_id.blank?
        tramite.where(:configuracion_aspirante_id => configuracion_aspirante_id).first
      else
        tramite = Tramite.none
      end
    else
      tramite = tramite.first
    end
    tramite
  end

  def update_tramite(debit_id, reinscripcion= nil)
    raise "Error con la configuración del aspirante" if (self.proceso == Tramite::FICHA || self.proceso == Tramite::PROPE) and self.configuracion_aspirante_id.blank?
    alumno = self.alumno
    raise "No se encuentra al alumno" if alumno.blank?
    ciclo = Ciclo.get_ciclo_actual
    clave = 1
    semestre = Semestr.where(:clave => clave).last
    semestre_id = semestre.id unless semestre.blank?
    parametros = Hash.new
    materias = Array.new
    if (self.proceso == Tramite::FICHA || self.proceso == Tramite::PROPE) and !self.configuracion_aspirante_id.blank?
      parametros[:ciclo_id] = ConfiguracionAspirante.find_by_id(self.configuracion_aspirante_id).ciclo_id
    else
      materias = arrange_asignaturas_for_update_reinscripcion(reinscripcion)
      parametros[:ciclo_id] = ciclo.id unless ciclo.blank?
      parametros[:status] = reinscripcion.first.last['status']
    end
    parametros[:semestre_id] = semestre_id
    parametros[:proceso] = self.proceso
    parametros[:carrera_id] = self.carrera_id
    parametros[:debit_id] = debit_id
    parametros[:configuracion_aspirante_id] = self.configuracion_aspirante_id
    parametros[:debits]  = !(Debit.consultar_deudas_del_alumno(self.alumno_id).blank?)
    parametros[:otros_adeudos] = alumno.tiene_otros_adeudos.blank? ? false : true
    if self.update(parametros)
      materias.each do |m|
        self.add_asignatura(m)
      end
    end
    self
  end

  def Tramite.actualizar_tramite(alumno_id, tramite, carrera_id, debit_id, configuracion_aspirante_id = nil, reinscripcion=nil, nuevo)
    raise "No se encuentra la carrera " if carrera_id.blank?
    raise "Error con la configuración del aspirante" if (tramite == Tramite::FICHA || tramite == Tramite::PROPE) and configuracion_aspirante_id.blank?
    alumno = Alumno.find_by_id(alumno_id)
    raise "No se encuentra al alumno" if alumno.blank? and !nuevo
    ciclo = Ciclo.get_ciclo_actual
    actualizar_tramite = Tramite.where(:alumno_id => alumno_id, :carrera_id => carrera_id, :proceso => tramite)
    clave = 1
    semestre = Semestr.where(:clave => clave).last
    semestre_id = semestre.id unless semestre.blank?
    parametros = Hash.new
    materias = Array.new
    if (tramite == Tramite::FICHA || tramite == Tramite::PROPE) and !configuracion_aspirante_id.blank?
      actualizar_tramite = actualizar_tramite.where(:configuracion_aspirante_id => configuracion_aspirante_id).first

      parametros[:ciclo_id] = ConfiguracionAspirante.find_by_id(configuracion_aspirante_id).ciclo_id
    else
      materias = arrange_asignaturas_for_update_reinscripcion(reinscripcion)
      if ciclo.present?
        actualizar_tramite = actualizar_tramite.where(ciclo_id: ciclo.id).first
        parametros[:ciclo_id] = ciclo.id
      end
      parametros[:status] = reinscripcion.blank? ? Inscripcion::REGULAR : reinscripcion.first.last['status']
    end
    parametros[:semestre_id] = semestre_id
    parametros[:proceso] = tramite
    parametros[:carrera_id] = carrera_id
    parametros[:debit_id] = debit_id
    parametros[:configuracion_aspirante_id] = configuracion_aspirante_id
    parametros[:debits]  = !(Debit.consultar_deudas_del_alumno(alumno_id).blank?)
    parametros[:otros_adeudos] = nuevo ? false : (alumno.tiene_otros_adeudos.blank? ? false : true)
    if actualizar_tramite.blank?
      parametros[:alumno_id] = alumno_id
      actualizar_tramite = Tramite.new(parametros)
      saved = actualizar_tramite.save
    else
      saved = actualizar_tramite.update_attributes(parametros)
    end
    if saved
      materias.each do |m|
        actualizar_tramite.add_asignatura(m)
      end
    end
    if actualizar_tramite.debit.present?
      description = Tramite.find_by_id(actualizar_tramite.id).get_debit_description()
      updated = actualizar_tramite.debit.update_attribute(:description, description)
    end
    actualizar_tramite
  end

  def Tramite.verificar_tramite(curp, proceso, id=nil)
    return nil if curp.blank? && id.blank?
    alumno_id = nil

    if id.blank?
      alumno = Alumno.find_by_curp(curp)
      alumno_id = alumno.id  unless alumno.blank?
    else
      alumno_id = id
    end
    verificar = Tramite.tramite(alumno_id,proceso) unless alumno_id.blank?
    verificar
  end


  def Tramite.current_ciclo_solicitud
    c = Ciclo.find_by_actual(true)
    return Ciclo.find_by_actual(true)
  end

  private
  def Tramite.tramite(alumno_id,proceso)
    resultado  = nil
    tramite_actual = nil
    @ciclo_nuevo = Tramite.current_ciclo_solicitud().id
    tramite_actual = Tramite.where(alumno_id: alumno_id, proceso: proceso, ciclo_id: @ciclo_nuevo).last
    return nil if alumno_id.blank?
    if tramite_actual.blank? #si no se ha iniciado el trámite, verificar si puede iniciarlo. Si puede retornar false, si no retornar nil
      case (proceso)
      when Tramite::FICHA
        resultado = false
      when Tramite::PROPE
        resultado = false if Token.aprobo_seleccion(alumno_id)
      when Tramite::INSCRIPCION
        # resultado =  Propedeutico.aprobo_curso_propedeutico(alumno_id) ?  false : nil
      when Tramite::REINSCRIPCION
        resultado = (@ciclo_nuevo)
      end
    else
      diferencia = TRAMITES[proceso] - TRAMITES[tramite_actual.proceso]
      if proceso.eql?(REINSCRIPCION)
        resultado  = (tramite_actual.ciclo_id == @ciclo_nuevo)
      elsif diferencia >= 0 && diferencia <= 1
        resultado = (TRAMITES[proceso] == TRAMITES[tramite_actual.proceso])
      end
    end

    return resultado
  end

  def self.checar_prope(aspirante_id)
    current_ciclo = Ciclo.get_actual
    ciclo = Ciclo.find_by_inicio_and_fin_and_tipo(current_ciclo.inicio, current_ciclo.fin, 'A')
    inscripcion = InscripcionPropedeutico.joins(:solicitud_propedeutico => :periodo_propedeutico).where("solicitudes_propedeuticos.aspirante_id = ? AND periodos_propedeuticos.ciclo_id = ?", aspirante_id, ciclo.id).order("periodos_propedeuticos.fecha_inicio, periodos_propedeuticos.fecha_fin")
    mensaje = ""
    if inscripcion.blank?
      mensaje = "El aspirante no ha cursardo el propedéutico en ningún periodo escolar."
    else
      if inscripcion.first.estado != InscripcionPropedeutico::ACEPTADO
        if inscripcion.first.estado == InscripcionPropedeutico::PENDIENTE
          mensaje = "Aún no ha sido aceptado, su estado es: #{inscripcion.first.estado}"
        end
        if inscripcion.first.estado == InscripcionPropedeutico::NO_ACEPTADO
            mensaje = "Su estado es: #{inscripcion.first.estado}. Para cualquier aclaración acudir al Departamento de Servicios Escolares."
        end
        if inscripcion.first.estado == InscripcionPropedeutico::PASAR_ESCOLARES
          mensaje = "Su estado está Pendiente de Aprobación, favor de acudir al Departamento de Servicios Escolares."
        end
      end
    end
    mensaje
  end
end
