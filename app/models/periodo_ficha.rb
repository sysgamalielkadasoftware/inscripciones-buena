class PeriodoFicha < ActiveRecord::Base
  CORTO = 'CORTO'
  LARGO = 'LARGO'

  has_many :sedes_fichas, :dependent => :destroy
  belongs_to :ciclo

  def self.is_period_of_application_for_tokens?
    activo = false
    periodos_fichas = PeriodoFicha.where("actual = ?", true)
    dia_actual = Time.now
    periodos_fichas.each do |periodo_ficha|
      # puts "dia actual:" + dia_actual.strftime("%Y-%m-%d").to_s
      # puts "fecha_inicio: " + periodo_ficha.fecha_inicio.to_s
      # puts "fecha_inicio: " + periodo_ficha.fecha_fin.to_s
      if dia_actual.strftime("%Y-%m-%d") >= periodo_ficha.fecha_inicio and dia_actual.strftime("%Y-%m-%d") <= periodo_ficha.fecha_fin and periodo_ficha.actual
        activo = true
        # puts activo.to_s
        break
      else
        activo = false
        # puts activo.to_s
      end
    end
    return activo
  end

  def self.get_current_token_period
    activo = false
    periodo_activo = PeriodoFicha.new
    periodos_fichas = PeriodoFicha.where("actual = ?", true)
    dia_actual = Time.now
    periodos_fichas.each do |periodo_ficha|
      if dia_actual.strftime("%Y-%m-%d") >= periodo_ficha.fecha_inicio and dia_actual.strftime("%Y-%m-%d") <= periodo_ficha.fecha_fin and periodo_ficha.actual
        activo = true
        periodo_activo = periodo_ficha
      else
        activo = false
        puts activo.to_s
      end
    end
    periodo_activo
  end
end
