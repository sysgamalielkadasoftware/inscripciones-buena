class HomeController < ApplicationController
  before_action :require_user

  def index
    @dato_prope=""
    @anuncio = Anuncio.all
    @aspirante = Aspirante.find_by_curp(current_usuario.username.upcase)
    @aspirante_dato = Aspirante.where('upper(curp) = ?', current_usuario.username.upcase).first
    #Guarda los datos del aspirante que inicia sesión tanto de ficha, si existe, como de prope si existe
    unless(@aspirante_dato.blank?)
    @dato_ficha = Ficha.where('aspirante_id = ?', @aspirante_dato.id).first
    periodos_fichas = PeriodoPropedeutico.where("actual = ?", true).first
    unless periodos_fichas.blank?
    @dato_prope = InscripcionPropedeutico.joins(:solicitud_propedeutico=>:periodo_propedeutico).
        where("inscripciones_propedeuticos.aspirante_id = ? AND solicitudes_propedeuticos.periodo_propedeutico_id =?", @aspirante_dato.id,periodos_fichas.id).first
    end
    #Si los datos existen  guarda el estado que tengan, sino, lo deja como NINGUNO, para hacer la comparación de los
    # al momento de mostrar el menú
    if @dato_ficha.blank?
      @estado='NINGUNO'
    else
      @estado=@dato_ficha.estado
    end
    if @dato_prope.blank?
      @estadoprop='NINGUNO'
    else
      @estadoprop=@dato_prope.estado
    end
    end
    unless @aspirante.blank?
      aspirante = Aspirante.find(@aspirante.id)
      aspirante.update(:usuario_id => current_usuario.id)
      @documentos_fichas = DocumentoFicha.select(:id,:nombre_documento).all
      # Se agrega condición para validar si ya existe el registro del documento asociado al aspirante, ya no se cree un nuevo registro.
      # Se realiza una búsqueda por todos los documentos registrados para determinar cuál ya existe y omitir su registro.
      @documentos_fichas.each do |doc|
        aspirante_documento = AspiranteDocumento.where(aspirante_id: @aspirante.id, documento_ficha_id: doc.id)
        # Se cambia forma de realizar el if para crear el documento en caso de no existir
        if aspirante_documento.blank?
          @aspirante_documentos = AspiranteDocumento.find_or_create_by!(aspirante_id: @aspirante.id, documento_ficha_id: doc.id)
        end
      end
    end
  end

end