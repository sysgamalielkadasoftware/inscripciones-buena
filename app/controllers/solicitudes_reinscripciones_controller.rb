require 'consulta/tutor_lib'
class SolicitudesReinscripcionesController < ApplicationController
  before_action :require_user
  include Consulta::TutorLib
  include Reinscripciones::Historial
  include Reinscripciones::Veranos
  before_action :require_user
  def index
    @anuncio = Anuncio.all
    alumno = Alumno.find_by_curp(current_usuario.username.upcase)
    ciclo = Ciclo.get_actual
    solicitud_reinscripcion = Tramite.find_by_alumno_id_and_ciclo_id(alumno.id, ciclo.id)
    unless solicitud_reinscripcion.blank?
      redirect_to solicitud_reinscripcion_path(solicitud_reinscripcion.id)
    else
      redirect_to new_solicitud_reinscripcion_path
    end
  end

  def new
    @tramite  =Tramite.new
    @campus=Campus.first
    @reinscripcion_prepayment = Concept.where("description like ? and status = ?", "%REINSCRIPCI%", Concept::ACTIVE).first
    @periodo_inscripcion = ConfiguracionAspirante.exist_period_inscription_reinscription?
    alumno = Alumno.find_by_curp(current_usuario.username.upcase)
    @first_inscription = get_first_inscription(alumno)
    historico = historial_inscripciones(alumno)
    periodo = Ciclo.get_actual
    @reinscripcion = Hash.new
    @formato = get_information_tutor(alumno.id)
    ultima_inscripcion = Inscripcion.joins(:ciclo).where("inscripciones.alumno_id = ? AND (inscripciones.status = 'REGULAR' OR inscripciones.status = 'IRREGULAR' OR inscripciones.status = 'REPETIDOR') AND (ciclos.tipo = 'A' OR ciclos.tipo = 'B') AND ciclos.id != ?", alumno.id, periodo.id).order("ciclos.ciclo desc").first
    beca_anterior = ultima_inscripcion.beca.to_s unless (ultima_inscripcion.nil? or ultima_inscripcion.beca.nil?)
    ciclo_anterior = Ciclo.find(ultima_inscripcion.ciclo_id) unless (ultima_inscripcion.blank? or ultima_inscripcion.ciclo_id.blank?)
    @debits = Debit.where("alumno_id = ? AND status = 'ALIVE'", alumno.id)
    @language_debit = Debit.where("alumno_id = ? AND description LIKE ? AND status = 'ALIVE'", alumno.id, 'REPETICI%').first
    fecha = '2016-2017A'

    @descuento = alumno.get_beca(periodo.id)
    errors = Array.new
    errors.push("No existe un periodo actual en la configuración.") if periodo.nil?
    # errors.push("El periodo de reinscripción debe ser del tipo A ó B.") if !periodo.nil? and periodo.is_tipo_v?
    errors.push("El alumno solicitado no existe en el sistema.") if alumno.nil?
    errors += alumno.errors.full_messages if !alumno.nil? and !alumno.valid?

    last_inscripcion = Inscripcion.get_last_by_alumno(alumno)

    errors.push("El alumno NO tiene inscripciones anteriores.") if !alumno.nil? and last_inscripcion.nil?
    errors.push("El alumno ya es EGRESADO.") if !alumno.nil? and alumno.is_egresado?

    @alumno_id = alumno
    @reinscripcion[:errors] = errors

    direccion = Address.find_by_tabla_type_and_tabla_id('Alumno', alumno)
    @datos_personales = Hash.new
    unless direccion.blank?
      if direccion.ciudad.blank?
        @datos_personales[:ciudad] = 'S/C'
      else
        @datos_personales[:ciudad] = direccion.ciudad.nombre + ", " + direccion.ciudad.estado.nombre
      end
      @datos_personales[:domicilio] = "C. " + direccion.get_short
      @datos_personales[:telefono] = direccion.telefono
    else
      @datos_personales[:ciudad] = 'Cidad No Encontrado'
      @datos_personales[:domicilio] = 'Domicilio No Encontrado'
      @datos_personales[:telefono] = 'S/T'
    end

    unless errors.any?
      clave_plan = get_plan_clave(:alumno_id => alumno)
      plan_estudio = get_plan_de_estudio(last_inscripcion.carrera, clave_plan, periodo)
      historial = get_calificacion_plan(last_inscripcion.carrera, alumno, plan_estudio)
      @historial = historial
      @reinscripcion[:nombre] = alumno.full_name
      @reinscripcion[:correo_electronico] = alumno.correo_electronico
      @reinscripcion[:matricula] = alumno.matricula
      @reinscripcion[:alumno_id] = alumno.id
      @reinscripcion[:tipo_alumno] = alumno.tipo
      @reinscripcion[:status] = alumno.tipo
      @reinscripcion[:grupo_anterior] = last_inscripcion.grupo_nombre
      @reinscripcion[:carrera] = last_inscripcion.carrera_nombre_carrera
      @reinscripcion[:carrera_id] = last_inscripcion.carrera_id
      @reinscripcion[:periodo] = periodo.ciclo
      @reinscripcion[:periodo_id] = periodo.id
      @reinscripcion[:beca_anterior]  =beca_anterior
      @reinscripcion[:beca_anterior_ciclo]  = beca_anterior.to_s + " (#{ciclo_anterior.ciclo})"

      if periodo.is_tipo_v?
        @reinscripcion[:datos] = evaluate_to_veranos(alumno, periodo)
      else
        if historico[0][:periodo].to_s >= fecha.to_s.strip
          @reinscripcion[:datos] = Reinscripciones::Historial::check_for_next2016(historial, alumno, periodo)
        else
          @reinscripcion[:datos] = Reinscripciones::Historial::check_for_next(historial, alumno, periodo)
        end
      end

      if @reinscripcion[:datos][:message].nil?
        @reinscripcion = Reinscripciones::Historial::check_for_especialidades(@reinscripcion)
        if @reinscripcion[:has_especialidades]
          @reinscripcion[:datos][:info_all].each do |semestre|
            if semestre[:clave] > semestre[:especialidad][:start]
              optativas = Array.new
              semestre[:especialidad][:optativas].each do |opt|
                optativa = Hash.new
                optativa[:ids_seriadas] = Asignatura.find_by_id(opt.id).get_ids_seriadas
                optativa = Reinscripciones::Historial::set_info_to_asignatura(optativa,   true, "POR CURSAR", Reinscripciones::Historial::get_status_asignatura(alumno, opt.id))
                optativa = Reinscripciones::Historial::verifica_seriacion(optativa,alumno)
                optativa[:id] = opt.id
                optativa[:clave] = opt.clave
                optativa[:nombre] = opt.nombre
                optativas << optativa
              end
              semestre[:especialidad][:optativas] = optativas
            end
          end
        end
      else
        @reinscripcion[:errors].push(@reinscripcion[:datos][:message])
      end
    else
      datos = Hash.new
      datos[:can] = false
      datos[:message] = errors
      @reinscripcion[:datos] = datos
    end
  end

  def create
    begin
    alumno_id = params[:tramite][:alumno_id]
    carrera_id = params[:tramite][:carrera_id]
    ciclo_id = params[:tramite][:ciclo_id]
    semestre_id = params[:tramite][:semestre_id]
    proceso = params[:tramite][:proceso]
    estado_tramite = params[:tramite][:estado_proceso]
    numero_referencia = params[:referencia_pago][:reinscripcion] unless params[:referencia_pago][:reinscripcion].blank?
    numero_referencia_ingles = params[:referencia_pago][:ingles] unless params[:referencia_pago][:ingles].blank?
    concept_id = params[:concept][:id]
    unless numero_referencia.blank?
      @preregistro = PreregistroPagoNes.new(alumno_id: alumno_id, concept_id: concept_id, numero_referencia: numero_referencia, ciclo_id: ciclo_id, estado: 'POR VERIFICAR')
      @preregistro.save
    end

    # @reinscripcion_prepayment = Concept.where("description like ? and status = ?", "%REINSCRIPCI%", Concept::ACTIVE).first
    english_concept = Concept.where("description like ? and status = ?", "%REPETICI%", Concept::ACTIVE).first
    unless numero_referencia_ingles.blank?
      preregistro_ingles = PreregistroPagoNes.new(alumno_id: alumno_id, concept_id: english_concept.id, numero_referencia: numero_referencia_ingles, ciclo_id: ciclo_id, estado: 'POR VERIFICAR')
      preregistro_ingles.save
    end
    #Se agrega como parámetro al ciclo_id para validar que no haya duplicados al momento de generarse el registro en la bd
    generar_adeudo = Debit.generar_adeudo(concept_id, alumno_id, '', ciclo_id)

    tramite = Tramite.new
    tramite.alumno_id = alumno_id
    tramite.proceso = proceso
    tramite.ciclo_id = ciclo_id
    tramite.carrera_id = carrera_id
    tramite.semestre_id = semestre_id
    tramite.debit_id = generar_adeudo
    tramite.debit_pagado = false
    tramite.estado_proceso = estado_tramite
    tramite.save

    redirect_to solicitud_reinscripcion_path(tramite)

    #Se agrega excepción para considerar el caso de registros duplicados, y en caso de que haya algún error
    rescue ActiveRecord::RecordNotUnique => e
    rescue Exception => error
      #En caso de haber un error, se indica al usuario y se redirige a la vista de reinscripción
      flash[:notice] = "Ocurrió un error al momento de registrar la Reinscripción"
      redirect_to new_solicitud_reinscripcion_path
  end
  end

  def show
    @solicitud_reinscripcion = Tramite.find(params[:id])
    #Se agrega la validación para saber si el usuario que esta intentando acceder a la página con el id proporcionado
    # es el mismo que esta logueado usando el user name, que es la curp y la curp registrada en los datos del alumno
    # Se convierte a mayúsculas tanto el valor de la curp como el usernam, en caso de haberse guardado en minúsculas.
    if(@solicitud_reinscripcion.alumno.curp.upcase == current_usuario.username.upcase)
      @campus=Campus.first
      @debits = Debit.where("alumno_id = ? AND status = 'ALIVE'", @solicitud_reinscripcion.alumno_id)
      ultima_inscripcion = Inscripcion.
        joins(:ciclo).
        where("inscripciones.alumno_id = ? AND (inscripciones.status = 'REGULAR' OR inscripciones.status = 'IRREGULAR' OR inscripciones.status = 'REPETIDOR') AND (ciclos.tipo = 'A' OR ciclos.tipo = 'B') AND ciclos.id != ?", @solicitud_reinscripcion.alumno_id, @solicitud_reinscripcion.ciclo_id).
        order("ciclos.ciclo desc").first
      @beca_anterior = (ultima_inscripcion.beca.to_s) unless ultima_inscripcion.beca.nil?
      @grupo_anterior = ultima_inscripcion.grupo.nombre
      @inscripcion = Inscripcion.where("ciclo_id = ? AND alumno_id = ? AND carrera_id = ?", @solicitud_reinscripcion.ciclo_id, @solicitud_reinscripcion.alumno_id, @solicitud_reinscripcion.carrera_id)
      concepto_reinscripcion = Concept.where("description like ? and status = ?", "%REINSCRIPCI%", Concept::ACTIVE).first
      @grupo_actual = Inscripcion.where(alumno_id: @solicitud_reinscripcion.alumno_id, semestre_id: @solicitud_reinscripcion.semestre_id, ciclo_id: @solicitud_reinscripcion.ciclo_id, carrera_id: @solicitud_reinscripcion.carrera_id).first
      @preregistro_pago = PreregistroPagoNes.where(alumno_id: @solicitud_reinscripcion.alumno_id, ciclo_id: @solicitud_reinscripcion.ciclo_id, concept_id: concepto_reinscripcion.id).first
    else
      redirect_to root_path
    end
  end

  def get_first_inscription(alumno)
    first_inscripcion = Inscripcion.select("distinct ciclos.ciclo, ciclos.inicio, ciclos.fin").joins(:ciclo).where("inscripciones.alumno_id = ? and status <> ?", alumno, 'OBSOLETA').order("ciclos.ciclo").first
    first_inscripcion = first_inscripcion[:ciclo] unless first_inscripcion.blank?
    first_inscripcion
  end

  def historial_inscripciones(alumno)
    historico = Array.new
    inscripciones = Inscripcion.where("alumno_id  = ?", alumno)
    unless inscripciones.blank?
      inscripciones.each do |i|
        asignaturas = Asignatura.find(i.cursos.map(&:materias_planes_id)).map(&:nombre)
        historico << {
            :inscripcion_id => i.id,
            :periodo => i.ciclo.ciclo,
            :carrera => i.carrera.nombre_carrera,
            :grupo => i.grupo.nombre,
            :tipo => i.status,
            :semestre => i.semestr.clave_semestre,
            :asignaturas => asignaturas,
            :semestreID => i.semestre_id,
            :cicloID => i.ciclo_id,
            :status => i.status
        }
      end
      historico.sort_by{|i| i[:periodo]}
    end
  end
end