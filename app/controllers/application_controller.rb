require 'resolv'
class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  protect_from_forgery with: :exception

  helper_method :current_usuario_session, :current_usuario

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password, :password_confirmation, :remember_me) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:username, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :email, :password, :password_confirmation, :current_password) }
  end

  private

  def require_user
    unless current_usuario
      session[:return_to] = request.url
      redirect_to new_usuario_session_url
      return false
    end
  end
  #metodo que impide el acceso a ciertas partes del sistema, si no esta logueado como tipo rool administrador
  def allow_access
    redirect_to root_path unless current_usuario.rol == 'ADMINISTRADOR'
  end

end
