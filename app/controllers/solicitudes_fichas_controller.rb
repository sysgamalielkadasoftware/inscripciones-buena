class SolicitudesFichasController < ApplicationController
  before_action :require_user
  require 'piet'

  include SendDoc
  def index
    # variable que servirá para oculta mensaje de aviso, en caso de que el redireccionamiento venga de intengar
    # registrar una segunda ficha en un mismo ciclo
    @ocultar = params[:ocultar]
    @anuncio = Anuncio.all
    periodo = PeriodoFicha.where("actual = ?", true)
    ciclo = Ciclo.find_by_id(periodo.first.ciclo_id)

    @solicitudes_fichas = SolicitudFicha.where("aspirante_id = ? AND ciclo_id = ?", current_usuario.aspirante.id, ciclo)

    @solicitudes_fichas.each do |solicitud|
      if solicitud.proceso == "EN LINEA"
        if (!(solicitud.tipo_archivo.eql?("application/pdf")))
          name = solicitud.nombre_archivo
          _path = SolicitudFicha::RUTA_COMPROBANTES
          fpath = File.join(_path,name)
  
          if(File.exists?(fpath))
            solicitud.comprobante = Base64.encode64(File.open(fpath, "rb").read)
          end
        end
      end
    end

    @corto = verify_amount_request_short(current_usuario.aspirante.id, ciclo)
    @largo = verify_amount_request_large(current_usuario.aspirante.id, ciclo)
    @campus=Campus.first
  end

  def readpdf
    ficha = SolicitudFicha.find_by_id(params[:id].to_i)
    name = ficha.nombre_archivo
    _path = SolicitudFicha::RUTA_COMPROBANTES
    fpath = File.join(_path,name)

    if(File.exists?(fpath))
      send_file(fpath, :type => "application/pdf")
    end
  end

  def new
    @anuncio = Anuncio.all
    periodo = PeriodoFicha.where("actual = ?", true)
    ciclo = Ciclo.find_by_id(periodo.first.ciclo_id)
    @solicitud_ficha = SolicitudFicha.new
    @linea_captura = ""
    @comprobante_pago = ""
    solicitudes_fichas = SolicitudFicha.where("aspirante_id = ? AND ciclo_id = ? AND estado != 'CANCELADO'", current_usuario.aspirante.id, ciclo)
    if solicitudes_fichas.blank?
      @carreras = Carrera.order(:nombre_carrera).all
    else
      carreras = Carrera.order(:nombre_carrera).all
      careers = Array.new
      solicitudes_fichas.each do |s|
        career = Carrera.find(s.carrera_id)
        careers << career
      end
      @carreras = carreras - careers
    end
  end

  def create
    params = sede_ficha_params
    @solicitud_ficha = SolicitudFicha.new(:aspirante_id => params[:aspirante_id].to_i, :carrera_id => params[:carrera_id].to_i, :sede_ficha_id => params[:sede_ficha_id].to_i)
    existe_linea_captura = PreregistroPago.where(:numero_referencia => params[:linea_captura]).where.not(:estado => 'CANCELADO')
    periodo = PeriodoFicha.where("actual = ?", true)
    ciclo = Ciclo.find_by_id(periodo.first.ciclo_id)
    # Se consulta los registros de la base de datos para verificar si ya existe una solicitud de ficha del aspirante para el ciclo
    solicitudes_fichas = SolicitudFicha.where("aspirante_id = ? AND ciclo_id = ? AND estado != 'CANCELADO'", current_usuario.aspirante.id, ciclo)

    if existe_linea_captura.blank? && solicitudes_fichas.blank?
      begin
        SolicitudFicha.transaction do

        @solicitud_ficha.estado = SolicitudFicha::ENVIADO
        @solicitud_ficha.proceso = SolicitudFicha::ONLINE
        @solicitud_ficha.ciclo_id = ciclo.id
        @solicitud_ficha.tipo_archivo = params[:comprobante].content_type

        periodo_ficha = PeriodoFicha.where("actual = ?", true).first
        aspirante = Aspirante.find(params[:aspirante_id].to_i)
        name = aspirante.curp.upcase + "_COMPROBANTE_"  + periodo_ficha.ciclo.ciclo.to_s + "_" + periodo_ficha.tipo.to_s + "." + params[:comprobante].original_filename.split('.').last

        @solicitud_ficha.nombre_archivo = name

        _path = SolicitudFicha::RUTA_COMPROBANTES
        fpath = File.join(_path,name)

        File.new(fpath, "w+b")
        File.open(fpath, "w+b") { |f| f.write(params[:comprobante].read) }

        if (!(params[:comprobante].content_type.eql?("application/pdf")))
          # obtener tamaño de la imagen, para poner condicion en caso de que se quiera reducir imágenes si exceden cierto tamaño
          # imagen de 794.1 kB = 0.76
          compressed_file_size = (File.size(fpath).to_f / 2**20).round(2)

          if compressed_file_size > 5
            # verbose: Whether you want to get the output of the command or not. It is interpreted as a Boolean value. Default: false.
            # quality: Adjust the output compression for JPEGs. Valid values are any integer between 0 and 100 (100 means no compression and highest quality). Default: 100
            # level: Adjust the optimization level for PNGs. Valid values are any integer between 0 and 7 (7 means highest compression and longest processing time). Default: 7
            Piet.optimize(fpath, {:verbose => true, :quality => 50})
          end
        end

        raise Exception unless @solicitud_ficha.save

        pre_pagos = PreregistroPago.new
        pre_pagos.aspirante_id = params[:aspirante_id].to_i
        pre_pagos.concept_id = Concept::FICHA_ID
        pre_pagos.ciclo_id = ciclo.id
        pre_pagos.estado = PreregistroPago::POR_VERIFICAR
        pre_pagos.numero_referencia = params[:linea_captura]
        pre_pagos.solicitud_ficha_id = @solicitud_ficha.id
        raise Exception unless pre_pagos.save
        end
        flash[:success] = 'Solicitud de Ficha Enviada'
        redirect_to solicitudes_fichas_path

        rescue Exception => error
        flash[:error] = error.message
        redirect_to new_solicitud_ficha_path
        return
      end
    else
      @linea_captura = params[:linea_captura]
      @comprobante_pago = params[:comprobante]
      if solicitudes_fichas.blank?
        @carreras = Carrera.order(:nombre_carrera).all
        # mensaje de línea de captura ya registrada y se renderiza a misma vista
        flash[:error] = 'La línea de captura ya se encuentra registrada.'
        render new_solicitud_ficha_path
      else
        carreras = Carrera.order(:nombre_carrera).all
        careers = Array.new
        solicitudes_fichas.each do |s|
          career = Carrera.find(s.carrera_id)
          careers << career
        end
        @carreras = carreras - careers
        @ocultar = true
        # mensaje de error por intentar registrar una segunda solicitud de ficha en un mismo periodo, se dirije a vista
        # de resumen de ficha solicitada para el ciclo.
        flash[:error] = 'Ya se ha enviado una solicitud de ficha para este periodo.'
        redirect_to solicitudes_fichas_path(:ocultar => @ocultar)
      end
    end
  end

  def verify_amount_request_short(aspirante, ciclo)
    corto = 0
    solicitudes = SolicitudFicha.where("aspirante_id = ? AND ciclo_id = ? AND estado != 'CANCELADO'", aspirante, ciclo.id)
    solicitudes.each do |solicitud|
      if solicitud.sede_ficha.periodo_ficha.tipo == 'CORTO'
        corto += 1
      end
    end
    corto
  end

  def verify_amount_request_large(aspirante, ciclo)
    largo = 0
    solicitudes = SolicitudFicha.where("aspirante_id = ? AND ciclo_id = ? AND estado != 'CANCELADO'", aspirante, ciclo.id)
    solicitudes.each do |solicitud|
      if solicitud.sede_ficha.periodo_ficha.tipo == 'LARGO'
        largo += 1
      end
    end
    largo
  end
#  #-------------------------------------------------------
  def aspi
    @periodo = params[:ciclo_id]
    usuario = current_usuario
    current_ciclo = Ciclo.get_actual
    ciclo = Ciclo.find_by_inicio_and_fin_and_tipo(current_ciclo.inicio, current_ciclo.fin, 'A')
    solicitud = SolicitudFicha.find_by_aspirante_id(usuario.aspirante.id)
    # puts"aca" +usuario.aspirante.id.to_s
    if usuario
      @universidad = solicitud.sede_ficha.sede
      @periodo= I18n.l(solicitud.sede_ficha.fecha_examen,:format => "%d de %m del %Y")
      @hora=solicitud.sede_ficha.hora_examen
    end
    @datos = []
    @encabezado = []
    contador = 0
    if !ciclo.blank?
      solicitud = SolicitudFicha.find_by_aspirante_id(usuario.aspirante.id)
      @solicitudes_fichas = Ficha.where("aspirante_id = ? AND ciclo_id = ?", usuario.aspirante.id, solicitud.ciclo_id)
      #ficha = Ficha.find_by_solicitud_ficha_id(params[:id].to_i)
      # ficha = Ficha.find_by_id(params[:id].to_i)
      campus = Campus.first
      universidad = Universidad.first
      contenido_comprobante_ficha = ContenidoComprobanteFichas.first
      if !solicitud.blank?  && SolicitudFicha::ONLINE
        carrera = Carrera.find_by_id(solicitud.carrera_id)
        @datos << { :id => usuario.aspirante.id,
                    :nombre=>    usuario.aspirante.nombre.upcase.to_s,
                    :ap_pat=>   usuario.aspirante.apellido_paterno.upcase.to_s,
                    :ap_mat=>    usuario.aspirante.apellido_materno.upcase.to_s,
                    :carrera=> ( carrera.blank? ? "---" : carrera.nombre_carrera.to_s),
                    :no_ficha=> (@solicitudes_fichas.blank? or @solicitudes_fichas.numero.blank?) ? "" : @solicitudes_fichas.numero,
                    :lema => (universidad.lema unless (universidad.blank? or universidad.lema.blank?)),
                    :ciudad => (campus.ciudad.to_s unless (campus.blank? or campus.ciudad.blank?)),
                    :avisos_en => (contenido_comprobante_ficha.avisos_en.to_s unless contenido_comprobante_ficha.blank?),
                    :texto_guias => (contenido_comprobante_ficha.texto_guias.to_s unless contenido_comprobante_ficha.blank?),
                    :muetramensaje => (faltan_documentos_opcionales(usuario.aspirante.id))

        }
      end
      
      @encabezado = {
          :universidad => solicitud.sede_ficha.sede,

      }

    end
    template = render_to_string('solicitudes_fichas/aspi.xml.builder',layout: false)
    xpath = '/reporte/informacion/datos'
    send_doc(template, xpath, "FichaExamenSeleccion", "FichaExamenSeleccion",'pdf')

  end

  def faltan_documentos_opcionales(aspirante_id)
    asp_docs_rechazados = AspiranteDocumento.where( :aspirante_id => aspirante_id, :aceptado => 'FALSE')
    asp_docs = AspiranteDocumento.where(:aspirante_id => aspirante_id)
    docs_opcionales = DocumentoFicha.where(:obligatorio => false)

    if asp_docs_rechazados.blank? and asp_docs.size >= 0
      return false
    end

    docs_opcionales.each do |doc|
      if !asp_docs_rechazados.select {|d| d.documento_ficha_id == doc.id}.blank?
        return true
      end
    end
    return false
  end
  private
  def sede_ficha_params
    params.require(:solicitud_ficha).permit(:aspirante_id, :carrera_id, :sede_ficha_id, :comprobante, :linea_captura)
  end

  def convierte_base64 path
    b64 = Base64.strict_encode64(File::read(path))
    b64 = b64.force_encoding('UTF-8')
    return b64
  end


end
