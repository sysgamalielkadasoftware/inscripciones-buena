class SolicitudesInscripcionesController < ApplicationController
  before_action :require_user
  def index
    @anuncio = Anuncio.all
    @campus=Campus.first
    alumno = Alumno.find_by_curp(current_usuario.username.upcase)
    unless alumno.blank?
      current_ciclo = Ciclo.get_actual
      ciclo = Ciclo.find_by_inicio_and_fin_and_tipo(current_ciclo.inicio, current_ciclo.fin, 'A')
      solicitud_inscripcion = Tramite.find_by_alumno_id_and_ciclo_id_and_proceso(alumno, ciclo.id, Tramite::INSCRIPCION)
      inscripcion_actual = Inscripcion.where("alumno_id = ? AND status != 'OBSOLETA' AND ciclo_id = ?", alumno.id, Ciclo.get_actual)
      unless solicitud_inscripcion.blank?
        redirect_to solicitud_inscripcion_path(solicitud_inscripcion.id)
      else
        unless inscripcion_actual.blank?
          redirect_to solicitud_inscripcion_path(alumno.id)
        else
          redirect_to new_solicitud_inscripcion_path

        end
      end
    else
      aspirante = Aspirante.find_by_curp(current_usuario.username)
      unless aspirante.blank?
        ciclo  = Ciclo.get_actual
        solicitud_inscripcion = Tramite.find_by_alumno_id_and_ciclo_id_and_proceso(aspirante.id, ciclo.id, Tramite::INSCRIPCION)

        redirect_to new_solicitud_inscripcion_path
      else
        redirect_to new_aspirante_solicitudes_inscripciones_path
      end
    end
  end

  def new_aspirante
    @aspirante = Aspirante.new
    @aspirante.build_dato_personal
    @aspirante.build_antecedente_academico
    @lenguas_indigenas = LenguaIndigena.order(:nombre).all
    @paises = Lugar.where("tipo = ?", Lugar::PAIS)
    @pais  = Lugar.get_mexico
    @estados = Lugar.where("tipo = 'ESTADO' AND lugar_id = ?", @pais.id)
  end

  def create_aspirante
    begin
      Direccion.transaction do
        @aspirante = Aspirante.new(aspirante_params)
        @aspirante.usuario_id = current_usuario.id
        calle = params[:direccion][:calle]
        numero = params[:direccion][:numero]
        colonia = params[:direccion][:colonia]
        codigo_postal = params[:direccion][:codigo_postal]
        pais = Lugar.find(params[:direccion][:pais])
        if pais.nombre == 'MÉXICO'
          lugar = Lugar.find(params[:direccion][:localidad])
          @direccion = Direccion.create(calle: calle, numero: numero, colonia: colonia, codigo_postal: codigo_postal, lugar_id: lugar.id)
        else
          @direccion = Direccion.create(calle: calle, numero: numero, colonia: colonia, codigo_postal: codigo_postal, lugar_id: pais.id)
        end
        @aspirante.save
        dp = @aspirante.dato_personal.id
        dato_personal  = DatoPersonal.find(dp)
        dato_personal.direccion_id = @direccion.id
        lenguas = params[:datos_lenguas][:lengua_indigena]
        lenguas.each do |lenguas|
          DatoPersonalLenguaIndigena.new(dato_personal_id: dato_personal.id, lengua_indigena_id: lenguas).save
        end

        padre = save_father(params[:familiar])
        AspiranteFamiliar.new(aspirante_id: @aspirante.id, familiar_id: padre.id).save


        madre = save_mother(params[:familiar])
        AspiranteFamiliar.new(aspirante_id: @aspirante.id, familiar_id: madre.id).save
        if params[:familiar][:parentesco_tutor] == Familiar::PADRE || params[:familiar][:parentesco_tutor] == Familiar::MADRE
        else
          @tutor = save_tutor(params[:familiar])
          AspiranteFamiliar.new(aspirante_id: @aspirante.id, familiar_id: @tutor.id).save
        end
        dato_medico = save_medical_data(params[:dato_medico])
        dato_personal.dato_medico_id = dato_medico.id
        dato_personal.save
        escuela = EscuelaProcedente.find_by_nombre(params[:dato_academico][:escuela_procedente_nombre])
        antecedente_academico = AntecedenteAcademico.new(escuela_procedente_id: escuela.id, aspirante_id: @aspirante.id, fecha_inicio: params[:dato_academico][:fecha_inicio], fecha_fin: params[:dato_academico][:fecha_fin], promedio: params[:dato_academico][:promedio], especialidad: params[:dato_academico][:especialidad], area_conocimiento_id: params[:dato_academico][:area_conocimiento_id], año_inicio: params[:dato_academico][:año_inicio], año_fin: params[:dato_academico][:año_fin]).save
        redirect_to solicitudes_inscripciones_path
      end
    rescue Exception => error
      flash[:notice] = "Error. Revise los campos a rellenar"
      @lenguas_indigenas = LenguaIndigena.order(:nombre).all
      @paises = Lugar.where("tipo = ?", Lugar::PAIS)
      @pais  = Lugar.get_mexico
      @estados = Lugar.where("tipo = 'ESTADO' AND lugar_id = ?", @pais.id)
      render :new
    end
  end

  def show
    @anuncio = Anuncio.all
    ventanilla = false
    @campus=Campus.first
    @solicitud_inscripcion = Tramite.find(params[:id])
    alumno = Alumno.find_by_curp(current_usuario.username.upcase)


    #Aqui se verifica que tipo de alumno es, si no tiene una solicitud de inscripción en linea o si existe una solicitud con los parametros enviados pero no coinciden
    # se marca como que el alumno fue inscrito en ventanilla no en linea.
    unless(alumno.blank?)
      ventanilla = true
    end

    #Se agrega la validación para saber si el usuario que esta intentando acceder a la página con el id proporcionado
    # es el mismo que esta logueado usando el user name, que es la curp y la curp registrada en los datos del alumno
    if(@solicitud_inscripcion.alumno.curp.upcase == current_usuario.username.upcase)
      ultima_inscripcion = Inscripcion.
        joins(:ciclo).
        where("inscripciones.alumno_id = ? AND (inscripciones.status = 'REGULAR' OR inscripciones.status = 'IRREGULAR' OR inscripciones.status = 'REPETIDOR') AND (ciclos.tipo = 'A' OR ciclos.tipo = 'B') AND ciclos.id != ?", @solicitud_inscripcion.alumno_id, @solicitud_inscripcion.ciclo_id).
        order("ciclos.ciclo desc").first
      @beca_anterior = (ultima_inscripcion.beca.to_s) unless ultima_inscripcion.blank?
      @grupo_anterior = ultima_inscripcion.grupo.nombre unless ultima_inscripcion.blank?
      @grupo_actual = Inscripcion.where(alumno_id: @solicitud_inscripcion.alumno_id, semestre_id: @solicitud_inscripcion.semestre_id, ciclo_id: @solicitud_inscripcion.ciclo_id, carrera_id: @solicitud_inscripcion.carrera_id).first
      @debits = Debit.where("alumno_id = ? AND status = 'ALIVE'", @solicitud_inscripcion.alumno_id)
      concepto_reinscripcion = Concept.where("description like ? and status = ?", "INSCRIPCI%", Concept::ACTIVE).first
      @preregistro_pago = PreregistroPagoNes.where(alumno_id: @solicitud_inscripcion.alumno_id, ciclo_id: @solicitud_inscripcion.ciclo_id, concept_id: concepto_reinscripcion.id).first
      @campus=Campus.first
    else
      if ventanilla
        @alumno = Alumno.find(params[:id])
        if current_usuario.username.upcase == @alumno.curp
          @solicitud_inscripcion.alumno.matricula  = @alumno.matricula
          @solicitud_inscripcion.alumno.nombre = @alumno.nombre
          @solicitud_inscripcion.alumno.apellido_paterno = @alumno.apellido_paterno
          @solicitud_inscripcion.alumno.apellido_materno = @alumno.apellido_materno
          @solicitud_inscripcion.alumno.correo_electronico = @alumno.correo_electronico
          @solicitud_inscripcion.alumno.carrera = @alumno.carrera
          @debits = Debit.where("alumno_id = ? AND status = 'ALIVE'", @alumno.id)
          concepto_reinscripcion = Concept.where("description like ? and status = ?", "INSCRIPCI%", Concept::ACTIVE).first

          @inscripcion = Inscripcion.where("alumno_id = ? AND status != 'OBSOLETA' AND ciclo_id = ?", @alumno.id, Ciclo.get_actual)
          p("Valor de inscripcion")
          p(@inscripcion.blank?)
          p("**********************")
          p(@inscripcion)
          p("**********************")
          p(@inscripcion[0].semestre_id)
          p("------------------------")
          @grupo_actual = Inscripcion.where(alumno_id: @alumno.id, semestre_id:@inscripcion[0].semestre_id, ciclo_id: Ciclo.get_actual, carrera_id: @alumno.carrera_id).first
          @preregistro_pago = PreregistroPagoNes.where(alumno_id: @alumno.id, ciclo_id: Ciclo.get_actual, concept_id: concepto_reinscripcion.id).first

        end

      else
        redirect_to root_path

      end
    end

  end

  def new

    alumno = Alumno.find_by_curp(current_usuario.username.upcase)
    unless alumno.blank?
          redirect_to solicitud_inscripcion_path(alumno.id)
      end


    @anuncio = Anuncio.all
    @campus=Campus.first
    current_ciclo = Ciclo.get_actual
    @ciclo = Ciclo.find_by_inicio_and_fin_and_tipo(current_ciclo.inicio, current_ciclo.fin, 'A')
    @tramite  = Tramite.new
    @inscripcion = Hash.new
    @reinscripcion_prepayment = Concept.where("description like ? and status = ?", "INSCRIPCI%", Concept::ACTIVE).first
    # @aspirante = Aspirante.find_by_curp(current_usuario.username.upcase) #|| Alumno.find_by_curp(current_usuario.username.upcase)
    @aspirante = Aspirante.where('upper(curp) = ?', current_usuario.username.upcase).first || Alumno.find_by_curp(current_usuario.username.upcase)
    clase = @aspirante.class
    if clase == "Alumno"
      @propedeutico = InscripcionPropedeutico.joins(:solicitud_propedeutico => :periodo_propedeutico).where("solicitudes_propedeuticos.alumno_id = ? AND periodos_propedeuticos.ciclo_id = ?", @aspirante.id, @ciclo.id)
    else
      @propedeutico = InscripcionPropedeutico.joins(:solicitud_propedeutico => :periodo_propedeutico).where("solicitudes_propedeuticos.aspirante_id = ? AND periodos_propedeuticos.ciclo_id = ?", @aspirante.id, @ciclo.id)
    end
    unless @propedeutico.blank?
      inscripcion_prepayment = Concept.where("description like ? and status = ?", "%REINSCRIPCI%", Concept::ACTIVE).first
      carrera = Carrera.find(@propedeutico.first.carrera_id)
      @inscripcion[:carrera] = carrera.nombre_carrera
      @inscripcion[:carrera_id]  = @propedeutico.first.carrera_id
      @inscripcion[:periodo_id] = @ciclo.id
      @inscripcion[:aspirante_id] = @aspirante.id
      @inscripcion[:curp] = @aspirante.curp
      @inscripcion[:concept_id] = inscripcion_prepayment.id
      @inscripcion[:concepto] = inscripcion_prepayment.description
      @inscripcion[:concept_amount] = inscripcion_prepayment.total_amount_nes
    end

    @debits = Debit.where("alumno_id = ? AND status = 'ALIVE'", @aspirante.id)
  end

  def create
    begin
      Tramite.transaction do
        @solicitud_inscripcion = Tramite.new
        @solicitud_inscripcion.carrera_id = params[:tramite][:carrera_id]
        @solicitud_inscripcion.ciclo_id = params[:tramite][:ciclo_id]
        @solicitud_inscripcion.semestre_id = params[:tramite][:semestre_id]
        @solicitud_inscripcion.estado_proceso = params[:tramite][:estado_proceso]
        @solicitud_inscripcion.proceso = params[:tramite][:proceso]
        aspirante = Aspirante.find(params[:aspirante][:id])
        @alumno = Alumno.find_by_curp(aspirante.curp.upcase)
        concept_id = params[:concept][:id]
        numero_referencia = params[:referencia_pago][:reinscripcion] unless params[:referencia_pago][:reinscripcion].blank?
        ciclo_id = params[:tramite][:ciclo_id]
        alumno_id = params[:tramite][:alumno_id]
        # unless numero_referencia.blank?
        #   @preregistro = PreregistroPagoNes.new(alumno_id: @alumno.id, concept_id: concept_id, numero_referencia: numero_referencia, ciclo_id: ciclo_id, estado: 'POR VERIFICAR')
        #   @preregistro.save
        # end
        # generar_adeudo = Debit.generar_adeudo(concept_id, @alumno.id, '')
        #
        # @solicitud_inscripcion.debit_id = generar_adeudo
        # @solicitud_inscripcion.debit_pagado = false

        if @alumno.blank?
          @alumno = Alumno.new
          begin
            Alumno.transaction do
              @alumno.curp = aspirante.curp.upcase
              @alumno.nombre = aspirante.nombre.upcase
              @alumno.apellido_paterno = aspirante.apellido_paterno.upcase
              @alumno.apellido_materno = aspirante.apellido_materno.upcase
              @alumno.tipo = Alumno::ALUMNO
              @alumno.nss = aspirante.dato_personal.nss.upcase
              @alumno.correo_electronico = aspirante.dato_personal.email
              @alumno.carrera_id = params[:tramite][:carrera_id]
              @alumno.save
            end
            tutores = Array.new
            aspirante.familiares.each do |familiar|
              tutor = Tutor.new
              Tutor.transaction do
                tutor.nombre = familiar.nombre
                tutor.apellido_paterno = familiar.apellido_paterno
                tutor.apellido_materno = familiar.apellido_materno
                tutor.ocupacion = familiar.ocupacion
                tutor.parentezco = familiar.parentesco
                tutor.tutor = familiar.tutor
                tutor.save
                tutores.push(tutor)
              end

              Address.transaction do
                direccion_tutor = Address.new
                direccion_tutor.calle =  familiar.direccion.calle
                direccion_tutor.numero = familiar.direccion.numero
                direccion_tutor.colonia = familiar.direccion.colonia
                direccion_tutor.codigo_postal = familiar.direccion.codigo_postal
                direccion_tutor.telefono = familiar.telefono
                direccion_tutor.tabla_type = 'Tutor'
                direccion_tutor.tabla_id = tutor.id

                nombre = familiar.direccion.lugar.nombre
                ciudad = Ciudad.where("nombre like ?", "%#{nombre}")
                municipio_inscripciones = Lugar.find_by_id_and_tipo(aspirante.dato_personal.direccion.lugar.lugar_id, Lugar::MUNICIPIO) unless (aspirante.dato_personal.direccion.blank? or aspirante.dato_personal.direccion.lugar.blank? or aspirante.dato_personal.direccion.lugar.lugar_id.blank?)
                estado_inscripciones = Lugar.find_by_id_and_tipo(municipio_inscripciones.lugar_id, Lugar::ESTADO) unless (municipio_inscripciones.blank? or municipio_inscripciones.lugar_id.blank?)
                pais_inscripciones = Lugar.find_by_id_and_tipo(estado_inscripciones.lugar_id, Lugar::PAIS) unless (estado_inscripciones.blank? or estado_inscripciones.lugar_id.blank?)
                unless ciudad.blank?
                  direccion_tutor.ciudad_id = ciudad.first.id
                else
                  c = Ciudad.new
                  c.nombre = aspirante.dato_personal.direccion.lugar.nombre unless (aspirante.dato_personal.direccion.blank? or aspirante.dato_personal.direccion.lugar.blank? or aspirante.dato_personal.direccion.lugar.nombre.blank?)
                  municipio = Municipio.find_by_nombre(municipio_inscripciones.nombre) unless (c.blank? or c.nombre.blank?)
                  unless municipio.blank?
                    c.municipio_id = municipio.id
                  else
                    m = Municipio.new
                    m.nombre = municipio_inscripciones.nombre unless municipio_inscripciones.blank?
                    m.save
                    c.municipio_id = m.id
                  end
                  estado = Estado.find_by_nombre(estado_inscripciones.nombre) unless estado_inscripciones.blank?
                  unless estado.blank?
                    c.estado_id = estado.id
                  else
                    e = Estado.new
                    e.nombre = estado_inscripciones.nombre unless estado_inscripciones.blank?
                    pais = Pais.find_by_nombre(pais_inscripciones.nombre) unless pais_inscripciones.blank?
                    unless pais.blank?
                      e.pais_id = pais.id
                    end
                    e.save
                  end
                  c.save
                  direccion_tutor.ciudad_id = c.id
                end
                direccion_tutor.save
              end
            end

            tutores.each do |tutor|
              AlumnoTutor.new(:tutor_id => tutor.id, :alumno_id => @alumno.id).save
            end

            AcademicBackground.transaction do
              dato_academico = AcademicBackground.new
              dato_academico.fecha_inicio = aspirante.antecedente_academico.fecha_inicio unless (aspirante.antecedente_academico.blank? or aspirante.antecedente_academico.fecha_inicio.blank?)
              dato_academico.fecha_fin = aspirante.antecedente_academico.fecha_fin unless (aspirante.antecedente_academico.blank? or aspirante.antecedente_academico.fecha_fin.blank?)
              dato_academico.anio_inicio = aspirante.antecedente_academico.fecha_inicio.strftime("%Y") unless (aspirante.antecedente_academico.blank? or aspirante.antecedente_academico.fecha_inicio.blank?)
              dato_academico.anio_fin = aspirante.antecedente_academico.fecha_fin.strftime("%Y") unless (aspirante.antecedente_academico.blank? or aspirante.antecedente_academico.fecha_fin.blank?)
              dato_academico.especialidad = aspirante.antecedente_academico.especialidad unless (aspirante.antecedente_academico.blank? or aspirante.antecedente_academico.especialidad.blank?)
              area = EscuelaProcedente::AREA_CONOCIMIENTO[aspirante.antecedente_academico.area_conocimiento_id] unless (aspirante.antecedente_academico.blank? or aspirante.antecedente_academico.area_conocimiento_id.blank?)
              dato_academico.area = area[:nombre] unless area.blank?
              dato_academico.promedio = aspirante.antecedente_academico.promedio unless (aspirante.antecedente_academico.blank? or aspirante.antecedente_academico.promedio.blank?)
              dato_academico.numero_cedula = aspirante.antecedente_academico.numero_cedula unless (aspirante.antecedente_academico.blank? or aspirante.antecedente_academico.numero_cedula.blank?)
              dato_academico.alumno_id = @alumno.id
              escuela = OriginSchool.where("nombre like ?", "%#{aspirante.antecedente_academico.escuela_procedente.nombre}%")  unless (aspirante.antecedente_academico.blank? or aspirante.antecedente_academico.escuela_procedente.blank? or aspirante.antecedente_academico.escuela_procedente.nombre.blank?)
              unless escuela.blank?
                dato_academico.escuela_procedente_id = escuela.first.id
              else
                aspirante.antecedente_academico.escuela_procedente.nombre unless (aspirante.antecedente_academico.blank? or aspirante.antecedente_academico.escuela_procedente.blank? or aspirante.antecedente_academico.escuela_procedente.nombre.blank?)
              end
              dato_academico.save
            end


            PersonalDetail.transaction do
              dato_personal = PersonalDetail.new
              dato_personal.alumno_id = @alumno.id
              dato_personal.sexo = aspirante.sexo
              dato_personal.fecha_nacimiento = aspirante.fecha_nacimiento
              estado_civil = DatoPersonal::ESTADO_CIVIL[aspirante.dato_personal.estado_civil.to_i]
              dato_personal.estado_civil = estado_civil[:nombre]
              dato_personal.nacionalidad = aspirante.dato_personal.nacionalidad
              tipo_sangre = DatoMedico::TIPO_SANGRE[aspirante.dato_personal.dato_medico.tipo_sangre_id] unless (aspirante.dato_personal.dato_medico.blank? or aspirante.dato_personal.dato_medico.tipo_sangre_id.blank?)
              dato_personal.tipo_sangre = tipo_sangre[:nombre] unless tipo_sangre.blank?
              discapacidad = DatoMedico::DISCAPACIDAD[aspirante.dato_personal.dato_medico.discapacidad_id] unless (aspirante.dato_personal.dato_medico.blank? or aspirante.dato_personal.dato_medico.discapacidad_id.blank?)
              dato_personal.discapacidad = discapacidad[:nombre] unless discapacidad.blank?
              dato_personal.enfermedad = aspirante.dato_personal.dato_medico.enfermedad_atencion_especial unless (aspirante.dato_personal.dato_medico.blank? or aspirante.dato_personal.dato_medico.enfermedad_atencion_especial.blank?)
              dato_personal.alergia = aspirante.dato_personal.dato_medico.alergia unless (aspirante.dato_personal.dato_medico.blank? or aspirante.dato_personal.dato_medico.alergia.blank?)
              dato_personal.medicamentos = aspirante.dato_personal.dato_medico.medicamento_enfermedad_atencion_especial + ", " + aspirante.dato_personal.dato_medico.medicamento_alergia unless (aspirante.dato_personal.dato_medico.blank? or aspirante.dato_personal.dato_medico.medicamento_enfermedad_atencion_especial.blank? or aspirante.dato_personal.dato_medico.medicamento_alergia.blank?)
              nombre = aspirante.dato_personal.direccion.lugar.nombre unless (aspirante.dato_personal.blank? or aspirante.dato_personal.direccion.blank? or aspirante.dato_personal.direccion.lugar.blank? or aspirante.dato_personal.direccion.lugar.nombre.blank?)
              ciudad = Ciudad.where("nombre like ?", "%#{nombre}")
              municipio_inscripciones = Lugar.find_by_id_and_tipo(aspirante.dato_personal.direccion.lugar.lugar_id, Lugar::MUNICIPIO) unless (aspirante.dato_personal.blank? or aspirante.dato_personal.direccion.blank? or aspirante.dato_personal.direccion.lugar.blank? or aspirante.dato_personal.direccion.lugar.lugar_id.blank?)
              estado_inscripciones = Lugar.find_by_id_and_tipo(municipio_inscripciones.lugar_id, Lugar::ESTADO) unless (municipio_inscripciones.blank? or municipio_inscripciones.lugar_id.blank?)
              pais_inscripciones = Lugar.find_by_id_and_tipo(estado_inscripciones.lugar_id, Lugar::PAIS) unless (estado_inscripciones.blank? or estado_inscripciones.lugar_id.blank?)
              unless ciudad.blank?
                dato_personal.ciudad_id = ciudad.first.id unless ciudad.blank?
              else
                c = Ciudad.new
                c.nombre = aspirante.dato_personal.direccion.lugar.nombre unless (aspirante.dato_personal.blank? or aspirante.dato_personal.direccion.blank? or aspirante.dato_personal.direccion.lugar.blank? or aspirante.dato_personal.direccion.lugar.nombre.blank?)
                municipio = Municipio.find_by_nombre(municipio_inscripciones.nombre) unless (municipio_inscripciones.blank? or municipio_inscripciones.nombre.blank?)
                unless municipio.blank?
                  c.municipio_id = municipio.id unless (municipio.blank? or municipio.id.blank?)
                else
                  m = Municipio.new
                  m.nombre = municipio_inscripciones.nombre unless (municipio_inscripciones.blank? or municipio_inscripciones.nombre.blank?)
                  m.save
                  c.municipio_id = m.id unless m.blank?
                end
                estado = Estado.find_by_nombre(estado_inscripciones.nombre) unless (estado_inscripciones.blank? or estado_inscripciones.nombre.blank?)
                unless estado.blank?
                  c.estado_id = estado.id unless (estado.blank? or estado.id.blank?)
                else
                  e = Estado.new
                  e.nombre = estado_inscripciones.nombre unless (estado_inscripciones.blank? or estado_inscripciones.nombre.blank?)
                  pais = Pais.find_by_nombre(pais_inscripciones.nombre) unless (pais_inscripciones.blank? or pais_inscripciones.nombre.blank?)
                  unless pais.blank?
                    e.pais_id = pais.id unless pais.blank?
                  end
                  e.save
                end
                c.save
                dato_personal.ciudad_id = c.id unless c.blank?
              end
              dato_personal.save
            end

            aspirante.dato_personal.lenguas_indigenas.each do |lengua|
              lengua_indigena = IndigenousLanguage.new
              PersonalDetailIndigenousLanguage.transaction do
                lengua_nes = IndigenousLanguage.find_by_nombre(lengua.nombre)
                unless lengua_nes.blank?
                  PersonalDetailIndigenousLanguage.new(:dato_personal_id => @alumno.personal_detail.id, :lengua_indigena_id => lengua_nes.id).save
                else
                  lengua_indigena.nombre = lengua.nombre
                  lengua_indigena.save
                  PersonalDetailIndigenousLanguage.new(:dato_personal_id => @alumno.personal_detail.id, :lengua_indigena_id => lengua_indigena.id).save
                end
              end
            end

            Address.transaction do
              direccion = Address.new
              direccion.calle = aspirante.dato_personal.direccion.calle unless (aspirante.dato_personal.blank? or aspirante.dato_personal.direccion.blank? or aspirante.dato_personal.direccion.calle.blank?)
              direccion.numero = aspirante.dato_personal.direccion.numero unless (aspirante.dato_personal.blank? or aspirante.dato_personal.direccion.blank? or aspirante.dato_personal.direccion.numero.blank?)
              direccion.colonia = aspirante.dato_personal.direccion.colonia unless (aspirante.dato_personal.blank? or aspirante.dato_personal.direccion.blank? or aspirante.dato_personal.direccion.colonia.blank?)
              direccion.codigo_postal = aspirante.dato_personal.direccion.codigo_postal unless (aspirante.dato_personal.blank? or aspirante.dato_personal.direccion.blank? or aspirante.dato_personal.direccion.codigo_postal.blank?)
              direccion.telefono = aspirante.dato_personal.telefono unless (aspirante.dato_personal.blank? or aspirante.dato_personal.telefono.blank?)
              direccion.ciudad_id = @alumno.personal_detail.ciudad_id unless (@alumno.personal_detail.blank? or @alumno.personal_detail.ciudad_id.blank?)
              direccion.tabla_id = @alumno.id unless @alumno.blank?
              direccion.tabla_type = 'Alumno'
              direccion.save
            end
          end
        end

        unless numero_referencia.blank?
          @preregistro = PreregistroPagoNes.new(alumno_id: @alumno.id, concept_id: concept_id, numero_referencia: numero_referencia, ciclo_id: ciclo_id, estado: 'POR VERIFICAR')
          @preregistro.save
        end
        #Se agrega como parámetro al ciclo_id para validar que no haya duplicados al momento de generarse el registro en la bd
        generar_adeudo = Debit.generar_adeudo(concept_id, @alumno.id, '', ciclo_id)

        @solicitud_inscripcion.debit_id = generar_adeudo
        @solicitud_inscripcion.debit_pagado = false

        @solicitud_inscripcion.alumno_id = @alumno.id
        @solicitud_inscripcion.save

        redirect_to solicitud_inscripcion_path(@solicitud_inscripcion)
      end
    end
  end
end