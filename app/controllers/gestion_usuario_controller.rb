class GestionUsuarioController < ApplicationController
  before_action :require_user, :allow_access

  def index
    @usuarios= Usuario.all
    @usuarios.each do |f|
      f.username.upcase!
    end
    @alumnos=Alumno.where(tipo:'ALUMNO')
    @alumnos.each do |f|
      f.curp.upcase!
    end
    @aspirantes= Aspirante.all
    @aspirantes.each do |f|
      f.curp.upcase!      
    end
    @lista=[]    
    @usuarios.each do |f|
      if f.rol != 'ADMINISTRADOR'
        @aux=@alumnos.find{ |item| item[:curp] == f.username}
        if !@aux.blank?
          #puts "alumno"
          @lista.push(@aux)
        else
          @aux=@aspirantes.find{ |item| item[:curp] == f.username}
          if !@aux.blank?
            @lista.push(@aux)       
          end
        end
      end
    end
  end 
  def show
  end
  def new 

  end
  def edit

  end

  def create
    redirect_to root_path  
  end
  def update

   

    puts "#{params[:submit]}==============}"
    if params[:tipo] == "ALUMNO"
      @usuario=Usuario.find_by(username: (params[:curp]).downcase)
      @usuario.password=params[:matricula]
      if @usuario.save(validate: false)
        flash[:success] = "Se restablecio la contraseña del alumno con su matricula correctamente."
        redirect_to root_path
      else
        flash[:error] = "Error al actualizar Contraseña"
        redirect_to gestion_usuario_index
      end
    else
          @usuario=Usuario.find_by(username: (params[:curp]).downcase)
        if @usuario.send_reset_password_instructions !=nil
          flash[:success] = "Se ha enviado un correo para restablecer su contraseña."
          redirect_to root_path
        else
          flash[:error] = "Error al encontrar al usuario"
          redirect_to gestion_usuario_index
        end
    end
  end



  def crear_cuentas
    ciclo = Ciclo.get_actual
    # Se considera el ciclo anterior además del actual, para poder revisar si tiene inscripción y se considere para crear su cuenta
    ciclo_anterior  = Ciclo.get_ciclo_anterior
    ciclos = [ciclo.id, ciclo_anterior.id]
    conditions = {ciclo_id: ciclos}
    alumnos = Alumno.select("distinct(alumnos.id)").joins(:inscripciones).where(inscripciones: conditions)
    alumnos.each do |a|
      alumno = Alumno.find(a.id)
      usuario = Usuario.find_by_username(alumno.curp)
      if usuario.blank?
        usuario = Usuario.new
        usuario.username = alumno.curp
        usuario.password = alumno.matricula
        usuario.password_confirmation = alumno.matricula
        usuario.email = alumno.correo_electronico.blank? ? 'sin_asignar@gmail.com' : alumno.correo_electronico
        usuario.save
      end
    end
    flash[:success] = "Proceso ejecutado correctamente."
    redirect_to root_path
  end
  
  def restablecer_cuentas
    usuarios = Usuario.all
    if (usuarios.count !=0)
      usuarios.each do |u|
        alumno = Alumno.where("curp = ?", u.username.upcase).first
        if !alumno.blank?
          usuario = Usuario.find_by_username(alumno.curp.downcase)
          unless usuario.blank?
            puts "Usuario existente"
            usuario.password = alumno.matricula
            usuario.password_confirmation = alumno.matricula
            if usuario.save
              puts "¡Finalizado!, Se han restablecido los contraseñas con éxito"
            else
              puts "#{usuario.username} Error al actualizar!!"
            end
          else
            puts "El usuario no existe"
          end
        end
      end 
      flash[:success] = "Proceso ejecutado correctamente."
      redirect_to root_path  
    end
  end
end

