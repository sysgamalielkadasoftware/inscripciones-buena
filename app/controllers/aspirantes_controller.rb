class AspirantesController < ApplicationController
  before_action :require_user
  require 'piet'
  def index; end
  def new
    @anuncio=Anuncio.all

    @aspirante = Aspirante.new
    @aspirante.build_dato_personal
    @aspirante.build_antecedente_academico
    @lenguas_indigenas = LenguaIndigena.order(:nombre).all
    @paises = Lugar.where("tipo = ?", Lugar::PAIS)
    @pais  = Lugar.get_mexico
    @estados = Lugar.where("tipo = 'ESTADO' AND lugar_id = ?", @pais.id)
    @parent= Parentesco.all
    @escuela_proc = EscuelaProcedente.all


  end
  def show
    @aspirante= Aspirante.find(params[:id])
  end


  def edit
    @accion ='edit'
    @aspirante = Aspirante.find(params[:id])
    @aspirante.dato_personal
    @aspirante.antecedente_academico
    @lugar=DatoPersonal.find_by_aspirante_id params[@aspirante.id]
    #@fam=AspiranteFamiliar.find_by_aspirante_id @aspirante.id
    #@padre = []
    #padre=Familiar.find (@fam.familiar_id).padre.where(:parentesco=>'MADRE')
    ma=@aspirante.familiares.where(:parentesco=>'MADRE').first
    @datamed=DatoPersonal.find_by_aspirante_id params[@aspirante.id]
    @dataaca=AntecedenteAcademico.find_by params[@aspirante.id]
    @aspirante.dato_personal.direccion
    @aspirante_estado_civil=@aspirante.dato_personal.estado_civil.first

    # @aspirante.dato_personal.lenguas_indigenas.each do |n|
    #   puts "direccions"+      n.nombre.to_s
    # end

    # obtiene el estado del aspirante
    @aspirante_localidad_id = @aspirante.dato_personal.direccion.lugar.id
    municipio_aux = Lugar.where(:id => @aspirante.dato_personal.direccion.lugar.lugar_id).first
    @aspirante_municipio_id = municipio_aux.id
    estado_aux = Lugar.where(:id => municipio_aux.lugar_id).first
    @aspirante_estado_id = estado_aux.id


    # obtiene el estado del padre del aspirante
    padre=@aspirante.familiares.where(:parentesco=>'PADRE').last
    @padre_localidad_id = padre.direccion.lugar.id
    municipio_aux = Lugar.where(:id => padre.direccion.lugar.lugar_id).first
    @padre_municipio_id = municipio_aux.id unless municipio_aux.blank?
    estado_aux = Lugar.where(:id => municipio_aux.lugar_id).first unless municipio_aux.blank?
    @padre_estado_id = estado_aux.id

    # obtiene el estado de la madre del aspirante
    madre=@aspirante.familiares.where(:parentesco=>'MADRE').last
    @madre_localidad_id = madre.direccion.lugar.id
    municipio_aux = Lugar.where(:id => madre.direccion.lugar.lugar_id).first
    @madre_municipio_id = municipio_aux.id unless municipio_aux.blank?
    estado_aux = Lugar.where(:id => municipio_aux.lugar_id).first unless municipio_aux.blank?
    @madre_estado_id = estado_aux.id
    # obtiene el estado de la madre del aspirante
    tu=@aspirante.familiares.where(:tutor=>'TRUE').last
    @tutor_localidad_id = tu.direccion.lugar.id
    municipio_aux = Lugar.where(:id => tu.direccion.lugar.lugar_id).first
    @tutor_municipio_id = municipio_aux.id unless municipio_aux.blank?
    estado_aux = Lugar.where(:id => municipio_aux.lugar_id).first unless municipio_aux.blank?
    @tutor_estado_id = estado_aux.id

    @dataaca=@aspirante.antecedente_academico
    @lenguas_indigenas = LenguaIndigena.order(:nombre)
    @paises = Lugar.where("tipo = ?", Lugar::PAIS)
    @pais  = Lugar.get_mexico
    @estados = Lugar.where("tipo = 'ESTADO' AND lugar_id = ?", @pais.id)
  end

  def aspi_params
    params.require(:aspirante).permit(:nombre, :apellido_paterno, :apellido_materno, :sexo)
  end
  def datoper_params
    params.require(:dato_personal).permit(:lugar_nacimiento, :telefono, :nss, :nacionalidad, :email, :estado_civil)
  end

  def update
    @accion ='edit'
    @aspirante = Aspirante.find(params[:id])
    @dataaca=@aspirante.antecedente_academico
    # puts "dsfffff"+@dataaca.area_conocimiento_id.to_s

    if @aspirante.update(aspirante_params)
      dato_medico = save_medical_data(params[:dato_medico])
      @aspirante.dato_personal.dato_medico_id = dato_medico.id
      pais = Lugar.find(params[:direccion][:pais])
      if pais.nombre == 'MÉXICO'
        if params[:direccion][:localidad].nil?
          lugar=Lugar.find '300598'
        else
          lugar = Lugar.find(params[:direccion][:localidad])
        end
        data_direccion=save_direccion_data(params[:direccion])
        @aspirante.dato_personal.direccion_id=data_direccion.id
        dp = @aspirante.dato_personal.id
        dato_personal  = DatoPersonal.find(dp)

        lenguas = params[:datos_lenguas][:lengua_indigena]
        lenguas.each do |lenguas|
          DatoPersonalLenguaIndigena.new(dato_personal_id: dato_personal.id, lengua_indigena_id: lenguas).save
        end
      end
      # puts "params familiar: " + params[:familiar].to_s
      # puts "padre nombre: " + params[:familiar][:nombre_padre]
      padre = save_father(params[:familiar])
      AspiranteFamiliar.new(aspirante_id: @aspirante.id, familiar_id: padre.id).save
      madre = save_mother(params[:familiar])
      AspiranteFamiliar.new(aspirante_id: @aspirante.id, familiar_id: madre.id).save
      if Parentesco.find(params[:parentesco_tutor]).parentesco == Familiar::PADRE || Parentesco.find(params[:parentesco_tutor]).parentesco == Familiar::MADRE

      else
        @tutor = save_tutor(params[:familiar])
        AspiranteFamiliar.new(aspirante_id: @aspirante.id, familiar_id: @tutor.id).save
      end
      escuela = EscuelaProcedente.find_by_nombre(params[:dato_academico][:escuela_procedente_nombre])
      # antecedente_academico = AntecedenteAcademico.new(escuela_procedente_id: escuela.id, aspirante_id: @aspirante.id, fecha_inicio: params[:dato_academico][:fecha_inicio], fecha_fin: params[:dato_academico][:fecha_fin], promedio: params[:dato_academico][:promedio], especialidad: params[:dato_academico][:especialidad], area_conocimiento_id: params[:dato_academico][:area_conocimiento_id], año_inicio: params[:dato_academico][:año_inicio], año_fin: params[:dato_academico][:año_fin]).save
      antecedente_academico = AntecedenteAcademico.new(escuela_procedente_id: escuela.id, aspirante_id: @aspirante.id, fecha_inicio: nil, fecha_fin: nil, promedio: params[:dato_academico][:promedio], especialidad: params[:dato_academico][:especialidad], area_conocimiento_id: params[:dato_academico][:area_conocimiento_id], año_inicio: params[:dato_academico][:año_inicio], año_fin: params[:dato_academico][:año_fin]).save

      @aspirante.dato_personal.save

      render 'show'
    else
      render 'edit'
    end
  end
  #@aspirante=DatoPersonal.find_by_aspirante_id @aspirante.id
  def create
    begin
      Direccion.transaction do
        @aspirante = Aspirante.new(aspirante_params)
        @aspirante.usuario_id = current_usuario.id
        calle = params[:direccion][:calle]
        numero = params[:direccion][:numero]
        colonia = params[:direccion][:colonia]
        codigo_postal = params[:direccion][:codigo_postal]
        pais = Lugar.find(params[:direccion][:pais])
        if pais.nombre == 'MÉXICO'
          if params[:direccion][:localidad].nil?
            lugar=Lugar.find '300598'
          else
            lugar = Lugar.find(params[:direccion][:localidad])
          end
          @direccion = Direccion.create(calle: calle, numero: numero, colonia: colonia, codigo_postal: codigo_postal, lugar_id: lugar.id)
        else
          @direccion = Direccion.create(calle: calle, numero: numero, colonia: colonia, codigo_postal: codigo_postal, lugar_id: pais.id)
        end
        @aspirante.save
        dp = @aspirante.dato_personal.id
        dato_personal  = DatoPersonal.find(dp)
        dato_personal.direccion_id = @direccion.id
        lenguas = params[:datos_lenguas][:lengua_indigena]
        lenguas.each do |lenguas|
          DatoPersonalLenguaIndigena.new(dato_personal_id: dato_personal.id, lengua_indigena_id: lenguas).save
        end
        
        # se verifica si se copiaron los datos del domicilio del aspirante para el padre
        if params[:copiar_datos_hijoI] == "1"
          params[:familiar][:localidad_padre] = params[:direccion][:localidad]
        end
        padre = save_father(params[:familiar])
        AspiranteFamiliar.new(aspirante_id: @aspirante.id, familiar_id: padre.id).save

        # se verifica si se copiaron los datos del domicilio del aspirante para la madre
        if params[:copiar_datos_hijoII] == "1"
          params[:familiar][:localidad_madre] = params[:direccion][:localidad]
        end
        madre = save_mother(params[:familiar])
        AspiranteFamiliar.new(aspirante_id: @aspirante.id, familiar_id: madre.id).save

        if !params[:parentesco_tutor].nil?
          aux = params[:parentesco_tutor][:id]
          @tutor = save_tutor(params[:familiar],aux)
          AspiranteFamiliar.new(aspirante_id: @aspirante.id, familiar_id: @tutor.id).save
        end
        dato_medico = save_medical_data(params[:dato_medico])
        dato_personal.dato_medico_id = dato_medico.id
        dato_personal.save
        escuela = EscuelaProcedente.find_by_nombre(params[:dato_academico][:escuela_procedente_nombre])
        # antecedente_academico = AntecedenteAcademico.new(escuela_procedente_id: escuela.id, aspirante_id: @aspirante.id, fecha_inicio: params[:dato_academico][:fecha_inicio], fecha_fin: params[:dato_academico][:fecha_fin], promedio: params[:dato_academico][:promedio], especialidad: params[:dato_academico][:especialidad], area_conocimiento_id: params[:dato_academico][:area_conocimiento_id], año_inicio: params[:dato_academico][:año_inicio], año_fin: params[:dato_academico][:año_fin]).save
        antecedente_academico = AntecedenteAcademico.new(escuela_procedente_id: escuela.id, aspirante_id: @aspirante.id, fecha_inicio: nil, fecha_fin: nil, promedio: params[:dato_academico][:promedio], especialidad: params[:dato_academico][:especialidad], area_conocimiento_id: params[:dato_academico][:area_conocimiento_id], año_inicio: params[:dato_academico][:año_inicio], año_fin: params[:dato_academico][:año_fin]).save
        redirect_to new_documento_aspirantes_path
      end
    rescue Exception => error
      flash[:notice] = error.message
      @lenguas_indigenas = LenguaIndigena.order(:nombre).all
      @paises = Lugar.where("tipo = ?", Lugar::PAIS)
      @pais  = Lugar.get_mexico
      @estados = Lugar.where("tipo = 'ESTADO' AND lugar_id = ?", @pais.id)
      @parent= Parentesco.all

      render :new
    end
  end

  def save_medical_data(params)
    dato_medico = DatoMedico.create(tipo_sangre_id: params[:tipo_sangre_id], discapacidad_id: params[:discapacidad_id], enfermedad_atencion_especial: params[:enfermedad_atencion_especial], medicamento_enfermedad_atencion_especial: params[:medicamento_enfermedad_atencion_especial], alergia: params[:alergia], medicamento_alergia: params[:medicamento_alergia])
    dato_medico
  end
  def aspirante_params
    params.require(:aspirante).permit(
        :nombre, :apellido_paterno, :apellido_materno, :curp, :fecha_nacimiento, :sexo, :edad,
        dato_personal_attributes:[:lugar_nacimiento, :telefono, :nss, :nacionalidad, :email, :estado_civil,direccion:[:calle, :numero, :colonia, :codigo_postal, :pais, :localidad], dato_medico:[:tipo_sangre_id,:discapacidad_id, :enfermedad_atencion_especial,:medicamento_enfermedad_atencion_especial,:alergia,:medicamento_alergia]],
        dato_academico_attribute:[:escuela_procedente_nombre, :escuela_procedente_id, :fecha_inicio, :fecha_fin, :promedio, :area_conocimiento_id, :especialidad],
        dato_familiar_attribute:[:nombre_padre, :apellido_paterno_padre,:apellido_materno_padre,parentesco:[:telefono_padre,:ocupacion_padre, :ocupacion_padre, :tutor]])
    #params.require(:direccion).permit(:calle, :numero, :colonia, :codigo_postal, :pais, :localidad)
    #params.require(:dato_personal).permit(:lugar_nacimiento, :telefono, :nss, :nacionalidad, :email, :estado_civil,:direccion_id, :dato_medico_id)
    #params.require(:dato_academico).permit(:escuela_procedente_nombre, :escuela_procedente_id, :fecha_inicio, :fecha_fin, :promedio, :area_conocimiento_id, :especialidad)
    #paras.require(:familiar).permit(:nombre_padre, :apellido_paterno_padre,:apellido_materno_padre,parentesco:[:telefono_padre,:ocupacion_padre, :ocupacion_padre, :tutor])
  end
  def save_direccion_data(params)

    Direccion.transaction do

      lugar = Lugar.find(params[:localidad])

      direccion = Direccion.create(calle: params[:calle], numero: params[:numero], colonia: params[:colonia], codigo_postal: params[:codigo_postal],lugar_id: lugar.id)

      #direccion = Direccion.create(calle: params[:calle], numero: params[:numero], colonia: params[:colonia], codigo_postal: params[:codigo_postal], lugar_id: params[:lugar_id])
      direccion
    end
  end

  def save_tutor(params,aux)
    direccion = save_address_tutor(params)
    tutor = Familiar.create(nombre: params[:nombre_tutor], apellido_paterno: params[:apellido_paterno_tutor], apellido_materno: params[:apellido_materno_tutor], parentesco: Parentesco.find(aux).parentesco, telefono: params[:telefono_tutor], ocupacion: params[:ocupacion_tutor], tutor: true, direccion_id: direccion.id)
    tutor
  end
  def save_mother(params)
    direccion = save_address_mother(params)
    #tutor = check_tutor_mother(params)
    @finadi=params[:finado_2]
    
    unless params[:tutor_2].nil?
      tutor = params[:tutor_2]
    
      if tutor =="on"
        tutor = TRUE
      else
        tutor = FALSE
      end
    else
      tutor = FALSE
    end
    # puts  @finadi.to_s
    if @finadi =="on"
      @finadi=TRUE
    else
      @finadi=FALSE
    end
    @finadi
    # puts "acas"+ @finadi.to_s
    madre = Familiar.create(nombre: params[:nombre_madre], apellido_paterno: params[:apellido_paterno_madre], apellido_materno: params[:apellido_materno_madre], parentesco: Familiar::MADRE, telefono: params[:telefono_madre], ocupacion: params[:ocupacion_madre], tutor: tutor, direccion_id: direccion.id, finado: @finadi)
    madre
  end
  def save_father(params)
    direccion = save_address_father(params)

    #tutor = check_tutor_father(params)
    @finado=params[:finado]

    unless params[:tutor].nil?
      tutor = params[:tutor]

      if tutor =="on"
        tutor = TRUE
      else
        tutor = FALSE
      end
    else
      tutor = FALSE
    end
    
    # puts  @finado.to_s
    if @finado =="on"
      @finado=TRUE
    else
      @finado=FALSE
    end
    @finado
    # puts "acas"+ @finado.to_s
    padre = Familiar.create(nombre: params[:nombre_padre], apellido_paterno: params[:apellido_paterno_padre], apellido_materno: params[:apellido_materno_padre], parentesco: Familiar::PADRE, telefono: params[:telefono_padre], ocupacion: params[:ocupacion_padre], tutor: tutor, direccion_id: direccion.id, finado: @finado)
    padre
  end

  def save_address_tutor(params)
    pais = Lugar.find(params[:pais_tutor])
    if pais.nombre == 'MÉXICO'
      lugar = Lugar.find(params[:localidad_tutor])
      @direccion = Direccion.create(calle: params[:calle_tutor], numero: params[:numero_tutor], colonia: params[:colonia_tutor], codigo_postal: params[:codigo_postal_tutor], lugar_id: lugar.id)
    else
      @direccion = Direccion.create(calle: params[:calle_tutor], numero: params[:numero_tutor], colonia: params[:colonia_tutor], codigo_postal: params[:codigo_postal_tutor], lugar_id: pais.id)
    end
    @direccion
  end
  def save_address_mother(params)
    pais = Lugar.find(params[:pais_madre])
    # puts "\n\n\n=========================================="
    # puts "PAIS madre: " +  pais.id.to_s
    if pais.nombre == 'MÉXICO'
      # puts "PARAMS: "
      # puts params[:localidad_madre].to_s
      unless params[:localidad_madre].blank?
        lugar = Lugar.find(params[:localidad_madre])
        # puts "\n\n\n\nTiene Luigar madre"
        @direccion = Direccion.create(calle: params[:calle_madre], numero: params[:numero_madre], colonia: params[:colonia_madre], codigo_postal: params[:codigo_postal_madre], lugar_id: lugar.id)
      else
        # puts "\n\n\n\nNO Tiene Luigar madre"
        @direccion = Direccion.create(calle: params[:calle_madre], numero: params[:numero_madre], colonia: params[:colonia_madre], codigo_postal: params[:codigo_postal_madre], lugar_id: pais.id)
      end
    else
      @direccion = Direccion.create(calle: params[:calle_madre], numero: params[:numero_madre], colonia: params[:colonia_madre], codigo_postal: params[:codigo_postal_madre], lugar_id: pais.id)
    end
    @direccion
  end
  def save_address_father(params)
    pais = Lugar.find(params[:pais_padre])
    # puts "\n\n\n=========================================="
    # puts "PAIS padre: " +  pais.id.to_s
    if pais.nombre == 'MÉXICO'
      # puts "PARAMS: "
      # puts params[:localidad_padre].to_s
      unless params[:localidad_padre].blank?
        # puts "NO ESTA VACIO UNLESS"
        lugar = Lugar.find(params[:localidad_padre])
        @direccion = Direccion.create(calle: params[:calle_padre], numero: params[:numero_padre], colonia: params[:colonia_padre], codigo_postal: params[:codigo_postal_padre], lugar_id: lugar.id)
      else
        # puts "ESTA VACIO UNLESS"
        @direccion = Direccion.create(calle: params[:calle_padre], numero: params[:numero_padre], colonia: params[:colonia_padre], codigo_postal: params[:codigo_postal_padre], lugar_id: pais.id)
      end
    else
      @direccion = Direccion.create(calle: params[:calle_padre], numero: params[:numero_padre], colonia: params[:colonia_padre], codigo_postal: params[:codigo_postal_padre], lugar_id: pais.id)
    end
    @direccion
  end
  def check_tutor_father(params)
    tutor = false
    if params[:nombre_padre] == params[:nombre_tutor] && "PADRE" == params[:parentesco_tutor]
      tutor = true
    else
      tutor = false
    end
    tutor
  end
  #def check_finado_father(params)
  #
  #  finado=params[:familiar][:finado]
  #  if finado==""
  #    finado=FALSE
  #  end
  #  finado
  #end
  def check_tutor_mother(params)
    tutor = false
    if params[:nombre_madre] == params[:nombre_tutor] && "MADRE" == params[:parentesco_tutor]
      tutor = true
    else
      tutor = false
    end
    tutor
  end
  def towns_options
    # puts "params: " + params.to_s
    state_id = params[:estado_id]
    @municipios = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::MUNICIPIO, state_id)
    @municipio = params[:municipio_id]
    @localidad = params[:localidad_id]
  end
  def towns_options_edit
    # puts "params: " + params.to_s
    state_id = params[:estado_id]
    @municipios = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::MUNICIPIO, state_id)
    @municipio = params[:municipio_id]
    @localidad = params[:localidad_id]
  end

  def localities_options
    town_id = params[:municipio_id]
    @localidades = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::LOCALIDAD, town_id)
    @localidad = params[:localidad_id]
  end
  def localities_options_edit
    town_id = params[:municipio_id]
    @localidades = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::LOCALIDAD, town_id)
    @localidad = params[:localidad_id]
  end
  def towns_options_father
    # puts "params: " + params.to_s
    state_id = params[:estado_id]
    @municipios = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::MUNICIPIO, state_id)
    @municipio = params[:municipio_id]
    @localidad = params[:localidad_id]
  end

  def towns_options_father_edit
    # puts "params: " + params.to_s
    state_id = params[:estado_id]
    @municipios = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::MUNICIPIO, state_id)
    @municipio = params[:municipio_id]
    @localidad = params[:localidad_id]
  end
  def localities_options_father
    town_id = params[:municipio_id]
    @localidades = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::LOCALIDAD, town_id)
    @localidad = params[:localidad_id]
  end
  def localities_options_father_edit
    town_id = params[:municipio_id]
    @localidades = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::LOCALIDAD, town_id)
    @localidad = params[:localidad_id]
  end
  def towns_options_mother
    # puts "params madre: " + params.to_s
    state_id = params[:estado_id]
    @municipios = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::MUNICIPIO, state_id)
    @municipio = params[:municipio_id]
    @localidad = params[:localidad_id]
  end
  def towns_options_mother_edit
    # puts "params madre: " + params.to_s
    state_id = params[:estado_id]
    @municipios = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::MUNICIPIO, state_id)
    @municipio = params[:municipio_id]
    @localidad = params[:localidad_id]
  end
  def registered_nss
    @tamaño=params[:nss].length
    if params[:nss].length==11
      @nss_disponible = (!DatoPersonal.exists?(:nss => params[:nss]))
    end
    if params[:nss]=="NINGUNO"
      @nss_disponible=TRUE
    end
    render :partial => "registered_nss"
  end
  def registered_escuela

    escuela = params[:escuela]

    puts(escuela)

    if(EscuelaProcedente.exists?(:nombre => escuela))
      @escuela_disponible= TRUE
    else
      @escuela_disponible= FALSE
    end
    render :partial => "registered_escuela"
  end

  def localities_options_mother
    town_id = params[:municipio_id]
    @localidades = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::LOCALIDAD, town_id)
    @localidad = params[:localidad_id]
  end
  def localities_options_mother_edit
    town_id = params[:municipio_id]
    @localidades = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::LOCALIDAD, town_id)
    @localidad = params[:localidad_id]
  end
  def towns_options_tutor
    # puts "params madre: " + params.to_s
    state_id = params[:estado_id]
    @municipios = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::MUNICIPIO, state_id)
    @municipio = params[:municipio_id]
    @localidad = params[:localidad_id]
  end
  def towns_options_tutor_edit
    # puts "params madre: " + params.to_s
    state_id = params[:estado_id]
    @municipios = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::MUNICIPIO, state_id)
    @municipio = params[:municipio_id]
    @localidad = params[:localidad_id]
  end
  def localities_options_tutor
    town_id = params[:municipio_id]
    @localidades = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::LOCALIDAD, town_id)
    @localidad = params[:localidad_id]
  end
  def localities_options_tutor_edit
    town_id = params[:municipio_id]
    @localidades = Lugar.where("tipo = ? AND lugar_id = ?", Lugar::LOCALIDAD, town_id)
    @localidad = params[:localidad_id]
  end
  def autocomplete_origin_school
    term = params[:term].upcase

    escuelas_procedente  = EscuelaProcedente.where("UPPER(nombre) like '#{term}%'")
    render json: escuelas_procedente.map{|ep| {:id => ep.id, :label => ep.nombre, :value => ep.nombre}}
  end

  def new_documento
    @anuncio = Anuncio.all
    @aspirante = Aspirante.find_by_curp(current_usuario.username.upcase)

    if @aspirante.blank?
      redirect_to root_path
    else
      # No se mostrará la página cuando el aspirante ya pueda realizar una solicitud, es decir que sus documentos ya existen en el sistema.
      if @aspirante.is_aspirante_with_request? == Aspirante::SOLICITUD
        redirect_to solicitudes_fichas_path
      end
      @documentos_fichas = DocumentoFicha.order(:obligatorio, :nombre_documento).all
      @aspirantes_documentos = AspiranteDocumento.new
    end
  end

  def save_documentos
    @aspirante = Aspirante.find(current_usuario.aspirante.id)
    documentos = params[:aspirante][:docs]
    periodo_ficha = PeriodoFicha.where("actual = ?", true).first

    begin
      Aspirante.transaction do
        documentos.each do |doc|
          # Se busca el documento del aspirante que se haya creado anteriormente, en caso de existir se actualiza, para no
          # generar registros duplicados. Si no se ha creado, se genera un nuevo registro con la información ingresada.
          aspirante_documento = AspiranteDocumento.where(:aspirante_id => current_usuario.aspirante.id, :documento_ficha_id => doc[0]).first
          # Se cambia forma de realizar el if para crear el documento en caso de no existir
          if aspirante_documento.blank?
            aspirante_documento = AspiranteDocumento.new
          end
          aspirante_documento.aspirante_id = current_usuario.aspirante.id
          aspirante_documento.documento_ficha_id = doc[0].to_i
          aspirante_documento.aceptado = nil
          aspirante_documento.tipo_archivo = doc[1].content_type

          name = @aspirante.curp.upcase + "_FICHA_DOCUMENTO_" + doc[0].to_s + "_" + periodo_ficha.ciclo.ciclo.to_s + "_" + periodo_ficha.tipo.to_s + "." + doc[1].original_filename.split('.').last
          aspirante_documento.nombre_archivo = name

          _path = SolicitudFicha::RUTA_COMPROBANTES
          fpath = File.join(_path,name)

          File.new(fpath, "wb")
          File.open(fpath, "wb") { |f| f.write(doc[1].read) }

          if (!(doc[1].content_type.eql?("application/pdf")))
            compressed_file_size = (File.size(fpath).to_f / 2**20).round(2)

            if compressed_file_size > 5
              # verbose: Whether you want to get the output of the command or not. It is interpreted as a Boolean value. Default: false.
              # quality: Adjust the output compression for JPEGs. Valid values are any integer between 0 and 100 (100 means no compression and highest quality). Default: 100
              # level: Adjust the optimization level for PNGs. Valid values are any integer between 0 and 7 (7 means highest compression and longest processing time). Default: 7
              Piet.optimize(fpath, {:verbose => true, :quality => 50})
            end
          end
          raise Exception unless aspirante_documento.save
        end

        @aspirante.tipo_archivo = params[:aspirante][:foto].content_type

        name = @aspirante.curp.upcase + "_FOTO_"  + periodo_ficha.ciclo.ciclo.to_s + "_" + periodo_ficha.tipo.to_s + "." + params[:aspirante][:foto].original_filename.split('.').last

        @aspirante.nombre_archivo = name

        _path = SolicitudFicha::RUTA_COMPROBANTES
        fpath = File.join(_path,name)

        File.new(fpath, "wb")
        File.open(fpath, "wb") { |f| f.write(params[:aspirante][:foto].read) }

        # obtener tamaño de la imagen, para poner condicion en caso de que se quiera reducir imágenes si exceden cierto tamaño
        # imagen de 794.1 kB = 0.76
        compressed_file_size = (File.size(fpath).to_f / 2**20).round(2)

        if compressed_file_size > 5 and (!File.extname(params[:aspirante][:foto].original_filename).eql?(".pdf"))
          # verbose: Whether you want to get the output of the command or not. It is interpreted as a Boolean value. Default: false.
          # quality: Adjust the output compression for JPEGs. Valid values are any integer between 0 and 100 (100 means no compression and highest quality). Default: 100
          # level: Adjust the optimization level for PNGs. Valid values are any integer between 0 and 7 (7 means highest compression and longest processing time). Default: 7
          Piet.optimize(fpath, {:verbose => true, :quality => 50})
        end

        raise Exception unless @aspirante.save
      end
      flash[:success] = 'Documentos enviados.'
      redirect_to new_solicitud_ficha_path
    rescue Exception => error
      flash[:error] = error.message
      @documentos_fichas = DocumentoFicha.order(:obligatorio, :nombre_documento).all
      @aspirantes_documentos = AspiranteDocumento.new
      redirect_to new_documento_aspirantes_path
      return
    end
  end

  def edit_documento
    @aspirante = Aspirante.find_by_curp(current_usuario.username.upcase)

    if @aspirante.blank?
      redirect_to root_path
    else
      asp_docs_aceptados = AspiranteDocumento.where( :aspirante_id => current_usuario.aspirante.id, :aceptado => 'true')
      asp_docs_rechazados = AspiranteDocumento.where( :aspirante_id => current_usuario.aspirante.id, :aceptado => 'FALSE')
      asp_doc_nulos = AspiranteDocumento.find_by_sql("SELECT * FROM aspirantes_documentos WHERE aspirante_id = #{current_usuario.aspirante.id} and aceptado is null")

      # No se mostrará la página cuando el aspirante ya pueda realizar una solicitud, es decir que sus documentos ya existen en el sistema y sin ningun documento rechazado.
      if @aspirante.is_aspirante_with_request? == Aspirante::NUEVO_DOCUMENTO
        redirect_to new_documento_aspirantes_path
      elsif (!asp_doc_nulos.blank? || !asp_docs_aceptados.blank?) && asp_docs_rechazados.blank? && @aspirante.fotografia_aceptado != false
        redirect_to solicitudes_fichas_path
      else
        periodos_fichas = PeriodoFicha.where("actual = ?", true).first
        @solicitud_ficha = SolicitudFicha.where("solicitudes_fichas.aspirante_id = ? AND solicitudes_fichas.ciclo_id = ? ", current_usuario.aspirante.id, periodos_fichas.ciclo_id).order("created_at desc").first

        @pedir_comprobante = false
        if !@solicitud_ficha.blank? and (@solicitud_ficha.comprobante_aceptado.blank? or @solicitud_ficha.comprobante_aceptado == false)
          @pedir_comprobante = true
        end

        @pedir_fotografia = false
        if !@aspirante.blank? and (@aspirante.fotografia_aceptado.blank? or @aspirante.fotografia_aceptado == false)
          @pedir_fotografia = true
        end

        @documentos_fichas = Array.new

        asp_docs_rechazados.each do |doc|
          doc_ficha = DocumentoFicha.find_by_id(doc.documento_ficha_id)
          @documentos_fichas << doc_ficha
        end
        @aspirantes_documentos = AspiranteDocumento.new
      end
    end
  end

  def save_edit_documentos
    @aspirante = Aspirante.find(current_usuario.aspirante.id)
    if !params[:aspirante].blank?
      documentos = params[:aspirante][:docs]

    periodo_ficha = PeriodoFicha.where("actual = ?", true).first

      begin
        Aspirante.transaction do
          if !documentos.blank?
            documentos.each do |doc|
              aspirante_documento = AspiranteDocumento.where(:aspirante_id => current_usuario.aspirante.id, :documento_ficha_id => doc[0]).first
              # Se cambia forma de realizar el if para crear el documento en caso de no existir
              if aspirante_documento.blank?
                aspirante_documento = AspiranteDocumento.new
              end
              aspirante_documento.aspirante_id = current_usuario.aspirante.id
              aspirante_documento.documento_ficha_id = doc[0].to_i
              aspirante_documento.tipo_archivo = doc[1].content_type
              aspirante_documento.aceptado = nil

              name = @aspirante.curp.upcase + "_FICHA_DOCUMENTO_" + doc[0].to_s + "_" + periodo_ficha.ciclo.ciclo.to_s + "_" + periodo_ficha.tipo.to_s + "." + doc[1].original_filename.split('.').last
              aspirante_documento.nombre_archivo = name

              _path = SolicitudFicha::RUTA_COMPROBANTES
              fpath = File.join(_path,name)

              File.new(fpath, "w+b")
              File.open(fpath, "w+b") { |f| f.write(doc[1].read) }

              if (!(doc[1].content_type.eql?("application/pdf")))
                compressed_file_size = (File.size(fpath).to_f / 2**20).round(2)

                if compressed_file_size > 5
                    # verbose: Whether you want to get the output of the command or not. It is interpreted as a Boolean value. Default: false.
                    # quality: Adjust the output compression for JPEGs. Valid values are any integer between 0 and 100 (100 means no compression and highest quality). Default: 100
                    # level: Adjust the optimization level for PNGs. Valid values are any integer between 0 and 7 (7 means highest compression and longest processing time). Default: 7
                    Piet.optimize(fpath, {:verbose => true, :quality => 50})
                  end
              end

              raise Exception unless aspirante_documento.save
            end
          end
          periodo_ficha = PeriodoFicha.where("actual = ?", true).first
          solicitud_ficha = SolicitudFicha.where("solicitudes_fichas.aspirante_id = ? AND solicitudes_fichas.ciclo_id = ? ", current_usuario.aspirante.id, periodo_ficha.ciclo_id).order("created_at desc").first

          if solicitud_ficha.comprobante_aceptado == false
            solicitud_ficha.tipo_archivo = params[:aspirante][:comprobante].content_type
            solicitud_ficha.comprobante_aceptado = nil

            name = @aspirante.curp.upcase + "_COMPROBANTE_"  + solicitud_ficha.sede_ficha.periodo_ficha.ciclo.ciclo.to_s + "_" + solicitud_ficha.sede_ficha.periodo_ficha.tipo.to_s + "." + params[:aspirante][:comprobante].original_filename.split('.').last

            solicitud_ficha.nombre_archivo = name

            _path = SolicitudFicha::RUTA_COMPROBANTES
            fpath = File.join(_path,name)

            File.new(fpath, "w+b")
            File.open(fpath, "w+b") { |f| f.write(params[:aspirante][:comprobante].read) }

            if (!(params[:aspirante][:comprobante].content_type.eql?("application/pdf")))
              # obtener tamaño de la imagen, para poner condicion en caso de que se quiera reducir imágenes si exceden cierto tamaño
              # imagen de 794.1 kB = 0.76
              compressed_file_size = (File.size(fpath).to_f / 2**20).round(2)

              if compressed_file_size > 5
                # verbose: Whether you want to get the output of the command or not. It is interpreted as a Boolean value. Default: false.
                # quality: Adjust the output compression for JPEGs. Valid values are any integer between 0 and 100 (100 means no compression and highest quality). Default: 100
                # level: Adjust the optimization level for PNGs. Valid values are any integer between 0 and 7 (7 means highest compression and longest processing time). Default: 7
                Piet.optimize(fpath, {:verbose => true, :quality => 50})
              end
            end
            raise Exception unless solicitud_ficha.save
          end

          if @aspirante.fotografia_aceptado == false
            @aspirante.tipo_archivo = params[:aspirante][:fotografia].content_type
            @aspirante.fotografia_aceptado = nil

            name = @aspirante.curp.upcase + "_FOTO_"  + periodo_ficha.ciclo.ciclo.to_s + "_" + periodo_ficha.tipo.to_s + "." + params[:aspirante][:fotografia].original_filename.split('.').last
            @aspirante.nombre_archivo = name

            _path = SolicitudFicha::RUTA_COMPROBANTES
            fpath = File.join(_path,name)

            File.new(fpath, "w+b")
            File.open(fpath, "w+b") { |f| f.write(params[:aspirante][:fotografia].read) }

            if (!(params[:aspirante][:fotografia].content_type.eql?("application/pdf")))
              # obtener tamaño de la imagen, para poner condicion en caso de que se quiera reducir imágenes si exceden cierto tamaño
              # imagen de 794.1 kB = 0.76
              compressed_file_size = (File.size(fpath).to_f / 2**20).round(2)

              if compressed_file_size > 5
                # verbose: Whether you want to get the output of the command or not. It is interpreted as a Boolean value. Default: false.
                # quality: Adjust the output compression for JPEGs. Valid values are any integer between 0 and 100 (100 means no compression and highest quality). Default: 100
                # level: Adjust the optimization level for PNGs. Valid values are any integer between 0 and 7 (7 means highest compression and longest processing time). Default: 7
                Piet.optimize(fpath, {:verbose => true, :quality => 50})
              end
            end
            raise Exception unless @aspirante.save
          end

        end
        flash[:success] = 'Documentos enviados.'
        redirect_to root_path
      rescue Exception => error
        flash[:error] = error.message
        @aspirante = Aspirante.find(current_usuario.aspirante.id)
        @documentos_fichas = Array.new
        asp_docs_rechazados = AspiranteDocumento.where( :aspirante_id => current_usuario.aspirante.id, :aceptado => 'FALSE')

        asp_docs_rechazados.each do |doc|
          doc_ficha = DocumentoFicha.find_by_id(doc.documento_ficha_id)
          if doc_ficha.obligatorio
            @documentos_fichas << doc_ficha
          end
        end
        @aspirantes_documentos = AspiranteDocumento.new
        redirect_to edit_documento_aspirantes_path
        return
      end
    else
      redirect_to root_path
    end
  end

  private
  def aspirante_params
    params.require(:aspirante).permit(
        :nombre, :apellido_paterno, :apellido_materno, :curp, :fecha_nacimiento, :sexo, :edad, :estudio,
        dato_personal_attributes: [:lugar_nacimiento, :telefono, :nss, :nacionalidad, :email, :estado_civil])
  end
  def direccion_params
    params.require(:direccion).permit(:calle, :numero, :colonia, :codigo_postal, :pais, :localidad)
  end
  def familiar_params
    # params.require(:familiar).permit()
  end
  def dato_academico_params
    params.require(:dato_academico).permit(:escuela_procedente_nombre, :escuela_procedente_id, :fecha_inicio, :fecha_fin, :promedio, :area_conocimiento_id, :especialidad)
  end

  def convierte_base64 path
    b64 = Base64.strict_encode64(File::read(path))
    b64 = b64.force_encoding('UTF-8')
    return b64
  end
end