class FichasController < ApplicationController
  before_action :require_user

  require 'piet'

  include SendDoc

  def index
    @anuncio = Anuncio.all
    ciclo = Ciclo.get_actual
    periodos_fichas = PeriodoFicha.where("actual = ?", true).first
    unless current_usuario.aspirante.blank?
      @fichas = SolicitudFicha.joins(:ficha).where("solicitudes_fichas.aspirante_id = ? AND solicitudes_fichas.ciclo_id = ? ", current_usuario.aspirante.id, periodos_fichas.ciclo_id)
      # @solicitud_ficha = SolicitudFicha.where("solicitudes_fichas.aspirante_id = ? AND solicitudes_fichas.ciclo_id = ? ", current_usuario.aspirante.id, periodos_fichas.ciclo_id).order("created_at desc").first
      @faltan_documentos = faltan_documentos_obligatorios(current_usuario.aspirante.id, periodos_fichas.ciclo_id)
    else
      @fichas = []
    end

  end

  def new
  end
  def aspi
    @periodo = params[:ciclo_id]
    usuario = current_usuario
    #Se identifica el periodo que esta como actual pero de fichas, no del sistema nes
    ciclo_periodo = PeriodoFicha.find_by_actual(true)
    current_ciclo = Ciclo.find_by_actual(true)
    ciclo = Ciclo.find_by_inicio_and_fin_and_tipo(current_ciclo.inicio, current_ciclo.fin, 'A')
    #solicitud = SolicitudFicha.find_by_aspirante_id(usuario.aspirante.id)
    # Se toma la solicitud hecha en ese periodo seleccionado
    unless ciclo_periodo.blank?
      solicitud = SolicitudFicha.where("aspirante_id = ? AND ciclo_id = ?", usuario.aspirante.id, ciclo_periodo.ciclo_id).last
    end

    # puts"aca" +usuario.aspirante.id.to_s
    if usuario
      @universidad = solicitud.sede_ficha.sede
      @periodo= I18n.l(solicitud.sede_ficha.fecha_examen,:format => "%d de %B del %Y")
      @hora=solicitud.sede_ficha.hora_examen
    end
    @datos = []
    @encabezado = []
    contador = 0
    if !ciclo.blank?
      #Al igual que arriba se busca la información pero tomando en cuenta el periodo de fichas actual no el del sistema NES
      unless ciclo_periodo.blank?
        solicitud = SolicitudFicha.where("aspirante_id = ? AND ciclo_id = ?", usuario.aspirante.id, ciclo_periodo.ciclo_id).last
        @solicitudes_fichas = Ficha.where("aspirante_id = ? AND ciclo_id = ?", usuario.aspirante.id, ciclo_periodo.ciclo_id).first
      end
      #ficha = Ficha.find_by_solicitud_ficha_id(params[:id].to_i)
      # ficha = Ficha.find_by_id(params[:id].to_i)

      campus = Campus.first
      universidad = Universidad.first
      contenido_comprobante_ficha = ContenidoComprobanteFichas.first
      if !solicitud.blank?  && SolicitudFicha::ONLINE
        carrera = Carrera.find_by_id(@solicitudes_fichas.carrera_id) unless @solicitudes_fichas.blank?
        @datos << { :id => usuario.aspirante.id,
                    :nombre=>    usuario.aspirante.nombre.upcase.to_s,
                    :ap_pat=>   usuario.aspirante.apellido_paterno.upcase.to_s,
                    :ap_mat=>    usuario.aspirante.apellido_materno.upcase.to_s,
                    :carrera=> ( carrera.blank? ? "---" : carrera.nombre_carrera.to_s),
                    :no_ficha=> (@solicitudes_fichas.blank? or @solicitudes_fichas.numero.blank?) ? "" : @solicitudes_fichas.numero,
                    :lema => (universidad.lema unless (universidad.blank? or universidad.lema.blank?)),
                    :ciudad => (campus.ciudad.to_s unless (campus.blank? or campus.ciudad.blank?)),
                    :avisos_en => (contenido_comprobante_ficha.avisos_en.to_s unless contenido_comprobante_ficha.blank?),
                    :texto_guias => (contenido_comprobante_ficha.texto_guias.to_s unless contenido_comprobante_ficha.blank?),
                    :muestramensaje =>(faltan_documentos_opcionales(usuario.aspirante.id)),
                    :proceso => (@solicitudes_fichas.solicitud_ficha.proceso == "EN LINEA") ? "TRÁMITE REALIZADO EN LÍNEA".to_s : "TRÁMITE REALIZADO EN VENTANILLA".to_s

        }
        name = usuario.aspirante.nombre_archivo
        if !name.blank?
          _path = SolicitudFicha::RUTA_COMPROBANTES
          fpath = File.join(_path,name)
          if(File.exists?(fpath))
            @fotoaspi = Base64.encode64(File.open(fpath, "rb").read)
          end
        end

        @muestramensaje = (faltan_documentos_opcionales(usuario.aspirante.id))
      end
      @encabezado = {
          :universidad => solicitud.sede_ficha.sede,

      }

    end
    template = render_to_string('solicitudes_fichas/aspi.xml.builder',layout: false)
    xpath = '/reporte/informacion/datos'
    send_doc(template, xpath, "FichaExamenSeleccion", "FichaExamenSeleccion",'pdf')

  end
  def faltan_documentos_obligatorios(aspirante_id, ciclo)
    solicitud_ficha = SolicitudFicha.where("solicitudes_fichas.aspirante_id = ? AND solicitudes_fichas.ciclo_id = ? ", aspirante_id, ciclo).order("created_at desc").first
    if !solicitud_ficha.blank?
      asp_docs_rechazados = AspiranteDocumento.where( :aspirante_id => aspirante_id, :aceptado => 'FALSE')
      asp_docs = AspiranteDocumento.where(:aspirante_id => aspirante_id)
      docs_obligatorios = DocumentoFicha.all #where(:obligatorio => true)
      aspirante = Aspirante.find(aspirante_id)

      if asp_docs_rechazados.blank? and asp_docs.size > 0 and (solicitud_ficha.comprobante_aceptado == true) and (aspirante.fotografia_aceptado == true)
        return true
      end

      if (solicitud_ficha.comprobante_aceptado == false)
        return false
      end

      if (aspirante.fotografia_aceptado == false)
        return false
      end

      docs_obligatorios.each do |doc|
        if !asp_docs_rechazados.select {|d| d.documento_ficha_id == doc.id}.blank?
          return false
        end
      end
    end
    return true
  end
  def faltan_documentos_opcionales(aspirante_id)
    asp_docs_rechazados = AspiranteDocumento.where( :aspirante_id => aspirante_id, :aceptado => 'FALSE')
    asp_docs = AspiranteDocumento.where(:aspirante_id => aspirante_id)
    docs_opcionales = DocumentoFicha.where(:obligatorio => false)

    if asp_docs_rechazados.blank? and asp_docs.size >= 0
      return false
    end
    docs_opcionales.each do |doc|
      if !asp_docs_rechazados.select {|d| d.documento_ficha_id == doc.id}.blank?
        return true
      end
    end
    return false
  end
end