class AnunciosController < ApplicationController
  before_action :require_user , :allow_access

  before_action :find_anuncio, only: [:show, :edit, :destroy,:update]

  add_flash_types :notice

  #Lista todos los registros de mensajes de la base de datos

  def index
    @anuncio=Anuncio.all
  end

  #Muestra la información de un registro

  def show
  end

  def edit
  end

  def update
    @anuncio.update(mensaje: params[:anuncio][:mensaje],desde: params[:anuncio][:desde], hasta: params[:anuncio][:hasta], modulo: params[:anuncio][:modulo])
    redirect_to action: :index
    flash[:notice] = "El mensaje ha sido actualizado correctamente"
  end

  #Crea el mensaje

  def new
    @anuncio = Anuncio.new
  end

  def create
    @anuncio = Anuncio.create(mensaje: params[:anuncio][:mensaje],desde: params[:anuncio][:desde], hasta: params[:anuncio][:hasta], modulo: params[:anuncio][:modulo])

    if @anuncio.save
      redirect_to action: :index
      flash[:notice] = "El mensaje ha sido creado correctamente"
    else
      render :new
    end
  end



  def destroy
    @anuncio.destroy
    redirect_to action: :index
    if @anuncio.destroy
      flash[:notice] = "El mensaje fue eliminado correctamente"
    else
      flash[:alert] = "El mensaje no se puede eliminar"
    end
  end

  def find_anuncio
    @anuncio = Anuncio.find(params[:id])
  end

end
