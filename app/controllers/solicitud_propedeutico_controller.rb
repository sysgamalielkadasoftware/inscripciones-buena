class SolicitudPropedeuticoController < ApplicationController
  before_action :require_user

  def index
    @anuncio = Anuncio.all
    aspirante_id = current_usuario.aspirante.id
    @periodo = PeriodoPropedeutico.where(:actual => true)
    @solicitudes = Array.new
    if !@periodo.blank?
      @periodo.each do| periodo_prope|
      @solicitud_propedeutico=SolicitudPropedeutico.where("aspirante_id = ? AND periodo_propedeutico_id = ?",aspirante_id,periodo_prope.id)
      @solicitud_propedeutico.each do|solicitud|
        solicitud_datos = Hash.new
        solicitud_datos[:inscrito] = false
        solicitud_datos[:carrera] = Carrera.find_by_id(solicitud.carrera_id).nombre_carrera
        solicitud_datos[:tipo] = PeriodoPropedeutico.find_by_id(solicitud.periodo_propedeutico_id).tipo
        if(solicitud.estado == "TERMINADO")
          solicitud_datos[:inscrito] = true
          inscripcion = InscripcionPropedeutico.where("solicitud_propedeutico_id = ?",solicitud[:id]).first
          solicitud_datos[:grupo] = GrupoPropedeutico.find_by_id(inscripcion[:grupo_propedeutico_id]).nombre
          solicitud_datos[:estado] = inscripcion.estado
        else
          solicitud_datos[:estado] = solicitud[:estado]
        end
        @solicitudes.push(solicitud_datos)
      end
      end
    @aspirante = Aspirante.find_by_id(aspirante_id)
    @campus=Campus.first
    end
  end

  def new
    aspirante_id = current_usuario.aspirante.id
    @campus=Campus.first
    aspirante = Aspirante.find_by_id(aspirante_id)
    existe_aspirante = Alumno.find_by_curp(aspirante.curp.upcase)

    if existe_aspirante.blank?
      ciclo = PeriodoPropedeutico.get_current_token_period
      fichas = Ficha.where("aspirante_id = ?",aspirante_id)
      @aspirante = Aspirante.find_by_id(aspirante_id)
      @fichas_asp = Array.new
      @periodo_propedeutico = PeriodoPropedeutico.get_current_token_period
      periodo_propedeutico = PeriodoPropedeutico.get_current_token_period
      if !periodo_propedeutico.blank?
        fichas = SolicitudFicha.joins(:ficha, :sede_ficha => :periodo_ficha).
            where("fichas.ciclo_id = ? AND fichas.aspirante_id = ? AND periodos_fichas.tipo = ?", periodo_propedeutico.ciclo_id, aspirante_id, periodo_propedeutico.tipo)
        @solicitud_propedeutico = SolicitudPropedeutico.where("aspirante_id = ? AND periodo_propedeutico_id = ?", aspirante_id, periodo_propedeutico.id)
      end

      if !ciclo.blank?
        fichas.each do |ficha|
          aspirante = Hash.new
          if ficha.ficha.estado == "ACEPTADO" or ficha.ficha.estado == "ACEPTADO CONDICIONADO" #&& ficha.solicitud_ficha.ciclo_id == ciclo.ciclo_id #Poner como condicion que el ciclo sea el actual
            aspirante[:carrera_id] = ficha.carrera_id
            aspirante[:nombre] = ficha.carrera.nombre_carrera unless ficha.carrera.blank?
            aspirante[:aspirante_id] = aspirante_id
            aspirante[:periodo_prope] = ficha.sede_ficha.periodo_ficha.tipo unless (ficha.sede_ficha.blank? or ficha.sede_ficha.periodo_ficha.blank?)
            @fichas_asp.push(aspirante)
          end
        end
      end
    else
      @existe = true
    end

  end

  def search_data_aspirantes_request
    carrera_id = params[:carrera_id]
    aspirante_id = current_usuario.aspirante.id

    ficha = SolicitudFicha.find_by_aspirante_id_and_carrera_id(aspirante_id, carrera_id)
    carrera = ficha.carrera.nombre_carrera
    render :partial => "input_carrera_nombre", :locals => { :carrera_nombre => carrera }
  end

  def search_data_tipo_request
    carrera_id = params[:carrera_id]
    aspirante_id = current_usuario.aspirante.id

    ficha = SolicitudFicha.find_by_aspirante_id_and_carrera_id(aspirante_id, carrera_id)
    tipo = ficha.sede_ficha.periodo_ficha.tipo
    render :partial => "input_tipo_prope", :locals => { :periodo => tipo }
  end

  def search_monto_request
    concepto = concepto_curso
    monto_imp = concepto[0].total_amount
    render :partial => "input_monto_prope", :locals => { :monto => monto_imp }
  end

  def search_concepto_request
    concepto = concepto_curso
    concepto_des = concepto[0].description
    render :partial => "input_concepto_prope", :locals => { :concepto => concepto_des }
  end

  def guardar_sol
    carrera_id   = params[:carrera][:id]
    numero_ref   = params[:num_ref]
    aspirante_id = current_usuario.aspirante.id
    concepto = concepto_curso
    concepto_id = concepto[0].id

    unless carrera_id.blank?

      sol_prope = SolicitudPropedeutico.new
      ficha = SolicitudFicha.find_by_aspirante_id_and_carrera_id(aspirante_id, carrera_id).sede_ficha.periodo_ficha.tipo
      periodo_prope = PeriodoPropedeutico.get_current_token_period

      unless periodo_prope.blank?
        sol_prope.aspirante_id = aspirante_id
        sol_prope.carrera_id = carrera_id
        sol_prope.periodo_propedeutico_id = periodo_prope.id
        sol_prope.fecha_solicitud = DateTime.now
        sol_prope.estado = SolicitudPropedeutico::ENVIADO
        sol_prope.proceso = SolicitudPropedeutico::ONLINE

        pre_pagos = PreregistroPago.new
        pre_pagos.aspirante_id = aspirante_id
        pre_pagos.concept_id =concepto_id
        pre_pagos.ciclo_id = periodo_prope.ciclo_id
        pre_pagos.estado = PreregistroPago::POR_VERIFICAR
        pre_pagos.numero_referencia = numero_ref

        if sol_prope.save && pre_pagos.save
          flash[:success] = 'Solicitud de Inscripcion Enviada'
          redirect_to solicitud_propedeutico_index_path
        else
          flash[:error] = 'Revise el valor de los campos solicitados'
          redirect_to new_solicitud_propedeutico_path
        end
      end
    else
      flash[:error] = 'No se selecciono ninguna opción para la solicitud de inscripcioón al propedéutico'
      redirect_to new_solicitud_propedeutico_path
    end
  end

  private
  def concepto_curso
    Concept.where("description like ? AND status = ?","%PROPE%",'ACTIVE')
  end

end
