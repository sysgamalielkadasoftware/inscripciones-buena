class RegistrationsController < Devise::RegistrationsController


# before_filter :configure_sign_up_params, only: [:create]
# before_filter :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  # def new
  #   super
  # end

  # POST /resource
  # def create
  #   super
  # end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  def update
    if params[:commit] == "Cancelar Información"
      flash[:error] = "Sin cambios"

      redirect_to root_path
    else
      @admin= Usuario.find(current_usuario)          
      case current_usuario.rol    
        when 'ADMINISTRADOR'              
          if params[:usuario][:password]== params[:usuario][:password_confirmation] 
            if params[:usuario][:password].length >=8 && params[:usuario][:password_confirmation].length >=8
                @admin.password= params[:usuario][:password]

              if Usuario.validacion_pass(params[:usuario][:password]) &&  Usuario.validacion_pass(params[:usuario][:password_confirmation]) 

                if @admin.save(validate: false)
                  flash[:success] = "Contraseña actualizada correctamente."
                  redirect_to root_path
                else
                  flash[:error] = "Error al actualizar Contraseña"
                  redirect_to edit_usuario_registration_path

                end
              else

                flash[:error] = "La contraseña debe de contener al menos una letra mayúscula, una letra minúscula,un número y un caracter especial"
                redirect_to edit_usuario_registration_path

              end

            else
              flash[:error] = "Las contraseñas deben de contener como minimo 8 caracteres"
              redirect_to edit_usuario_registration_path

            end
          else
            flash[:error] = "Las contraseñas no coinciden"
            redirect_to edit_usuario_registration_path
          end
        when nil  
          super

        
      end
    end

      
  end

  # def set_minimum_password_length
  #  if devise_mapping.validatable?
  #    @minimum_password_length = resource_class.password_length.min
  #  end
  #end
  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #  super
  # end

  # protected

  # You can put the params you want to permit in the empty array.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.for(:sign_up) << :attribute
  # end

  # You can put the params you want to permit in the empty array.
  # def configure_account_update_params
  #  devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:password) }
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #  super(resource)
  # end

  # The path used after sign up for inactive accounts.
  #def after_inactive_sign_up_path_for(resource)
  # super(resource)
  # end
end
