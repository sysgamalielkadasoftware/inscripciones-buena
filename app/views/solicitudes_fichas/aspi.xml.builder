xml.instruct!
xml.reporte do
  xml.informacion do
    xml.fechaheader I18n.l(Date.today,:format => "%d de %B del %Y")
    xml.universidad @universidad
    xml.campus @campus
    xml.periodo @periodo
    xml.hora @hora
    xml.foto @fotoaspi
		@datos.each do |d|
			xml << d.to_xml(
		        :dasherize=>true,
		        :skip_instruct=>true,
		        :only=>[
		          :id,
		          :no_ficha,
		          :nombre,
		          :ap_pat,
		          :ap_mat,
		          :carrera,
		          :fecha_sol,
              :lema,
              :ciudad,
              :avisos_en,
              :texto_guias,
              :muestramensaje,
              :proceso
            ],
		        :root=>"datos")
    end
    xml << @encabezado.to_xml(
        :dasherize=>true,
        :skip_instruct=>true,
        :only=>[ :universidad, :campus, :periodo],:root=>"encabezado")
   end
end