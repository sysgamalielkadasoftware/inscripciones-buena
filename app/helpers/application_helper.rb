module ApplicationHelper
  def flash_class(tipo)
    case tipo
      when 'notice' then "alert-info"
      when 'success' then "alert-success"
      when 'error' then "alert-danger"
      when 'alert' then "alert-warning"
    end
  end
end
