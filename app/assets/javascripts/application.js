// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require moment
//= require bootstrap-datetimepicker
//= require jquery-ui/widgets/autocomplete
//= require autocomplete-rails
//= require dataTables/jquery.dataTables
//= require_tree .
$(function () {
    // DataTable Simple
    $('.datatable').DataTable({
        //Establece tipo de paginación "full numbers"
        "sPaginationType": "full_numbers",
        //Cambio de etiquetas de lenguage a español
        "oLanguage": {
            "sZeroRecords": "El usuario que busca no se ha encontrado en el sistema.",
            "oAria": {
                "sSortAscending": " - click/enter para ordenar ascendente",
                "sSortDescending": " - click/enter para ordenar descendente"
            },
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "sEmptyTable": "No hay datos con la información proporcionada.",
            "sInfo": "Total _TOTAL_ registros, mostrando (_START_ a _END_)",
            "sInfoEmpty": "No hay registros que mostrar",
            "sInfoFiltered": " - filtrado desde _MAX_ registros",
            "sSearch": "Buscar en toda la tabla:",
            //Parámetros del menú de nuúmero de elementos por página
            "sLengthMenu": 'Mostrar <select>' +
                '<option value="10">10</option>' +
                '<option class="impar" value="15">15</option>' +
                '<option value="20">20</option>' +
                //'<option value="50">50</option>'+
                //'<option class="impar" value="-1">Todos</option>'+
                '</select> registros'
        }


    });
});

$(document).on("page:load", function () {

    if (document.getElementById("search")) {
        //alert("I'm LOADING... alive ...and ready");
        var search_form = $('#searching');
        var formulario = document.getElementById('searchin');
        //formulario.reload();
        //

        if (!window.location.hash) {
            window.location = window.location + '#searching';
            //search_form.reload();
            //search_form.submit();
            //
            //formulario.location.reload();
            window.location.reload();
            //var caja = document.getElementById('search').focus();
        }

    }

});


$(document).on("page:load ready", function () {
    $('input[type=text].has_to_be_uppercase').blur(function () {
        $(this).val($(this).val().toUpperCase());
    });
    $('textarea.has_to_be_uppercase').blur(function () {
        $(this).val($(this).val().toUpperCase());
    });



    $('#concept_amount').on('change', function () {
        change_total_amount();
    });
    $('#concept_tax').on('change', function () {
        change_total_amount();
    });

    function change_total_amount() {
        var impuesto_aplicable = 0.12;
        var amount = parseFloat($('#concept_amount').val());
        var ponderacion = 1.0;
        var tax = $('#concept_tax');
        if (tax.is(':checked')) {
            ponderacion += impuesto_aplicable;
        }
        var total = parseFloat((amount * ponderacion).toFixed(0)).toFixed(2);
        console.log(amount, total);
        $('#concept_total_amount').val(total);
        return true;
    }
    ;
});



jQuery(function () {
    $('.import_results').dataTable({
        "iDisplayLength": 10,
        //Establece tipo de paginación "full numbers"
        "sPaginationType": "full_numbers",
        //Cambio de etiquetas de lenguage a español
        "oLanguage": {
            "sZeroRecords": "No hay datos con la información de búsqueda.",
            "oAria": {
                "sSortAscending": " - click/enter para ordenar ascendente",
                "sSortDescending": " - click/enter para ordenar descendente"
            },
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "sEmptyTable": "No hay datos con la información proporcionada.",
            "sInfo": "Total _TOTAL_ registros, mostrando (_START_ a _END_)",
            "sInfoEmpty": "No hay registros que mostrar",
            "sInfoFiltered": " - filtrado desde _MAX_ registros",
            "sSearch": "Buscar en toda la tabla:",
            //Parámetros del menú de nuúmero de elementos por página
            "sLengthMenu": 'Mostrar <select>' +
                '<option class="impar" value="5">5</option>' +
                '<option value="10">10</option>' +
                '<option class="impar" value="12">12</option>' +
                //'<option value="50">50</option>'+
                //'<option class="impar" value="-1">Todos</option>'+
                '</select> registros'
        }
    });

});