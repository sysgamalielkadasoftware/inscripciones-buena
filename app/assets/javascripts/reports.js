var asInitVals = new Array();
var table;
var table2;
var table3;
//solucionado error
jQuery(function() {
 //DataTable de asignaturas para hacer la interfaz mas amigable
  table = $('table#reporte_table').dataTable({
    //Numero de registros inicial por página
    "iDisplayLength": 10,
    //Deshabilita columnas que no son ordenables
    //"aoColumnDefs": [{
    //  "bSortable": false,
    //  "aTargets": [7]
    //}],
    // Establece el ancho de las columnas.
    "aoColumns": [
      {"sWidth": "15%"}, //0
      {"sWidth": "15%"}, //1
      {"sWidth": "3%"}, //2
      {"sWidth": "3%"}, //3
      {"sWidth": "30%"}, 
      {"sWidth": "6%"}, //5
      {"sWidth": "3%"}, 
      {"sWidth": "10%"}
    ],
    //Establece tipo de paginación "full numbers"
    "sPaginationType": "full_numbers",
    //Cambio de etiquetas de lenguage a español
    "oLanguage": {
      "sZeroRecords": "No hay datos con la información de búsqueda.",
      "oAria": {
        "sSortAscending": " - click/enter para ordenar ascendente",
        "sSortDescending": " - click/enter para ordenar descendente"
      },
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },
      "sEmptyTable": "No hay datos con la información proporcionada.",
      "sInfo": "Total _TOTAL_ registros, mostrando (_START_ a _END_)",
      "sInfoEmpty": "No hay registros que mostrar",
      "sInfoFiltered": " - filtrado desde _MAX_ registros",
      "sSearch": "Buscar en toda la tabla:" ,
      //Parámetros del menú de nuúmero de elementos por página
      "sLengthMenu": 'Mostrar <select>'+
    '<option class="impar" value="5">5</option>'+
    '<option value="10">10</option>'+
    '<option value="12">12</option>'+
    //'<option value="50">50</option>'+
    //'<option class="impar" value="-1">Todos</option>'+
    '</select> registros'
    }
  });

 table3 = $('table#reporte_table_folio').dataTable({
    //Numero de registros inicial por página
    "iDisplayLength": 10,
    //Deshabilita columnas que no son ordenables
    //"aoColumnDefs": [{
    //  "bSortable": false,
    //  "aTargets": [7]
    //}],
    // Establece el ancho de las columnas.
    "aoColumns": [
      {"sWidth": "15%"}, //0
      {"sWidth": "15%"}, //1
      {"sWidth": "3%"}, //2
      {"sWidth": "3%"}, //3
      {"sWidth": "30%"}, 
      {"sWidth": "6%"}, //5
      {"sWidth": "3%"}, 
      {"sWidth": "10%"},
      {"sWidth": "3%"}
    ],
    //Establece tipo de paginación "full numbers"
    "sPaginationType": "full_numbers",
    //Cambio de etiquetas de lenguage a español
    "oLanguage": {
      "sZeroRecords": "No hay datos con la información de búsqueda.",
      "oAria": {
        "sSortAscending": " - click/enter para ordenar ascendente",
        "sSortDescending": " - click/enter para ordenar descendente"
      },
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },
      "sEmptyTable": "No hay datos con la información proporcionada.",
      "sInfo": "Total _TOTAL_ registros, mostrando (_START_ a _END_)",
      "sInfoEmpty": "No hay registros que mostrar",
      "sInfoFiltered": " - filtrado desde _MAX_ registros",
      "sSearch": "Buscar en toda la tabla:" ,
      //Parámetros del menú de nuúmero de elementos por página
      "sLengthMenu": 'Mostrar <select>'+
    '<option class="impar" value="5">5</option>'+
    '<option value="10">10</option>'+
    '<option value="12">12</option>'+
    //'<option value="50">50</option>'+
    //'<option class="impar" value="-1">Todos</option>'+
    '</select> registros'
    }
  });
  
  
  table2 = $('table#reporte_table_types').dataTable({
    //Numero de registros inicial por página
    "iDisplayLength": 10,
    //Deshabilita columnas que no son ordenables
    //"aoColumnDefs": [{
    //  "bSortable": false,
    //  "aTargets": [7]
    //}],
    // Establece el ancho de las columnas.
    "aoColumns": [
      {"sWidth": "3%"}, //0
      {"sWidth": "15%"}, //1
      {"sWidth": "6%"}, //2
      {"sWidth": "5%"}, //3
      {"sWidth": "5%"}, 
      {"sWidth": "3%"}, //5
      {"sWidth": "5%"}, 
      {"sWidth": "5%"}, 
      {"sWidth": "10%"},
      {"sWidth": "6%"}
    ],
    //Establece tipo de paginación "full numbers"
    "sPaginationType": "full_numbers",
    //Cambio de etiquetas de lenguage a español
    "oLanguage": {
      "sZeroRecords": "No hay datos con la información de búsqueda.",
      "oAria": {
        "sSortAscending": " - click/enter para ordenar ascendente",
        "sSortDescending": " - click/enter para ordenar descendente"
      },
      "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
      },
      "sEmptyTable": "No hay datos con la información proporcionada.",
      "sInfo": "Total _TOTAL_ registros, mostrando (_START_ a _END_)",
      "sInfoEmpty": "No hay registros que mostrar",
      "sInfoFiltered": " - filtrado desde _MAX_ registros",
      "sSearch": "Buscar en toda la tabla:",
      //Parámetros del menú de nuúmero de elementos por página
      "sLengthMenu": 'Mostrar <select>'+
    '<option class="impar" value="5">5</option>'+
    '<option value="10">10</option>'+
    '<option class="impar" value="12">12</option>'+
    //'<option value="50">50</option>'+
    //'<option class="impar" value="-1">Todos</option>'+
    '</select> registros'
    }
  });

  $("tfoot input").keyup(function(){
    /* Filter on the column (the index) of this element */
    oTable.fnFilter( this.value, $("tfoot input").index(this) );
  });

  /** Support functions to provide a little bit of 'user friendlyness'
   *to the textboxes in the footer
   **/
  $("tfoot input").each(function(i){
    asInitVals[i] = this.value;
  });

  $("tfoot input").focus(function(){
    if(this.className == "search_init"){
      this.className = "";
      this.value = "";
    }
  });

  $("tfoot input").blur(function(i){
    if(this.value == ""){
      this.className = "search_init";
      this.value = asInitVals[$("tfoot input").index(this)];
    }
  });

});







jQuery(function($   ) {
    
    $(document).on("page:load ready", function(){
        $("select#group_report_carrera_id").on("change", function(){
          update_group();
        });
        $("select#group_report_ciclo_id").on("change", function(){
          update_group();
        });
      
    });
function update_group(){
            var carrera = $("select#group_report_carrera_id :selected").val();
            var ciclo = $('select#group_report_ciclo_id :selected').val();
            var mensaje = "";


            $("select#group_report_grupo_id option[value='']").attr("selected", true);


            if(carrera == "")
              mensaje = "Seleccione la carrera.\n";
            if(ciclo == "")
              mensaje += "Seleccione el periodo.";
            if(carrera == "" || ciclo == ""){
              alert(mensaje);
              $("select#group_report_carrera_id option[value='']").attr("selected", true);
            }
            else{
              jQuery.get('/reports/update_group_select', {carrera_id: carrera, ciclo_id: ciclo}, function(data){
                $("div#grupo_select_div").html(data);
              });
            }
          }    

 
    
});

jQuery(function($   ) {
    $(document).on("page:load ready", function(){
        $("select#inscritos_report_carrera_id").on("change", function(){
            update_group_inscritos();
        });
    });
    function update_group_inscritos(){
        var carrera = $("select#inscritos_report_carrera_id :selected").val();
        var mensaje = "";

        $("select#group_report_grupo_id option[value='']").attr("selected", true);

        if(carrera == "")
            mensaje = "Seleccione la carrera.\n";
        if(carrera == ""){
            alert(mensaje);
            $("select#inscritos_report_carrera_id option[value='']").attr("selected", true);
        }
        else{
            jQuery.get('/reports/update_group_inscritos_select', {carrera_id: carrera}, function(data){
                $("div#grupo_select_div").html(data);
            });
        }
    }
});