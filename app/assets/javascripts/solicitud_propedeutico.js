// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
$(function () {
    $("select#carrera_id").change(function(){
        var carrera_id = $("select#carrera_id").val();

        if(carrera_id != ''){
            $.get('/solicitud_propedeutico/search_data_aspirantes_request', {
                carrera_id: carrera_id
            }, function(data){
                $("input#carrera_nombre").val(data);
            });

            $.get('/solicitud_propedeutico/search_data_tipo_request', {
                carrera_id: carrera_id
            }, function(data){
                $("input#periodo").val(data);
            });

            $.get('/solicitud_propedeutico/search_monto_request', {
                carrera_id: carrera_id
            }, function(data){
                $("input#precio").val(data);
            });

            $.get('/solicitud_propedeutico/search_concepto_request', {
                carrera_id: carrera_id
            }, function(data){
                $("input#concepto").val(data);
            });
        }
        else{
            $("input#carrera_nombre").val("");
            $("input#periodo").val("");
            alert('Debes seleccionar una opción.')
        }

    })
});