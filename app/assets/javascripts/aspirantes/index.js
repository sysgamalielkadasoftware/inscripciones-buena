const configuracion_datepicker = {
    format:'YYYY-MM-DD',
    widgetPositioning: { horizontal: 'left', vertical: 'bottom' },
    // dayOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
    // months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    // monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    icons: {
        previous: 'fa fa-chevron-circle-left',
        next: 'fa fa-chevron-circle-right'
    }};


$(function(){
    /** INICIO: DatePickers para anio inicio y fin  en nuevo aspirante */
    $("#dato_academico_año_inicio").datetimepicker(configuracion_datepicker);
    $("#dato_academico_año_fin").datetimepicker(configuracion_datepicker);
    /** FIN: DatePickers para anio inicio y fin  ficha en nuevo aspirante   */
   

    /***** Elementos de la pestaña del aspirante *****/
    $("#extranjero").click(function() {
        if($("#extranjero").is(':checked')) {
            $('label#etiquetaEstado').css("display", "none");
            $('select#estado_id').css("display", "none");
            $('label#etiquetaMunicipio').css("display", "none");
            $('select#municipio_id').css("display", "none");
            $('label#etiquetaLocalidad').css("display", "none");
            $('select#localidad_id').css("display", "none");
            $('#pais_id').removeAttr('disabled');
            $("select#estado_id").prop('required',false);
            $("select#municipio_id").prop('required',false);
            $("select#localidad_id").prop('required',false);
            $("input#aspirante_dato_personal_attributes_nacionalidad").val("EXTRANJERA");
        } else {
            $('label#etiquetaEstado').css("display", "block");
            $('select#estado_id').css("display", "block");
            $('label#etiquetaMunicipio').css("display", "block");
            $('select#municipio_id').css("display", "block");
            $('label#etiquetaLocalidad').css("display", "block");
            $('select#localidad_id').css("display", "block");
            $("#pais_id").attr('disabled','disabled');
            $('#pais_id option').filter(function () { return $(this).html() == "MÉXICO"; }).prop("selected", true);
            pais = $("select#pais_id :selected").val();
            $("#pais_key").val(pais);
            $("#direccion_pais").val(pais);

            /* añadir require */
            $("select#estado_id").attr("required", true);
            $("select#municipio_id").attr("required", true);
            $("select#localidad_id").attr("required", true);


            $("input#aspirante_dato_personal_attributes_nacionalidad").val("MEXICANA");
        }
    });
    $("select#pais_id").on("change", function () {
        pais = $("select#pais_id :selected").val();
        $("#direccion_pais").val(pais);
    });
    /***** Elementos de la pestaña del aspirante *****/

    /***** Elementos de la pestaña del padre *****/
    $("#extranjero_padre").click(function() {
        if($("#extranjero_padre").is(':checked')) {
            $('label#etiquetaEstadoPadre').css("display", "none");
            $('select#estado_padre_id').css("display", "none");
            $('label#etiquetaMunicipioPadre').css("display", "none");
            $('select#municipio_padre_id').css("display", "none");
            $('label#etiquetaLocalidadPadre').css("display", "none");
            $('select#localidad_padre_id').css("display", "none");
            $('#pais_padre_id').removeAttr('disabled');
        } else {
            $('label#etiquetaEstadoPadre').css("display", "block");
            $('select#estado_padre_id').css("display", "block");
            $('label#etiquetaMunicipioPadre').css("display", "block");
            $('select#municipio_padre_id').css("display", "block");
            $('label#etiquetaLocalidadPadre').css("display", "block");
            $('select#localidad_padre_id').css("display", "block");
            $("#pais_padre_id").attr('disabled','disabled');
            $('#pais_padre_id option').filter(function () { return $(this).html() == "MÉXICO"; }).prop("selected", true);
            pais = $("select#pais_padre_id :selected").val();
            $("#familiar_pais_padre").val(pais);

            /* añadir require */
        }
    });
    $("select#pais_padre_id").on("change", function () {
        pais = $("select#pais_padre_id :selected").val();
        $("#familiar_pais_padre").val(pais);
    });
    $("#copiar_datos_hijoI").click(function() {
        if($("#copiar_datos_hijoI").is(':checked')) {
            $("#copiar_datos_hijoI").val(1);
            pais_aspirante = $("select#pais_id :selected").val();
            estado_aspirante = $("select#estado_id :selected").val();
            municipio_aspirante = $("select#municipio_id :selected").val();
            localidad_aspirante = $("select#localidad_id :selected").val();

            $("input#familiar_calle_padre").val($("input#direccion_calle").val());
            $("input#familiar_numero_padre").val($("input#direccion_numero").val());
            $("input#familiar_colonia_padre").val($("input#direccion_colonia").val());
            $("input#familiar_codigo_postal_padre").val($("input#direccion_codigo_postal").val());

            if($("#extranjero").is(':checked')) {
                document.getElementById("extranjero_padre").checked = true;
                $('label#etiquetaEstadoPadre').css("display", "none");
                $('select#estado_padre_id').css("display", "none");
                $('label#etiquetaMunicipioPadre').css("display", "none");
                $('select#municipio_padre_id').css("display", "none");
                $('label#etiquetaLocalidadPadre').css("display", "none");
                $('select#localidad_padre_id').css("display", "none");
                $('#pais_padre_id').removeAttr('disabled');

                $("select#pais_padre_id").val(pais_aspirante);
            } else {
                $('label#etiquetaEstadoPadre').css("display", "block");
                $('select#estado_padre_id').css("display", "block");
                $('label#etiquetaMunicipioPadre').css("display", "block");
                $('select#municipio_padre_id').css("display", "block");
                $('label#etiquetaLocalidadPadre').css("display", "block");
                $('select#localidad_padre_id').css("display", "block");
                $("#pais_padre_id").attr('disabled','disabled');
                $('#pais_padre_id option').filter(function () { return $(this).html() == "MÉXICO"; }).prop("selected", true);
                pais = $("select#pais_padre_id :selected").val();
                $("#familiar_pais_padre").val(pais);

                /* añadir require */

                document.getElementById("extranjero_padre").checked = false;
                municipios = document.getElementById('municipio_id');
                towns_options = municipios.innerHTML;

                municipios_padre = document.getElementById('municipio_padre_id');
                municipios_padre.innerHTML = towns_options;

                localidades = document.getElementById('localidad_id');
                localities_options = localidades.innerHTML;

                localidades_padre = document.getElementById('localidad_padre_id');
                localidades_padre.innerHTML = localities_options;

                $("select#estado_padre_id").val(estado_aspirante);
                $("select#municipio_padre_id").val(municipio_aspirante);
                $("select#localidad_padre_id").val(localidad_aspirante);
            }
        } else { 
            $("#copiar_datos_hijoI").val(0);
        }
    });
    /***** Elementos de la pestaña del padre *****/

    /***** Elementos de la pestaña de la madre *****/
    $("#extranjero_madre").click(function() {
        if($("#extranjero_madre").is(':checked')) {
            $('label#etiquetaEstadoMadre').css("display", "none");
            $('select#estado_madre_id').css("display", "none");
            $('label#etiquetaMunicipioMadre').css("display", "none");
            $('select#municipio_madre_id').css("display", "none");
            $('label#etiquetaLocalidadMadre').css("display", "none");
            $('select#localidad_madre_id').css("display", "none");
            $('#pais_madre_id').removeAttr('disabled');
            $("select#estado_madre_id").prop('required',false);
            $("select#municipio_madre_id").prop('required',false);
            $("select#localidad_madre_id").prop('required',false);
        } else {
            $('label#etiquetaEstadoMadre').css("display", "block");
            $('select#estado_madre_id').css("display", "block");
            $('label#etiquetaMunicipioMadre').css("display", "block");
            $('select#municipio_madre_id').css("display", "block");
            $('label#etiquetaLocalidadMadre').css("display", "block");
            $('select#localidad_madre_id').css("display", "block");
            $("#pais_madre_id").attr('disabled','disabled');
            $('#pais_madre_id option').filter(function () { return $(this).html() == "MÉXICO"; }).prop("selected", true);
            pais = $("select#pais_madre_id :selected").val();
            $("#familiar_pais_madre").val(pais);

            /* añadir require */
            $("select#estado_madre_id").attr("required", true);
            $("select#municipio_madre_id").attr("required", true);
            $("select#localidad_madre_id").attr("required", true);
        }
    });
    $("select#pais_madre_id").on("change", function () {
        pais = $("select#pais_madre_id :selected").val();
        $("#familiar_pais_madre").val(pais);
    });
    $("#copiar_datos_hijoII").click(function() {
        if($("#copiar_datos_hijoII").is(':checked')) {
            $("#copiar_datos_hijoII").val(1);
            pais_aspirante = $("select#pais_id :selected").val();
            estado_aspirante = $("select#estado_id :selected").val();
            municipio_aspirante = $("select#municipio_id :selected").val();
            localidad_aspirante = $("select#localidad_id :selected").val();

            $("input#familiar_calle_madre").val($("input#direccion_calle").val());
            $("input#familiar_numero_madre").val($("input#direccion_numero").val());
            $("input#familiar_colonia_madre").val($("input#direccion_colonia").val());
            $("input#familiar_codigo_postal_madre").val($("input#direccion_codigo_postal").val());

            if($("#extranjero").is(':checked')) {
                document.getElementById("extranjero_madre").checked = true;
                $('label#etiquetaEstadoMadre').css("display", "none");
                $('select#estado_madre_id').css("display", "none");
                $('label#etiquetaMunicipioMadre').css("display", "none");
                $('select#municipio_madre_id').css("display", "none");
                $('label#etiquetaLocalidadMadre').css("display", "none");
                $('select#localidad_madre_id').css("display", "none");
                $('#pais_madre_id').removeAttr('disabled');
                $("select#estado_madre_id").prop('required',false);
                $("select#municipio_madre_id").prop('required',false);
                $("select#localidad_madre_id").prop('required',false);

                $("select#pais_madre_id").val(pais_aspirante);
            } else {
                $('label#etiquetaEstadoMadre').css("display", "block");
                $('select#estado_madre_id').css("display", "block");
                $('label#etiquetaMunicipioMadre').css("display", "block");
                $('select#municipio_madre_id').css("display", "block");
                $('label#etiquetaLocalidadMadre').css("display", "block");
                $('select#localidad_madre_id').css("display", "block");
                $("#pais_madre_id").attr('disabled','disabled');
                $('#pais_madre_id option').filter(function () { return $(this).html() == "MÉXICO"; }).prop("selected", true);
                pais = $("select#pais_madre_id :selected").val();
                $("#familiar_pais_madre").val(pais);

                /* añadir require */
                $("select#estado_madre_id").attr("required", true);
                $("select#municipio_madre_id").attr("required", true);
                $("select#localidad_madre_id").attr("required", true);

                document.getElementById("extranjero_madre").checked = false;
                municipios = document.getElementById('municipio_id');
                towns_options = municipios.innerHTML;

                municipios_madre = document.getElementById('municipio_madre_id');
                municipios_madre.innerHTML = towns_options;

                localidades = document.getElementById('localidad_id');
                localities_options = localidades.innerHTML;

                localidades_madre = document.getElementById('localidad_madre_id');
                localidades_madre.innerHTML = localities_options;

                $("select#estado_madre_id").val(estado_aspirante);
                $("select#municipio_madre_id").val(municipio_aspirante);
                $("select#localidad_madre_id").val(localidad_aspirante);
            }
        } else{
            $("#copiar_datos_hijoII").val(0);
        }
    });
    /***** Elementos de la pestaña de la madre *****/

    /***** Elementos de la pestaña del tutor *****/
    $("#extranjero_tutor").click(function() {
        if($("#extranjero_tutor").is(':checked')) {
            $('label#etiquetaEstadoTutor').css("display", "none");
            $('select#estado_tutor_id').css("display", "none");
            $('label#etiquetaMunicipioTutor').css("display", "none");
            $('select#municipio_tutor_id').css("display", "none");
            $('label#etiquetaLocalidadTutor').css("display", "none");
            $('select#localidad_tutor_id').css("display", "none");
            $('#pais_tutor_id').removeAttr('disabled');
            $("select#estado_tutor_id").prop('required',false);
            $("select#municipio_tutor_id").prop('required',false);
            $("select#localidad_tutor_id").prop('required',false);
        } else {
            $('label#etiquetaEstadoTutor').css("display", "block");
            $('select#estado_tutor_id').css("display", "block");
            $('label#etiquetaMunicipioTutor').css("display", "block");
            $('select#municipio_tutor_id').css("display", "block");
            $('label#etiquetaLocalidadTutor').css("display", "block");
            $('select#localidad_tutor_id').css("display", "block");
            $("#pais_tutor_id").attr('disabled','disabled');
            $('#pais_tutor_id option').filter(function () { return $(this).html() == "MÉXICO"; }).prop("selected", true);
            pais = $("select#pais_tutor_id :selected").val();
            $("#familiar_pais_tutor").val(pais);

            /* añadir require */
            $("select#estado_tutor_id").attr("required", true);
            $("select#municipio_tutor_id").attr("required", true);
            $("select#localidad_tutor_id").attr("required", true);
        }
    });
    $("select#pais_tutor_id").on("change", function () {
        pais = $("select#pais_tutor_id :selected").val();
        $("#familiar_pais_tutor").val(pais);
    });
    $("#copyPadre").click(function() {
        copiar_datos_padre();
    });
    $("#copyMadre").click(function() {
        copiar_datos_madre();
    });
    /***** Elementos de la pestaña del tutor *****/

    $('#dato_academico_fecha_inicio').datetimepicker({
        format:'DD/MM/YYYY',
        icons: {
            previous: 'fa fa-chevron-circle-left',
            next: 'fa fa-chevron-circle-right'
        }
    });
    $('#dato_academico_fecha_fin').datetimepicker({
        format:'DD/MM/YYYY',
        icons: {
            previous: 'fa fa-chevron-circle-left',
            next: 'fa fa-chevron-circle-right'
        }
    });

    let datosAsp = false;
    let datosPer = false;
    let datosLen = false;
    let datosAca = false;
    let datosMed = false;
    let datosPad = false;
    let datosTut = false;
    //document.getElementById("id_guardar").disabled = true;
    function on(){
        console.log("Hemos pulsado en on");
    }

    //var checkbox_finado_padre = document.getElementById('finado_padre');
    //var checkbox_finado_madre = document.getElementById('finado_madre');
    //var finado_padre =  false;
    //var finado_madre =  false;
    var checkbox = document.getElementById('finado_padre');


    /*
        checkbox.addEventListener("change", comprueba, false);

        function comprueba(){
            if(checkbox.checked){
                console.log("si entro pues ");
            }else{
                console.log("no entro pues ");
            }
        }*/

    // checkbox_finado_padre.addEventListener( 'change', function() {
    /*
    if(this.checked) {
        console.log("Entro al finado padre");
        $("#familiar_ocupacion_padre").prop("disabled", true);
        $("#familiar_telefono_padre").prop("disabled", true);
        $("#estado_padre_id").prop("disabled", true);
        $("#municipio_padre_id").prop("disabled", true);
        $("#localidad_padre_id").prop("disabled", true);
        $("#familiar_calle_padre").prop("disabled", true);
        $("#familiar_numero_padre").prop("disabled", true);
        $("#familiar_colonia_padre").prop("disabled", true);
        $("#familiar_codigo_postal_padre").prop("disabled", true);
        $("#copiar_datos_hijoI").prop("disabled", true);
        finado_padre = true;
    }else {
        $("#familiar_ocupacion_padre").prop("disabled", false);
        $("#familiar_telefono_padre").prop("disabled", false);
        $("#estado_padre_id").prop("disabled", false);
        $("#municipio_padre_id").prop("disabled", false);
        $("#localidad_padre_id").prop("disabled", false);
        $("#familiar_calle_padre").prop("disabled", false);
        $("#familiar_numero_padre").prop("disabled", false);
        $("#familiar_colonia_padre").prop("disabled", false);
        $("#familiar_codigo_postal_padre").prop("disabled", false);
        $("#copiar_datos_hijoI").prop("disabled", false);
        finado_padre = false;
    }*/
    //});
    //checkbox_finado_madre.addEventListener( 'change', function() {
    /*
    if(this.checked) {
        console.log("Entro al finado padre");
        $("#familiar_ocupacion_madre").prop("disabled", true);
        $("#familiar_telefono_madre").prop("disabled", true);
        $("#estado_madre_id").prop("disabled", true);
        $("#municipio_madre_id").prop("disabled", true);
        $("#localidad_madre_id").prop("disabled", true);
        $("#familiar_calle_madre").prop("disabled", true);
        $("#familiar_numero_madre").prop("disabled", true);
        $("#familiar_colonia_madre").prop("disabled", true);
        $("#familiar_codigo_postal_madre").prop("disabled", true);
        $("#copiar_datos_hijoII").prop("disabled", true);
        finado_madre =  true;
    }else {
        $("#familiar_ocupacion_madre").prop("disabled", false);
        $("#familiar_telefono_madre").prop("disabled", false);
        $("#estado_madre_id").prop("disabled", false);
        $("#municipio_madre_id").prop("disabled", false);
        $("#localidad_madre_id").prop("disabled", false);
        $("#familiar_calle_madre").prop("disabled", false);
        $("#familiar_numero_madre").prop("disabled", false);
        $("#familiar_colonia_madre").prop("disabled", false);
        $("#familiar_codigo_postal_madre").prop("disabled", false);
        $("#copiar_datos_hijoII").prop("disabled", false);
        finado_madre =  false;
    }*/
    //});


    /**Validación tag Datos aspirantes**/
    $("#button_aspirante").click(function() {
        var  nombre = document.getElementById('aspirante_nombre').value;
        var  ap = document.getElementById('aspirante_apellido_paterno').value;
        var  am = document.getElementById('aspirante_apellido_materno').value;
        //var  product = document.getElementById('aspirante_nombre').value;
        var eno,eap,eam;

        eno=espacios_cadena(nombre);
        eap=espacios_cadena(ap);
        eam=espacios_cadena(am);

        if (nombre == "") {
            alert("No has rellenado el campo nombre");
        }else if(eno == 1 ){
            alert("Existen mas de dos espacios seguidos en el campo nombre o el campo solo contiene espacios")
        }else if(ap == "" ){
            alert("No has rellenado el campo apellido paterno");
        }else if(eap == 1 ){
            alert("Existen mas de dos espacios seguidos en el campo apellido paterno o el campo solo contiene espacios")
        }else if(am == ""){
            alert("No has rellenado el campo apellido materno");
        }else if(eam == 1 ){
            alert("Existen mas de dos espacios seguidos en el campo apellido materno o el campo solo contiene espacios")
        }else if(am == ""){
        }else{
            if(valida_guardar()) {
                alert("Has rellenado todos los campos satisfactoriamente.");
            }
            else {
                if (datosPer == false) {
                    alert("Has rellenado todos los campos satisfactoriamente, continua con Datos Personales");
                }
                else{
                    alert("Has rellenado todos los campos satisfactoriamente, continua con los otros datos");
                }
            }
            datosAsp = true;
            return false;

            //$('#dos').removeAttr('disabled');
            //document.getElementById("myText").disabled = true
            //$('#uno a').attr('disabled', true);<
            //document.getElementById('dos').href = '#'
            //document.getElementById('un').href = '#';
        }

        return false;
    });
    $("#submit_button").click(function() {
        var  username = document.getElementById('usuario_username').value;
        var  pass = document.getElementById('usuario_password').value;


        if(username == ""){
            document.getElementById("mensaje").style.display = "block";
            return false;
        }else   if(pass == "" ){
            document.getElementById("mensaje").style.display = "block";
            return false;
        }
        else
        {
            document.getElementById("mensaje").style.display = "none";
        }
    });
    function espacios_cadena(cadena){
        var e=0;
        for(var i = 0; i<cadena.length; i++){
            if(cadena[i] == " " && cadena[i+1]==" "){
                i=cadena.length;
                e=1;
            }else if(cadena.length == 1 && cadena[i]==" "){
                i=cadena.length;
                e=1;
            }
        }
        return e;
    }

    /**Validación datos personales**/
    $("#datos_personales_id").click(function() {
        var  lugar_nacimiento = document.getElementById('aspirante_dato_personal_attributes_lugar_nacimiento').value;
        var  nss = document.getElementById('aspirante_dato_personal_attributes_nss').value;
        var  telefono = document.getElementById('aspirante_dato_personal_attributes_telefono').value;
        var  estado_civil = document.getElementById('aspirante_dato_personal_attributes_estado_civil').value;
        var  calle = document.getElementById('direccion_calle').value;
        var  numero = document.getElementById('direccion_numero').value;
        var  colonia = document.getElementById('direccion_colonia').value;
        var  cp = document.getElementById('direccion_codigo_postal').value;
        var  estado = document.getElementById("estado_id").value;
        var  municipio = document.getElementById("municipio_id").value;
        var  localidad = document.getElementById("localidad_id").value;
        var bandNSS = false;
        var bandNumero = false;
        var eln,enss,etel,ecalle,enume,eco,ecp;
        if(nss === "NINGUNO" || nss.length==11) {
            var img = document.getElementById("x");
            valor = img.attributes[0].value;
            if (valor === "acep") {
                bandNSS = true
            }
        }
        eln=espacios_cadena(lugar_nacimiento);
        enss=espacios_cadena(nss);
        etel=espacios_cadena(telefono);
        ecalle=espacios_cadena(calle);
        enume=espacios_cadena(numero);
        eco=espacios_cadena(colonia);
        ecp=espacios_cadena(cp);



        if(isNaN(numero)== false || numero === "S/N"  || numero === "s/n"){
            bandNumero = true;
        }

        var  extranjero=false;
        //Si está seleccionado muestra su value

        if($("#extranjero").is(':checked')) {
            var  extranjero=true;
        }

        if (lugar_nacimiento == "") {
            alert("No has rellenado el campo lugar de nacimiento");
        }else if(eln){
            alert("Existen mas de dos espacios seguidos en el campo Lugar de nacimiento o el campo solo contiene espacios")
        }else if(nss == "" || bandNSS == false){
            alert("El campo NSS, debe contener 11 digitos si no cuenta con el, colocar NINGUNO.");
        }else if(enss){
            alert("Existen mas de dos espacios seguidos en el campo NSS o el campo solo contiene espacios")
        }else if(telefono == "" || (!isNaN(telefono) && (telefono.length < 10 || telefono.length > 10)) || (isNaN(telefono) && (telefono.toLowerCase() !== "ninguno"))){
            alert("El número de teléfono debe contener 10 digitos o escribir 'NINGUNO'");
            return false;
        }else if(etel){
            alert("Existen mas de dos espacios seguidos en el campo telefono o el campo solo contiene espacios")
        }else if(estado_civil ==  ""){
            alert("No has seleccionado el campo estado civil");
        }else if(calle == ""){
            alert("No has rellenado el campo Calle");
        }else if(ecalle){
            alert("Existen mas de dos espacios seguidos en el campo calle o el campo solo contiene espacios")
        }else if(numero == "" ){
            alert("No has rellenado el campo Número, si su domicilio no cuenta con número colocar S/N");
        }else if(enume){
            alert("Existen mas de dos espacios seguidos en el campo numero o el campo solo contiene espacios")
        }else if(colonia == ""){
            alert("No has rellenado el campo Colonia");
        }else if(eco){
            alert("Existen mas de dos espacios seguidos en el campo colonia o el campo solo contiene espacios")
        }else if(cp == "" || cp.length < 5 || cp.length >5){
            alert("El campo Codigo Postal, debe contener 5 digitos");
        }else if(ecp){
            alert("Existen mas de dos espacios seguidos en el campo codigo postal o el campo solo contiene espacios")
        }else if(estado == "" && extranjero==false){
            alert("No has seleccionado un Estado ");
        }else if(municipio == "" && extranjero==false){
            alert("No has seleccionado un Municipio");
        }else if(localidad == "" && extranjero==false){
            alert("No has seleccionado una Localidad");
        }else{
            if(valida_guardar()) {
                alert("Has rellenado todos los campos satisfactoriamente.");
            }
            else {
                if (datosLen == false) {
                    alert("Has rellenado todos los campos satisfactoriamente, continua con Datos Lenguas Indigenas");
                }
                else{
                    alert("Has rellenado todos los campos satisfactoriamente, continua con los otros datos");
                }
            }
            datosPer = true;
            return false;
        }
        return false;
    });


    /** Validación lenguas indigenas**/
    $("#button_lenguas_indigenas").click(function() {
        var check = null;
        $("input:checkbox:checked").each(function() {
            check = $(this).val();
        });
        if(check !=null){
            if(valida_guardar()) {
                alert("Has rellenado todos los campos satisfactoriamente.");
            }
            else{
                if(datosAca == false){
                    alert("Has rellenado todos los campos satisfactoriamente, continua con Datos Académicos");
                }
                else{
                    alert("Has rellenado todos los campos satisfactoriamente, continua con los otros datos");
                }
            }
            datosLen = true;
            return false;
        }else{
            alert("Si no habla alguna lengua indigena seleccione la opción NINGUNA");
        }
        return false;
    });

    /** Validación de datos medicos**/
    $("#button_datos_medicos").click(function() {
        var  tipoSangre = document.getElementById('dato_medico_tipo_sangre_id').value;
        var  discapacidad = document.getElementById('dato_medico_discapacidad_id').value;
        var  enfermedadEspecial = document.getElementById('dato_medico_enfermedad_atencion_especial').value;
        var  medEnfEspecial = document.getElementById('dato_medico_medicamento_enfermedad_atencion_especial').value;
        var  alergia = document.getElementById('dato_medico_alergia').value;
        var  medAlergia = document.getElementById('dato_medico_medicamento_alergia').value;

        var enom,emedesp, ealnom, ealmed;

        var enom=espacios_cadena(enfermedadEspecial)
        var emedesp=espacios_cadena(medEnfEspecial)
        var ealnom=espacios_cadena(alergia)
        var ealmed=espacios_cadena(medAlergia)


        if(tipoSangre == ""){
            alert("No has seleccionado el tipo de Sangre");
        }else if(discapacidad == ""){
            alert("No has seleccionado la Discapacidad");
        }else if(enfermedadEspecial == ""){
            alert("En caso de no tener alguna enfermedad rellenar el campo con la palabra \"NINGUNA\" ");
        }else if(enom){
            alert("Existen mas de dos espacios seguidos en el campo nombre o el campo solo contiene espacios ");
        }else if(medEnfEspecial == ""){
            alert("En caso de no tomar algun medicamento rellenar el campo con la palabra \"NINGUNA\" ");
        }else if(emedesp){
            alert("Existen mas de dos espacios seguidos en el campo medicamento o el campo solo contiene espacios ");
        }else if(alergia == ""){
            alert("En caso de no tener alguna alergia rellenar el campo con la palabra \"NINGUNA\" ");
        }else if(ealnom){
            alert("Existen mas de dos espacios seguidos en el campo nombre  o el campo solo contiene espacios ");
        }else if(medAlergia == ""){
            alert("En caso de no tomar algun medicamento para alergia rellenar el campo con la palabra \"NINGUNA\" ");
        }else if(ealmed){
            alert("Existen mas de dos espacios seguidos en el campo medicamento  o el campo solo contiene espacios ");
        }else{
            if(valida_guardar()) {
                alert("Has rellenado todos los campos satisfactoriamente.");
            }
            else{
                if(datosPad == false){
                    alert("Has rellenado todos los campos satisfactoriamente, continua con Datos de los Padres");
                }
                else{
                    alert("Has rellenado todos los campos satisfactoriamente, continua con los otros datos");
                }
            }
            datosMed = true;
            return false;
        }
        return false;
    });

    /** Validación antedecedentes academicos **/
    $("#button_antecedente_academico").click(function() {
        var  escuelaDeProcedencia = document.getElementById('dato_academico_escuela_procedente_id').value;
        var  anioInicio = document.getElementById('dato_academico_año_inicio').value;
        var  anioFin = document.getElementById('dato_academico_año_fin').value;
        var  promedio = document.getElementById('dato_academico_promedio').value;
        var  areaDeConocimiento = document.getElementById('dato_academico_area_conocimiento_id').value;
        var  especialidad = document.getElementById('dato_academico_especialidad').value;
        var  band=false;
        var esp=espacios_cadena(especialidad)
        
        if(escuelaDeProcedencia != "") {
            var img = document.getElementById("y");
            valor = img.attributes[0].value;
            if (valor == "acep") {
                band = true
            }
        }

        //var  product = document.getElementById('aspirante_nombre').value;
        if (escuelaDeProcedencia == "" || band == false) {
            alert("Debe seleccionar una opción para el campo Escuela de Procedencia, si NO EXISTE su Escuela dentro de las opciones elige la opción OTRA");
       }else if(anioInicio == ""){
            alert("No has selecionado Año de Inicio");
        }else if(anioFin == ""){
            alert("No has selecionado Año de Fin");
        }else if(new Date(anioFin) < new Date(anioInicio)){
            alert("La Fecha de Fin no puede ser menor a la Fecha de Inicio");
        }else if(especialidad == ""){
            alert("No has rellenado el campo Especialidad");
        }else if(esp == 1){
            alert("Existen mas de dos espacios seguidos en el campo especialidad o el campo solo contiene espacios");
        }else if(areaDeConocimiento == ""){
            alert("No has selecionado el Área de Conocimiento");
        }else if(promedio == ""  || promedio.length>10){
            alert("El promedio tiene que ser menor o igual a 10");
        }else{

            if(valida_guardar()) {
                alert("Has rellenado todos los campos satisfactoriamente.");
            }
            else{
                if(datosMed == false){
                    alert("Has rellenado todos los campos satisfactoriamente, continua con Datos Médicos");
                }
                else{
                    alert("Has rellenado todos los campos satisfactoriamente, continua con los otros datos");
                }
            }
            datosAca = true;
            return false;
        }
        return false;
    });
    /**Validacion Datos de los Padres**/
    $("#button_datos_padres").click(function() {
        var  famNombrePadre = document.getElementById('familiar_nombre_padre').value;
        var  famApePatPadre = document.getElementById('familiar_apellido_paterno_padre').value;
        var  famApeMatPadre = document.getElementById('familiar_apellido_materno_padre').value;
        var  ocupacionPadre = document.getElementById('familiar_ocupacion_padre').value;
        var  telefonoPadre = document.getElementById('familiar_telefono_padre').value;

        var  estadoPadre = document.getElementById('estado_padre_id').value;
        var  municipioPadre = document.getElementById('municipio_padre_id').value;
        var  localidadPadre = document.getElementById('localidad_padre_id').value;
        var tutor_padre=document.getElementById('tutor_padre').checked;
        var tutor_madre=document.getElementById('tutor_madre').checked;

        var  callePadre = document.getElementById('familiar_calle_padre').value;
        var  numeroPadre = document.getElementById('familiar_numero_padre').value;
        var  coloniaPadre = document.getElementById('familiar_colonia_padre').value;
        var  cpPadre = document.getElementById('familiar_codigo_postal_padre').value;

        var  famNombreMadre = document.getElementById('familiar_nombre_madre').value;
        var  famApePatMadre = document.getElementById('familiar_apellido_paterno_madre').value;
        var  famApeMatMadre = document.getElementById('familiar_apellido_materno_madre').value;
        var  ocupacionMadre = document.getElementById('familiar_ocupacion_madre').value;
        var  telefonoMadre = document.getElementById('familiar_telefono_madre').value;

        var  estadoMadre = document.getElementById('estado_madre_id').value;
        var  municipioMadre = document.getElementById('municipio_madre_id').value;
        var  localidadMadre = document.getElementById('localidad_madre_id').value;

        var  calleMadre = document.getElementById('familiar_calle_madre').value;
        var  numeroMadre = document.getElementById('familiar_numero_madre').value;
        var  coloniaMadre = document.getElementById('familiar_colonia_madre').value;
        var  cpMadre = document.getElementById('familiar_codigo_postal_madre').value;
        var checkbox_finado_padre = document.getElementById('finado_padre');
        var checkbox_finado_madre = document.getElementById('finado_madre');
        var check_padre = false;
        var check_madre = false;
        var bandP = false;
        var bandM = false;

        //var checkbox = document.getElementById('checkbox');

        //checkbox_finado_padre.addEventListener("change", comprueba, false);

        //function comprueba(){
        //console.log("Entro a la funcion compruieba");

        if( checkbox_finado_padre.checked){
            /*    console.log("Esta sera una nueva funcion para el check de la padre");
                if(famNombrePadre == ""){
                    alert("No has rellenado el campo Nombre del Padreeeeee1");
                }else if(famApePatPadre == ""){
                    alert("No has rellenado el campo Apellido Paterno del Padre");
                }else if(famApeMatPadre == ""){
                    alert("No has rellenado el campo Apellido Materno del Padre");
                }*/
            check_padre = true;
        }

        if(checkbox_finado_madre.checked){
            /*
            console.log("Esta sera una nueva funcion para el check de la madre");
            if (famNombreMadre == ""){
                alert("No has rellenado el campo Nombre de la Madre");
            }else if(famApePatMadre == ""){
                alert("No has rellenado el campo Apellido Paterno de la Madre");
            }else if(famApeMatMadre == ""){
                alert("No has rellenado el campo Apellido Materno de la Madre");
            }else{
                bandP = true;
            }*/
            check_madre = true;
        }
        /*
        if(check_padre || check_madre){
            console.log("se cumpliron las condiciones anteriores");
            //datosPad = true;

            //return true;
        }*/
        //}

        let bandNP = false;
        let bandNM = false;

        if(isNaN(numeroPadre)== false || numeroPadre === "S/N" || numeroPadre === "s/n" || (numeroPadre.toLowerCase()  === "x")){
            bandNP = true;
        }
        if(isNaN(numeroMadre)== false || numeroMadre === "S/N"|| numeroPadre === "s/n"  || (numeroMadre.toLowerCase()  === "x")){
            bandNM = true;
        }
        var  extranjeroPadre=false;
        //Si está seleccionado muestra su value

        if($("#extranjero_padre").is(':checked')) {
            var  extranjeroPadre=true;
        }




        var  extranjeroMadre=false;
        //Si está seleccionado muestra su value

        if($("#extranjero_madre").is(':checked')) {
            var  extranjeroMadre=true;
        }


        //var  product = document.getElementById('aspirante_nombre').value;

        var enp,eappa,eampa,eopa,etepa,ecapa,enumpa,ecolpa,ecppa;
        enp=espacios_cadena(famNombrePadre);
        eappa=espacios_cadena(famApePatPadre);
        eampa=espacios_cadena(famApeMatPadre);
        eopa=espacios_cadena(ocupacionPadre);
        etepa=espacios_cadena(telefonoPadre);
        ecapa=espacios_cadena(callePadre);
        enumpa=espacios_cadena(numeroPadre);
        ecolpa=espacios_cadena(coloniaPadre);
        ecppa=espacios_cadena(cpPadre);

        var enm,eapma,eamma,eoma,etema,ecama,enumma,ecolma,ecpma;
        enm=espacios_cadena(famNombreMadre);
        eapma=espacios_cadena(famApePatMadre);
        eamma=espacios_cadena(famApeMatMadre);
        eoma=espacios_cadena(ocupacionMadre);
        etema=espacios_cadena(telefonoMadre);
        ecama=espacios_cadena(calleMadre);
        enumma=espacios_cadena(numeroMadre);
        ecolma=espacios_cadena(coloniaMadre);
        ecpma=espacios_cadena(cpMadre);



        if(check_padre || check_madre) {
            if(check_padre && check_madre){
                if (famNombrePadre == "") {
                    alert("No has rellenado el campo Nombre del Padre");
                } else if (enp) {
                    alert("Existen mas de dos espacios seguidos en el campo Nombre del Padre o el campo solo contiene espacios");
                } else if (famApePatPadre == "") {
                    alert("No has rellenado el campo Apellido Paterno del Padre");
                } else if (eappa) {
                    alert("Existen mas de dos espacios seguidos en el campo Apellido Paterno del Padre o el campo solo contiene espacios");
                } else if (famApeMatPadre == "") {
                    alert("No has rellenado el campo Apellido Materno del Padre");
                } else if (eampa) {
                    alert("Existen mas de dos espacios seguidos en el campo Apellido Materno del Padre o el campo solo contiene espacios");
                } else if (famNombreMadre == "") {
                    alert("No has rellenado el campo nombre de la Madre padre");
                } else if (enm) {
                    alert("Existen mas de dos espacios seguidos en el campo Nombre de la Madre o el campo solo contiene espacios");
                } else if (famApePatMadre == "") {
                    alert("No has rellenado el campo Apellido Paterno de la Madre");
                } else if (eapma) {
                    alert("Existen mas de dos espacios seguidos en el campo Apellido Paterno de la Madre o el campo solo contiene espacios");
                } else if (famApeMatMadre == "") {
                    alert("No has rellenado el campo nombre de la Madre padre");
                } else if (eamma) {
                    alert("Existen mas de dos espacios seguidos en el campo Apellido Materno de la Madre o el campo solo contiene espacios");
                }  else{
                    if (valida_guardar()) {
                        alert("Has rellenado todos los campos Satisfactoriamente.");
                    } else {
                        if (datosTut == false) {
                            alert("Has rellenado todos los campos Satisfactoriamente, continua con Datos del Tutor");
                        }
                        else {
                            alert("Has rellenado todos los campos satisfactoriamente, continua con los otros datos");
                        }
                    }
                    datosPad = true;
                    return false;
                }
            }else if(check_padre) {
                console.log("Esta sera una nueva funcion para el check de la padre");
                if (famNombrePadre == "") {
                    alert("No has rellenado el campo Nombre del Padre");
                } else if (enp) {
                    alert("Existen mas de dos espacios seguidos en el campo Nombre del padre o el campo solo contiene espacios");
                } else if (famApePatPadre == "") {
                    alert("No has rellenado el campo Apellido Paterno del Padre");
                } else if (eappa) {
                    alert("Existen mas de dos espacios seguidos en el campo Apellido Paterno del Padre o el campo solo contiene espacios");
                } else if (famApeMatPadre == "") {
                    alert("No has rellenado el campo Apellido Materno del Padre");
                } else if (eampa) {
                    alert("Existen mas de dos espacios seguidos en el campo Apellido Materno del Padre o el campo solo contiene espacios");
                } else if (famNombreMadre == "") {
                    alert("No has rellenado el campo nombre de la Madre padre");
                } else if (enm) {
                    alert("Existen mas de dos espacios seguidos en el campo Nombre de la Madre o el campo solo contiene espacios");
                } else if (famApePatMadre == "") {
                    alert("No has rellenado el campo Apellido Paterno de la Madre");
                } else if (eapma) {
                    alert("Existen mas de dos espacios seguidos en el campo Apellido Paterno de la Madre o el campo solo contiene espacios");
                } else if (famApeMatMadre == "") {
                    alert("No has rellenado el campo Apellido Materno de la Madre");
                } else if (eamma) {
                    alert("Existen mas de dos espacios seguidos en el campo Apellido Materno de la Madre o el campo solo contiene espacios");
                } else if (ocupacionMadre == "") {
                    alert("No has rellenado el campo Ocupación de la Madre");
                } else if (eoma) {
                    alert("Existen mas de dos espacios seguidos en el campo Ocupación de la Madre o el campo solo contiene espacios");
                } else if (telefonoMadre == "" || (!isNaN(telefonoMadre) && (telefonoMadre.length < 10 || telefonoMadre.length > 10)) || (isNaN(telefonoMadre) && (telefonoMadre.toLowerCase() !== "ninguno") && (telefonoMadre.toLowerCase() !== "x"))) {
                    alert("El número de teléfono de la Madre debe contener 10 digitos o escribir 'NINGUNO'");
                    return false;
                } else if (etema) {
                    alert("Existen mas de dos espacios seguidos en el campo Telefono de la Madre o el campo solo contiene espacios");
                }  else if (estadoMadre == "" && extranjeroMadre == false) {
                    alert("No has seleccionado el Estado de la Madre");
                } else if (municipioMadre == "" && extranjeroMadre == false) {
                    alert("No has seleccionado el Municipio de la Madre");
                } else if (localidadMadre == "" && extranjeroMadre == false) {
                    alert("No has seleccionado la Localidad de la Madre");
                } else if (calleMadre == "") {
                    alert("No has rellenado el campo Calle de la Madre");
                } else if (ecama) {
                    alert("Existen mas de dos espacios seguidos en el campo Calle de la Madre o el campo solo contiene espacios");
                } else if (numeroMadre == "" ) {
                    alert("No has rellenado el campo Número de la Madre, si su domicilio no cuenta con número colocar S/N");
                } else if (enumma) {
                    alert("Existen mas de dos espacios seguidos en el campo Numero de la Madre o el campo solo contiene espacios");
                } else if (coloniaMadre == "") {
                    alert("No has rellenado el campo Colonia de la Madre");
                } else if (ecolma) {
                    alert("Existen mas de dos espacios seguidos en el campo Colonia de la Madre o el campo solo contiene espacios");
                }  else if (cpMadre == "" || cpMadre.length < 5 || cpMadre.length > 5) {
                    alert("El campo Codigo Postal, debe contener 5 digitos");
                } else if (ecpma) {
                    alert("Existen mas de dos espacios seguidos en el campo Cp de la Madre o el campo solo contiene espacios");
                }  else {
                    if (valida_guardar()) {
                        alert("Has rellenado todos los campos Satisfactoriamente.");
                    } else {
                        if (datosTut == false) {
                            alert("Has rellenado todos los campos Satisfactoriamente, continua con Datos del Tutor");
                        }
                        else {
                            alert("Has rellenado todos los campos satisfactoriamente, continua con los otros datos");
                        }
                    }
                    datosPad = true;
                    return false;
                }

            } else if (check_madre) {
                if (famNombreMadre == "") {
                    alert("No has rellenado el campo Nombre de la Madre");
                } else if (enm) {
                    alert("Existen mas de dos espacios seguidos en el campo Nombre de la Madre o el campo solo contiene espacios");
                } else if (famApePatMadre == "") {
                    alert("No has rellenado el campo Apellido Paterno de la Madre");
                } else if (eapma) {
                    alert("Existen mas de dos espacios seguidos en el campo Apellido Paterno de la Madre o el campo solo contiene espacios");
                } else if (famApeMatMadre == "") {
                    alert("No has rellenado el campo Apellido Materno de la Madre");
                } else if (eamma) {
                    alert("Existen mas de dos espacios seguidos en el campo Apellido Materno de la Madre o el campo solo contiene espacios");
                } else if (famNombrePadre == "") {
                    alert("No has rellenado el campo Nombre del Padre");
                } else if (enp) {
                    alert("Existen mas de dos espacios seguidos en el campo Nombre del padre o el campo solo contiene espacios");
                } else if (famApePatPadre == "") {
                    alert("No has rellenado el campo Apellido Paterno del Padre");
                } else if (eappa) {
                    alert("Existen mas de dos espacios seguidos en el campo Apellido Paterno del Padre o el campo solo contiene espacios");
                } else if (famApeMatPadre == "") {
                    alert("No has rellenado el campo Apellido Materno del Padre");
                } else if (eampa) {
                    alert("Existen mas de dos espacios seguidos en el campo Apellido Materno del Padre o el campo solo contiene espacios");
                } else if (ocupacionPadre == "") {
                    alert("No has rellenado el campo Ocupación del Padre");
                } else if (eopa) {
                    alert("Existen mas de dos espacios seguidos en el campo Ocupación del Padre o el campo solo contiene espacios");
                } else if (telefonoPadre == ""  || (!isNaN(telefonoPadre) && (telefonoPadre.length < 10 || telefonoPadre.length > 10)) || (isNaN(telefonoPadre) && (telefonoPadre.toLowerCase() !== "ninguno"))) {
                    alert("El número de teléfono del Padre debe contener 10 digitos o escribir 'NINGUNO'");
                    return false
                    return false;
                } else if (etepa) {
                    alert("Existen mas de dos espacios seguidos en el campo Telefono del Padre o el campo solo contiene espacios");
                }  else if (estadoPadre == "" && extranjeroPadre == false) {
                    alert("No has seleccionado el Estado del Padre");
                } else if (municipioPadre == "" && extranjeroPadre == false) {
                    alert("No has seleccionado el Municipio del Padre");
                } else if (localidadPadre == "" && extranjeroPadre == false) {
                    alert("No has seleccionado la Localidad del Padre");
                } else if (callePadre == "") {
                    alert("No has rellenado el campo Calle del Padre");
                } else if (ecapa) {
                    alert("Existen mas de dos espacios seguidos en el campo Calle del Padre o el campo solo contiene espacios");
                } else if (numeroPadre == "" ) {
                    alert("No has rellenado el campo número del Padre, si su domicilio no cuenta con número colocar S/N");
                } else if (enumpa) {
                    alert("Existen mas de dos espacios seguidos en el campo Numero del Padre o el campo solo contiene espacios");
                } else if (coloniaPadre == "") {
                    alert("No has rellenado el campo Colonia del Padre");
                } else if (ecolpa) {
                    alert("Existen mas de dos espacios seguidos en el campo Colonia del Padre o el campo solo contiene espacios");
                } else if (cpPadre == "" || cpPadre.length < 5 || cpPadre.length > 5) {
                    alert("El campo Codigo Postal, debe contener 5 digitos");
                } else if (ecppa) {
                    alert("Existen mas de dos espacios seguidos en el campo Cp del Padre o el campo solo contiene espacios");
                } else {
                    if (valida_guardar()) {
                        alert("Has rellenado todos los campos Satisfactoriamente.");
                    }
                    else {
                        if (datosTut == false) {
                            alert("Has rellenado todos los campos Satisfactoriamente, continua con Datos del Tutor");
                        }
                        else {
                            alert("Has rellenado todos los campos satisfactoriamente, continua con los otros datos");
                        }
                    }
                    datosPad = true;
                    return false;
                }
            }
        }else{
            if (famNombrePadre == "") {
                alert("No has rellenado el campo Nombre del Padre");
            } else if (enp) {
                alert("Existen mas de dos espacios seguidos en el campo Nombre del padre o el campo solo contiene espacios");
            }else if(famApePatPadre == ""){
                alert("No has rellenado el campo Apellido Paterno del Padre");
            } else if (eappa) {
                alert("Existen mas de dos espacios seguidos en el campo Apellido Paterno del Padre o el campo solo contiene espacios");
            }else if(famApeMatPadre == ""){
                alert("No has rellenado el campo Apellido Materno del Padre");
            } else if (eampa) {
                alert("Existen mas de dos espacios seguidos en el campo Apellido Materno del Padre o el campo solo contiene espacios");
            }else if(ocupacionPadre == ""){
                alert("No has rellenado el campo Ocupación del Padre");
            } else if (eopa) {
                alert("Existen mas de dos espacios seguidos en el campo Ocupación del Padre o el campo solo contiene espacios");
            }else if(telefonoPadre == "" || (!isNaN(telefonoPadre) && (telefonoPadre.length < 10 || telefonoPadre.length > 10)) || (isNaN(telefonoPadre) && (telefonoPadre.toLowerCase() !== "ninguno") && (telefonoPadre.toLowerCase() !== "x"))){
                alert("El número de teléfono del Padre debe contener 10 digitos o escribir 'NINGUNO'");
                return false;
            } else if (etepa) {
                alert("Existen mas de dos espacios seguidos en el campo Telefono del Padre o el campo solo contiene espacios");
            }else if(estadoPadre == "" && extranjeroPadre==false){
                alert("No has seleccionado el Estado del Padre");
            }else if(municipioPadre == "" && extranjeroPadre==false){
                alert("No has seleccionado el Municipio del Padre");
            }else if(localidadPadre == "" && extranjeroPadre==false){
                alert("No has seleccionado la Localidad del Padre");
            }else if(callePadre == ""){
                alert("No has rellenado el campo Calle del Padre");
            } else if (ecapa) {
                alert("Existen mas de dos espacios seguidos en el campo Calle del Padre o el campo solo contiene espacios");
            }else if(numeroPadre == "" ){
                alert("No has rellenado el campo número del Padre, si su domicilio no cuenta con número colocar S/N");
            } else if (enumpa) {
                alert("Existen mas de dos espacios seguidos en el campo Numero del Padre o el campo solo contiene espacios");
            }else if(coloniaPadre == "") {
                alert("No has rellenado el campo Colonia del Padre");
            } else if (ecolpa) {
                alert("Existen mas de dos espacios seguidos en el campo Colonia del Padre o el campo solo contiene espacios");
            }else if(cpPadre == "" || (!isNaN(cpPadre) && (cpPadre.length < 5 || cpPadre.length > 5)) || (isNaN(cpPadre) && (cpPadre.toLowerCase() !== "ninguno") && (cpPadre.toLowerCase() !== "x"))){
                alert("El campo Codigo Postal, debe contener 5 digitos");
            } else if (ecppa) {
                alert("Existen mas de dos espacios seguidos en el campo Cp del Padre o el campo solo contiene espacios");
            }else  if (famNombreMadre == "") {
                alert("No has rellenado el campo nombre de la Madre");
            } else if (enm) {
                alert("Existen mas de dos espacios seguidos en el campo Nombre de la Madre o el campo solo contiene espacios");
            }else if(famApePatMadre == ""){
                alert("No has rellenado el campo Apellido Paterno de la Madre");
            } else if (eapma) {
                alert("Existen mas de dos espacios seguidos en el campo Apellido Paterno de la Madre o el campo solo contiene espacios");
            }else if(famApeMatMadre == ""){
                alert("No has rellenado el campo Apellido Materno de la Madre");
            } else if (eamma) {
                alert("Existen mas de dos espacios seguidos en el campo Apellido Materno de la Madre o el campo solo contiene espacios");
            }else if(ocupacionMadre == ""){
                alert("No has rellenado el campo Ocupación de la Madre");
            } else if (eoma) {
                alert("Existen mas de dos espacios seguidos en el campo Ocupación de la Madre o el campo solo contiene espacios");
            }else if(telefonoMadre == "" || (!isNaN(telefonoMadre) && (telefonoMadre.length < 10 || telefonoMadre.length > 10)) || (isNaN(telefonoMadre) && (telefonoMadre.toLowerCase() !== "ninguno") && (telefonoMadre.toLowerCase() !== "x") )){
                alert("El número de teléfono de la Madre debe contener 10 digitos o escribir 'NINGUNO'");
                return false;
            } else if (etema) {
                alert("Existen mas de dos espacios seguidos en el campo Telefono de la Madre o el campo solo contiene espacios");
            }else if(estadoMadre == "" && extranjeroMadre==false){
                alert("No has seleccionado el Estado de la Madre");
            }else if(municipioMadre == "" && extranjeroMadre==false){
                alert("No has seleccionado el Municipio de la Madre");
            }else if(localidadMadre == "" && extranjeroMadre==false){
                alert("No has seleccionado la Localidad de la Madre");
            }else if(calleMadre == ""){
                alert("No has rellenado el campo Calle de la Madre");
            } else if (ecama) {
                alert("Existen mas de dos espacios seguidos en el campo Calle de la Madre o el campo solo contiene espacios");
            }else if(numeroMadre == "" ){
                alert("No has rellenado el campo Número de la Madre, si su domicilio no cuenta con número colocar S/N");
            } else if (enumma) {
                alert("Existen mas de dos espacios seguidos en el campo Numero de la Madre o el campo solo contiene espacios");
            }else if(coloniaMadre == ""){
                alert("No has rellenado el campo Colonia de la Madre");
            } else if (ecolma) {
                alert("Existen mas de dos espacios seguidos en el campo Colonia de la Madre o el campo solo contiene espacios");
            }else if(cpMadre == "" || (!isNaN(cpMadre) && (cpMadre.length < 5 || cpMadre.length > 5)) || (isNaN(cpMadre) && (cpMadre.toLowerCase() !== "ninguno") && (cpMadre.toLowerCase() !== "x"))){
                alert("El campo Codigo Postal, debe contener 5 digitos");
            } else if (ecpma) {
                alert("Existen mas de dos espacios seguidos en el campo Cp de la Madre o el campo solo contiene espacios");
            }else{
                if(valida_guardar()) {
                    alert("Has rellenado todos los campos Satisfactoriamente.");
                }
                else{
                    if(datosTut == false) {
                        if(!tutor_padre && !tutor_madre) {
                            alert("Has rellenado todos los campos obligatorios satisfactoriamente, No has seleccionado un tutor en la siguiente sección tendrás que definir uno");
                        }else{
                            alert("Has rellenado todos los campos satisfactoriamente, continua con los otros datos");
                        }
                    }else{
                        alert("Has rellenado todos los campos satisfactoriamente, continua con los otros datos");
                    }
                }
                datosPad = true;
                return false;
            }
        }
        return false;
    });

    $("#id_datos_tutor").click(function(){
        var check_tutor_padre = document.getElementById('tutor_padre').checked;
        var check_tutor_madre = document.getElementById('tutor_madre').checked
        
        if (check_tutor_padre){
            copiar_datos_padre();
            deshabilitar_campos_datos_tutor();
        } else if (check_tutor_madre){
            copiar_datos_madre();
            deshabilitar_campos_datos_tutor();
        } else{
            habilitar_campos_datos_tutor();
        }
    });

    /**Datos del tutor**/
    $("#id_guardar_asp").click(function() {
        var  nombreTutor = document.getElementById('familiar_nombre_tutor').value;
        var  apPaTutor = document.getElementById('familiar_apellido_paterno_tutor').value;
        var  apMaTutor = document.getElementById('familiar_apellido_materno_tutor').value;
        var  ocupacionTutor = document.getElementById('familiar_ocupacion_tutor').value;
        var  parentescoTutor = document.getElementById('parentesco_tutor_id').value;
        var  telefonoTutor = document.getElementById('familiar_telefono_tutor').value;

        var  estadoTutor = document.getElementById('estado_tutor_id').value;
        var  municipioTutor = document.getElementById('municipio_tutor_id').value;
        var  localidadTutor = document.getElementById('localidad_tutor_id').value;

        var  calleTutor = document.getElementById('familiar_calle_tutor').value;
        var  numeroTutor = document.getElementById('familiar_numero_tutor').value;
        var  coloniaTutor = document.getElementById('familiar_colonia_tutor').value;
        var  cpTutor = document.getElementById('familiar_codigo_postal_tutor').value;
        let bandTutor = false;
        if(isNaN(numeroTutor)== false || numeroTutor === "S/N"|| numeroTutor === "s/n"){
            bandTutor = true;

        }
        var  extranjeroTutor=false;
        //Si está seleccionado muestra su value

        var ent,eaptu,eamtu,eotu,etetu,ecatu,enumtu,ecoltu,ecptu;
        ent=espacios_cadena(nombreTutor);
        eaptu=espacios_cadena(apPaTutor);
        eamtu=espacios_cadena(apMaTutor);
        eotu=espacios_cadena(ocupacionTutor);
        etetu=espacios_cadena(telefonoTutor);
        ecatu=espacios_cadena(calleTutor);
        enumtu=espacios_cadena(numeroTutor);
        ecoltu=espacios_cadena(coloniaTutor);
        ecptu=espacios_cadena(cpTutor);

        if($("#extranjero_Tutor").is(':checked')) {
            var  extranjeroTutor=true;
        }
        if (nombreTutor == "") {
            alert("No has rellenado el campo Nombre del Tutor");
        } else if (ent) {
            alert("Existen mas de dos espacios seguidos en el campo Nombre del tutor o el campo solo contiene espacios");
        }else if(apPaTutor == ""){
            alert("No has rellenado el campo Apellido Paterno del Tutor");
        } else if (eaptu) {
            alert("Existen mas de dos espacios seguidos en el campo Apellido Paterno del tutor o el campo solo contiene espacios");
        }else if(apMaTutor == ""){
            alert("No has rellenado el campo Apellido Materno del Tutor");
        } else if (eamtu) {
            alert("Existen mas de dos espacios seguidos en el campo Apellido Materno del tutor o el campo solo contiene espacios");
        }else if(ocupacionTutor == ""){
            alert("No has rellenado el campo Ocupación del Tutor");
        } else if (eotu) {
            alert("Existen mas de dos espacios seguidos en el campo Ocupación del tutor o el campo solo contiene espacios");
        }else if(parentescoTutor == ""){
            alert("No has rellenado el campo Parentesco del Tutor");
        }else if(telefonoTutor == "" || (!isNaN(telefonoTutor) && (telefonoTutor.length < 10 || telefonoTutor.length > 10)) || (isNaN(telefonoTutor) && (telefonoTutor.toLowerCase() !== "ninguno"))){
            alert("El número de teléfono del Tutor debe contener 10 digitos o escribir 'NINGUNO'");
        return false;
        }  else if (etetu) {
            alert("Existen mas de dos espacios seguidos en el campo Telefono del tutor o el campo solo contiene espacios");
        }else if(estadoTutor == "" && extranjeroTutor==false){
            alert("No has seleccionado el Estado del Tutor");
        }else if(municipioTutor == "" && extranjeroTutor==false){
            alert("No has seleccionado el Municipio del Tutor");
        }else if(localidadTutor == "" && extranjeroTutor==false){
            alert("No has seleccionado la Localidad del Tutor");
        }else if(calleTutor == ""){
            alert("No has rellenado el campo Calle del Tutor");
        } else if (ecatu) {
            alert("Existen mas de dos espacios seguidos en el campo Calle del tutor o el campo solo contiene espacios");
        }else if(numeroTutor == "" ){
            alert("No has rellenado el campo número del Tutor, si su domicilio no cuenta con número colocar S/N");
        } else if (enumtu) {
            alert("Existen mas de dos espacios seguidos en el campo Número del tutor o el campo solo contiene espacios");
        }else if(coloniaTutor == ""){
            alert("No has rellenado el campo Colonia del Tutor");
        } else if (ecoltu) {
            alert("Existen mas de dos espacios seguidos en el campo Colonia del tutor o el campo solo contiene espacios");
        }else if(cpTutor == "" || cpTutor.length<5 || cpTutor.length>5 ){
            alert("El campo Codigo Postal, debe contener 5 digitos");
        } else if (ecptu) {
            alert("Existen mas de dos espacios seguidos en el campo Cp del tutor o el campo solo contiene espacios");
        }else{
            datosTut = true;
            var validados = tabs_validados();

            if (validados === true){
                $("#go").submit();
            }
            else {
                return false;
            }

        }

    });

    function tabs_validados(){
        if(datosAsp == false){
            alert("No has verificado el formulario Datos Aspirante");
            return false;
        }else if(datosPer == false){
            alert("No has verificado el formulario Datos Personales");
            return false;
        }else if(datosLen == false){
            alert("No has verificado el formulario Datos Lenguas Indigenas");
            return false;
        }else if(datosAca == false){
            alert("No has verificado el formulario Datos Académicos");
            return false;
        }else if(datosMed == false){
            alert("No has verificado el formulario Datos Médicos");
            return false;
        }else if(datosPad == false){
            alert("No has verificado el formulario Datos de los padres");
            return false;
        }else if(datosTut == false){
            alert("No has verificado el formulario Datos del Tutor");
            return false;
        }else{
            alert("Felicidades has completado satisfactoriamente la operación");
            // $("#go").submit();
            return true;
        }
        return false;
    }

    function valida_guardar(){
        if(datosAsp == false){
            return false;
        }else if(datosPer == false){
            return false;
        }else if(datosLen == false){
            return false;
        }else if(datosAca == false){
            return false;
        }else if(datosMed == false){
            return false;
        }else if(datosPad == false){
            return false;
        }else if(datosTut == false){
            return false;
        }else{
            return true;
        }
        return false;
    }

    function copiar_datos_padre(){
        pais_padre = $("select#pais_padre_id :selected").val();
        estado_padre = $("select#estado_padre_id :selected").val();
        municipio_padre = $("select#municipio_padre_id :selected").val();
        localidad_padre = $("select#localidad_padre_id :selected").val();

        $("input#familiar_nombre_tutor").val($("input#familiar_nombre_padre").val());
        $("input#familiar_apellido_paterno_tutor").val($("input#familiar_apellido_paterno_padre").val());
        $("input#familiar_apellido_materno_tutor").val($("input#familiar_apellido_materno_padre").val());
        $("input#familiar_ocupacion_tutor").val($("input#familiar_ocupacion_padre").val());
        $("input#familiar_telefono_tutor").val($("input#familiar_telefono_padre").val());
        $("select#parentesco_tutor_id").val("2");
        $("input#familiar_calle_tutor").val($("input#familiar_calle_padre").val());
        $("input#familiar_numero_tutor").val($("input#familiar_numero_padre").val());
        $("input#familiar_colonia_tutor").val($("input#familiar_colonia_padre").val());
        $("input#familiar_codigo_postal_tutor").val($("input#familiar_codigo_postal_padre").val());

        if($("#extranjero_tutor").is(':checked')) {
            document.getElementById("extranjero_tutor").checked = true;
            $('label#etiquetaEstadoTutor').css("display", "none");
            $('select#estado_tutor_id').css("display", "none");
            $('label#etiquetaMunicipioTutor').css("display", "none");
            $('select#municipio_tutor_id').css("display", "none");
            $('label#etiquetaLocalidadTutor').css("display", "none");
            $('select#localidad_tutor_id').css("display", "none");
            $('#pais_tutor_id').removeAttr('disabled');
            $("select#estado_tutor_id").prop('required',false);
            $("select#municipio_tutor_id").prop('required',false);
            $("select#localidad_tutor_id").prop('required',false);

            $("select#pais_tutor_id").val(pais_padre);
        } else {
            $('label#etiquetaEstadoTutor').css("display", "block");
            $('select#estado_tutor_id').css("display", "block");
            $('label#etiquetaMunicipioTutor').css("display", "block");
            $('select#municipio_tutor_id').css("display", "block");
            $('label#etiquetaLocalidadTutor').css("display", "block");
            $('select#localidad_tutor_id').css("display", "block");
            $("#pais_tutor_id").attr('disabled','disabled');
            $('#pais_tutor_id option').filter(function () { return $(this).html() == "MÉXICO"; }).prop("selected", true);
            pais = $("select#pais_tutor_id :selected").val();
            $("#familiar_pais_tutor").val(pais);

            /* añadir require */
            $("select#estado_tutor_id").attr("required", true);
            $("select#municipio_tutor_id").attr("required", true);
            $("select#localidad_tutor_id").attr("required", true);

            document.getElementById("extranjero_tutor").checked = false;
            municipios = document.getElementById('municipio_padre_id');
            towns_options = municipios.innerHTML;

            municipios_tutor = document.getElementById('municipio_tutor_id');
            municipios_tutor.innerHTML = towns_options;

            localidades = document.getElementById('localidad_padre_id');
            localities_options = localidades.innerHTML;

            localidades_tutor = document.getElementById('localidad_tutor_id');
            localidades_tutor.innerHTML = localities_options;

            $("select#estado_tutor_id").val(estado_padre);
            $("select#municipio_tutor_id").val(municipio_padre);
            $("select#localidad_tutor_id").val(localidad_padre);
        }
    }
    
    function copiar_datos_madre(){
        pais_madre = $("select#pais_madre_id :selected").val();
        estado_madre = $("select#estado_madre_id :selected").val();
        municipio_madre = $("select#municipio_madre_id :selected").val();
        localidad_madre = $("select#localidad_madre_id :selected").val();

        $("input#familiar_nombre_tutor").val($("input#familiar_nombre_madre").val());
        $("input#familiar_apellido_paterno_tutor").val($("input#familiar_apellido_paterno_madre").val());
        $("input#familiar_apellido_materno_tutor").val($("input#familiar_apellido_materno_madre").val());
        $("input#familiar_ocupacion_tutor").val($("input#familiar_ocupacion_madre").val());
        $("input#familiar_telefono_tutor").val($("input#familiar_telefono_madre").val());
        $("select#parentesco_tutor_id").val("1");
        $("input#familiar_calle_tutor").val($("input#familiar_calle_madre").val());
        $("input#familiar_numero_tutor").val($("input#familiar_numero_madre").val());
        $("input#familiar_colonia_tutor").val($("input#familiar_colonia_madre").val());
        $("input#familiar_codigo_postal_tutor").val($("input#familiar_codigo_postal_madre").val());

        if($("#extranjero_tutor").is(':checked')) {
            document.getElementById("extranjero_tutor").checked = true;
            $('label#etiquetaEstadoTutor').css("display", "none");
            $('select#estado_tutor_id').css("display", "none");
            $('label#etiquetaMunicipioTutor').css("display", "none");
            $('select#municipio_tutor_id').css("display", "none");
            $('label#etiquetaLocalidadTutor').css("display", "none");
            $('select#localidad_tutor_id').css("display", "none");
            $('#pais_tutor_id').removeAttr('disabled');
            $("select#estado_tutor_id").prop('required',false);
            $("select#municipio_tutor_id").prop('required',false);
            $("select#localidad_tutor_id").prop('required',false);

            $("select#pais_tutor_id").val(pais_madre);
        } else {
            $('label#etiquetaEstadoTutor').css("display", "block");
            $('select#estado_tutor_id').css("display", "block");
            $('label#etiquetaMunicipioTutor').css("display", "block");
            $('select#municipio_tutor_id').css("display", "block");
            $('label#etiquetaLocalidadTutor').css("display", "block");
            $('select#localidad_tutor_id').css("display", "block");
            $("#pais_tutor_id").attr('disabled','disabled');
            $('#pais_tutor_id option').filter(function () { return $(this).html() == "MÉXICO"; }).prop("selected", true);
            pais = $("select#pais_tutor_id :selected").val();
            $("#familiar_pais_tutor").val(pais);

            /* añadir require */
            $("select#estado_tutor_id").attr("required", true);
            $("select#municipio_tutor_id").attr("required", true);
            $("select#localidad_tutor_id").attr("required", true);

            document.getElementById("extranjero_tutor").checked = false;
            municipios = document.getElementById('municipio_madre_id');
            towns_options = municipios.innerHTML;

            municipios_tutor = document.getElementById('municipio_tutor_id');
            municipios_tutor.innerHTML = towns_options;

            localidades = document.getElementById('localidad_madre_id');
            localities_options = localidades.innerHTML;

            localidades_tutor = document.getElementById('localidad_tutor_id');
            localidades_tutor.innerHTML = localities_options;

            console.log(estado_madre);
            console.log(municipio_madre);
            console.log(localidad_madre)
            $("select#estado_tutor_id").val(estado_madre);
            $("select#municipio_tutor_id").val(municipio_madre);
            $("select#localidad_tutor_id").val(localidad_madre)//.prop('selected', true);
            //texto_localidad = $("#localidad_tutor_id option:selected").text();

            //$('#localidad_tutor_id option').filter(function () { return $(this).html() == texto_localidad; }).prop("selected", true);


            //console.log(texto_localidad);
            //$("select#localidad_tutor_id option").filter(":selected").val(localidad_madre);
            //$("#localidad_tutor_id option[value=" + municipio_madre + "]").attr('selected', 'selected');
            //$("select#localidad_tutor_id").val(localidad_madre).prop('selected', true);
            //alert($("select#localidad_tutor_id").val());
            //.prop('selected', true);
        }
    }

    //funcion para deshabilitar todos los campos de la sección Datos del Tutor en caso de seleccionar uno en la sección de Datos de los Padres
    function deshabilitar_campos_datos_tutor(){
        $("input#familiar_nombre_tutor").prop('disabled', 'disabled');
        $("input#familiar_apellido_paterno_tutor").prop('disabled', 'disabled');
        $("input#familiar_apellido_materno_tutor").prop('disabled', 'disabled');
        $("input#familiar_ocupacion_tutor").prop('disabled', 'disabled');
        $("input#familiar_telefono_tutor").prop('disabled', 'disabled');
        $("select#parentesco_tutor_id").prop('disabled', 'disabled');
        $("input#familiar_calle_tutor").prop('disabled', 'disabled');
        $("input#familiar_numero_tutor").prop('disabled', 'disabled');
        $("input#familiar_colonia_tutor").prop('disabled', 'disabled');
        $("input#familiar_codigo_postal_tutor").prop('disabled', 'disabled');
        $("select#estado_tutor_id").prop('disabled', 'disabled');
        $("select#municipio_tutor_id").prop('disabled', 'disabled');
        $("select#localidad_tutor_id").prop('disabled', 'disabled');
        //$("input#copyPadre").removeAttr('checked');
        //$("input#copyMadre").removeAttr('checked');
        $("input#copyMadre").prop('disabled', 'disabled');
        $("input#copyPadre").prop('disabled', 'disabled');
    }

    //funcion para habilitar todos los campos de la sección Datos del Tutor en caso de no seleccionar ningun tutor en la sección de Datos de los Padres
    //Esto es en el caso de que el aspirante seleccione un tutor y despues desmarque dicha casilla
    function habilitar_campos_datos_tutor(){
        $("input#familiar_nombre_tutor").removeAttr('disabled');
        $("input#familiar_apellido_paterno_tutor").removeAttr('disabled');
        $("input#familiar_apellido_materno_tutor").removeAttr('disabled');
        $("input#familiar_ocupacion_tutor").removeAttr('disabled');
        $("input#familiar_telefono_tutor").removeAttr('disabled');
        $("select#parentesco_tutor_id").removeAttr('disabled');
        $("input#familiar_calle_tutor").removeAttr('disabled');
        $("input#familiar_numero_tutor").removeAttr('disabled');
        $("input#familiar_colonia_tutor").removeAttr('disabled');
        $("input#familiar_codigo_postal_tutor").removeAttr('disabled');
        $("select#estado_tutor_id").removeAttr('disabled');
        $("select#municipio_tutor_id").removeAttr('disabled');
        $("select#localidad_tutor_id").removeAttr('disabled');
        $("input#copyPadre").removeAttr('disabled');
        $("input#copyMadre").removeAttr('disabled');
    }

    // Validación para opción ninguna en lenguas indigenas
    $("input#datos_lenguas_lengua_indigena_").click(function() {
        var selected_checkbox_val = $(this).val();
        var lengua =  $(this).next('label').text();

        // Cuando se selecciona la opción NINGUNA, se deshabilitan las demás opciones y se quita la selección en caso de aplicar.
        if(lengua.trim().toUpperCase() == 'NINGUNA'){
            if ($(this).is(':checked')){
                var check = null;
                $("input#datos_lenguas_lengua_indigena_").each(function() {
                    if($(this).val() != selected_checkbox_val){
                        $(this).prop('disabled', true);
                        $(this).prop("checked", false);
                    }
                });
            }
            else{
                    // Si se quita la selección a la opción NINGUNA, se habilitan todos los checkbox de lenguas.
                $("input#datos_lenguas_lengua_indigena_").each(function() {
                    if($(this).val() != selected_checkbox_val){
                        $(this).prop('disabled', false);
                    }
                });
            }
        }
    });
});