class CreateAspirantesFamiliares < ActiveRecord::Migration
  def change
    create_table :aspirantes_familiares do |t|
      t.references :aspirante
      t.references :familiar

      t.timestamps null: false
    end
  end
end
