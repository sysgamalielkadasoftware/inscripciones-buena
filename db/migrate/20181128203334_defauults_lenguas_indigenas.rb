class DefauultsLenguasIndigenas < ActiveRecord::Migration
  def change
    LenguaIndigena.new(:nombre => 'AMUZGO').save
    LenguaIndigena.new(:nombre => 'AMUZGO DE GUERRERO').save
    LenguaIndigena.new(:nombre => 'AMUZGO DE OAXACA').save
    LenguaIndigena.new(:nombre => 'CHATINO').save
    LenguaIndigena.new(:nombre => 'CHINANTECO').save
    LenguaIndigena.new(:nombre => 'CHOCHOLTECO').save
    LenguaIndigena.new(:nombre => 'HUAVE').save
    LenguaIndigena.new(:nombre => 'MAZATECO').save
    LenguaIndigena.new(:nombre => 'MIXE').save
    LenguaIndigena.new(:nombre => 'MIXTECO').save
    LenguaIndigena.new(:nombre => 'MIXTECO DE LA COSTA').save
    LenguaIndigena.new(:nombre => 'MIXTECO DE LA MIXTECA ALTA').save
    LenguaIndigena.new(:nombre => 'MIXTECO DE LA MIXTECA BAJA').save
    LenguaIndigena.new(:nombre => 'MIXTECO DE LA ZONA MAZATECA').save
    LenguaIndigena.new(:nombre => 'MIXTECO DE PUEBLA').save
    LenguaIndigena.new(:nombre => 'NINGUNA ').save
    LenguaIndigena.new(:nombre => 'PURÉPECHA (TARASCO)').save
    LenguaIndigena.new(:nombre => 'TLAPANECO').save
    LenguaIndigena.new(:nombre => 'TRIQUI').save
    LenguaIndigena.new(:nombre => 'ZAPOTECO').save
    LenguaIndigena.new(:nombre => 'ZOQUE').save
  end
end
