class AddDeshabilitadoToSedesFichas < ActiveRecord::Migration
  def change
    add_column :sedes_fichas, :deshabilitado, :boolean, default: false
  end
end
