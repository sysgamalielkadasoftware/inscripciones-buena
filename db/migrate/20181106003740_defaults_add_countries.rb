class DefaultsAddCountries < ActiveRecord::Migration
  def change
    Lugar.new(:nombre => 'Afganistán'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Albania'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Alemania'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Andorra'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Angola'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Antigua y Barbuda'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Arabia Saudita'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Argelia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Argentina'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Armenia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Australia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Austria'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Azerbaiyán'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Bahamas'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Bangladesh'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Barbados'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Baréin'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Bélgica'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Belice'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Benin'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Bielorrusia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Birmania'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Bolivia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Bosnia y Herzegovina'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Botsuana'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Brasil'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Brunéi'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Bulgaria'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Burkina Faso'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Burundi'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Bután'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Cabo Verde'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Camboya'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Camerún'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Canadá'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Catar'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Chad'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Chile'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'China'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Chipre'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Colombia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Comoras'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Corea del Norte'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Corea del Sur'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Costa de Marfil'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Costa Rica'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Croacia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Cuba'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Dinamarca'.mb_chars.upcase, :tipo => 'PAIS').save
    # Lugar.new(:nombre => 'Djibouti'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Dominica'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Ecuador'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Egipto'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'El Salvador'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Emiratos Árabes Unidos'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Eritrea'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Eslovaquia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Eslovenia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'España'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Estados Unidos'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Estonia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Etiopía'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Filipinas'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Finlandia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Fiyi'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Francia'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Gabón'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Gambia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Georgia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Ghana'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Granada'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Grecia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Guatemala'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Guinea'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Guinea-Bisáu'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Guinea Ecuatorial'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Guyana'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Haití'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Honduras'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Hungría'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'India'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Indonesia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Irak'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Irán'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Irlanda'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Irlanda del Norte'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Islandia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Israel'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Islas Marshall'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Islas Salomón'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Italia'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Jamaica'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Japón'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Jordania'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Kazajistán'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Kenia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Kirguistán'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Kiribati'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Kuwait'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Laos'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Lesoto'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Letonia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Líbano'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Liberia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Libia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Liechtenstein'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Lituania'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Luxemburgo'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Macedonia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Madagascar'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Malasia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Malaui'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Maldivas'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Malí'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Malta'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Marruecos'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Mauricio'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Mauritania'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'México'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Micronesia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Moldavia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Mónaco'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Mongolia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Montenegro'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Mozambique'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Namibia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Nauru'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Nepal'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Nicaragua'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Níger'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Nigeria'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Noruega'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Nueva Zelanda'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Omán'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Países Bajos'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Pakistán'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Palaos'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Palestina'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Panamá'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Papúa Nueva Guinea'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Paraguay'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Perú'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Polonia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Portugal'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Reino Unido'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'República Centroafricana'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'República Checa'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'República del Congo'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'República Democrática del Congo'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'República Dominicana'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Ruanda'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Rumania'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Rusia'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Sahara Occidental'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Samoa'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'San Cristóbal y Nieves'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'San Marino'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'San Vicente y las Granadinas'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Santa Lucía'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Santo Tomé y Príncipe'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Senegal'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Serbia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Seychelles'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Sierra Leona'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Singapur'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Siria'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Somalia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Sri Lanka'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Suazilandia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Sudáfrica'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Sudán'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Sudán del Sur'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Suecia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Suiza'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Surinam'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Tailandia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Taiwán'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Tanzania'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Tayikistán'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Timor Oriental'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Togo'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Tonga'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Trinidad y Tobago'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Túnez'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Turkmenistán'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Turquía'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Tuvalu'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Ucrania'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Uganda'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Uruguay'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Uzbekistán'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Vanuatu'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Vaticano '.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Venezuela'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Vietnam'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Yemen'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Yibuti'.mb_chars.upcase, :tipo => 'PAIS').save

    Lugar.new(:nombre => 'Zambia'.mb_chars.upcase, :tipo => 'PAIS').save
    Lugar.new(:nombre => 'Zimbabue'.mb_chars.upcase, :tipo => 'PAIS').save
  end
end
