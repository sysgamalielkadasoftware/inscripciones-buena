class AddComprobanteToFichas < ActiveRecord::Migration
  def change
    array_columns = Ficha.column_names

    if array_columns.include?("comprobante") && array_columns.include?("tipo_archivo") && array_columns.include?("nombre_archivo")
    else      
      add_column :fichas, :comprobante, :binary
      add_column :fichas, :tipo_archivo, :string
      add_column :fichas, :nombre_archivo, :string
    end

  end
end
