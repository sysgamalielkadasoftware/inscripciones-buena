class AddDeviseCreateUsuarios < ActiveRecord::Migration
  array_columns = Usuario.column_names

  if array_columns.include?("rol")
  else
    add_column :usuarios, :rol, :string
  end
end
