class CreateExamenesSeleccion < ActiveRecord::Migration
  def change
    create_table :examenes_seleccion do |t|
      t.references :ficha
      t.references :aspirante
      t.references :alumno
      t.references :sede_ficha

      t.timestamps null: false
    end
  end
end
