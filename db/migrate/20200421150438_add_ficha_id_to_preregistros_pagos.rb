class AddFichaIdToPreregistrosPagos < ActiveRecord::Migration
  def change
    array_columns = PreregistroPago.column_names

    if array_columns.include?("ficha_id")
    else      
      add_column :preregistros_pagos, :ficha_id, :integer
    end

  end
end
