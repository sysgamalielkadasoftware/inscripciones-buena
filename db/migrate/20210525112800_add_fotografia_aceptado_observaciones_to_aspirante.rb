class AddFotografiaAceptadoObservacionesToAspirante < ActiveRecord::Migration
  array_columns = Aspirante.column_names

  if array_columns.include?("fotografia_aceptado") && array_columns.include?("observaciones")
  else
    add_column :aspirantes, :fotografia_aceptado, :boolean
    add_column :aspirantes, :observaciones, :string
  end
end

