class AddArchivoTipoNombreToAspirantesDocumentos < ActiveRecord::Migration
  def change
    array_columns = AspiranteDocumento.column_names

    if array_columns.include?("archivo") && array_columns.include?("tipo_archivo") && array_columns.include?("nombre_archivo")
    else      
      add_column :aspirantes_documentos, :archivo, :binary
      add_column :aspirantes_documentos, :tipo_archivo, :string
      add_column :aspirantes_documentos, :nombre_archivo, :string
    end

  end
end
