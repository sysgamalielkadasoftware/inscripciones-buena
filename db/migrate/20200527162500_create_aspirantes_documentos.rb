class CreateAspirantesDocumentos < ActiveRecord::Migration
  def self.up
    create_table :aspirantes_documentos do |t|
      t.references :aspirante
      t.references :documento_ficha      

      t.timestamps
    end
  end

  def self.down
      drop_table :aspirantes_documentos
  end
end
