class CreateDirecciones < ActiveRecord::Migration
  def change
    create_table :direcciones do |t|
      t.string :calle
      t.string :numero
      t.string :colonia
      t.string :codigo_postal
      t.references :lugar

      t.timestamps null: false
    end
  end
end
