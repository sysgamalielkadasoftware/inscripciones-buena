class AddFotoToAspirantes < ActiveRecord::Migration
  def change
    array_columns = Aspirante.column_names

    if array_columns.include?("foto") && array_columns.include?("tipo_archivo") && array_columns.include?("nombre_archivo")
    else      
      add_column :aspirantes, :foto, :binary
      add_column :aspirantes, :tipo_archivo, :string
      add_column :aspirantes, :nombre_archivo, :string
    end

  end
end
