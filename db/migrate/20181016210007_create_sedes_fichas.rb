class CreateSedesFichas < ActiveRecord::Migration
  def change
    create_table :sedes_fichas do |t|
      t.string :sede, null: false
      t.datetime :fecha_examen
      t.string :hora_examen
      t.references :periodo_ficha

      t.timestamps null: false
    end
  end
end
