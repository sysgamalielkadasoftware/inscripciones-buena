class CreateExamenesMaterias < ActiveRecord::Migration
  def change
    create_table :examenes_materias do |t|
      t.references :materia_examen_seleccion
      t.references :examen_seleccion
      t.decimal :calificacion

      t.timestamps null: false
    end
  end
end
