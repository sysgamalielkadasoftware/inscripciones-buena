class CreateSolicitudesFichas < ActiveRecord::Migration
  def change
    create_table :solicitudes_fichas do |t|
      t.references :aspirante
      t.references :carrera
      t.references :ciclo
      t.string :proceso
      t.string :estado
      t.string :aprobado
      t.references :alumno
      t.references :sede_ficha

      t.timestamps null: false
    end
  end
end
