class AddCorreoInstitucionalToAspirantes < ActiveRecord::Migration
  def change
    array_columns = Aspirante.column_names

    if array_columns.include?("correo_institucional")
    else      
      add_column :aspirantes, :correo_institucional, :string
    end

  end
end
