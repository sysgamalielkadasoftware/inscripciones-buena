class DefaultsAddStates < ActiveRecord::Migration
  def change
    estados = [
        { :clave => '1', :nombre => 'Aguascalientes', :abreviatura => 'Ags.'},
        { :clave => '2', :nombre => 'Baja California', :abreviatura => 'BC'},
        { :clave => '3', :nombre => 'Baja California Sur', :abreviatura => 'BCS'},
        { :clave => '4', :nombre => 'Campeche', :abreviatura => 'Camp.'},
        { :clave => '7', :nombre => 'Chiapas', :abreviatura => 'Chis.'},
        { :clave => '8', :nombre => 'Chihuahua', :abreviatura => 'Chih.'},
        { :clave => '9', :nombre => 'Ciudad de México', :abreviatura => 'CDMX'},
        { :clave => '5', :nombre => 'Coahuila de Zaragoza', :abreviatura => 'Coah.'},
        { :clave => '6', :nombre => 'Colima', :abreviatura => 'Col.'},
        { :clave => '10', :nombre => 'Durango', :abreviatura => 'Dgo.'},
        { :clave => '11', :nombre => 'Guanajuato', :abreviatura => 'Gto.'},
        { :clave => '12', :nombre => 'Guerrero', :abreviatura => 'Gro.'},
        { :clave => '13', :nombre => 'Hidalgo', :abreviatura => 'Hgo.'},
        { :clave => '14', :nombre => 'Jalisco', :abreviatura => 'Jal.'},
        { :clave => '15', :nombre => 'México', :abreviatura => 'Mex.'},
        { :clave => '16', :nombre => 'Michoacán de Ocampo', :abreviatura => 'Mich.'},
        { :clave => '17', :nombre => 'Morelos', :abreviatura => 'Mor.'},
        { :clave => '18', :nombre => 'Nayarit', :abreviatura => 'Nay.'},
        { :clave => '19', :nombre => 'Nuevo León', :abreviatura => 'NL'},
        { :clave => '20', :nombre => 'Oaxaca', :abreviatura => 'Oax.'},
        { :clave => '21', :nombre => 'Puebla', :abreviatura => 'Pue.'},
        { :clave => '22', :nombre => 'Querétaro', :abreviatura => 'Qro.'},
        { :clave => '23', :nombre => 'Quintana Roo', :abreviatura => 'Q. Roo'},
        { :clave => '24', :nombre => 'San Luis Potosí', :abreviatura => 'SLP'},
        { :clave => '25', :nombre => 'Sinaloa', :abreviatura => 'Sin.'},
        { :clave => '26', :nombre => 'Sonora', :abreviatura => 'Son.'},
        { :clave => '27', :nombre => 'Tabasco', :abreviatura => 'Tab.'},
        { :clave => '28', :nombre => 'Tamaulipas', :abreviatura => 'Tamps.'},
        { :clave => '29', :nombre => 'Tlaxcala', :abreviatura => 'Tlax.'},
        { :clave => '30', :nombre => 'Veracruz de Ignacio de la Llave', :abreviatura => 'Ver.'},
        { :clave => '31', :nombre => 'Yucatán', :abreviatura => 'Yuc.'},
        { :clave => '32', :nombre => 'Zacatecas', :abreviatura => 'Zac.'}
    ]

    pais = Lugar.find_by_nombre("MÉXICO")

    estados.each do |e|
      Lugar.new(clave: e[:clave], nombre: e[:nombre].mb_chars.upcase, abreviatura: e[:abreviatura], tipo: 'ESTADO', lugar_id: pais.id).save
    end
  end
end
