class AddDireccionUsuarioPasswordToConfiguraciones < ActiveRecord::Migration
  array_columns = Configuraciones.column_names

  if array_columns.include?("direccion") && array_columns.include?("usuario") && array_columns.include?("password")
  else
    add_column :configuraciones, :direccion, :string
    add_column :configuraciones, :usuario, :string
    add_column :configuraciones, :password, :string
  end
end

