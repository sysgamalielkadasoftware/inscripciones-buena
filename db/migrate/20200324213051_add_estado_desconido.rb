class AddEstadoDesconido < ActiveRecord::Migration
  def change
  	#Agregar estado desconocido
	sin_est = {
  		clave: 33,
  		nombre: "DESCONOCIDO",
  		abreviatura: "S/E"
	}

	pais = Lugar.find_by_nombre("MÉXICO")
	Lugar.new(clave: sin_est[:clave], nombre: sin_est[:nombre].mb_chars.upcase, abreviatura: sin_est[:abreviatura], tipo: 'ESTADO', lugar_id: pais.id).save
	
	#Agregar estado desconocido
	sin_mun ={
  		clave: 57,
  		nombre: "DESCONOCIDO",
  		estado: 33
	}

	estado = Lugar.find_by_clave_and_tipo(33,'ESTADO')
	Lugar.new(:clave => sin_mun[:clave], :nombre => sin_mun[:nombre].mb_chars.upcase, :lugar_id => estado.id, :tipo => 'MUNICIPIO').save

	#Agregar localidad desconocido
	sin_loc ={
  		clave: 1,
  		nombre: "DESCONOCIDO",
  		estado: 33,
  		municipio: 57
	}

	estado_d = Lugar.find_by_clave_and_tipo(33, 'ESTADO')
    municipio_d = Lugar.find_by_clave_and_tipo_and_lugar_id(57, 'MUNICIPIO', estado_d.id)

    Lugar.new(clave: sin_loc[:clave], nombre: sin_loc[:nombre].mb_chars.upcase, tipo: 'LOCALIDAD', lugar_id: municipio_d.id).save

  end
end
