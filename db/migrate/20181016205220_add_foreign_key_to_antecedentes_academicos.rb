class AddForeignKeyToAntecedentesAcademicos < ActiveRecord::Migration
  def change
    add_foreign_key :antecedentes_academicos, :aspirantes
    add_foreign_key :antecedentes_academicos, :escuelas_procedentes
  end
end
