class RemoveComprobanteFromFichas < ActiveRecord::Migration
  def change
    array_columns = Ficha.column_names

    if array_columns.include?("comprobante") && array_columns.include?("tipo_archivo") && array_columns.include?("nombre_archivo")
      remove_column :fichas, :comprobante
      remove_column :fichas, :tipo_archivo
      remove_column :fichas, :nombre_archivo
    end

  end
end
