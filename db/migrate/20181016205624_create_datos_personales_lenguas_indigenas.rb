class CreateDatosPersonalesLenguasIndigenas < ActiveRecord::Migration
  def change
    create_table :datos_personales_lenguas_indigenas do |t|
      t.references :dato_personal
      t.references :lengua_indigena

      t.timestamps null: false
    end
  end
end
