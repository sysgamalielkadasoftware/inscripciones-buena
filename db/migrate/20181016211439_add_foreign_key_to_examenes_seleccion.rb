class AddForeignKeyToExamenesSeleccion < ActiveRecord::Migration
  def change
    add_foreign_key :examenes_seleccion, :fichas
    add_foreign_key :examenes_seleccion, :aspirantes
    add_foreign_key :examenes_seleccion, :sedes_fichas
  end
end
