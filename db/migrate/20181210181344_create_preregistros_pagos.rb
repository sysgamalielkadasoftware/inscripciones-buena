class CreatePreregistrosPagos < ActiveRecord::Migration
  def change
    create_table :preregistros_pagos do |t|
      t.references :aspirante, index: true
      t.references :concept, index: true
      t.references :ciclo, index: true
      t.string :estado
      t.string :numero_referencia

      t.timestamps null: false
    end
    add_foreign_key :preregistros_pagos, :aspirantes
  end
end
