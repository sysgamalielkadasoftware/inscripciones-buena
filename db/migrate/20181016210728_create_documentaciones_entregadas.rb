class CreateDocumentacionesEntregadas < ActiveRecord::Migration
  def change
    create_table :documentaciones_entregadas do |t|
      t.references :aspirante
      t.references :documento
      t.string :estado
      t.boolean :original
      t.boolean :copia
      t.datetime :fecha_entrega_original
      t.datetime :fecha_entrega_copia
      t.datetime :fecha_limite
      t.datetime :fecha_prorroga
      t.string :observaciones

      t.timestamps null: false
    end
  end
end
