class CreateAspirantes < ActiveRecord::Migration
  def change
    create_table :aspirantes do |t|
      t.string :curp
      t.string :nombre
      t.string :apellido_paterno
      t.string :apellido_materno
      t.string :sexo
      t.integer :edad
      t.datetime :fecha_nacimiento
      t.references :usuario

      t.timestamps null: false
    end
  end
end
