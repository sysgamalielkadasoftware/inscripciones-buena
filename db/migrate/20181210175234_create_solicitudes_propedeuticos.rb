class CreateSolicitudesPropedeuticos < ActiveRecord::Migration
  def change
    create_table :solicitudes_propedeuticos do |t|
      t.references :aspirante
      t.references :alumno
      t.references :carrera, index: true
      t.references :periodo_propedeutico, index: true
      t.datetime :fecha_solicitud
      t.string :estado
      t.string :proceso

      t.timestamps null: false
    end
    add_foreign_key :solicitudes_propedeuticos, :aspirantes
    add_foreign_key :solicitudes_propedeuticos, :periodos_propedeuticos
  end
end
