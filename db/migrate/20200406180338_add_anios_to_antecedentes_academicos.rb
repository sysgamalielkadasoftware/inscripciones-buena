class AddAniosToAntecedentesAcademicos < ActiveRecord::Migration
  def change
    array_columns = AntecedenteAcademico.column_names

    if array_columns.include?("año_inicio") && array_columns.include?("año_fin")
    else      
      add_column :antecedentes_academicos, :año_inicio, :integer
      add_column :antecedentes_academicos, :año_fin, :integer
    end

  end
end
