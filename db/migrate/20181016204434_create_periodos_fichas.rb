class CreatePeriodosFichas < ActiveRecord::Migration
  def change
    create_table :periodos_fichas do |t|
      t.string :tipo
      t.datetime :fecha_inicio
      t.datetime :fecha_fin
      t.string :observacion
      t.references :ciclo

      t.timestamps null: false
    end
  end
end
