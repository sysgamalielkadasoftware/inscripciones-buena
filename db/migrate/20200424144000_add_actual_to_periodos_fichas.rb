class AddActualToPeriodosFichas < ActiveRecord::Migration
  def change
    array_columns = PeriodoFicha.column_names

    if array_columns.include?("actual")
    else      
      add_column :periodos_fichas, :actual, :boolean, :default => false
    end

  end
end
