class AddForeignKeyToDatosPersonales < ActiveRecord::Migration
  def change
    add_foreign_key :datos_personales, :aspirantes
    add_foreign_key :datos_personales, :direcciones
    add_foreign_key :datos_personales, :datos_medicos
  end
end
