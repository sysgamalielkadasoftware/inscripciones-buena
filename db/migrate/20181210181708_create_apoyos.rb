class CreateApoyos < ActiveRecord::Migration
  def change
    create_table :apoyos do |t|
      t.string :nombre
      t.string :estado

      t.timestamps null: false
    end
  end
end
