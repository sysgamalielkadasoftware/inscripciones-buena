class CreateDatosMedicos < ActiveRecord::Migration
  def change
    create_table :datos_medicos do |t|
      t.references :tipo_sangre
      t.references :discapacidad
      t.string :enfermedad_atencion_especial
      t.string :medicamento_enfermedad_atencion_especial
      t.string :alergia
      t.string :medicamento_alergia

      t.timestamps null: false
    end
  end
end
