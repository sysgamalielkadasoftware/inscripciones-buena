class ChangeAniosTypeFromIntToDateInAntecedentesAcademicos < ActiveRecord::Migration
  def self.up
    change_column :antecedentes_academicos, :año_inicio,"date USING to_date(año_inicio::varchar, 'yyyy')"
    # rename_column :antecedentes_academicos, :anio_inicio, :fecha_inicio
    change_column :antecedentes_academicos, :año_fin, "date USING to_date(año_fin::varchar, 'yyyy')"
    # rename_column :antecedentes_academicos, :anio_fin, :fecha_fin
  end
  def self.down
    change_column :antecedentes_academicos, :año_inicio,"int USING to_char(año_inicio, 'YYYY')::integer;"
    # rename_column :antecedentes_academicos, :anio_inicio, :fecha_inicio
    change_column :antecedentes_academicos, :año_fin, "int USING to_char(año_fin, 'YYYY')::integer;"
    # rename_column :antecedentes_academicos, :anio_fin, :fecha_fin
  end
end
