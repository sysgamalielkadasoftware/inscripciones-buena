class AddForeignKeyToSolicitudesFichas < ActiveRecord::Migration
  def change
    add_foreign_key :solicitudes_fichas, :aspirantes
    add_foreign_key :solicitudes_fichas, :sedes_fichas
  end
end
