class CreateAntecedentesAcademicos < ActiveRecord::Migration
  def change
    create_table :antecedentes_academicos do |t|
      t.references :escuela_procedente
      t.references :aspirante
      t.datetime :fecha_inicio
      t.datetime :fecha_fin
      t.decimal :promedio, null: false, precision: 4, scale: 2
      t.string :especialidad
      t.references :area_conocimiento
      t.string :numero_cedula
      t.string :tipo

      t.timestamps null: false
    end
  end
end
