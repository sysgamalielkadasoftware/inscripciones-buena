class AddForeignKeyToDatosPersonalesLenguasIndigenas < ActiveRecord::Migration
  def change
    add_foreign_key :datos_personales_lenguas_indigenas, :datos_personales
    add_foreign_key :datos_personales_lenguas_indigenas, :lenguas_indigenas
  end
end
