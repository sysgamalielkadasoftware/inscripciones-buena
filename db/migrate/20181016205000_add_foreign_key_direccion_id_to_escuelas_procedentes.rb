class AddForeignKeyDireccionIdToEscuelasProcedentes < ActiveRecord::Migration
  def change
    add_foreign_key :escuelas_procedentes, :direcciones
  end
end
