class CreateAnuncios < ActiveRecord::Migration
  def change
    create_table :anuncios do |t|
      t.string :mensaje
      t.timestamp :desde
      t.timestamp :hasta

      t.timestamps null: false
    end
  end
end
