class CreateFichas < ActiveRecord::Migration
  def change
    create_table :fichas do |t|
      t.references :aspirante
      t.string :numero
      t.string :estado
      t.references :carrera
      t.references :ciclo
      t.datetime :fecha_entrega
      t.references :solicitud_ficha
      t.references :alumno
      t.timestamps null: false
    end
  end
end
