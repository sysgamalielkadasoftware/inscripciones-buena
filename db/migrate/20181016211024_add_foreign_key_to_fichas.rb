class AddForeignKeyToFichas < ActiveRecord::Migration
  def change
    add_foreign_key :fichas, :aspirantes
    add_foreign_key :fichas, :solicitudes_fichas
  end
end
