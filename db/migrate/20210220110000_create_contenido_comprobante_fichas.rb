class CreateContenidoComprobanteFichas < ActiveRecord::Migration
  def self.up
    create_table :contenido_comprobante_fichas do |t|
      t.string :avisos_en
      t.string :texto_guias

      t.timestamps
    end
  end

  def self.down
      drop_table :contenido_comprobante_fichas
  end
end
