class AddSolicitudFichaIdToPreregistrosPagos < ActiveRecord::Migration
  def change
    array_columns = PreregistroPago.column_names

    if array_columns.include?("solicitud_ficha_id")      
    else
	add_column :preregistros_pagos, :solicitud_ficha_id, :integer
    end

  end
end
