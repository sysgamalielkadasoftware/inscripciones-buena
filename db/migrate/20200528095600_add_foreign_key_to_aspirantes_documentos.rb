class AddForeignKeyToAspirantesDocumentos < ActiveRecord::Migration
  def change
    add_foreign_key :aspirantes_documentos, :aspirantes
    add_foreign_key :aspirantes_documentos, :documentos_fichas
  end
end
