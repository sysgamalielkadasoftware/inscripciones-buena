class CreateLenguasIndigenas < ActiveRecord::Migration
  def change
    create_table :lenguas_indigenas do |t|
      t.string :nombre

      t.timestamps null: false
    end
  end
end
