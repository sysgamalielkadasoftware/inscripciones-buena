class CreateInscripcionesPropedeuticos < ActiveRecord::Migration
  def change
    create_table :inscripciones_propedeuticos do |t|
      t.references :aspirante
      t.references :alumno
      t.references :grupo_propedeutico, index: true
      t.references :solicitud_propedeutico, index: true
      t.string :estado
      t.references :carrera, index: true
      t.references :autorizacion, index: true

      t.timestamps null: false
    end
    add_foreign_key :inscripciones_propedeuticos, :aspirantes
    add_foreign_key :inscripciones_propedeuticos, :grupos_propedeuticos
    add_foreign_key :inscripciones_propedeuticos, :solicitudes_propedeuticos
    add_foreign_key :inscripciones_propedeuticos, :autorizaciones
  end
end
