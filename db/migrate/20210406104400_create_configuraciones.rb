class CreateConfiguraciones < ActiveRecord::Migration
  def self.up
    create_table :configuraciones do |t|
      t.string :documentos_fichas_path

      t.timestamps
    end
  end

  def self.down
    drop_table :configuraciones
  end
end
