class AddComprobanteAceptadoObservacionesToSolicitudFicha < ActiveRecord::Migration
  array_columns = SolicitudFicha.column_names

  if array_columns.include?("comprobante_aceptado") && array_columns.include?("observaciones")
  else
    add_column :solicitudes_fichas, :comprobante_aceptado, :boolean
    add_column :solicitudes_fichas, :observaciones, :string
  end
end

