class DefaultsAddMateriasExamenSeleccion < ActiveRecord::Migration
  def change
    MateriaExamenSeleccion.new(nombre: 'FÍSICA', clave: '02001', visible: true, carrera_id: 2).save
    MateriaExamenSeleccion.new(nombre: 'MATEMÁTICAS', clave: '02002', visible: true, carrera_id: 2).save
    MateriaExamenSeleccion.new(nombre: 'FÍSICA', clave: '04001', visible: true, carrera_id: 4).save
    MateriaExamenSeleccion.new(nombre: 'MATEMÁTICAS', clave: '04002', visible: true, carrera_id: 4).save
    MateriaExamenSeleccion.new(nombre: 'FÍSICA', clave: '03001', visible: true, carrera_id: 3).save
    MateriaExamenSeleccion.new(nombre: 'MATEMÁTICAS', clave: '03002', visible: true, carrera_id: 3).save
    MateriaExamenSeleccion.new(nombre: 'MATEMÁTICAS', clave: '05001', visible: true, carrera_id: 5).save
    MateriaExamenSeleccion.new(nombre: 'ADMINISTRACIÓN', clave: '05002', visible: true, carrera_id: 5).save
    MateriaExamenSeleccion.new(nombre: 'DERECHO', clave: '05003', visible: true, carrera_id: 5).save
    MateriaExamenSeleccion.new(nombre: 'ECONOMÍA', clave: '05004', visible: true, carrera_id: 5).save
    MateriaExamenSeleccion.new(nombre: 'CONTABILIDAD', clave: '05005', visible: true, carrera_id: 5).save
    MateriaExamenSeleccion.new(nombre: 'FÍSICA', clave: '07001', visible: true, carrera_id: 7).save
    MateriaExamenSeleccion.new(nombre: 'MATEMÁTICAS', clave: '07002', visible: true, carrera_id: 7).save
    MateriaExamenSeleccion.new(nombre: 'FÍSICA', clave: '06001', visible: true, carrera_id: 6).save
    MateriaExamenSeleccion.new(nombre: 'MATEMÁTICAS', clave: '06002', visible: true, carrera_id: 6).save
    MateriaExamenSeleccion.new(nombre: 'QUÍMICA', clave: '06003', visible: true, carrera_id: 6).save
    MateriaExamenSeleccion.new(nombre: 'FÍSICA', clave: '011001', visible: true, carrera_id: 8).save
    MateriaExamenSeleccion.new(nombre: 'MATEMÁTICAS', clave: '011002', visible: true, carrera_id: 8).save
    MateriaExamenSeleccion.new(nombre: 'FÍSICA', clave: '014001', visible: true, carrera_id: 9).save
    MateriaExamenSeleccion.new(nombre: 'MATEMÁTICAS', clave: '014002', visible: true, carrera_id: 9).save
    MateriaExamenSeleccion.new(nombre: 'FÍSICA', clave: '017001', visible: true, carrera_id: 10).save
    MateriaExamenSeleccion.new(nombre: 'MATEMÁTICAS', clave: '017002', visible: true, carrera_id: 10).save
    MateriaExamenSeleccion.new(nombre: 'CONOCIMIENTOS GENERALES', clave: '014003', visible: true, carrera_id: 9).save
    MateriaExamenSeleccion.new(nombre: 'CONOCIMIENTOS GENERALES', clave: '02003', visible: true, carrera_id: 2).save
    MateriaExamenSeleccion.new(nombre: 'CONOCIMIENTOS GENERALES', clave: '03003', visible: true, carrera_id: 3).save
    MateriaExamenSeleccion.new(nombre: 'CONOCIMIENTOS GENERALES', clave: '04003', visible: true, carrera_id: 4).save
    MateriaExamenSeleccion.new(nombre: 'CONOCIMIENTOS GENERALES', clave: '05006', visible: true, carrera_id: 5).save
    MateriaExamenSeleccion.new(nombre: 'CONOCIMIENTOS GENERALES', clave: '06004', visible: true, carrera_id: 6).save
    MateriaExamenSeleccion.new(nombre: 'CONOCIMIENTOS GENERALES', clave: '07003', visible: true, carrera_id: 7).save
    MateriaExamenSeleccion.new(nombre: 'CONOCIMIENTOS GENERALES', clave: '011003', visible: true, carrera_id: 8).save
    MateriaExamenSeleccion.new(nombre: 'CONOCIMIENTOS GENERALES', clave: '017003', visible: true, carrera_id: 10).save
    MateriaExamenSeleccion.new(nombre: 'FÍSICA', clave: '031001', visible: true, carrera_id: 11).save
    MateriaExamenSeleccion.new(nombre: 'MATEMÁTICAS', clave: '031002', visible: true, carrera_id: 11).save
    MateriaExamenSeleccion.new(nombre: 'CONOCIMIENTOS GENERALES', clave: '031003', visible: true, carrera_id: 11).save
  end
end
