class AddObservacionesToAspirantesDocumentos < ActiveRecord::Migration
  array_columns = AspiranteDocumento.column_names

  if array_columns.include?("observaciones")
  else
    add_column :aspirantes_documentos, :observaciones, :string
  end
end

