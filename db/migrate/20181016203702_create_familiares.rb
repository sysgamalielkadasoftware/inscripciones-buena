class CreateFamiliares < ActiveRecord::Migration
  def change
    create_table :familiares do |t|
      t.string :nombre
      t.string :apellido_paterno
      t.string :apellido_materno
      t.string :parentesco
      t.boolean :tutor
      t.string :telefono
      t.string :ocupacion
      t.references :direccion

      t.timestamps null: false
    end
  end
end
