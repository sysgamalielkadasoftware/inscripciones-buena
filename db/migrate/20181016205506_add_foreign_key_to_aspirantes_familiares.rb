class AddForeignKeyToAspirantesFamiliares < ActiveRecord::Migration
  def change
    add_foreign_key :aspirantes_familiares, :aspirantes
    add_foreign_key :aspirantes_familiares, :familiares
  end
end
