class RemoveFichaIdFromPreregistrosPagos < ActiveRecord::Migration
  def change
    array_columns = PreregistroPago.column_names

    if array_columns.include?("ficha_id")
      remove_column :preregistros_pagos, :ficha_id
    end

  end
end
