class AddForeignKeyPeriodoFichaIdToSedesFichas < ActiveRecord::Migration
  def change
    add_foreign_key :sedes_fichas, :periodos_fichas
  end
end
