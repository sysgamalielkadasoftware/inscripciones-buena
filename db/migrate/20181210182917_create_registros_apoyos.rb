class CreateRegistrosApoyos < ActiveRecord::Migration
  def change
    create_table :registros_apoyos do |t|
      t.references :aspirante, index: true
      t.references :alumno
      t.references :apoyo, index: true
      t.datetime :fecha_solicitud
      t.string :estado

      t.timestamps null: false
    end
    add_foreign_key :registros_apoyos, :aspirantes
    add_foreign_key :registros_apoyos, :apoyos
  end
end
