class AddAlumnoIdToDocumentacionEntregada < ActiveRecord::Migration
  def change
    add_reference :documentaciones_entregadas, :alumno
  end
end
