class CreateMateriasPropedeuticos < ActiveRecord::Migration
  def change
    create_table :materias_propedeuticos do |t|
      t.string :clave
      t.string :nombre
      t.boolean :visible
      t.references :carrera, index: true

      t.timestamps null: false
    end
  end
end
