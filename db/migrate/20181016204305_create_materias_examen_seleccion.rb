class CreateMateriasExamenSeleccion < ActiveRecord::Migration
  def change
    create_table :materias_examen_seleccion do |t|
      t.string :clave
      t.string :nombre, null: false
      t.boolean :visible
      t.references :carrera

      t.timestamps null: false
    end
  end
end
