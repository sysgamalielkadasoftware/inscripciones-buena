class CreateLugares < ActiveRecord::Migration
  def change
    create_table :lugares do |t|
      t.string :clave
      t.string :nombre
      t.string :abreviatura
      t.string :tipo
      t.references :lugar

      t.timestamps null: false
    end
  end
end
