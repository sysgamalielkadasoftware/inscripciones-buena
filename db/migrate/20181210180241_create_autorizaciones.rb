class CreateAutorizaciones < ActiveRecord::Migration
  def change
    create_table :autorizaciones do |t|
      t.string :nombre

      t.timestamps null: false
    end
  end
end
