class CreateGruposPropedeuticos < ActiveRecord::Migration
  def change
    create_table :grupos_propedeuticos do |t|
      t.string :nombre
      t.references :periodo_propedeutico, index: true
      t.references :carrera, index: true
      t.integer :contador

      t.timestamps null: false
    end
    add_foreign_key :grupos_propedeuticos, :periodos_propedeuticos
  end
end
