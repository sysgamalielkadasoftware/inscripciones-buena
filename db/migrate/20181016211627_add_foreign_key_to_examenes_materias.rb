class AddForeignKeyToExamenesMaterias < ActiveRecord::Migration
  def change
    add_foreign_key :examenes_materias, :materias_examen_seleccion
    add_foreign_key :examenes_materias, :examenes_seleccion
  end
end
