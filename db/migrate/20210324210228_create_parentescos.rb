class CreateParentescos < ActiveRecord::Migration
  def change
    create_table :parentescos do |t|
      t.string :parentesco

      t.timestamps null: false
    end
  end
end
