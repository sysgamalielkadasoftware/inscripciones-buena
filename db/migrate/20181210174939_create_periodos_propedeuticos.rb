class CreatePeriodosPropedeuticos < ActiveRecord::Migration
  def change
    create_table :periodos_propedeuticos do |t|
      t.datetime :fecha_inicio
      t.datetime :fecha_fin
      t.string :tipo
      t.boolean :actual
      t.references :ciclo, index: true

      t.timestamps null: false
    end
  end
end
