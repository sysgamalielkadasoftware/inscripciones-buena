class AddForeignKeyDireccionIdToFamiliares < ActiveRecord::Migration
  def change
    add_foreign_key :familiares, :direcciones
  end
end
