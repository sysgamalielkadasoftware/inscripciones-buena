class DefaultsDocumentos < ActiveRecord::Migration
  def change
    Documento.new(:nombre => 'ACTA DE NACIMIENTO', estado: Documento::ACTIVO).save
    Documento.new(:nombre => 'CERTIFICADO DE SECUNDARIA', estado: Documento::ACTIVO).save
    Documento.new(:nombre => 'CERTIFICADO DE BACHILLERATO', estado: Documento::ACTIVO).save
    Documento.new(:nombre => 'CURP', estado: Documento::ACTIVO).save
    Documento.new(:nombre => 'FOTOGRAFIAS', estado: Documento::ACTIVO).save
    Documento.new(:nombre => 'FORMATO DE PAGO DE FICHA', estado: Documento::ACTIVO).save
  end
end
