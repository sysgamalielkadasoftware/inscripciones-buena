class CreateDatosPersonales < ActiveRecord::Migration
  def change
    create_table :datos_personales do |t|
      t.references :aspirante
      t.string :email
      t.string :lugar_nacimiento
      t.string :nss
      t.string :telefono
      t.string :estado_civil
      t.string :nacionalidad
      t.references :direccion
      t.references :dato_medico

      t.timestamps null: false
    end
  end
end
