class CreateDocumentosFichas < ActiveRecord::Migration
  def self.up
    create_table :documentos_fichas do |t|
      t.string :nombre_documento
      t.boolean :obligatorio, :default => false
      t.string :observaciones

      t.timestamps
    end
  end

  def self.down
    drop_table :documentos_fichas
  end
end
