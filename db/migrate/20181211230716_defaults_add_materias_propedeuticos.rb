class DefaultsAddMateriasPropedeuticos < ActiveRecord::Migration
  def change
    MateriaPropedeutico.new(nombre: 'FÍSICA', clave: '02001', visible: true, carrera_id: 2).save
    MateriaPropedeutico.new(nombre: 'MATEMÁTICAS', clave: '02002', visible: true, carrera_id: 2).save
    MateriaPropedeutico.new(nombre: 'FÍSICA', clave: '04001', visible: true, carrera_id: 4).save
    MateriaPropedeutico.new(nombre: 'MATEMÁTICAS', clave: '04002', visible: true, carrera_id: 4).save
    MateriaPropedeutico.new(nombre: 'FÍSICA', clave: '03001', visible: true, carrera_id: 3).save
    MateriaPropedeutico.new(nombre: 'MATEMÁTICAS', clave: '03002', visible: true, carrera_id: 3).save
    MateriaPropedeutico.new(nombre: 'MATEMÁTICAS', clave: '05001', visible: true, carrera_id: 5).save
    MateriaPropedeutico.new(nombre: 'FÍSICA', clave: '07001', visible: true, carrera_id: 7).save
    MateriaPropedeutico.new(nombre: 'MATEMÁTICAS', clave: '07002', visible: true, carrera_id: 7).save
    MateriaPropedeutico.new(nombre: 'FÍSICA', clave: '06001', visible: true, carrera_id: 6).save
    MateriaPropedeutico.new(nombre: 'MATEMÁTICAS', clave: '06002', visible: true, carrera_id: 6).save
    MateriaPropedeutico.new(nombre: 'QUÍMICA', clave: '06003', visible: true, carrera_id: 6).save
    MateriaPropedeutico.new(nombre: 'FÍSICA', clave: '011001', visible: true, carrera_id: 8).save
    MateriaPropedeutico.new(nombre: 'MATEMÁTICAS', clave: '011002', visible: true, carrera_id: 8).save
    MateriaPropedeutico.new(nombre: 'FÍSICA', clave: '014001', visible: true, carrera_id: 9).save
    MateriaPropedeutico.new(nombre: 'MATEMÁTICAS', clave: '014002', visible: true, carrera_id: 9).save
    MateriaPropedeutico.new(nombre: 'FÍSICA', clave: '017001', visible: true, carrera_id: 10).save
    MateriaPropedeutico.new(nombre: 'MATEMÁTICAS', clave: '017002', visible: true, carrera_id: 10).save
    MateriaPropedeutico.new(nombre: 'LÓGICA MATEMÁTICA', clave: '02003', visible: true, carrera_id: 2).save
    MateriaPropedeutico.new(nombre: 'DISEÑO DE ALGORITMOS', clave: '02004', visible: true, carrera_id: 2).save
    MateriaPropedeutico.new(nombre: 'DISEÑO DE ALGORITMOS', clave: '04003', visible: true, carrera_id: 4).save
    MateriaPropedeutico.new(nombre: 'LÓGICA MATEMÁTICA', clave: '04004', visible: true, carrera_id: 4).save
    MateriaPropedeutico.new(nombre: 'LÓGICA MATEMÁTICA', clave: '03003', visible: true, carrera_id: 3).save
    MateriaPropedeutico.new(nombre: 'LÓGICA MATEMÁTICA', clave: '05003', visible: true, carrera_id: 5).save
    MateriaPropedeutico.new(nombre: 'LÓGICA MATEMÁTICA', clave: '06004', visible: true, carrera_id: 6).save
    MateriaPropedeutico.new(nombre: 'LÓGICA MATEMÁTICA', clave: '011003', visible: true, carrera_id: 8).save
    MateriaPropedeutico.new(nombre: 'DISEÑO DE ALGORITMOS', clave: '014003', visible: true, carrera_id: 9).save
    MateriaPropedeutico.new(nombre: 'LÓGICA MATEMÁTICA', clave: '014004', visible: true, carrera_id: 9).save
    MateriaPropedeutico.new(nombre: 'LÓGICA MATEMÁTICA', clave: '017003', visible: true, carrera_id: 10).save
    MateriaPropedeutico.new(nombre: 'DISEÑO DE ALGORITMOS', clave: '7004', visible: true, carrera_id: 7).save
    MateriaPropedeutico.new(nombre: 'BASES DE ADMINISTRACIÓN', clave: '5006', visible: true, carrera_id: 5).save
    MateriaPropedeutico.new(nombre: 'INTRODUCCIÓ A LA CONTABILIDAD', clave: '05007', visible: true, carrera_id: 5).save
    MateriaPropedeutico.new(nombre: 'DISEÑO DE ALGORITMOS', clave: '03005', visible: true, carrera_id: 3).save
    MateriaPropedeutico.new(nombre: 'DISEÑO DE ALGORITMOS', clave: '017004', visible: true, carrera_id: 10).save
    MateriaPropedeutico.new(nombre: 'DISEÑO DE ALGORITMOS', clave: '011004', visible: true, carrera_id: 8).save
    MateriaPropedeutico.new(nombre: 'LÓGICA MATEMÁTICA', clave: '07005', visible: true, carrera_id: 7).save
    MateriaPropedeutico.new(nombre: 'FÍSICA', clave: '031001', visible: true, carrera_id: 11).save
    MateriaPropedeutico.new(nombre: 'MATEMÁTICAS', clave: '031002', visible: true, carrera_id: 11).save
    MateriaPropedeutico.new(nombre: 'LÓGICA MATEMÁTICA', clave: '031003', visible: true, carrera_id: 11).save
    MateriaPropedeutico.new(nombre: 'DISEÑO DE ALGORITMOS', clave: '06006', visible: true, carrera_id: 6).save
    MateriaPropedeutico.new(nombre: 'DISEÑO DE ALGORITMOS', clave: '031004', visible: true, carrera_id: 11).save
    MateriaPropedeutico.new(nombre: 'PROBLEMAS DE MATEMÁTICAS', clave: '02005', visible: false, carrera_id: 2).save
    MateriaPropedeutico.new(nombre: 'INTRODUCCIÓN A LA COMPUTACIÓN', clave: '02006', visible: false, carrera_id: 2).save
    MateriaPropedeutico.new(nombre: 'PROBLEMAS DE MATEMÁTICAS', clave: '03004', visible: false, carrera_id: 3).save
    MateriaPropedeutico.new(nombre: 'PROBLEMAS DE MATEMÁTICAS', clave: '04005', visible: false, carrera_id: 4).save
    MateriaPropedeutico.new(nombre: 'ADMINISTRACIÓN', clave: '05002', visible: false, carrera_id: 5).save
    MateriaPropedeutico.new(nombre: 'MATEMÁTICAS FINANCIERAS', clave: '05004', visible: false, carrera_id: 5).save
    MateriaPropedeutico.new(nombre: 'CONTABILIDAD', clave: '05005', visible: false, carrera_id: 5).save
    MateriaPropedeutico.new(nombre: 'DISEÑO DE ALGORITMOS', clave: '5007', visible: false, carrera_id: 5).save
    MateriaPropedeutico.new(nombre: 'BIOLOGÍA', clave: '06005', visible: false, carrera_id: 6).save
    MateriaPropedeutico.new(nombre: 'LÓGICA Y CONJUNTOS', clave: '07003', visible: false, carrera_id: 7).save
  end
end
