class DefaultsAddDocumentosFichas < ActiveRecord::Migration
  def change
    documentos_fichas = [
        { :nombre_documento => 'CURP', :obligatorio => true},
        { :nombre_documento => 'COPIA DE ACTA DE NACIMIENTO', :obligatorio => false},
        { :nombre_documento => 'COPIA DE CERTIFICADO O CONSTANCIA DE ESTUDIOS', :obligatorio => false},
        { :nombre_documento => 'COPIA DE CERTIFICADO DE SECUNDARIA', :obligatorio => false}
    ]
  
    documentos_fichas.each do |d|
      DocumentoFicha.new(nombre_documento: d[:nombre_documento], obligatorio: d[:obligatorio]).save
    end
  end
end
