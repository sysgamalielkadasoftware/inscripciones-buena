class AddAceptadoToAspirantesDocumentos < ActiveRecord::Migration
  def change
    array_columns = AspiranteDocumento.column_names

    if array_columns.include?("aceptado")
    else      
      add_column :aspirantes_documentos, :aceptado, :boolean
    end

  end
end
