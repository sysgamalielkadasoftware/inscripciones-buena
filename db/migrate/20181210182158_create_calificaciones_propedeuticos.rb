class CreateCalificacionesPropedeuticos < ActiveRecord::Migration
  def change
    create_table :calificaciones_propedeuticos do |t|
      t.references :materia_propedeutico
      t.decimal :calificacion, :precision => 4, :scale => 1
      t.string :descripcion
      t.references :inscripcion_propedeutico

      t.timestamps null: false
    end
    add_foreign_key :calificaciones_propedeuticos, :materias_propedeuticos
    add_foreign_key :calificaciones_propedeuticos, :inscripciones_propedeuticos
  end
end
