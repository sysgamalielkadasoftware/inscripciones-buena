class AddForeignKeyToDocumentacionesEntregadas < ActiveRecord::Migration
  def change
    add_foreign_key :documentaciones_entregadas, :aspirantes
    add_foreign_key :documentaciones_entregadas, :documentos
  end
end
