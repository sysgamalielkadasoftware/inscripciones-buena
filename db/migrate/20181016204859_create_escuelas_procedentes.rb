class CreateEscuelasProcedentes < ActiveRecord::Migration
  def change
    create_table :escuelas_procedentes do |t|
      t.string :nombre, null: false
      t.references :direccion

      t.timestamps null: false
    end
  end
end
