class AddComprobanteToSolicitudesFichas < ActiveRecord::Migration
  def change
    array_columns = SolicitudFicha.column_names

    if array_columns.include?("comprobante") && array_columns.include?("tipo_archivo") && array_columns.include?("nombre_archivo")
    else      
      add_column :solicitudes_fichas, :comprobante, :binary
      add_column :solicitudes_fichas, :tipo_archivo, :string
      add_column :solicitudes_fichas, :nombre_archivo, :string
    end

  end
end
