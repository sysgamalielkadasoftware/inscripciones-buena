class CreateEspecialidades < ActiveRecord::Migration
  def change
    create_table :especialidades do |t|
      t.string :nombre
      t.string :estado
      t.timestamps null: false
    end
  end
end
