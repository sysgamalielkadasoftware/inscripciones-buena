# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20210406212246) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "antecedentes_academicos", force: :cascade do |t|
    t.integer  "escuela_procedente_id"
    t.integer  "aspirante_id"
    t.datetime "fecha_inicio"
    t.datetime "fecha_fin"
    t.decimal  "promedio",              precision: 4, scale: 2, null: false
    t.string   "especialidad"
    t.integer  "area_conocimiento_id"
    t.string   "numero_cedula"
    t.string   "tipo"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.integer  "año_inicio"
    t.integer  "año_fin"
  end

  create_table "apoyos", force: :cascade do |t|
    t.string   "nombre"
    t.string   "estado"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "aspirantes", force: :cascade do |t|
    t.string   "curp"
    t.string   "nombre"
    t.string   "apellido_paterno"
    t.string   "apellido_materno"
    t.string   "sexo"
    t.integer  "edad"
    t.datetime "fecha_nacimiento"
    t.integer  "usuario_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.binary   "foto"
    t.string   "tipo_archivo"
    t.string   "nombre_archivo"
    t.string   "correo_institucional"
    t.boolean  "estudio"
  end

  create_table "aspirantes_documentos", force: :cascade do |t|
    t.integer  "aspirante_id"
    t.integer  "documento_ficha_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "aceptado"
    t.binary   "archivo"
    t.string   "tipo_archivo"
    t.string   "nombre_archivo"
    t.string   "observaciones"
  end

  create_table "aspirantes_familiares", force: :cascade do |t|
    t.integer  "aspirante_id"
    t.integer  "familiar_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "autorizaciones", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "calificaciones_propedeuticos", force: :cascade do |t|
    t.integer  "materia_propedeutico_id"
    t.decimal  "calificacion",                precision: 4, scale: 1
    t.string   "descripcion"
    t.integer  "inscripcion_propedeutico_id"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
  end

  create_table "configuraciones", force: :cascade do |t|
    t.string   "documentos_fichas_path"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contenido_comprobante_fichas", force: :cascade do |t|
    t.string   "avisos_en"
    t.string   "texto_guias"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "datos_medicos", force: :cascade do |t|
    t.integer  "tipo_sangre_id"
    t.integer  "discapacidad_id"
    t.string   "enfermedad_atencion_especial"
    t.string   "medicamento_enfermedad_atencion_especial"
    t.string   "alergia"
    t.string   "medicamento_alergia"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  create_table "datos_personales", force: :cascade do |t|
    t.integer  "aspirante_id"
    t.string   "email"
    t.string   "lugar_nacimiento"
    t.string   "nss"
    t.string   "telefono"
    t.string   "estado_civil"
    t.string   "nacionalidad"
    t.integer  "direccion_id"
    t.integer  "dato_medico_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "datos_personales_lenguas_indigenas", force: :cascade do |t|
    t.integer  "dato_personal_id"
    t.integer  "lengua_indigena_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "direcciones", force: :cascade do |t|
    t.string   "calle"
    t.string   "numero"
    t.string   "colonia"
    t.string   "codigo_postal"
    t.integer  "lugar_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "documentaciones_entregadas", force: :cascade do |t|
    t.integer  "aspirante_id"
    t.integer  "documento_id"
    t.string   "estado"
    t.boolean  "original"
    t.boolean  "copia"
    t.datetime "fecha_entrega_original"
    t.datetime "fecha_entrega_copia"
    t.datetime "fecha_limite"
    t.datetime "fecha_prorroga"
    t.string   "observaciones"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "alumno_id"
  end

  create_table "documentos", force: :cascade do |t|
    t.string   "nombre"
    t.string   "estado"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "documentos_fichas", force: :cascade do |t|
    t.string   "nombre_documento"
    t.boolean  "obligatorio",      default: false
    t.string   "observaciones"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "escuelas_procedentes", force: :cascade do |t|
    t.string   "nombre",       null: false
    t.integer  "direccion_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "especialidades", force: :cascade do |t|
    t.string   "nombre"
    t.string   "estado"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "examenes_materias", force: :cascade do |t|
    t.integer  "materia_examen_seleccion_id"
    t.integer  "examen_seleccion_id"
    t.decimal  "calificacion"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "examenes_seleccion", force: :cascade do |t|
    t.integer  "ficha_id"
    t.integer  "aspirante_id"
    t.integer  "alumno_id"
    t.integer  "sede_ficha_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "familiares", force: :cascade do |t|
    t.string   "nombre"
    t.string   "apellido_paterno"
    t.string   "apellido_materno"
    t.string   "parentesco"
    t.boolean  "tutor"
    t.string   "telefono"
    t.string   "ocupacion"
    t.integer  "direccion_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.boolean  "finado"
  end

  create_table "fichas", force: :cascade do |t|
    t.integer  "aspirante_id"
    t.string   "numero"
    t.string   "estado"
    t.integer  "carrera_id"
    t.integer  "ciclo_id"
    t.datetime "fecha_entrega"
    t.integer  "solicitud_ficha_id"
    t.integer  "alumno_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.binary   "comprobante"
    t.string   "tipo_archivo"
    t.string   "nombre_archivo"
  end

  create_table "grupos_propedeuticos", force: :cascade do |t|
    t.string   "nombre"
    t.integer  "periodo_propedeutico_id"
    t.integer  "carrera_id"
    t.integer  "contador"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "grupos_propedeuticos", ["carrera_id"], name: "index_grupos_propedeuticos_on_carrera_id", using: :btree
  add_index "grupos_propedeuticos", ["periodo_propedeutico_id"], name: "index_grupos_propedeuticos_on_periodo_propedeutico_id", using: :btree

  create_table "inscripciones_propedeuticos", force: :cascade do |t|
    t.integer  "aspirante_id"
    t.integer  "alumno_id"
    t.integer  "grupo_propedeutico_id"
    t.integer  "solicitud_propedeutico_id"
    t.string   "estado"
    t.integer  "carrera_id"
    t.integer  "autorizacion_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "inscripciones_propedeuticos", ["autorizacion_id"], name: "index_inscripciones_propedeuticos_on_autorizacion_id", using: :btree
  add_index "inscripciones_propedeuticos", ["carrera_id"], name: "index_inscripciones_propedeuticos_on_carrera_id", using: :btree
  add_index "inscripciones_propedeuticos", ["grupo_propedeutico_id"], name: "index_inscripciones_propedeuticos_on_grupo_propedeutico_id", using: :btree
  add_index "inscripciones_propedeuticos", ["solicitud_propedeutico_id"], name: "index_inscripciones_propedeuticos_on_solicitud_propedeutico_id", using: :btree

  create_table "lenguas_indigenas", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lugares", force: :cascade do |t|
    t.string   "clave"
    t.string   "nombre"
    t.string   "abreviatura"
    t.string   "tipo"
    t.integer  "lugar_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "materias_examen_seleccion", force: :cascade do |t|
    t.string   "clave"
    t.string   "nombre",     null: false
    t.boolean  "visible"
    t.integer  "carrera_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "materias_propedeuticos", force: :cascade do |t|
    t.string   "clave"
    t.string   "nombre"
    t.boolean  "visible"
    t.integer  "carrera_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "materias_propedeuticos", ["carrera_id"], name: "index_materias_propedeuticos_on_carrera_id", using: :btree

  create_table "parentescos", force: :cascade do |t|
    t.string   "parentesco"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "periodos_fichas", force: :cascade do |t|
    t.string   "tipo"
    t.datetime "fecha_inicio"
    t.datetime "fecha_fin"
    t.string   "observacion"
    t.integer  "ciclo_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "actual",       default: false
  end

  create_table "periodos_propedeuticos", force: :cascade do |t|
    t.datetime "fecha_inicio"
    t.datetime "fecha_fin"
    t.string   "tipo"
    t.boolean  "actual"
    t.integer  "ciclo_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "periodos_propedeuticos", ["ciclo_id"], name: "index_periodos_propedeuticos_on_ciclo_id", using: :btree

  create_table "preregistros_pagos", force: :cascade do |t|
    t.integer  "aspirante_id"
    t.integer  "concept_id"
    t.integer  "ciclo_id"
    t.string   "estado"
    t.string   "numero_referencia"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "ficha_id"
    t.integer  "solicitud_ficha_id"
  end

  add_index "preregistros_pagos", ["aspirante_id"], name: "index_preregistros_pagos_on_aspirante_id", using: :btree
  add_index "preregistros_pagos", ["ciclo_id"], name: "index_preregistros_pagos_on_ciclo_id", using: :btree
  add_index "preregistros_pagos", ["concept_id"], name: "index_preregistros_pagos_on_concept_id", using: :btree

  create_table "registros_apoyos", force: :cascade do |t|
    t.integer  "aspirante_id"
    t.integer  "alumno_id"
    t.integer  "apoyo_id"
    t.datetime "fecha_solicitud"
    t.string   "estado"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "registros_apoyos", ["apoyo_id"], name: "index_registros_apoyos_on_apoyo_id", using: :btree
  add_index "registros_apoyos", ["aspirante_id"], name: "index_registros_apoyos_on_aspirante_id", using: :btree

  create_table "sedes_fichas", force: :cascade do |t|
    t.string   "sede",             null: false
    t.datetime "fecha_examen"
    t.string   "hora_examen"
    t.integer  "periodo_ficha_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "solicitudes_fichas", force: :cascade do |t|
    t.integer  "aspirante_id"
    t.integer  "carrera_id"
    t.integer  "ciclo_id"
    t.string   "proceso"
    t.string   "estado"
    t.string   "aprobado"
    t.integer  "alumno_id"
    t.integer  "sede_ficha_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.binary   "comprobante"
    t.string   "tipo_archivo"
    t.string   "nombre_archivo"
    t.boolean  "comprobante_aceptado"
    t.string   "observaciones"
  end

  create_table "solicitudes_propedeuticos", force: :cascade do |t|
    t.integer  "aspirante_id"
    t.integer  "alumno_id"
    t.integer  "carrera_id"
    t.integer  "periodo_propedeutico_id"
    t.datetime "fecha_solicitud"
    t.string   "estado"
    t.string   "proceso"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "solicitudes_propedeuticos", ["carrera_id"], name: "index_solicitudes_propedeuticos_on_carrera_id", using: :btree
  add_index "solicitudes_propedeuticos", ["periodo_propedeutico_id"], name: "index_solicitudes_propedeuticos_on_periodo_propedeutico_id", using: :btree

  create_table "usuarios", force: :cascade do |t|
    t.string   "username",                            null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "usuarios", ["email"], name: "index_usuarios_on_email", unique: true, using: :btree
  add_index "usuarios", ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "antecedentes_academicos", "aspirantes"
  add_foreign_key "antecedentes_academicos", "escuelas_procedentes"
  add_foreign_key "aspirantes", "usuarios"
  add_foreign_key "aspirantes_documentos", "aspirantes"
  add_foreign_key "aspirantes_documentos", "documentos_fichas"
  add_foreign_key "aspirantes_familiares", "aspirantes"
  add_foreign_key "aspirantes_familiares", "familiares"
  add_foreign_key "calificaciones_propedeuticos", "inscripciones_propedeuticos"
  add_foreign_key "calificaciones_propedeuticos", "materias_propedeuticos"
  add_foreign_key "datos_personales", "aspirantes"
  add_foreign_key "datos_personales", "datos_medicos"
  add_foreign_key "datos_personales", "direcciones"
  add_foreign_key "datos_personales_lenguas_indigenas", "datos_personales"
  add_foreign_key "datos_personales_lenguas_indigenas", "lenguas_indigenas"
  add_foreign_key "direcciones", "lugares"
  add_foreign_key "documentaciones_entregadas", "aspirantes"
  add_foreign_key "documentaciones_entregadas", "documentos"
  add_foreign_key "escuelas_procedentes", "direcciones"
  add_foreign_key "examenes_materias", "examenes_seleccion"
  add_foreign_key "examenes_materias", "materias_examen_seleccion"
  add_foreign_key "examenes_seleccion", "aspirantes"
  add_foreign_key "examenes_seleccion", "fichas"
  add_foreign_key "examenes_seleccion", "sedes_fichas"
  add_foreign_key "familiares", "direcciones"
  add_foreign_key "fichas", "aspirantes"
  add_foreign_key "fichas", "solicitudes_fichas"
  add_foreign_key "grupos_propedeuticos", "periodos_propedeuticos"
  add_foreign_key "inscripciones_propedeuticos", "aspirantes"
  add_foreign_key "inscripciones_propedeuticos", "autorizaciones"
  add_foreign_key "inscripciones_propedeuticos", "grupos_propedeuticos"
  add_foreign_key "inscripciones_propedeuticos", "solicitudes_propedeuticos"
  add_foreign_key "lugares", "lugares"
  add_foreign_key "preregistros_pagos", "aspirantes"
  add_foreign_key "registros_apoyos", "apoyos"
  add_foreign_key "registros_apoyos", "aspirantes"
  add_foreign_key "sedes_fichas", "periodos_fichas"
  add_foreign_key "solicitudes_fichas", "aspirantes"
  add_foreign_key "solicitudes_fichas", "sedes_fichas"
  add_foreign_key "solicitudes_propedeuticos", "aspirantes"
  add_foreign_key "solicitudes_propedeuticos", "periodos_propedeuticos"
end
