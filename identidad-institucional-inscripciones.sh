#! /bin/bash
# identidad-institucional-inscripciones	Actualiza la identidad institucional de acuerdo a la universidad
#
# Author: 
# description: Reemplaza archivos para identidad de acuerdo a cada universidad
#
#

usage ()
{
        echo $"Uso: $0 {utm|unsij|unsis|nova|unicha|uncos|unistmo|unca|umar|unpa}"
	exit 1
}

replace_aplica_todos()
{
	echo -e "Iniciando el copiado de los archivos generales de identidad para la ${UNIVERSIDAD}...\n"

	cp app/identidad_suneo/"$UNIVERSIDAD"/logotipos/logo.png app/assets/images/Logo-UTM1.png
	echo "Copiando Logo-UTM1.png ${UNIVERSIDAD}"

	echo -e "\nProceso terminado ${UNIVERSIDAD}"
}

replace_unca()
{
	echo -e "Iniciando el copiado de los archivos específicos para la ${UNIVERSIDAD}...\n"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/shared/_mensajes.html.erb app/views/shared/_mensajes.html.erb
#	echo "Copiando _mensajes.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_fichas/index.html.erb app/views/solicitudes_fichas/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_inscripciones/show.html.erb app/views/solicitudes_inscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/show.html.erb app/views/solicitudes_reinscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/new.html.erb app/views/solicitudes_reinscripciones/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/index.html.erb app/views/solicitud_propedeutico/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/new.html.erb app/views/solicitud_propedeutico/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"

	echo -e "\nProceso terminado ${UNIVERSIDAD}"
	exit 1
}

replace_uncos()
{
	echo -e "Iniciando el copiado de los archivos específicos para la ${UNIVERSIDAD}...\n"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/shared/_mensajes.html.erb app/views/shared/_mensajes.html.erb
#	echo "Copiando _mensajes.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_fichas/index.html.erb app/views/solicitudes_fichas/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_inscripciones/show.html.erb app/views/solicitudes_inscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/show.html.erb app/views/solicitudes_reinscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/new.html.erb app/views/solicitudes_reinscripciones/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/index.html.erb app/views/solicitud_propedeutico/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/new.html.erb app/views/solicitud_propedeutico/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"

	echo -e "\nProceso terminado ${UNIVERSIDAD}"
	exit 1
}

replace_utm()
{
	echo -e "Iniciando el copiado de los archivos específicos para la ${UNIVERSIDAD}...\n"
	echo -e "\nProceso terminado ${UNIVERSIDAD}"
	exit 1
}

replace_unsij()
{
	echo -e "Iniciando el copiado de los archivos específicos para la ${UNIVERSIDAD}...\n"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/shared/_mensajes.html.erb app/views/shared/_mensajes.html.erb
#	echo "Copiando _mensajes.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_fichas/index.html.erb app/views/solicitudes_fichas/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_inscripciones/show.html.erb app/views/solicitudes_inscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/show.html.erb app/views/solicitudes_reinscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/new.html.erb app/views/solicitudes_reinscripciones/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/index.html.erb app/views/solicitud_propedeutico/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/new.html.erb app/views/solicitud_propedeutico/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"

	echo -e "\nProceso terminado ${UNIVERSIDAD}"
	exit 1
}

replace_nova()
{
	echo -e "Iniciando el copiado de los archivos específicos para la ${UNIVERSIDAD}...\n"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/shared/_mensajes.html.erb app/views/shared/_mensajes.html.erb
#	echo "Copiando _mensajes.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_fichas/index.html.erb app/views/solicitudes_fichas/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_inscripciones/show.html.erb app/views/solicitudes_inscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/show.html.erb app/views/solicitudes_reinscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/new.html.erb app/views/solicitudes_reinscripciones/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/index.html.erb app/views/solicitud_propedeutico/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/new.html.erb app/views/solicitud_propedeutico/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"

	echo -e "\nProceso terminado ${UNIVERSIDAD}"
	exit 1
}

replace_unicha()
{
	echo -e "Iniciando el copiado de los archivos específicos para la ${UNIVERSIDAD}...\n"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/shared/_mensajes.html.erb app/views/shared/_mensajes.html.erb
#	echo "Copiando _mensajes.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_fichas/index.html.erb app/views/solicitudes_fichas/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_inscripciones/show.html.erb app/views/solicitudes_inscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/show.html.erb app/views/solicitudes_reinscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/new.html.erb app/views/solicitudes_reinscripciones/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/index.html.erb app/views/solicitud_propedeutico/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/new.html.erb app/views/solicitud_propedeutico/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"

	echo -e "\nProceso terminado ${UNIVERSIDAD}"
	exit 1
}

replace_unistmo()
{
	echo -e "Iniciando el copiado de los archivos específicos para la ${UNIVERSIDAD}...\n"
	echo -e "\nProceso terminado ${UNIVERSIDAD}"
	exit 1
}

replace_umar()
{
	echo -e "Iniciando el copiado de los archivos específicos para la ${UNIVERSIDAD}...\n"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/shared/_mensajes.html.erb app/views/shared/_mensajes.html.erb
#	echo "Copiando _mensajes.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_fichas/index.html.erb app/views/solicitudes_fichas/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_inscripciones/show.html.erb app/views/solicitudes_inscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/show.html.erb app/views/solicitudes_reinscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/new.html.erb app/views/solicitudes_reinscripciones/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/index.html.erb app/views/solicitud_propedeutico/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/new.html.erb app/views/solicitud_propedeutico/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"
#	echo -e "\nProceso terminado ${UNIVERSIDAD}"
	exit 1
}

replace_unpa()
{
	echo -e "Iniciando el copiado de los archivos específicos para la ${UNIVERSIDAD}...\n"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/shared/_mensajes.html.erb app/views/shared/_mensajes.html.erb
#	echo "Copiando _mensajes.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_fichas/index.html.erb app/views/solicitudes_fichas/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_inscripciones/show.html.erb app/views/solicitudes_inscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/show.html.erb app/views/solicitudes_reinscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/new.html.erb app/views/solicitudes_reinscripciones/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/index.html.erb app/views/solicitud_propedeutico/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/new.html.erb app/views/solicitud_propedeutico/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"

	echo -e "\nProceso terminado ${UNIVERSIDAD}"
	exit 1
}

replace_unsis()
{
	echo -e "Iniciando el copiado de los archivos específicos para la ${UNIVERSIDAD}...\n"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/shared/_mensajes.html.erb app/views/shared/_mensajes.html.erb
#	echo "Copiando _mensajes.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_fichas/index.html.erb app/views/solicitudes_fichas/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_inscripciones/show.html.erb app/views/solicitudes_inscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/show.html.erb app/views/solicitudes_reinscripciones/show.html.erb
#	echo "Copiando show.html.erb ${UNIVERSIDAD}"

#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitudes_reinscripciones/new.html.erb app/views/solicitudes_reinscripciones/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/index.html.erb app/views/solicitud_propedeutico/index.html.erb
#	echo "Copiando index.html.erb ${UNIVERSIDAD}"
#
#	cp app/identidad_suneo/"$UNIVERSIDAD"/app/views/solicitud_propedeutico/new.html.erb app/views/solicitud_propedeutico/new.html.erb
#	echo "Copiando new.html.erb ${UNIVERSIDAD}"

	echo -e "\nProceso terminado ${UNIVERSIDAD}"
	exit 1
}


case "$1" in
    unca|UNCA) 
	UNIVERSIDAD="${1^^}"
	replace_aplica_todos
	replace_unca ;;
    uncos|UNCOS) 
	UNIVERSIDAD="${1^^}"
	replace_aplica_todos
	replace_uncos ;;
    utm|UTM)
	UNIVERSIDAD="${1^^}"
	replace_aplica_todos
	replace_utm ;;
    unsij|UNSIJ)
	UNIVERSIDAD="${1^^}"
	replace_aplica_todos
	replace_unsij ;;
    unsis|UNSIS)
	UNIVERSIDAD="${1^^}"
	replace_aplica_todos
	replace_unsis ;;
    nova|NOVA)
	UNIVERSIDAD="${1^^}"
	replace_aplica_todos
	replace_nova ;;
    unicha|UNICHA)
	UNIVERSIDAD="${1^^}"
	replace_aplica_todos
	replace_unicha ;;
    unistmo|UNISTMO)
	UNIVERSIDAD="${1^^}"
	replace_aplica_todos
	replace_unistmo ;;
    umar|UMAR)
	UNIVERSIDAD="${1^^}"
	replace_aplica_todos
	replace_umar ;;
    unpa|UNPA)
	UNIVERSIDAD="${1^^}"
	replace_aplica_todos
	replace_unpa ;;
    *) usage ;;
esac

exit 0



