namespace :users do
  desc "Genera los usuarios para los alumnos inscritos en el ciclo escolar indicado"
  task :add_users_alumnos => :environment do
    ciclo = Ciclo.get_actual
    alumnos = Alumno.select("distinct(alumnos.id)").joins(:inscripciones).where("inscripciones.ciclo_id = ?", ciclo.id)
    alumnos.each do |a|
      alumno = Alumno.find(a.id)
      usuario = Usuario.find_by_username(alumno.curp)
      unless usuario.blank?
        puts "usuario existente"
      else
        puts "usuario no existe se crea"
        usuario = Usuario.new
        usuario.username = alumno.curp
        usuario.password = alumno.matricula
        usuario.password_confirmation = alumno.matricula
        usuario.email = alumno.correo_electronico.nil? ? 'sin_asignar@gmail.com' : alumno.correo_electronico
        usuario.save
      end
    end
  end

  desc "Restablece la contraseña de todos los usuarios existentes"
  task :restablecer_password_usuarios => :environment do
    usuarios = Usuario.all
    if (usuarios.count !=0)
      usuarios.each do |u|
        alumno = Alumno.where("curp = ?", u.username.upcase).first
        if !alumno.blank?
          usuario = Usuario.find_by_username(alumno.curp.downcase)
          unless usuario.blank?
            puts "Usuario existente"
            usuario.password = alumno.matricula
            usuario.password_confirmation = alumno.matricula
            if usuario.save
              puts "¡Finalizado!, Se han restablecido los contraseñas con éxito"
            else
              puts "#{usuario.username} Error al actualizar!!"
            end
          else
            puts "El usuario no existe"
          end
        end
      end   
    end
  end

end