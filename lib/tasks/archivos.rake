namespace :archivos do
  desc "convierte la informacion de documentos de fichas en la bd a archivos"
  task :convierte => :environment do
    _path = SolicitudFicha::RUTA_COMPROBANTES
    periodo_ficha = PeriodoFicha.where("actual = ?", true).first

    aspirantes = Aspirante.where.not(:foto => nil)

    if !periodo_ficha.blank?
      aspirantes.each do |aspirante|
        if (!aspirante.blank? && !aspirante.foto.blank?)
          name = aspirante.curp.upcase + "_FOTO_"  + periodo_ficha.ciclo.ciclo.to_s + "_" + periodo_ficha.tipo.to_s + "." + aspirante.nombre_archivo.split('.').last
          fpath = File.join(_path,name)

          decoded_data=Base64.decode64(aspirante.foto)

          File.new(fpath, "w+b")
          File.open(fpath, "w+b") { |f| f.write(decoded_data) }

          # if (!(aspirante.tipo_archivo.eql?("application/pdf")))
          #   compressed_file_size = (File.size(fpath).to_f / 2**20).round(2)
          #
          #   if compressed_file_size > 5
          #     Piet.optimize(fpath, {:verbose => true, :quality => 50})
          #   end
          # end

          puts "Convirtiendo... #{fpath}"
          aspirante.nombre_archivo = name
          aspirante.foto = nil
          aspirante.save
        end

        solicitud_ficha = SolicitudFicha.where(:aspirante_id => aspirante.id).order("created_at desc").first

        if !solicitud_ficha.blank?
          if !solicitud_ficha.comprobante.blank?
            name = aspirante.curp.upcase + "_COMPROBANTE_"  + periodo_ficha.ciclo.ciclo.to_s + "_" + periodo_ficha.tipo.to_s + "." + solicitud_ficha.nombre_archivo.split('.').last
            fpath = File.join(_path,name)

            decoded_data=Base64.decode64(solicitud_ficha.comprobante)

            File.new(fpath, "w+b")
            File.open(fpath, "w+b") { |f| f.write(decoded_data) }

            # if (!(solicitud_ficha.tipo_archivo.eql?("application/pdf")))
            #   compressed_file_size = (File.size(fpath).to_f / 2**20).round(2)
            #
            #   if compressed_file_size > 5
            #     Piet.optimize(fpath, {:verbose => true, :quality => 50})
            #   end
            # end

            puts "Convirtiendo... #{fpath}"
            solicitud_ficha.nombre_archivo = name
            solicitud_ficha.comprobante = nil
            solicitud_ficha.save
          end
        end

        aspirante_documentos = AspiranteDocumento.where(:aspirante_id => aspirante.id)

        aspirante_documentos.each do |documento|
          name = aspirante.curp.upcase + "_FICHA_DOCUMENTO_" + documento.documento_ficha_id.to_s + "_" + periodo_ficha.ciclo.ciclo.to_s + "_" + periodo_ficha.tipo.to_s + "." + documento.nombre_archivo.split('.').last
          fpath = File.join(_path,name)

          decoded_data=Base64.decode64(documento.archivo)

          File.new(fpath, "w+b")
          File.open(fpath, "w+b") { |f| f.write(decoded_data) }

          # if (!(documento.tipo_archivo.eql?("application/pdf")))
          #   compressed_file_size = (File.size(fpath).to_f / 2**20).round(2)
          #
          #   if compressed_file_size > 5
          #     Piet.optimize(fpath, {:verbose => true, :quality => 50})
          #   end
          # end

          puts "Convirtiendo... #{fpath}"
          documento.nombre_archivo = name
          documento.archivo = nil
          documento.save
        end
      end
      puts "Conversión de archivos finalizado."
    end
  end
end