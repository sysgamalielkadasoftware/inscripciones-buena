module Reinscripciones
  module Historial
    include Calificaciones
    include Planes

    def Historial.check_for_next(historial_academico, alumno, periodo)
      last_inscripcion = Inscripcion.get_last_by_alumno(alumno)
      reinscripcion = Hash.new
      reinscripcion[:can] = false

      if Historial::is_invalid_for_current_periodo?(historial_academico, alumno, periodo)
        reinscripcion[:message] = "El alumno ya se encuentra ReInscrito en el periodo seleccionado."
        return reinscripcion
      end

      asignaturas_reprobadas = 0
      semestres_aprobados = 0
      debe_mas_asignaturas_permitidas = false
      max_to_evaluate = 0
      data = Array.new

      historial_academico.each do |historial|
        asignaturas_reprobadas += historial[:count_reprobadas]
        data.push(historial)

        puts "Senestres. " + historial[:clave].to_s
        semestre_valid = Historial::is_semestre_valid?(historial[:clave], periodo)
        puts "------------************ #{asignaturas_reprobadas}"

        max_to_evaluate += 1 if !max_to_evaluate.eql?(0)

        if Historial::should_stop?(historial)
          asignaturas_reprobadas -= historial[:count_reprobadas]
          data.pop unless semestre_valid
          break
        end

        if historial[:aprobado].eql?(Inscripcion::APROBADO)
          semestres_aprobados += 1
          data.pop
          break if max_to_evaluate >= 3
          next
        end

        if max_to_evaluate >= 3
          data.pop unless semestre_valid
          break
        end

        max_to_evaluate += 1 if max_to_evaluate.eql?(0)

        if asignaturas_reprobadas.eql?(1)
          data.pop unless semestre_valid
          next
        end

        if asignaturas_reprobadas.eql?(2)
          data.pop unless semestre_valid
          if periodo.is_tipo_a?
            asignaturas_reprobadas -= historial[:count_reprobadas]
            debe_mas_asignaturas_permitidas = true
            break
          end
          next if periodo.is_tipo_b?
        end

        if asignaturas_reprobadas >= 3
          asignaturas_reprobadas -= historial[:count_reprobadas]
          data.pop unless semestre_valid
          debe_mas_asignaturas_permitidas = true
          break
        end
      end

      if semestres_aprobados.eql?(Semestr::DECIMO)
        reinscripcion[:message] = "El alumno ha concluido satisfactoriamente con el plan de estudios."
        return reinscripcion
      end

      if data.empty? and debe_mas_asignaturas_permitidas
        reinscripcion[:message] = "El alumno no se puede reinscribir, debe más asignaturas de las permitidas. (Reglamento 2012)"
        return reinscripcion
      end

      if data.empty?
        reinscripcion[:message] = "El periodo no corresponde al semestre de inscripción del alumno."
        return reinscripcion
      end

      beca_base =[{:name => "Sin Asignar" ,:id => nil}, {:name => "  0%" ,:id => 0},{:name => " 25%", :id => 25},{:name => " 50%", :id => 50},{:name => " 75%", :id => 75},{:name => "100%", :id => 100}]

      data.each do |d|
        inscripcion = Inscripcion.joins(:semestr).
            where(:inscripciones => {:alumno_id => alumno, :ciclo_id => periodo},
                  :semestres => {:clave => d[:clave]}).first

        puts "SEMESTRE: " + d[:clave].to_s
        if inscripcion.blank?
          d[:is_inscrito] = false
          d[:groups_list] = Historial::get_groups_list(last_inscripcion.carrera, d[:clave], periodo)
          d[:beca] =  beca_base
        else
          d[:is_inscrito] = true
          d[:groups_list] = inscripcion.grupo_nombre
          d[:con_beca] =  inscripcion.beca.nil? ? false : true
          d[:beca] = inscripcion.beca.nil? ? beca_base : inscripcion.beca
        end
        d[:status] = Historial::get_status_semestre(alumno, d, asignaturas_reprobadas)
        d[:asignaturas] = Historial::validate_asignaturas_anteriores(alumno, periodo, d[:clave], d[:asignaturas])

#        reinscripcion[:message] = "El Plan de Estudios vigente no corresponde a esta Inscripción" unless Planes::is_plan_vigente?(last_inscripcion, d[:clave])

      end

      data.delete_if { |d| !Historial::are_asignaturas_valids?(d[:asignaturas]) }

      if !data.empty?
        reinscripcion[:can] = true
      else
        reinscripcion[:message] = "El alumno no puede cursar ninguna asignatura en este periodo."
      end

      reinscripcion[:info_all] = data

      reinscripcion
    end

    def Historial.check_for_next2016(historial_academico, alumno, periodo)
      last_inscripcion = Inscripcion.get_last_by_alumno(alumno)
      reinscripcion = Hash.new
      reinscripcion[:can] = false

      if Historial::is_invalid_for_current_periodo?(historial_academico, alumno, periodo)
        reinscripcion[:message] = "El alumno ya se encuentra Reinscrito en el periodo seleccionado. (Reglamento 2016)"
        return reinscripcion
      end

      asignaturas_reprobadas = 0
      semestres_aprobados = 0
      debe_mas_asignaturas_permitidas = false
      max_to_evaluate = 0
      data = Array.new

      historial_academico.each do |historial|
        asignaturas_reprobadas += historial[:count_reprobadas]
        data.push(historial)
        puts "Senestres. " + historial[:clave].to_s
        semestre_valid = Historial::is_semestre_valid?(historial[:clave], periodo)
        puts "------------************ #{asignaturas_reprobadas}"

        max_to_evaluate += 1 if !max_to_evaluate.eql?(0)

        if Historial::should_stop?(historial)
          asignaturas_reprobadas -= historial[:count_reprobadas]
          data.pop unless semestre_valid
          break
        end

        if historial[:aprobado].eql?(Inscripcion::APROBADO)
          semestres_aprobados += 1
          data.pop
          break if max_to_evaluate > 3
          next
        end

        if max_to_evaluate > 3
          data.pop unless semestre_valid
          break
        end

        max_to_evaluate += 1 if max_to_evaluate.eql?(0)

        if asignaturas_reprobadas.eql?(1)
          data.pop unless semestre_valid
          next
        end

        if asignaturas_reprobadas.eql?(2)
          data.pop unless semestre_valid
          if periodo.is_tipo_a?
            asignaturas_reprobadas -= historial[:count_reprobadas]
            debe_mas_asignaturas_permitidas = true
            break
          end
          next if periodo.is_tipo_b?
        end

        if asignaturas_reprobadas >= 3
          asignaturas_reprobadas -= historial[:count_reprobadas]
          data.pop unless semestre_valid
          debe_mas_asignaturas_permitidas = true
          break
        end
      end

      if semestres_aprobados.eql?(Semestr::DECIMO)
        reinscripcion[:message] = "El alumno ha concluido satisfactoriamente con el plan de estudios."
        return reinscripcion
      end

      if data.empty? and debe_mas_asignaturas_permitidas
        reinscripcion[:message] = "No se puede reinscribir, debe más asignaturas de las permitidas. (Reglamento 2016)"
        return reinscripcion
      end

      if data.empty?
        reinscripcion[:message] = "El periodo no corresponde al semestre de inscripción del alumno."
        return reinscripcion
      end

      beca_base =[{:name => "Sin Asignar" ,:id => nil}, {:name => "  0%" ,:id => 0},{:name => " 25%", :id => 25},{:name => " 50%", :id => 50},{:name => " 75%", :id => 75},{:name => "100%", :id => 100}]

      data.each do |d|
        inscripcion = Inscripcion.joins(:semestr).
            where(:inscripciones => {:alumno_id => alumno, :ciclo_id => periodo},
                  :semestres => {:clave => d[:clave]}).first
        if inscripcion.blank?
          d[:is_inscrito] = false
          d[:groups_list] = Historial::get_groups_list(last_inscripcion.carrera, d[:clave], periodo)
          d[:beca] =  beca_base
        else
          d[:is_inscrito] = true
          d[:groups_list] = inscripcion.grupo_nombre
          d[:con_beca] =  inscripcion.beca.nil? ? false : true
          d[:beca] = inscripcion.beca.nil? ? beca_base : inscripcion.beca
        end
        d[:status] = Historial::get_status_semestre(alumno, d, asignaturas_reprobadas)
        d[:asignaturas] = Historial::validate_asignaturas_anteriores(alumno, periodo, d[:clave], d[:asignaturas])

#        reinscripcion[:message] = "El Plan de Estudios vigente no corresponde a esta Inscripción" unless Planes::is_plan_vigente?(last_inscripcion, d[:clave])

      end

      data.delete_if { |d| !Historial::are_asignaturas_valids?(d[:asignaturas]) }

      if !data.empty?
        data.each do |d|
          d[:asignaturas].each do |asignatura|
            if asignatura[:can]
              result = InscripcionCurso.cuantos_inscrito?(alumno[:id], asignatura[:id])
              if result.size >= 2
                reinscripcion[:message] = "El alumno NO puede cursar más de dos veces una asignatura."
                return reinscripcion
              end
            end
          end
        end
      end

      if !data.empty?
        reinscripcion[:can] = true
      else
        reinscripcion[:message] = "El alumno no puede cursar ninguna asignatura en este periodo. Nuevo Reglamento"
      end

      reinscripcion[:info_all] = data

      reinscripcion
    end

    def Historial.check_for_especialidades(reinscripcion)
      if reinscripcion[:has_especialidades]
        reinscripcion[:datos][:info_all].each do |semestre|
          especialidad = Hash.new
          semestre[:has_especialidad] = Planes::has_especialidades_in_semestre?(reinscripcion[:carrera_id], reinscripcion[:plan_estudios], semestre[:clave])
          especialidad[:start] = Planes::which_semestre_especialidades_start?(reinscripcion[:carrera_id], reinscripcion[:plan_estudios])
          especialidad[:end] = Planes::which_semestre_especialidades_end?(reinscripcion[:carrera_id], reinscripcion[:plan_estudios])
          semestre[:asignaturas].delete_if { |a| a[:is_optativa] and !a[:optativas].find{|x| /OI/.match(x[:clave])} }

          if semestre[:clave] > especialidad[:start]
            especialidad_nombre = Especialidad.select("distinct especialidades.id, especialidades.nombre").
                joins(:asignaturas => {:cursos => :inscripciones}).
                where(:inscripciones => {:carrera_id => reinscripcion[:carrera_id], :alumno_id => reinscripcion[:alumno_id]}).first

            especialidad[:nombre] = especialidad_nombre.nombre
            especialidad[:optativas] = Planes::asignaturas_for_especialidad(especialidad_nombre.id, semestre[:clave])
          else
            especialidad[:nombre] = Planes::get_especialidades(reinscripcion[:carrera_id], reinscripcion[:plan_estudios])
          end

          semestre[:especialidad] = especialidad
        end
      end

      reinscripcion
    end

    def Historial.is_invalid_for_current_periodo?(historial_academico, alumno, periodo)
      current_inscripciones = Inscripcion.where("alumno_id = ? and ciclo_id = ?", alumno, periodo)
      return false if current_inscripciones.empty?
      historial_academico.each do |historial|
        current_inscripciones.each do |current_inscripcion|
          semestre = current_inscripcion.semestr
          if semestre.clave.eql?(historial[:clave])
            historial[:asignaturas].each do |asignatura|
              next if asignatura[:aprobada].eql?(InscripcionCurso::APROBADA)
              if asignatura[:is_optativa]
                exist = false
                asignatura[:optativas].each do |optativa|
                  exist = InscripcionCurso.exist?(alumno, periodo, semestre.clave, optativa[:id])
                  break if exist
                end
                return false unless exist
              else
                return false unless InscripcionCurso.exist?(alumno, periodo, semestre.clave, asignatura[:id])
              end
            end
          end
        end
      end

      return true
    end

    def Historial.is_semestre_valid?(clave, periodo)
      ((clave.odd? and periodo.is_tipo_a?) or (clave.even? and periodo.is_tipo_b?))
    end

    def Historial.should_stop?(historial)
      return true if historial[:periodo].nil? and historial[:aprobado].eql?(Inscripcion::REPROBADO)
      return true if historial[:count_reprobadas].eql?(historial[:asignaturas].size)

      false
    end

    def Historial.get_groups_list(carrera, clave, periodo)
      groups_all = Grupo.select("distinct grupos.id, grupos.nombre, grupos.contador").
          joins(:inscripciones => :semestr).
          where(:inscripciones => {:ciclo_id => periodo, :carrera_id => carrera}, :semestres => {:clave => clave}).
          order('grupos.nombre')

      groups_list = Array.new
      group_basic = clave.to_s + carrera.codigo_carrera.to_s

      groups_all.each do |grupo|
        group_info = Hash.new
        group_info[:name] = grupo.get_description
        group_info[:id] = grupo.nombre
        groups_list.push(group_info)
      end

      cont = 0
      index = 0

      while cont < 5 and index < Grupo::LETRA.size
        group_tmp = group_basic + '-' + Grupo::LETRA[index.to_s].to_s

        unless Grupo.joins(:inscripciones).exists?(:grupos => {:ciclo_id => periodo, :nombre => group_tmp}, :inscripciones => {:carrera_id => carrera})
          group_info = Hash.new
          group_info[:name] = group_tmp + ' | NUEVO GRUPO'
          group_info[:id] = group_tmp
          groups_list.push(group_info)
          cont += 1
        end

        index += 1
      end

      groups_list
    end

    def Historial.get_status_semestre(alumno, semestre, asignaturas_reprobadas)
      unless Inscripcion.exist?(alumno, semestre[:clave])
        asignaturas_reprobadas.eql?(0) ? Inscripcion::REGULAR : Inscripcion::IRREGULAR
      else
        Inscripcion::REPETIDOR
      end
    end

    def Historial.validate_asignaturas_anteriores(alumno, periodo, semestre, asignaturas)
      break_bucle = false
      # Para cada asignatura se revisa si se puede reinscribir
      asignaturas.each do |asignatura|
        # Se verifica si está inscrito para el periodo actual.
        if InscripcionCurso.exist?(alumno, periodo, semestre, asignatura[:id])
          asignatura = Historial::set_info_to_asignatura(asignatura, false, "INSCRITO", nil)
          # Revisamos la siguiente asignatura.
          next
        end

        # Revisamos si la asignatura es optativa
        if asignatura[:is_optativa]
          asignatura[:optativas].each do |optativa|
            if InscripcionCurso.exist?(alumno, periodo, semestre, optativa[:id])
              asignatura = Historial::set_info_to_asignatura(asignatura, false, "INSCRITO", nil)
              break_bucle = true
              break
            end
          end
          # Indica que ya se encuentra inscrito para alguna optativa.
          next if break_bucle
        end
        if asignatura[:is_seriada]
          # Se verifica para la seriación de la asignatura a reinscribir.
          asignatura =  Historial::verifica_seriacion(asignatura,alumno)
          # Revisamos la siguiente asignatura.
          next
        end
        # Verifica cual de las optativas se ha inscrito
        if asignatura[:is_optativa]
          # Si el alumno no ha cursado alguna optativa se verifica la siguiente asignatura.
          unless asignatura[:is_selected]
            asignatura = Historial::set_info_to_asignatura(asignatura, true, "POR CURSAR", InscripcionCurso::REGULAR)
            next
          end
          # Hay que revisar cuál optaviva ya seleccionó.
          asignatura[:optativas].each do |optativa|
            if optativa[:selected] and optativa[:aprobada].eql?(InscripcionCurso::APROBADA)
              asignatura = Historial::set_info_to_asignatura(asignatura, false, "APROBADA", nil)
              break
            else
              asignatura[:id] = optativa[:id]
              asignatura = Historial::set_info_to_asignatura(asignatura, true, "POR CURSAR", Historial::get_status_asignatura(alumno, optativa[:id]))
            end
          end
          # Revisamos la siguiente asignatura.
          next
        end

        # Si no es ni seriada ni optativa, verificamos que la haya aprobado.
        asignatura = if asignatura[:aprobada].eql?(InscripcionCurso::APROBADA)
                       Historial::set_info_to_asignatura(asignatura, false, "APROBADA", nil)
                     else
                       Historial::set_info_to_asignatura(asignatura, true, "POR CURSAR", Historial::get_status_asignatura(alumno, asignatura[:id]))
                     end
      end

      asignaturas
    end

    def Historial.set_info_to_asignatura(asignatura, can, motivo, status)
      asignatura[:can] = can
      asignatura[:motivo] = motivo
      asignatura[:status] = status

      asignatura
    end

    def Historial.verifica_seriacion(asignatura,alumno)

      asignatura[:ids_seriadas].each do |id_seriada|
        break_bucle = false
        calificaciones_por_asignatura = Calificacion.get_all_by_alumno_and_asignatura(alumno, id_seriada)
        if calificaciones_por_asignatura.empty?
          asignatura = Historial::set_info_to_asignatura(asignatura, false, "NO HA CURSADO LA SERIADA", nil)
          next
        end
        calificaciones_por_asignatura.each do |calificacion|
          if calificacion.is_aprobada?
            if asignatura[:aprobada].eql?(InscripcionCurso::APROBADA)
              asignatura = Historial::set_info_to_asignatura(asignatura, false, "APROBADA", nil)
            else
              asignatura = Historial::set_info_to_asignatura(asignatura, true, "POR CURSAR", Historial::get_status_asignatura(alumno, asignatura[:id]))
            end
            break
          else
            asignatura = Historial::set_info_to_asignatura(asignatura, false, "POR SERIACION", nil)
            break_bucle = true
            break if calificaciones_por_asignatura.size.eql?(1)
          end
        end
        break if break_bucle
      end
      asignatura
    end

    def Historial.get_status_asignatura(alumno, asignatura)
      InscripcionCurso.is_inscrito?(alumno, asignatura) ? InscripcionCurso::REPETIDOR : InscripcionCurso::REGULAR
    end

    def Historial.are_asignaturas_valids?(asignaturas)
      asignaturas.each { |asignatura| return true if asignatura[:can] }
      false
    end
  end
end
