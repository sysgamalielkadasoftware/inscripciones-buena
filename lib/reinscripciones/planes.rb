module Reinscripciones
  module Planes
    def get_plan_clave(params)
      params_size = params.size
      case params_size
      when 1 then
        plan_de_estudio = PlanEstudio.select("planes_estudio.clave_plan").
            joins(:asignaturas => {:cursos => {:inscripciones => :ciclo}}).
            where(:inscripciones => {:alumno_id => params[:alumno_id], :status => ["REGULAR","IRREGULAR","REPETIDOR"]}).
            order("ciclos.ciclo desc").first
      else
        plan_de_estudio = nil
      end

      !plan_de_estudio.blank? ? plan_de_estudio.clave_plan : nil
    end

    def get_plan_de_estudio(carrera, clave, periodo)
      # Hash que contendrá todas las asignaturas registradas en el plan de estudios.
      plan_de_estudios = Array.new
      semestres = Semestr.order(:clave)

      asignaturas_omitidas = AsignaturaOmitidaCiclo.select("materias_planes_id").where(:ciclo_id => periodo)
      semestres.each { |semestre| plan_de_estudios << get_plan_semestre(carrera, clave, semestre, asignaturas_omitidas) }

      plan_de_estudios || nil
    end

    def get_plan_semestre(carrera, clave, semestre, asignaturas_omitidas)
      asignaturas_semestre = Hash.new
      asignaturas_semestre[:semestre_id] = semestre.id
      asignaturas_semestre[:semestre] = semestre.clave_semestre  == "PRIMERO" ? "PRIMER" : (semestre.clave_semestre  == "TERCERO" ? "TERCER" : semestre.clave_semestre )
      asignaturas_semestre[:clave] = semestre.clave
      asignaturas_semestre[:tipo] = semestre.clave % 2 == 1 ? 'A' : 'B'
      asignaturas_semestre[:asignaturas] = Array.new

      asignaturas = Asignatura.joins(:plan_estudio).
          where(:planes_estudio => {:carrera_id => carrera, :semestre_id => semestre, :clave_plan => clave},
                :materias_planes => {:optativa_hija => false}).where("materias_planes.id NOT IN (?)", asignaturas_omitidas).
          order("materias_planes.orden")
      # asignaturas = Asignatura.joins(:plan_estudio).
      #     where(:planes_estudio => {:carrera_id => 8, :semestre_id => 5, :clave_plan => 'LENF 011012'},
      #           :materias_planes => {:optativa_hija => false}).where("materias_planes.id NOT IN (?)", [408]).order("materias_planes.orden")

      asignaturas.each {|asignatura| asignaturas_semestre[:asignaturas] << get_plan_asignatura(asignatura)}

      asignaturas_semestre
    end

    def get_plan_asignatura(asignatura)
      asignaturas_plan = Hash.new

      asignaturas_plan[:id] = asignatura.id
      asignaturas_plan[:clave] = asignatura.clave
      asignaturas_plan[:nombre] = asignatura.nombre
      asignaturas_plan[:orden] = asignatura.orden
      asignaturas_plan[:creditos] = asignatura.creditos

      # Obtiene la información si la asignatura es seriada.
      if asignatura.is_seriada?
        asignaturas_plan[:is_seriada] = true
        asignaturas_plan[:claves_seriadas] = asignatura.get_claves_seriadas
        asignaturas_plan[:ids_seriadas] = asignatura.get_ids_seriadas
      else
        asignaturas_plan[:is_seriada] = false
      end
      # Obtiene la información si la asignatura es optativa.
      if asignatura.is_optativa?
        asignaturas_plan[:is_optativa] = true
        asignaturas_plan[:optativas] = Array.new
        asignatura.optativas.each { |opt| asignaturas_plan[:optativas] << get_plan_optativas(opt) }
      else
        asignaturas_plan[:is_optativa] = false
      end

      asignaturas_plan
    end

    def get_plan_optativas(opt)
      opt_hash = Hash.new
      asignatura_optativa = Asignatura.find_by_id(opt.materia_optativa_id)
      opt_hash[:id] = opt.materia_optativa_id
      opt_hash[:clave] = asignatura_optativa.clave
      opt_hash[:optativa_padre] = Optativa.father_name(asignatura_optativa[:id]).to_s
      # puts "OPTATIVA: "
      concatenar = Optativa.father_name(asignatura_optativa[:id]).to_s
      # puts concatenar
      opt_hash[:nombre] = asignatura_optativa.nombre + " (" + convertir(concatenar) + ")"
      opt_hash[:creditos] = asignatura_optativa.creditos
      opt_hash[:especialidad] = asignatura_optativa.especialidad.nombre.to_s unless asignatura_optativa.especialidad.blank?
      opt_hash[:has_especialidad] = asignatura_optativa.especialidad ? true : false
      opt_hash[:is_optativa] = true
      opt_hash[:is_seriada] = false
      if asignatura_optativa.is_seriada?
        opt_hash[:is_seriada] = true
        opt_hash[:claves_seriadas] = asignatura_optativa.get_claves_seriadas
        opt_hash[:ids_seriadas] = asignatura_optativa.get_ids_seriadas
      end
      opt_hash
    end

    def convertir(string)
      puts "\n"
      string.capitalize!
      words_no_cap = ["de", "la", "del", "i", "ii", "iii", "iv"]
      phrase = string.split(" ").
          map {|word|
            if words_no_cap.include?(word)
              word.upcase
            else
              word.mb_chars.capitalize
            end
          }.join(" ")
      phrase
    end

    def Planes.has_especialidades?(carrera_id, clave)
      Especialidad.joins(:asignaturas => :plan_estudio).
          exists?(:planes_estudio => {:carrera_id => carrera_id, :clave_plan => clave})
    end

    def Planes.which_semestre_especialidades_start?(carrera_id, clave)
      semestre = Semestr.joins(:planes_estudio => {:asignaturas => :especialidad}).
          where(:planes_estudio => {:carrera_id => carrera_id, :clave_plan => clave}).
          order("semestres.clave").first

      semestre.nil? ? 0 : semestre.clave
    end

    def Planes.which_semestre_especialidades_end?(carrera_id, clave)
      semestre = Semestr.joins(:planes_estudio => {:asignaturas => :especialidad}).
          where(:planes_estudio => {:carrera_id => carrera_id, :clave_plan => clave}).
          order("semestres.clave desc").first

      semestre.nil? ? 0 : semestre.clave
    end

    def Planes.asignaturas_for_especialidad(especialidad_id, clave)
      unless clave.nil?
        Asignatura.select("distinct materias_planes.id, materias_planes.clave, materias_planes.nombre, materias_planes.orden").
            joins(:plan_estudio => :semestr).
            where(:materias_planes => {:especialidad_id => especialidad_id}, :semestres => {:clave => clave}).
            order("materias_planes.orden")
      else
        Asignatura.select("distinct materias_planes.id, materias_planes.clave, materias_planes.nombre, materias_planes.orden").
            where(:materias_planes => {:especialidad_id => especialidad_id}).
            order("materias_planes.orden")
      end
    end

    def Planes.get_especialidades(carrera_id, clave)
      Especialidad.select("distinct especialidades.id, especialidades.nombre, especialidades.plan_estudio_id").joins(:asignaturas => :plan_estudio).where(:planes_estudio => {:carrera_id => carrera_id, :clave_plan => clave}).order("especialidades.nombre")
    end
  end
end