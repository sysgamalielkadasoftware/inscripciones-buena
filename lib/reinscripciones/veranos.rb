# To change this template, choose Tools | Templates
# and open the template in the editor.

module Reinscripciones
  #
  # Modulo que contendrá definiciones que ayudarán a realizar las inscripciones
  # a los cursos de verano.
  #
  module Veranos
    include Historial

    #
    # Determina a qué semestres puede cursar asignaturas de verano
    #
    def evaluate_to_veranos(alumno, periodo)
      last_inscripcion = Inscripcion.get_last_by_alumno(alumno)
      reinscripcion = Hash.new
      reinscripcion[:can] = false

      # Verifica que la información del alumno (modelo) sea válido.
      unless alumno.valid?
        reinscripcion[:message] = "La CURP del alumno no es válida: #{alumno.curp}. Deberá actualizarla para poder reinscribirlo."
        return reinscripcion
      end
=begin
      if is_invalid_for_current_periodo?(alumno, periodo)
        reinscripcion[:message] = "El alumno ya se encuentra Inscrito a todas las asignaturas posibles de verano para este periodo."
        return reinscripcion
      end
=end
      # El periodo para cursos de verano, debe de ser del tipo V.
      if periodo.is_tipo_a? or periodo.is_tipo_b?
        reinscripcion[:message] = "El Periodo Actual debe ser del tipo V (Verano)."
        return reinscripcion
      end

      semestres_aprobados = 0
      data = Array.new

      @historial.each do |historial|
        data.push(historial)

        if Historial::should_stop?(historial)
          data.pop
          break
        end

        if historial[:aprobado].eql?(Inscripcion::APROBADO)
          semestres_aprobados += 1
          data.pop
          next
        end
      end

      # Si el alumno ha terminado satisfactoriamente con el plan de estudios.
      if semestres_aprobados.eql?(Semestr::DECIMO)
        reinscripcion[:message] = "Ya concluiste satisfactoriamente con el plan de estudios."
        return reinscripcion
      end

      # Si el alumno no tiene asignaturas reprobadas.
      if data.empty?
        reinscripcion[:message] = "NO tienes asignaturas reprobadas para cursar en verano."
        return reinscripcion
      end
      
      data.each do |d|
        inscripcion = Inscripcion.joins(:semestr).
          where(:inscripciones => {:alumno_id => alumno, :ciclo_id => periodo},
          :semestres => {:clave => d[:clave]}).first
        if inscripcion.blank?
          d[:is_inscrito] = false
          d[:groups_list] = Historial::get_groups_list(last_inscripcion.carrera, d[:clave],periodo)
        else
          d[:is_inscrito] = true
          d[:groups_list] = inscripcion.grupo_nombre
        end
        d[:status] = Inscripcion::REPETIDOR
        d[:asignaturas] = Historial::validate_asignaturas_anteriores(alumno, periodo, d[:clave], d[:asignaturas])
      end

      data.delete_if do |d|
        !Historial::are_asignaturas_valids?(d[:asignaturas])
      end

      if !data.empty?
        if !data[0].blank? and data[0][:count_reprobadas] > 2
          reinscripcion[:message] = "Debes más de dos asignaturas."
          reinscripcion[:can] = false
        else
          reinscripcion[:can] = true
        end
      else
        reinscripcion[:message] = "No puedes cursar ninguna asignatura en verano."
      end

      reinscripcion[:info_all] = data
      
      return reinscripcion
    end
  end
end
