module Reinscripciones
  module Calificaciones
    def get_calificacion_plan(carrera, alumno, plan_de_estudio)
      asignaturas_aprobadas = 0
      count_semestres = 0
      count_asignaturas = 0.0
      promedio_general = 0.0
      plan_de_estudio.each do |plan|
        asignaturas_aprobadas = 0
        inscripcion = Inscripcion.get_first_by_carrera_and_alumno_and_semestre(carrera, alumno, plan[:clave])
        plan[:periodo] = inscripcion.get_periodo unless inscripcion.blank?
        puts "periodo: " + inscripcion.get_periodo.to_s unless inscripcion.blank?
        plan[:grupo] = inscripcion.get_grupo unless inscripcion.blank?
        puts "grupo: " + inscripcion.get_grupo.to_s unless inscripcion.blank?
        plan[:inscripcion_id] = inscripcion.id unless inscripcion.blank?
        puts "id inscripcion: " + inscripcion.id.to_s unless inscripcion.blank?
        plan[:promedio_semestral] = 0.0
        contador_materia = 0

        plan[:asignaturas].each do |asignatura|
          contador_materia += 1
          puts "Asignatura: " + asignatura[:nombre]
          asignatura[:aprobada] = InscripcionCurso::REPROBADA
          calificaciones = Calificacion.get_all_by_alumno_and_asignatura(alumno, asignatura[:id])
          asignatura[:has_inscripcion_curso] = InscripcionCurso.is_inscrito?(alumno, asignatura[:id])  #InscripcionCurso.joins(:curso => :asignatura).exists?(:inscripciones_cursos => {:inscripcion_id => inscripcion.id },:materias_planes => {:id => asignatura[:id]}) unless inscripcion.blank?
          calificaciones.each do |calificacion|
            asignatura[:parcial1] = calificacion.parcial1
            puts "Parcial 1: " + calificacion.parcial1.to_s
            asignatura[:parcial2] = calificacion.parcial2
            puts "Parcial 2: " + calificacion.parcial2.to_s
            asignatura[:parcial3] = calificacion.parcial3
            puts "Parcial 3: " + calificacion.parcial3.to_s
            asignatura[:promedio_parciales] = calificacion.promedio_parciales
            asignatura[:ordinario] = calificacion.final
            puts "Ordinario: " + calificacion.final.to_s
            asignatura[:promedio] = calificacion.promedio
            asignatura[:extra1] = calificacion.get_extra1_sd_or_np
            asignatura[:extra2] = calificacion.get_extra2_sd_or_np
            asignatura[:especial] = calificacion.get_especial_sd_or_np
            asignatura[:periodo] = calificacion.ciclo
            asignatura[:fecha_ordinario] = calificacion.fecha_final
            asignatura[:fecha_extra1] = calificacion.fecha_extra1
            asignatura[:fecha_extra2] = calificacion.fecha_extra2
            asignatura[:fecha_especial] = calificacion.fecha_especial
            asignatura[:aprobado_extraordinario_1] = (calificacion.get_extra1_sd_or_np.to_f >= 6.0)
            asignatura[:aprobado_extraordinario_2] = (calificacion.get_extra2_sd_or_np.to_f >= 6.0)
            asignatura[:aprobado_especial] = (calificacion.get_especial_sd_or_np.to_f >= 6.0)
            asignatura[:aprobado_promedio] = (calificacion.promedio.to_f >= 6.0)
            asignatura[:fecha_aprobo] = calificacion.fecha_final
            asignatura[:calificacion_aprobado] = asignatura[:promedio] if asignatura[:aprobado_promedio]
            puts "Calificacion final: " + asignatura[:promedio].to_s
            asignatura[:calificacion_aprobado] = asignatura[:extra1] if asignatura[:aprobado_extraordinario_1]
            puts "Extra 1: " + asignatura[:extra1].to_s
            asignatura[:calificacion_aprobado] = asignatura[:extra2] if asignatura[:aprobado_extraordinario_2]
            puts "Extra 2: " + asignatura[:extra2].to_s
            asignatura[:calificacion_aprobado] = asignatura[:especial] if asignatura[:aprobado_especial]
            puts "Especial: " + asignatura[:especial].to_s
            if asignatura[:ordinario] and asignatura[:calificacion_aprobado]
              plan[:promedio_semestral] += asignatura[:calificacion_aprobado]*10.0
              promedio_general += asignatura[:calificacion_aprobado]*10.0
              puts  "entro"
              # count_asignaturas += 1 unless asignatura[:has_inscripcion_curso].nil?
            end

            if calificacion.is_aprobada?
              asignatura[:aprobada] = InscripcionCurso::APROBADA
              asignaturas_aprobadas += 1
              break
            else
              asignatura[:aprobada] = InscripcionCurso::REPROBADA
              break
            end

          end
          if asignatura[:is_optativa]
            asignatura[:is_selected] = false
            asignatura[:optativas].each do |optativa|
              puts "Asignatura: " + optativa[:nombre]
              optativa[:aprobada] = InscripcionCurso::REPROBADA
              next unless InscripcionCurso.is_inscrito?(alumno, optativa[:id])
              asignatura[:is_selected] = true
              optativa[:selected] = true

              calificaciones = Calificacion.get_all_by_alumno_and_asignatura(alumno, optativa[:id])

              calificaciones.each do |calificacion|
                optativa[:parcial1] = calificacion.parcial1
                puts "Parcial 1: " + calificacion.parcial1.to_s
                optativa[:parcial2] = calificacion.parcial2
                puts "Parcial 2: " + calificacion.parcial1.to_s
                optativa[:parcial3] = calificacion.parcial3
                puts "Parcial 3: " + calificacion.parcial1.to_s
                optativa[:promedio_parciales] = calificacion.promedio_parciales
                optativa[:ordinario] = calificacion.final
                puts "Ordinario: " + calificacion.final.to_s
                optativa[:promedio] = calificacion.promedio
                optativa[:extra1] = calificacion.get_extra1_sd_or_np
                optativa[:extra2] = calificacion.get_extra2_sd_or_np
                optativa[:especial] = calificacion.get_especial_sd_or_np
                optativa[:periodo] = calificacion.ciclo
                optativa[:fecha_ordinario] = calificacion.fecha_final
                optativa[:fecha_extra1] = calificacion.fecha_extra1
                optativa[:fecha_extra2] = calificacion.fecha_extra2
                optativa[:fecha_especial] = calificacion.fecha_especial
                optativa[:aprobado_extraordinario_1] = (calificacion.get_extra1_sd_or_np.to_f >= 6.0)
                optativa[:aprobado_extraordinario_2] = (calificacion.get_extra2_sd_or_np.to_f >= 6.0)
                optativa[:aprobado_especial] = (calificacion.get_especial_sd_or_np.to_f >= 6.0)
                optativa[:aprobado_promedio] = (calificacion.promedio.to_f >= 6.0)
                optativa[:fecha_aprobo] = calificacion.fecha_final
                optativa[:calificacion_aprobado] = optativa[:promedio] if optativa[:aprobado_promedio]
                puts "Calificacion final: " + optativa[:promedio].to_s
                optativa[:calificacion_aprobado] = optativa[:extra1] if optativa[:aprobado_extraordinario_1]
                puts "Calificacion extra 1: " + optativa[:extra1].to_s
                optativa[:calificacion_aprobado] = optativa[:extra2] if optativa[:aprobado_extraordinario_2]
                puts "Calificacion extra 2: " + optativa[:extra2].to_s
                optativa[:calificacion_aprobado] = optativa[:especial] if optativa[:aprobado_especial]
                puts "Calificacion especial: " + optativa[:especial].to_s

                if asignatura[:ordinario] and optativa[:calificacion_aprobado]
                  plan[:promedio_semestral] += optativa[:calificacion_aprobado]*10.0
                  promedio_general += optativa[:calificacion_aprobado]*10.0
                  puts  "entro"
                end

                if calificacion.is_aprobada?
                  optativa[:aprobada] = InscripcionCurso::APROBADA
                  asignaturas_aprobadas += 1
                  break
                else
                  optativa[:aprobada] = InscripcionCurso::REPROBADA
                  break
                end
              end
              break
            end
          end
          if plan[:promedio_semestral] != 0.0
            count_asignaturas += 1
          end
        end # termina plan[:asignaturas]
        puts "Contador: Asignaturas: " + contador_materia.to_s
        puts "Promedio Semestral: " + plan[:promedio_semestral].to_s
        plan[:promedio_semestral] =(plan[:promedio_semestral]/contador_materia) if !plan[:promedio_semestral].nan?
        plan[:promedio_semestral] = plan[:promedio_semestral].floor.fdiv(10) if !plan[:promedio_semestral].nan?
        puts "Promedio Semestral: " + plan[:promedio_semestral].to_s
        plan[:count_reprobadas] = plan[:asignaturas].size - asignaturas_aprobadas
        plan[:aprobado] = plan[:asignaturas].size.eql?(asignaturas_aprobadas) ? Inscripcion::APROBADO : Inscripcion::REPROBADO
        puts "\n"
      end
      puts "Total de materias: " + count_asignaturas.to_s
      promedio_general = promedio_general/count_asignaturas if count_asignaturas > 0
      promedio_general = promedio_general.floor.fdiv(10) if (!promedio_general.nan? and count_asignaturas > 0)
      puts "Promedio General: " + promedio_general.to_s
      plan_de_estudio
    end
  end
end