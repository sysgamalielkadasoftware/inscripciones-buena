module Nes
  module Registro
    module Inscripciones
      class BaseClass
        attr_reader :alumno
        attr_reader :errors
        attr_reader :plan_estudio
        attr_reader :aspirante

        def initialize(alumno_id, ciclo)
          @errors = Array.new
          if Alumno.exists?(alumno_id)
            @alumno = Alumno.find(alumno_id)
          else
            add_error("El alumno solicitado, no se encuentra registrado en la base de datos.")
          end
          @ciclo = ciclo
          @plan_estudio = nil
        end

        private
        def get_groups_list(carrera_id, clave)
          groups_all = Grupo.select("distinct grupos.id, grupos.nombre, grupos.contador").
              joins(:inscripciones => :semestr).
              where(:inscripciones => {:ciclo_id => @ciclo, :carrera_id => carrera_id}, :semestres => {:clave => clave}).
              order('grupos.nombre')

          carrera = Carrera.find(carrera_id)
          groups_list = Array.new
          group_basic = clave.to_s + carrera.codigo_carrera.to_s

          groups_all.each do |grupo|
            group_info = Hash.new
            group_info[:name] = grupo.get_description
            group_info[:id] = grupo.nombre
            groups_list.push(group_info)
          end

          cont = 0
          index = 0

          # Se agregan cinco grupos nuevos más a la lista de grupos que ya se encuentran registrados,
          # mismos que serán presentados al usuario.
          while cont < 5 and index < Grupo::LETRA.size
            group_tmp = group_basic + '-' + Grupo::LETRA[index.to_s].to_s

            unless Grupo.joins(:inscripciones).exists?(:grupos => {:ciclo_id => @ciclo, :nombre => group_tmp}, :inscripciones => {:carrera_id => carrera})
              group_info = Hash.new
              group_info[:name] = group_tmp + ' | NUEVO GRUPO'
              group_info[:id] = group_tmp
              groups_list.push(group_info)
              cont += 1
            end

            index += 1
          end

          groups_list
        end

        protected
        def add_error(error)
          @errors.push(error)
        end

        def create_for_first_semestr(clave, carrera_id)
          begin
            @plan_estudio = PlanEstudioClass.new(clave, carrera_id)
            @plan_estudio.find_by_semestr(Semestr::PRIMERO)
          rescue Exception => error
            add_error(error)
          end
        end

        public
        def alumno_nombre
          @alumno.full_name
        end

        def alumno_estado
          @alumno.tipo
        end

        def periodo_actual
          @ciclo.ciclo
        end

        def alumno_id
          @alumno.id
        end

        def ciclo_id
          @ciclo.id
        end

        def grupo_anterior
          inscripcion = Inscripcion.get_last_by_alumno(@alumno)

          inscripcion.blank? ? "El alumno no tiene inscripciones." : inscripcion.grupo_nombre
        end

        def any_error?
          @errors.any?
        end
      end
    end
  end
end