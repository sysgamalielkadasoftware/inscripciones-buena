module Nes
  module Registro
    module Inscripciones
      class Inscripcion < BaseClass
        def initialize(alumno_id = nil, ciclo = nil)
          super(alumno_id, ciclo)
          @semestre = Semestr.find_by_clave(Semestr::PRIMERO)

          @inscripcion = Inscripcion.
              where(:inscripciones => {
                  :alumno_id => @alumno,
                  :semestre_id => @semestre,
                  :status => [Inscripcion::REGULAR, Inscripcion::REPETIDOR, Inscripcion::IRREGULAR]
              }).
              last
          if @ciclo.nil?
            add_error("El periodo de inscripción, no es válido.")
            add_error("El periodo de inscripción deber estar definido como actual.")
            add_error("El periodo debe ser del tipo A.")
          else
            add_error("El periodo de inscripciones a primer semestre, debe ser de tipo A.") unless @ciclo.is_tipo_a?
          end
          add_error("El alumno ya se encuentra inscrito.") unless @inscripcion.nil?
        end

        private
        def generate_matricula(carrera_id)

          carrera = Carrera.find(carrera_id)
          campus = carrera.campus
          matricula = ''
          # Dos dígitos para el número de campus; ejemplo: 01
          campus_clave = campus.numero || '00'
          # Dos dígitos para el indentificar el año de ingreso, se toman las decenas y unidades del
          # año de inicio del periodo de inscripciones, el periodo es el que esté configurado como actual.
          year_ingreso = @ciclo.inicio.to_s[2,3]
          # Dos dígitos para el número de carrera
          carrera_code = carrera.codigo_carrera

          # Se forma la base de la matricula
          matricula_base = campus_clave + year_ingreso

          # Obtiene el número de alumnos inscritos a primer semestre por primera vez para el campus
          # al que pertence la carrera que el alumno eligió.
          total_alumnos = Inscripcion.select("distinct inscripciones.alumno_id").joins(:alumno, :carrera).
              where("campus_id = ? and semestre_id = ? and ciclo_id = ? and status = ? and matricula like ?",
                    carrera.campus_id, @semestre.id, @ciclo.id, Inscripcion::REGULAR, matricula_base + "%").
              count

          # Incrementamos el consecutivo
          total_alumnos += 1

          # Creamos la matrícula
          matricula = matricula_base + "__" + ('%04d' % total_alumnos)

          # Verificamos que la matrícula generada no exista, si existe, hay que incrementar el consecutivo
          # hasta encontrar una matrícula disponible.
          while Alumno.exists?(["matricula like ?", matricula])
            # Incrementamos el consecutivo
            total_alumnos += 1
            # Creamos la matrícula
            matricula = matricula_base + "__" + ('%04d' % total_alumnos)
          end

          (matricula_base + carrera_code + ('%04d' % total_alumnos))
        end

        def check_prope
          prope = Propedeutico.joins(:ciclo).where(:alumno_id => @alumno).order("ciclos.ciclo desc").first

          if prope.nil?
            add_error("El alumno no ha cursardo el propedéutico en ningún periodo escolar.")
          else
            unless prope.is_aceptado?
              add_error("El alumno no ha sido aceptado, su estado es: #{prope.status}")
            end
          end
        end

        def checar_prope(aspirante_id)
          inscripcion = InscripcionPropedeutico.joins(:solicitud_propedeutico => :periodo_propedeutico).where("solicitudes_propedeuticos.aspirante_id = ?", aspirante_id).order("periodos_propedeuticos.fecha_inicio desc, periodos_propedeuticos.fecha_fin desc")
          if inscripcion.nil?
            add_error("El aspirante no ha cursardo el propedéutico en ningún periodo escolar.")
          else
            unless inscripcion.is_aceptado?
              add_error("El alumno no ha sido aceptado, su estado es: #{inscripcion.estado}")
            end
          end
        end

        def semestre_id
          @semestre.id
        end

        def to_json
          hash = Hash.new

          hash[:campus_nombre] = @plan_estudio.campus
          hash[:carrera_nombre] = @plan_estudio.carrera
          hash[:plan_clave] = @plan_estudio.clave

          semestre = @plan_estudio.get_by_semestr(Semestr::PRIMERO)

          hash[:semestre_nombre] = semestre.nombre
          hash[:asignaturas] = semestre.to_array

          hash[:grupos] = get_groups_list(@plan_estudio.carrera_id, Semestr::PRIMERO)

          hash.to_json
        end
      end
    end
  end
end