module Consulta
  module TutorLib
    CATEGORIAS = [:alumno, :aspirante, :antecedentes, :propedeutico, :tutores, :personales , :direccion, :ubicacion] #el orden no debe cambiarse
    CATEGORIA_ALUMNO = {:curp => :curp, :nombre => :nombre, :apellido_paterno => :apellido_paterno, :apellido_materno => :apellido_materno,
                        :matricula => :matricula, :nss => :nss, :email => :correo_electronico, :carrera_id => :carrera_id, :tipo => :tipo}
    CATEGORIA_PERSONALES = {:fecha_nacimiento => :fecha_nacimiento, :sexo => :sexo, :estado_civil => :estado_civil, :nacionalidad => :nacionalidad,
                            :tipo_sanguineo => :tipo_sangre, :enfermedades => :enfermedad, :alergias => :alergia, :medicamentos => :medicamentos,
                            :lengua_indigena => :nombre }
    #CATEGORIA_LENGUA = {:lengua_indigena => :nombre }
    CATEGORIA_UBICACION = {:poblacion => :nombre, :municipio => :nombre, :distrito => :nombre, :estado => :nombre, :pais => :nombre , :lugar_nacimiento => :ciudad_id}
    CATEGORIA_DIRECCION = {:calle => :calle, :numero => :numero, :colonia => :colonia, :codigo_postal => :codigo_postal, :telefono_con_lada => :telefono}
    CATEGORIA_ANTECEDENTES = {:nombre_escuela_procedencia => :nombre, :promedio_escuela_procedencia => :promedio, :inicio_escuela_procedencia => :anio_inicio, :fin_escuela_procedencia => :anio_fin,
                              :especialidad_escuela_procedencia => :especialidad, :area_conocimiento_escuela_procedencia => :area,
    }
    CATEGORIA_ASPIRANTE = {:configuracion_aspirante_id => :configuracion_aspirante_id , :curp => :curp, :nombre => :nombre, :apellido_paterno => :apellido_paterno,
                           :apellido_materno => :apellido_materno,:fecha_nacimiento => :fecha_nacimiento, :estado_civil => :estado_civil,
                           :nacionalidad => :nacionalidad, :carrera_id => :carrera_id, :sexo => :sexo}
    CATEGORIA_PROPEDEUTICO = {:configuracion_aspirante_id => :configuracion_aspirante_id , :carrera_id => :carrera_id}
    CATEGORIA_TUTOR = {:nombre_tutor => :nombre, :apellido_paterno_tutor => :apellido_paterno, :apellido_materno_tutor => :apellido_materno,
                       :ocupacion_tutor => :ocupacion , :parentesco_tutor => :parentezco }
    CAMPOS_INMUTABLES_F02 = [:curp, :fecha_nacimiento, :nombre, :apellido_paterno, :apellido_materno, :estado_civil, :nacionalidad, :lugar_nacimiento, :lengua_indigena]
    CAMPOS_INMUTABLES_F03 = [:curp, :fecha_nacimiento, :carrera_id, :nombre, :apellido_paterno, :apellido_materno, :fecha_solicitud, :estado_civil, :nacionalidad, :lugar_nacimiento, :lengua_indigena, :tipo_sangre]
    CAMPOS_INMUTABLES_F05 = [:curp, :fecha_nacimiento, :carrera_id, :nombre, :apellido_paterno, :apellido_materno , :configuracion_aspirante_id, :fecha_solicitud, :estado_civil, :nacionalidad, :lugar_nacimiento, :lengua_indigena, :tipo_sangre]
    CAMPOS_INMUTABLES_F05_R = [:curp, :fecha_nacimiento, :carrera_id, :nombre, :apellido_paterno, :apellido_materno , :configuracion_aspirante_id, :fecha_solicitud, :estado_civil, :nacionalidad, :lugar_nacimiento, :lengua_indigena, :tipo_sangre,
                               :pais_escuela_procedencia, :poblacion_escuela_procedencia, :distrito_escuela_procedencia, :numero, :colonia, :municipio, :distrito  ]

    FORMA = {:nombre => "Erick", :nombre_tutor => "Emilia", :municipio => "Matias Romero" , :distrito => "", :medicamentos => "aspirinas"}

    @errors = nil
    @tramite_string = nil
    @curp = nil

    def get_information_tutor(alumno_id)
      formato_actual = Hash.new
      categorias = CATEGORIAS.clone if categorias.blank?
      alumno = Alumno.find_by_id(alumno_id)
      unless alumno.blank?
        formato_actual.merge!(obtener_datos_tutores(alumno_id))
        formato_actual.merge!(obtener_datos_direccion(alumno_id))
        formato_actual.delete_if{|k,v| v.nil?}
        formato_actual = formato_actual.map{|k,v| [k, v.nil? ? String(v) : v]}.to_h
        f1 = Formato.new(formato_actual)
        claves_f = Formato.attribute_names.map{|v| v.to_sym}
        claves_f.each do |c|
          f1[c] = "" if f1[c].nil?
          f1[c] = 0 if f1[c].nil?
        end
        puts "Datos: " + f1.to_s
      end
      f1
    end

    def obtener_datos_direccion(alumno_id)
      alumno = Alumno.find_by_id(alumno_id)
      claves = CATEGORIA_DIRECCION.clone
      unless alumno.blank?
        direccion_alumno = alumno.addresses.last

        unless direccion_alumno.blank?
          direccion_alumno =  direccion_alumno.attributes.map{|a,v| [a.to_sym, v]}.to_h
          datos_ubicacion_alumno = obtener_datos_ubicacion(direccion_alumno[:ciudad_id])
          direccion_alumno = decode_tabla_to_formato(direccion_alumno, claves)
          direccion_alumno.merge!(datos_ubicacion_alumno)
        end
        direccion_alumno = {nil => nil} if  direccion_alumno.blank?
        direccion_tutor = alumno.tutores.last.address unless alumno.tutores.blank?
        unless direccion_tutor.blank?
          direccion_tutor = direccion_tutor.attributes.map{|a,v| [a.to_sym, v]}.to_h
          datos_ubicacion_tutor = obtener_datos_ubicacion(direccion_tutor[:ciudad_id])
          datos_ubicacion_tutor.delete(:distrito)
          direccion_tutor = decode_tabla_to_formato(direccion_tutor, claves)
          direccion_tutor.merge!(datos_ubicacion_tutor)
          direccion_tutor = direccion_tutor.map{|a,v| [(String(a) + "_tutor").to_sym, v]}.to_h
        else
          direccion_tutor = {nil => nil}
        end

        direcciones =  Hash.new
        direcciones = direccion_alumno.merge(direccion_tutor)
        return direcciones
      end
      return {nil => nil}
    end

    def obtener_datos_ubicacion(ciudad_id)
      return {nil => nil} if ciudad_id.blank?
      ciudad = Ciudad.find_by_id(ciudad_id)
      return {nil => nil} if ciudad.blank?
      ciudad_nombre = ciudad.nombre
      datos_ubicacion = Hash.new
      municipio_nombre = (ciudad.municipio.blank? ? ",sin municipio" :  ciudad.municipio.nombre)
      distrito_nombre = (ciudad.distrito.blank? ? ",sin distrito" :  ciudad.distrito.nombre)
      estado_nombre = (ciudad.estado.blank? ? ",sin estado" :  ciudad.estado.nombre)
      datos_ubicacion = {:poblacion => ciudad_nombre, :municipio => municipio_nombre, :distrito=> distrito_nombre , :estado => estado_nombre}
      return datos_ubicacion
    end

    def obtener_datos_tutores(alumno_id)
      alumno = Alumno.find_by_id(alumno_id)
      return {nil => nil} if alumno.blank?
      claves_tutores = CATEGORIA_TUTOR.clone
      tutor_asignado = false
      datos = Hash.new
      tutores = alumno.tutores
      sufijo = "_tutor"
      tutores.each do |t|
        sub_datos = Hash.new
        sub_datos = t.attributes.map{|a,v| [a.to_sym, v]}.to_h
        case t.parentezco
        when "PADRE"
          sufijo = "_padre"
          sub_datos.delete(:parentezco)
        when "MADRE"
          sufijo = "_madre"
          sub_datos.delete(:parentezco)
        else
          sufijo = "_tutor"
          tutor_asignado = true
        end
        sub_datos = decode_tabla_to_formato(sub_datos, claves_tutores)
        sub_datos = sub_datos.map{|a,v| [(String(a).gsub("_tutor", sufijo)).to_sym, v]}.to_h
        datos.merge!(sub_datos)
      end
      unless tutor_asignado
        datos = datos.map{|a,v| [(String(a).gsub(sufijo, "_tutor")).to_sym, v]}.to_h
        datos[:parentesco_tutor] = (sufijo.eql?("_padre") ?  "PADRE" : "MADRE")
      end
      return datos
    end

    def decode_tabla_to_formato(datos, claves)
      formato = Hash.new
      datos.each do |table_key, table_value|
        if claves.has_value?(table_key)
          formato[claves.key(table_key)] = table_value
          formato[claves.key(table_key)] = String(table_value) if table_value.nil?
        end
      end
      return formato
    end
  end
end