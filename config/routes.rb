Rails.application.routes.draw do
  resources :gestion_usuario do
    get :crear_cuentas, :on => :collection
    get :restablecer_cuentas, :on =>:collection
  end
   



  resources :solicitudes_fichas do
    post :readpdf, :on => :collection
    get :readpdf, :on => :collection
    post :aspi, :on => :collection
    get :aspi, :on => :collection
  end

  get 'anuncios/index'
  get "/anuncios", to: "anuncios#index"

  get "anuncios", to:"anuncios#index"
  get "anuncios/new", to: "anuncios#new"
  get "anuncios/:id", to:"anuncios#show"
  get "anuncios/:id/edit", to:"anuncios#edit"
  patch "/anuncios/:id", to:"anuncios#update", as: :anuncio
  post "anuncios", to:"anuncios#create"
  delete "/anuncios/:id", to:"anuncios#destroy"


  match 'solicitudes_fichas/aspi', :controller => 'solicitudes_fichas', :action => 'aspi', via: :get

  resources :solicitud_propedeutico do
    get :search_data_aspirantes_request, :on => :collection
    get :search_data_tipo_request, :on => :collection
    get :search_monto_request, :on => :collection
    get :search_concepto_request, :on => :collection
    post :guardar_sol, :on => :collection
  end

  resources :solicitudes_inscripciones do
    get :new_aspirante, :on => :collection
  end

  resources :solicitudes_reinscripciones

  resources :aspirantes do
    # get :show, :on => :collection
    post :towns_options, :on => :collection
    post :localities_options, :on => :collection

    post :towns_options_edit, :on => :collection
    post :localities_options_edit, :on => :collection

    post :towns_options_father, :on => :collection
    post :localities_options_father, :on => :collection
    post :towns_options_father_edit, :on => :collection
    post :localities_options_father_edit, :on => :collection


    post :towns_options_mother, :on => :collection
    post :localities_options_mother, :on => :collection

    post :towns_options_mother_edit, :on => :collection
    post :localities_options_mother_edit, :on => :collection


    post :towns_options_tutor, :on => :collection
    post :localities_options_tutor, :on => :collection

    post :towns_options_tutor_edit, :on => :collection
    post :localities_options_tutor_edit, :on => :collection

    get :autocomplete_origin_school, :on => :collection
    get :edit, :on => :collection
    get :new_documento, :on => :collection
    get :edit_documento, :on => :collection
    post :save_documentos, :on => :collection
    post :save_edit_documentos, :on => :collection
    get :registered_nss, :on => :collection
    get :registered_escuela, :on => :collection
  end
  resources :escolares

  resources :fichas do
    post :aspi, :on => :collection
    get :aspi, :on => :collection
  end

  devise_for :usuarios,controllers: { registrations: 'registrations' },  path: '', path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'new_account', edit: 'edit/profile' }

  root 'home#index'
end
