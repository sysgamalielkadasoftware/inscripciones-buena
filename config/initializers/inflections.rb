ActiveSupport::Inflector.inflections do |inflect|
  inflect.plural /(r|n|d|l)$/i, '\1es'
  inflect.singular /(r|n|d|l)es$/i, '\1'

  inflect.irregular 'usuario', 'usuarios'
  inflect.irregular 'Usuario', 'Usuarios'

  inflect.irregular 'dato_personal', 'datos_personales'
  inflect.irregular 'DatoPersonal', 'DatosPersonales'

  inflect.irregular 'dato_medico', 'datos_medicos'
  inflect.irregular 'DatoMedico', 'DatosMedicos'

  inflect.irregular 'dato_familiar', 'datos_familiares'
  inflect.irregular 'DatoFamiliar', 'DatosFamiliares'

  inflect.irregular 'Antecedente_academico', 'antecedentes_academicos'
  inflect.irregular 'AntecedenteAcademico', 'AntecedentesAcademicos'

  inflect.irregular 'escuela_procedente', 'escuelas_procedentes'
  inflect.irregular 'EscuelaProcedente', 'EscuelasProcedentes'

  inflect.irregular 'lengua_indigena', 'lenguas_indigenas'
  inflect.irregular 'LenguaIndigena', 'LenguasIndigenas'

  inflect.irregular 'aspirante_familiar', 'aspirantes_familiares'
  inflect.irregular 'AspiranteFamiliar', 'AspirantesFamiliares'

  inflect.irregular 'dato_personal_lengua_indigena', 'datos_personales_lenguas_indigenas'
  inflect.irregular 'DatoPersonalLenguaIndigena', 'DatosPersonalesLenguasIndigenas'

  inflect.irregular 'periodo_ficha', 'periodos_fichas'
  inflect.irregular 'PeriodoFicha', 'PeriodosFichas'

  inflect.irregular 'solicitud_ficha', 'solicitudes_fichas'
  inflect.irregular 'SolicitudFicha', 'SolicitudesFichas'

  inflect.irregular 'examen_seleccion', 'examenes_seleccion'
  inflect.irregular 'ExamenSeleccion', 'ExamenesSeleccion'

  inflect.irregular 'materia_examen_seleccion', 'materias_examen_seleccion'
  inflect.irregular 'MateriaExamenSeleccion', 'MateriasExamenSeleccion'

  inflect.irregular 'examen_materia', 'examenes_materias'
  inflect.irregular 'ExamenMateria', 'ExamenesMaterias'

  inflect.irregular 'documentacion_entregada', 'documentaciones_entregadas'
  inflect.irregular 'DocumentacionEntregada', 'DocumentacionesEntregadas'

  inflect.irregular 'sede_ficha', 'sedes_fichas'
  inflect.irregular 'SedeFicha', 'SedesFichas'

  # tablas para propedeuticos
  inflect.irregular 'periodo_propedeutico', 'periodos_propedeuticos'
  inflect.irregular 'PeriodoPropedeutico', 'PeriodosPropedeuticos'

  inflect.irregular 'grupo_propedeutico', 'grupos_propedeuticos'
  inflect.irregular 'GrupoPropedeutico', 'GruposPropedeuticos'

  inflect.irregular 'solicitud_propedeutico', 'solicitudes_propedeuticos'
  inflect.irregular 'SolicitudPropedeutico', 'SolicitudesPropedeuticos'

  inflect.irregular 'preregistro_pago', 'preregistros_pagos'
  inflect.irregular 'PreregistroPago', 'PreregistrosPagos'

  inflect.irregular 'inscripcion_propedeutico','inscripciones_propedeuticos'
  inflect.irregular 'InscripcionPropedeutico','InscripcionesPropedeuticos'

  inflect.irregular 'materia_propedeutico', 'materias_propedeuticos'
  inflect.irregular 'MateriaPropedeutico', 'MateriasPropedeuticos'

  inflect.irregular 'calificacion_propedeutico', 'calificaciones_propedeuticos'
  inflect.irregular 'CalificacionPropedeutico', 'CalificacionesPropedeuticos'

  inflect.irregular 'registro_apoyo', 'registros_apoyos'
  inflect.irregular 'RegistroApoyo', 'RegistrosApoyos'

  # inscripciones/reinscripciones
  inflect.irregular 'preregistro_pago_nes', 'preregistros_pagos_nes'
  inflect.irregular 'PreregistroPagoNes', 'PreregistrosPagosNes'

  inflect.irregular 'grupo_curso', 'grupos_cursos'
  inflect.irregular 'GrupoCurso', 'GruposCursos'

  inflect.irregular 'inscripcion_curso', 'inscripciones_cursos'
  inflect.irregular 'InscripcionCurso', 'InscripcionesCursos'

  inflect.irregular 'plan_estudio', 'planes_estudio'
  inflect.irregular 'PlanEstudio', 'PlanesEstudio'

  inflect.irregular 'configuracion_aspirante', 'configuraciones_aspirantes'
  inflect.irregular 'ConfiguracionAspirante', 'ConfiguracionesAspirantes'

  inflect.irregular 'token', 'tokens'
  inflect.irregular 'Token', 'Tokens'

  inflect.irregular 'solicitud_inscripcion', 'solicitudes_inscripciones'
  inflect.irregular 'SolicitudInscripcion', 'SolicitudesInscripciones'

  inflect.irregular 'solicitud_reinscripcion', 'solicitudes_reinscripciones'
  inflect.irregular 'SolicitudReinscripcion', 'SolicitudesReinscripciones'

  inflect.uncountable(%w(campus))

  inflect.irregular 'personal_detail', 'personal_details'
  inflect.irregular 'PersonalDetail', 'PersonalDetails'

  inflect.irregular 'address', 'addresses'
  inflect.irregular 'Address', 'Addresses'

  inflect.irregular 'pais', 'paises'
  inflect.irregular 'Pais', 'Paises'

  inflect.irregular 'academic_background', 'academic_backgrounds'
  inflect.irregular 'AcademicBackground', 'AcademicBackgrounds'

  inflect.irregular 'origin_school', 'origin_schools'
  inflect.irregular 'OriginSchool', 'OriginSchools'

  inflect.irregular 'alumno_tutor', 'alumnos_tutores'
  inflect.irregular 'AlumnoTutor', 'AlumnosTutores'

  inflect.irregular 'indigenous_language', 'indigenous_languages'
  inflect.irregular 'IndigenousLanguage', 'IndigenousLanguages'

  inflect.irregular 'personal_detail_indigenous_language', 'personal_details_indigenous_languages'
  inflect.irregular 'PersonalDetailIndigenousLanguage', 'PersonalDetailsIndigenousLanguages'
  # inscripciones/reinscripciones
  inflect.irregular 'aspirante_documento', 'aspirantes_documentos'
  inflect.irregular 'AspiranteDocumento', 'AspirantesDocumentos'

  inflect.irregular 'documento_ficha', 'documentos_fichas'
  inflect.irregular 'DocumentoFicha', 'DocumentosFichas'

  inflect.irregular 'contenido_comprobante_ficha', 'contenido_comprobante_fichas'
  inflect.irregular 'ContenidoComprobanteFichas', 'ContenidoComprobanteFichas'
end