Rails.application.configure do
  config.cache_classes = false
  config.eager_load = false
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false
  config.action_mailer.raise_delivery_errors = false
  config.active_support.deprecation = :log
  config.active_record.migration_error = :page_load
  config.assets.debug = true
  config.assets.digest = true
  config.assets.raise_runtime_errors = true




  config.action_mailer.default_url_options = { :host => 'localhost:3000' }


  config.action_mailer.perform_deliveries = true
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.default :charset => "utf-8"
  #These settings are for the sending out email for active admin and consequently the devise mailer
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    address: 'smtp.gmail.com',
    port:    587,
    domain:    'gmail.com', #you can also use google.com
    user_name: 'nes.kada.system@gmail.com',
    password: '1nscr1pc10n3s',
    authentication:   'plain'

  }
end